<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		     = $_SESSION["loggedin_user"];
	$role 		     = $_SESSION["loggedin_role"];
	$loggedin_name   = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$unit_id     = $_POST["unit_id"];
	$active      = $_POST["action"];
	
	$project_uom_master_update_data = array("active"=>$active);
	$delete_project_uom_master_result = i_delete_project_uom_master($unit_id,$project_uom_master_update_data);
	
	if($delete_project_uom_master_result["status"] == FAILURE)
	{
		echo $delete_project_uom_master_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>