<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_list.php
CREATED ON	: 06-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries
*/
define('CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID','111');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID,'4','1');
	// Query String / Get form Data
	if(isset($_GET["project"]))
	{
		$project = $_GET["project"];
	}
	else
	{
		$project = "";
	}
	
	if(isset($_POST["enquiry_search_submit"]))
	{			
		if($_POST["dt_start_date"] != "")
		{
			$start_date_form = $_POST["dt_start_date"];
			$start_date      = $start_date_form." 00:00:00";
		}
		else
		{
			$start_date_form = "";
			$start_date      = "";
		}
		
		if($_POST["dt_end_date"] != "")
		{
			$end_date_form = $_POST["dt_end_date"];
			$end_date      = $end_date_form." 23:59:59";
		}
		else
		{
			$end_date_form = "";
			$end_date      = "";
		}
		
		if($_POST["dt_request_start_date"] != "")
		{
			$request_start_date_form = $_POST["dt_request_start_date"];
			$request_start_date      = $request_start_date_form." 00:00:00";
		}
		else
		{
			$request_start_date_form = "";
			$request_start_date      = "";
		}
		
		if($_POST["dt_request_end_date"] != "")
		{
			$request_end_date_form = $_POST["dt_request_end_date"];
			$request_end_date      = $request_end_date_form." 23:59:59";
		}
		else
		{
			$request_end_date_form = "";
			$request_end_date      = "";
		}

		$source          = $_POST["ddl_search_source"];
		
		if(($role == 1) || ($role == 5))
		{
			$assigned_to = $_POST["ddl_search_assigned_by"];
		}
		else
		{
			$assigned_to = '';
		}
		
		if(($role == 1) || ($role == 5))
		{
			$requester = $_POST["ddl_requested_by"];
		}
		else
		{
			$requester = '';
		}
		$enquiry_number  = $_POST["stxt_enquiry_num"];
		$cell_number     = $_POST["stxt_cust_cell_num"];
	}
	else
	{		
		$start_date         = "";
		$end_date           = "";
		$request_start_date = "";
		$request_end_date   = "";
		$source             = "";
		$assigned_to        = "-1";
		$requester          = "-1";
		$enquiry_number     = '';
		$cell_number        = '';
	}

	// Temp data
	$alert = "";	
	
	$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assigned_to,'',$start_date,$end_date,'','','','','','',$user);
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
	}
	
	// Source List
	$source_list = i_get_enquiry_source_list('','1');
	if($source_list["status"] == SUCCESS)
	{
		$source_list_data = $source_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$source_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Enquiries Marked as Unqualifiable</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Enquiries marked as unqualifiable (Total Enquiries = <span id="total_count_section">
			  <?php
			  if($enquiry_list["status"] == SUCCESS)
			  {
				$preload_count = "<i>Calculating</i>";			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:120px; padding-top:10px;">               
			  <form method="post" id="enquiry_search" action="crm_enquiry_list_unq.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  Enquiry Date Start &nbsp;
			  <input type="date" name="dt_start_date" value="<?php echo $start_date_form; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Enquiry Date End &nbsp;
			  <input type="date" name="dt_end_date" value="<?php echo $end_date_form; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_source">
			  <option value="">- - Select Source - -</option>
			  <?php
				for($count = 0; $count < count($source_list_data); $count++)
				{
					?>
					<option value="<?php echo $source_list_data[$count]["enquiry_source_master_id"]; ?>" <?php 
					if($source == $source_list_data[$count]["enquiry_source_master_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $source_list_data[$count]["enquiry_source_master_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  if(($role == 1) || ($role == 5))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_requested_by">
			  <option value="">- - Select Requester - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{					
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($requester == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?><br />
			  <span style="padding-left:8px; padding-right:8px;">
			  Request Date Start &nbsp;
			  <input type="date" name="dt_request_start_date" value="<?php echo $request_start_date_form; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Request Date End &nbsp;
			  <input type="date" name="dt_request_end_date" value="<?php echo $request_end_date_form; ?>" />
			  </span>
			  <?php
			  if(($role == 1) || ($role == 5))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_assigned_by">
			  <option value="">- - Select Assignee - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assigned_to == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_enquiry_num" value="<?php echo $enquiry_number; ?>" placeholder="Full Enquiry Number" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_cust_cell_num" value="<?php echo $cell_number; ?>" placeholder="Full Mobile Number of Customer" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="enquiry_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Reason</th>
					<th>Request Date</th>
					<th>Requested By</th>
					<th>Source</th>
					<th>Walk In</th>					
					<th>Enquiry Date</th>
					<th>Follow Up Count</th>					
					<th>Site Visit Count</th>
					<th>Assignee</th>					
					<th>&nbsp;</th>															
				</tr>
				</thead>
				<tbody>							
				<?php
				if($enquiry_list["status"] == SUCCESS)
				{
					$sl_no                = 0;
					$unq_count            = 0;
					$int_filter_out_count = 0;
					$date_filter_count    = 0;
					$user_filter_count    = 0;
					for($count = 0; $count < count($enquiry_list_data); $count++)
					{						
						$latest_follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','desc','0','1');						
						$follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','','','');
						if($follow_up_data["status"] == SUCCESS)
						{
							$fup_count = count($follow_up_data["data"]);
						}
						else
						{
							$fup_count = 0;
						}
						
						// Site Visit Data
						$site_visit_data = i_get_site_travel_plan_list('','',$enquiry_list_data[$count]["enquiry_id"],'','','','2','','');
						if($site_visit_data["status"] == SUCCESS)
						{
							$sv_count = count($site_visit_data["data"]);
						}
						else
						{
							$sv_count = 0;
						}
						
						// Is the enquiry unqualified
						$unqualified_data = i_get_unqualified_leads('',$enquiry_list_data[$count]["enquiry_id"],'','','','','','','');
						if($unqualified_data["status"] != SUCCESS)
						{
							if($latest_follow_up_data["data"][0]["crm_cust_interest_status_id"] != '4') // Not marked as Unqualifiable
							{
								$int_filter_out_count++;
							}
							else
							{
								if(($requester == "") || ($requester == $latest_follow_up_data["data"][0]["enquiry_follow_up_added_by"]))
								{
								if((($request_start_date == "") || (strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_added_on"]) >= strtotime($request_start_date))) && ((($request_end_date == "") || (strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_added_on"]) <= strtotime($request_end_date)))))
								{
							$sl_no++;
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["enquiry_number"]; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["project_name"]; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["cell"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $latest_follow_up_data["data"][0]["enquiry_follow_up_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $latest_follow_up_data["data"][0]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["enquiry_source_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($enquiry_list_data[$count]["walk_in"] == "1")
					{
						echo "Yes";
					}
					else
					{
						echo "No";
					}?></td>					
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_list_data[$count]["added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $fup_count; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $sv_count; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["user_name"]; ?></td>					
					<td style="word-wrap:break-word;"><?php					
					if(($role == '1') || ($role == '5') || ($user == $latest_follow_up_data["data"][0]["enquiry_follow_up_added_by"]))
					{
					?>
					<a href="crm_enquiry_fup_list.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Follow Up</a></td>
					<?php																				
					}
					?>
					</tr>
					<?php
						}
						else
						{
							$date_filter_count++;
						}
						}
						else
						{
							$user_filter_count++;
						}
					}
					}
					else
					{
						$unq_count++;
					}
					}
				}
				else
				{
				?>
				<tr><td colspan="14">No enquiries!</td></tr>
				<?php
				}					
				if($enquiry_list["status"] == SUCCESS)
				{
					$final_count = count($enquiry_list_data) - $unq_count - $int_filter_out_count - $date_filter_count - $user_filter_count;			
				}
				else
				{
					$final_count = 0;
				}
				 ?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $final_count; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>