<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
 
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update Project Machine Payment
	$machine_payment_id  = $_POST["machine_payment_id"];
	$action   	  		 = $_POST["action"];
	$accepted_by  		 = $user;
	$accepted_on  		 = date("Y-m-d H:i:s");
	
	//Get TDS Master
	$project_tds_deduction_master_search_data = array("master_type"=>"Machine");
	$project_tds_list = i_get_project_tds_deduction_master($project_tds_deduction_master_search_data);
	if($project_tds_list['status'] == SUCCESS)
	{
		$project_tds_list_data = $project_tds_list['data'];
		$tds = $project_tds_list_data[0]["project_tds_deduction_master_deduction"];
	}	
	else
	{
		$tds = 0;
	}
	
	$project_actual_machine_payment_update_data = array("status"=>$action,"accepted_by"=>$accepted_by,"accepted_on"=>$accepted_on,"tds"=>$tds);
	$approve_payment_machine_result = i_update_project_payment_machine($machine_payment_id,$project_actual_machine_payment_update_data);
	
	if($approve_payment_machine_result["status"] == FAILURE)
	{
		echo $approve_payment_machine_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>