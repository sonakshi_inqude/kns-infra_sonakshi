<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'Projectmgmnt';

/* DEFINES - START */
define('PROJECT_TASK_APPROVED_ACTUAL_MANPOWER_LIST_FUNC_ID', '255');
/* DEFINES - END */
// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_TASK_APPROVED_ACTUAL_MANPOWER_LIST_FUNC_ID, '1', '1');
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_TASK_APPROVED_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');

    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $search_project = "-1";
    $search_vendor  = "";
    $search_work_type  = "";
    $start_date     = "";
    $end_date	    = "";

    if (isset($_POST['manpower_search_submit'])) {
        $search_vendor    = $_POST["search_vendor"];
        $search_project   = $_POST["search_project"];
        $search_work_type = $_POST["search_work_type"];
        $start_date       = $_POST["dt_start_date"];
        $end_date	      = $_POST["dt_end_date"];
    }
    if (isset($_POST["items_submit"])) {
        $item_nos = $_POST['hd_quote_count'];
        $start_date = "0000-00-00";
        $end_date = "0000-00-00";
        $no_of_men_hrs = "";
        $no_of_women_hrs = "";
        $no_of_mason_hrs = "";
        $total_amount = "";
        $item_count = 0;
        $bill_no	 		  = p_generate_manpower_bill_no();
        $payment_manpower_iresult = i_add_project_actual_payment_manpower('', '', '', '', '', '', '', $user, $bill_no);
        if ($payment_manpower_iresult["status"] == SUCCESS) {
            $payment_manpower_id = $payment_manpower_iresult["data"];
        }
        for ($item_count = 0 ; $item_count < $item_nos; $item_count++) {
            if (isset($_POST['cb_item_'.$item_count])) {
                $no_of_men_hrs  		 = $no_of_men_hrs + $_POST['hd_man_count_'.$item_count];
                $no_of_women_hrs   		 = $no_of_women_hrs + $_POST['hd_women_count_'.$item_count];
                $no_of_mason_hrs    	 = $no_of_mason_hrs + $_POST['hd_mason_count_'.$item_count];
                $men_rate		         = $_POST['hd_man_rate_'.$item_count];
                $women_rate		         = $_POST['hd_women_rate_'.$item_count];
                $mason_rate		         = $_POST['hd_mason_rate_'.$item_count];
                $manpower_date		     = $_POST['hd_manpower_date_'.$item_count];
                $man_power_vendor_id 	 = $_POST['hd_vendor_count_'.$item_count];
                $man_power_id 			 = $_POST['cb_item_'.$item_count];
                var_dump($man_power_id);
                if ($start_date == "0000-00-00") {
                    $start_date = $manpower_date;
                } elseif (strtotime($manpower_date) < strtotime($start_date)) {
                    $start_date = $manpower_date;
                }

                if ($start_date == "0000-00-00") {
                    $end_date = $manpower_date;
                } elseif (strtotime($manpower_date) > strtotime($end_date)) {
                    $end_date = $manpower_date;
                }
                $payment_manpower_mapping_iresult = i_add_project_payment_manpower_mapping($man_power_id, $payment_manpower_id, '', $user);
            }
        }
        $total_men_cost_count = $no_of_men_hrs * $men_rate;
        $total_women_cost_count = $no_of_women_hrs  * $women_rate;
        $total_mason_cost_count = $no_of_mason_hrs * $mason_rate;
        $total_amount = $total_men_cost_count + $total_women_cost_count + $total_mason_cost_count;
        $man_power_start_date = date("Y-m-d", strtotime($start_date)).' '."00:00:00";
        $man_power_end_date = date("Y-m-d", strtotime($end_date)) .' '."00:00:00";
        $total_no_of_hrs_worked = $no_of_men_hrs + $no_of_women_hrs + $no_of_mason_hrs ;

        //Update Payment Manpower
        $project_actual_payment_manpower_update_data = array("vendor_id"=>$man_power_vendor_id,"amount"=>$total_amount,"no_of_hours"=>$total_no_of_hrs_worked,"men_hrs"=>$no_of_men_hrs,"women_hrs"=>$no_of_women_hrs,"mason_hrs"=>$no_of_mason_hrs,"from_date"=>$man_power_start_date,"to_date"=>$man_power_end_date);
        $payment_manpower_uresults = i_update_project_actual_payment_manpower($payment_manpower_id, $project_actual_payment_manpower_update_data);
        if ($payment_manpower_uresults["status"] == SUCCESS) {
            header("location:project_add_bill_no.php?manpower_id=$payment_manpower_id");
        }
    }
    // Temp data
    $man_power_search_data = array("active"=>'1',"display_status"=>"approved","project"=>$search_project,"agency"=>$search_vendor,"work_start_date"=>$start_date,"work_end_date"=>$end_date,"work_type"=>$search_work_type);
    $man_power_list = i_get_man_power_list($man_power_search_data);
    if ($man_power_list["status"] == SUCCESS) {
        $man_power_list_data = $man_power_list["data"];
    } else {
        $alert = $alert."Alert: ".$man_power_list["data"];
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Daily Approved Manpower List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Daily Approved Manpower List</h3><span style="float:right; padding-right:20px;"><!--<a href="project_add_task_actual_manpower.php">Project Add Task Actual Man Power</a>--></span>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<div class="widget-header" style="height:80px; padding-top:10px;">
			<?php
            if ($view_perms_list['status'] == SUCCESS) {
                ?>
			  <form method="post" id="file_search_form" action="project_task_approved_actual_manpower_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
              for ($project_count = 0; $project_count < count($project_manpower_agency_list_data); $project_count++) {
                  ?>
			  <option value="<?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
			  <?php
              } ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="-1">- - Select Project - -</option>
			  <?php
              for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
              } ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_work_type">
			  <option value="">- - Select Work Type - -</option>
			  <option value="Regular" <?php if ($search_work_type == "Regular") {
                  ?> selected="selected" <?php
              } ?>>- - Regular Work - -</option>
			  <option value="Rework" <?php if ($search_work_type == "Rework") {
                  ?> selected="selected" <?php
              } ?>>- - Rework - -</option>

			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>

			  <input type="submit" name="manpower_search_submit" />
			  </form>
			  <?php
            } else {
                echo 'You are not authorized to view this page';
            }
                ?>
            </div>

			<?php
            if ($view_perms_list['status'] == SUCCESS) {
                ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
					<th>Task Name</th>
					<th>Road Name</th>
					<th>Work Type</th>
					<th>Measurment</th>
					<th>UOM</th>
					<th>%</th>
					<th>Date</th>
					<th>Agency</th>
					<th>Men Hrs</th>
					<th>Women Hrs</th>
					<th>Mason Hrs</th>
					<th>No of People</th>
					<th>Amount</th>
					<th>Remarks</th>
					<th>Approved By</th>
					<th>Approved On</th>
					<th colspan="3" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<form method="post" action="project_task_approved_actual_manpower_list.php">
				<input type="hidden" name="hd_vendor_id" value="<?php echo $search_vendor; ?>" />
				<?php
                if ($man_power_list["status"] == SUCCESS) {
                    ?>
					<input type="hidden" name="hd_quote_count" value="<?php echo count($man_power_list_data); ?>" />
					<?php
                    $sl_no = 0;
                    $man_power_men_rate = 0;
                    $man_power_women_rate = 0;
                    $man_power_mason_rate = 0;
                    $total_cost = 0;
                    for ($count = 0; $count < count($man_power_list_data); $count++) {
                        if (($man_power_list_data[$count]["project_task_actual_manpower_road_id"] != "No Roads")) {
                            $road_name = $man_power_list_data[$count]["project_site_location_mapping_master_name"];
                        } else {
                            $road_name = "No Roads";
                        }
                        //Get Measurement
                        $get_details_data = i_get_details('', $man_power_list_data[$count]["project_task_actual_manpower_task_id"], $man_power_list_data[$count]["project_task_actual_manpower_road_id"]);
                        $uom = $get_details_data["uom"];
                        $project_payment_manpower_mapping_search_data = array("manpower_id"=>$man_power_list_data[$count]["project_task_actual_manpower_id"]);
                        $payment_manpower_mapping_list = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
                        if ($payment_manpower_mapping_list["status"] == SUCCESS) {
                            $payment_manpower = true;
                        } else {
                            $payment_manpower = false;
                        }

                        $man_power_men_rate = $man_power_list_data[$count]["project_task_actual_manpower_men_rate"];
                        $man_power_women_rate = $man_power_list_data[$count]["project_task_actual_manpower_women_rate"];
                        $man_power_mason_rate = $man_power_list_data[$count]["project_task_actual_manpower_mason_rate"];
                        $total_men_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] * $man_power_men_rate;
                        $total_women_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] * $man_power_women_rate;
                        $total_mason_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] * $man_power_mason_rate;
                        $total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;
                        $men_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"];
                        $women_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"];
                        $mason_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"];

                        $no_of_men = $men_hrs/8;
                        $no_of_women = $women_hrs/8;
                        $no_of_mason = $mason_hrs/8;

                        $total_hrs = $men_hrs + $women_hrs + $mason_hrs;
                        $no_of_people = $total_hrs/8; ?>
					<input type="hidden" name="hd_man_rate_<?php echo $count; ?>" value="<?php echo $man_power_men_rate; ?>" />
					<input type="hidden" name="hd_women_rate_<?php echo $count; ?>" value="<?php echo $man_power_women_rate; ?>" />
					<input type="hidden" name="hd_mason_rate_<?php echo $count; ?>" value="<?php echo $man_power_mason_rate; ?>" />

					<?php if ($payment_manpower == false) {
                            $sl_no++; ?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $man_power_list_data[$count]["project_master_name"]; ?></td>
						<td><?php echo $man_power_list_data[$count]["project_process_master_name"]; ?></td>
						<td><?php echo $man_power_list_data[$count]["project_task_master_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $road_name; ?></td>
						<td style="word-wrap:break-word;"><?php echo $work_type = $man_power_list_data[$count]["project_task_actual_manpower_work_type"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_completed_msmrt"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $uom; ?></td>
						<td style="word-wrap:break-word;"><?php
                        if ($work_type == "Regular") {
                            echo $man_power_list_data[$count]["project_task_actual_manpower_completion_percentage"];
                        } elseif ($work_type == "Rework") {
                            echo $man_power_list_data[$count]["project_task_actual_manpower_rework_completion"];
                        } ?></td>

						<input type="hidden" name="hd_manpower_date_<?php echo $count; ?>" value="<?php echo date("d-M-Y", strtotime($man_power_list_data[$count][
                        "project_task_actual_manpower_date"])); ?>" />
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($man_power_list_data[$count][
                        "project_task_actual_manpower_date"])); ?></td>

						<input type="hidden" name="hd_vendor_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_agency"]; ?>" />
						<td><?php echo $man_power_list_data[$count]["project_manpower_agency_name"]; ?></td>

						<input type="hidden" name="hd_man_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"]; ?>" />
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"]; ?>
					  &nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_men_rate ; ?>
					  Men :<?php echo $no_of_men; ?></td>

						<input type="hidden" name="hd_women_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"]; ?>" />
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"]; ?>
					&nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_women_rate ; ?> Women :<?php echo $no_of_women; ?></td>

						<input type="hidden" name="hd_mason_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"]; ?>" />
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"]; ?>
					 &nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_mason_rate ; ?> Mason :<?php echo $no_of_mason ; ?></td>


						<td style="word-wrap:break-word;"><?php echo $no_of_people ; ?></td>

						<input type="hidden" name="hd_total_<?php echo $count; ?>" value="<?php echo $total_cost ; ?>" />
						<td><?php echo round($total_cost) ; ?></td>

						<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_remarks"]; ?></td>
						<td><?php echo $man_power_list_data[$count]["approved_by"]; ?></td>
						<td><?php echo date("d-M-Y", strtotime($man_power_list_data[$count]["project_task_actual_manpower_approved_on"])); ?></td>
						<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_task_actual_manpower('<?php echo $man_power_list_data[$count]["project_task_actual_manpower_id"]; ?>');">Edit </a></div></td>
						<td><?php if (($man_power_list_data[$count]["project_task_actual_manpower_active"] == "1")) {
                            ?><a href="#" onclick="return project_delete_task_actual_manpower(<?php echo $man_power_list_data[$count]["project_task_actual_manpower_id"]; ?>);">Delete</a><?php
                        } ?></td>-->
						<td><?php if (($man_power_list_data[$count]["project_task_actual_manpower_display_status"] == "approved") && ($view_perms_list['status'] == SUCCESS)) {
                            ?><a href="#" target="_blank" onclick="return go_to_manpower_print(<?php echo $man_power_list_data[$count]["project_task_actual_manpower_id"]; ?>);">Print</a><?php
                        } ?></td>
						<td><input type="checkbox" name="cb_item_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_id"] ; ?>"><br></td>
					</tr>
					<?php
                        }
                    }
                } else {
                    ?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
                } ?>

				<?php if (($search_vendor != "") && ($add_perms_list["status"] == SUCCESS) && ($search_project != "")) {
                    ?>
			  <div class="modal-footer">
                 <button type="submit" name="items_submit" class="btn btn-primary">Submit</button>
                 </div>
				 <?php
                } ?>
			  </form>
                </tbody>
              </table>

			  <?php
            } else {
                echo 'You are not authorized to view this page';
            }
            ?>



            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_actual_manpower(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_actual_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_actual_manpower(man_power_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_actual_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","man_power_id");
	hiddenField1.setAttribute("value",man_power_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function project_approve_task_actual_manpower(man_power_id,task_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_man_power.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
function go_to_manpower_print(manpower_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_labour_report_print.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","manpower_id");
	hiddenField1.setAttribute("value",manpower_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
