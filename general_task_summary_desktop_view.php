0<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: general_task_summary_desktop_view.php

CREATED ON	: 08-Nov-2015

CREATED BY	: Nitin Kashyap

PURPOSE     : List of Task Plans - Brief Summary

*/
/* DEFINES - START */
define('GENERAL_TASK_SUMMARY_DESKTOP_VIEW_FUNC_ID','361');
/* DEFINES - END */


/*

TBD:

1. Date display and calculation

2. Session management

3. Linking Tasks

*/
$_SESSION['module'] = 'General Task';


// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',GENERAL_TASK_SUMMARY_DESKTOP_VIEW_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',GENERAL_TASK_SUMMARY_DESKTOP_VIEW_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',GENERAL_TASK_SUMMARY_DESKTOP_VIEW_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',GENERAL_TASK_SUMMARY_DESKTOP_VIEW_FUNC_ID,'1','1');



	// Temp data

	$alert = "";

	if(isset($_REQUEST['search_user']))
	{
		$search_user = $_REQUEST['search_user'];


	}
	else
	{
		$search_user = $user;

	}
	var_dump($search_user);
	if(isset($_REQUEST['search_department']))
	{
		$search_department = $_REQUEST['search_department'];


	}
	else
	{
		$search_department = "";

	}

	if(isset($_REQUEST['search_project']))
	{
		$search_project = $_REQUEST['search_project'];


	}
	else
	{
		$search_project = "";

	}


	// Initialize

	$search_task_type  = "";

	$search_status     = "";


	// Form Data
	if(isset($_POST["task_search_submit"]))
	{

		$search_task_type  = $_POST["search_task_type"];

		$search_status     = $_POST["search_status"];
		if($_POST["search_user"] != "")
		{
			$search_user       = $_POST["search_user"];
		}
		else if($_POST["search_user"] == "")
		{
			$search_user       = "";
		}
		else
		{
			$search_user = $user;
		}


		$search_department = $_POST["search_department"];

	}

	if($search_user != "")

	{

		$task_user = $search_user;

	}
	else if(($role == 1) && ($search_user == ""))
	{
		$task_user = $search_user;
	}

	 $search_project   	 = "";
	if(isset($_POST["search_project"]))
	{
		$search_project   = $_POST["search_project"];
	}
	$general_task_plan_list = i_get_gen_task_plan_list('',$search_task_type,'',$search_department,'','','',$search_assigner,$search_status,$task_user,'',$search_project);

	if($general_task_plan_list["status"] == SUCCESS)

	{

		$general_task_plan_list_data = $general_task_plan_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$general_task_plan_list["data"];

	}



	// Get task type list

	$gen_task_type_list = i_get_gen_task_type_list('','1');

	if($gen_task_type_list["status"] == SUCCESS)

	{

		$gen_task_type_list_data = $gen_task_type_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$gen_task_type_list["data"];

		$alert_type = 0; // Failure

	}



	// Get list of users

	$user_list = i_get_user_list('','','','','1');

	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$user_list["data"];

		$alert_type = 0; // Failure

	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}


	// Get department list

	$department_list = i_get_department_list('','1');

	if($department_list["status"] == SUCCESS)

	{

		$department_list_data = $department_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$department_list["data"];

		$alert_type = 0; // Failure

	}



	// Get user list

}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>General Tasks - Summary</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">



          <div class="span6" style="width:100%;">



          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>General Tasks - Consolidated Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of Tasks: <span id="total_tasks"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <?php
			if($edit_perms_list["status"] == SUCCESS)
			{
			?><a href="general_task_summary_export.php?search_project=<?php echo $search_project ?>&search_department=<?php echo $search_department ?>&search_task_type=<?php echo $search_task_type ?>&search_status=<?php echo $search_status ?>&task_user=<?php echo $task_user ?>" target="_blank">Export to Excel</a><?php } ?>

			  <a href="add_general_task.php">Add Task</a></h3>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">
            <?php
			if($view_perms_list["status"] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:84px; padding-top:10px;">

			  <form method="post" id="task_search_form" action="general_task_summary_desktop_view.php">

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_task_type" style="font-weight:bold;">

			  <option value="">- - Select Task Type - -</option>

			  <?php

			  for($count = 0; $count < count($gen_task_type_list_data); $count++)

			  {

			  ?>

			  <option style="font-weight:bold;" value="<?php echo $gen_task_type_list_data[$count]["general_task_type_id"]; ?>" <?php if($search_task_type == $gen_task_type_list_data[$count]["general_task_type_id"]) { ?> selected="selected" <?php } ?>><?php echo $gen_task_type_list_data[$count]["general_task_type_name"]; ?></option>

			  <?php

			  }

			  ?>

			  </select>

			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_status" style="font-weight:bold;">

			  <option value="">- - Select Status - -</option>

			  <option style="font-weight:bold;" value="0" <?php if($search_status == "0") { ?> selected="selected" <?php } ?>>NOT STARTED</option>

			  <option style="font-weight:bold;" value="1" <?php if($search_status == "2") { ?> selected="selected" <?php } ?>>IN PROGRESS</option>

			  <option style="font-weight:bold;" value="3" <?php if($search_status == "3") { ?> selected="selected" <?php } ?>>COMPLETED</option>

			  </select>

			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_department" style="font-weight:bold;">

			  <option value="">- - Select Department - -</option>

			  <?php

			  for($count = 0; $count < count($department_list_data); $count++)

			  {

			  ?>

			  <option style="font-weight:bold;" value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php if($search_department == $department_list_data[$count]["general_task_department_id"]) { ?> selected="selected" <?php } ?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>

			  <?php

			  }

			  ?>

			  </select>

			  </span>

			  <br />

			  <?php if($role == 1)

			  {?>

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="search_user" style="font-weight:bold;">

			  <option style="font-weight:bold;" value="">- - Select User - -</option>

			  <?php

			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)

			  {

			  ?>

			  <option style="font-weight:bold;" value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($task_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>

			  <?php

			  }

			  ?>

			  </select>

			  </span>



			  <?php

			  }

			  else

			  {

			  ?>

			  <input type="hidden" name="search_user" value="<?php echo $user; ?>" />

			  <?php

			  }

			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="submit" name="task_search_submit" />

			  </span>

			  </form>

            </div>
            <?php
			}
			?>

			<?php
			if($view_perms_list["status"] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

					<th width="2%">SL No</th>

					<th width="20%">Task Details</th>

					<th width="11%">Department</th>

					<th width="11%">Project</th>

					<th width="13%">Start Date</th>

					<th width="13%">End Date</th>

					<th width="8%">Assigned By</th>

					<th width="8%">Assigned To</th>

					<th width="6%">Assigned Date</th>

					<th width="6%">Planned End Date</th>

					<th width="6%">Days</th>

					<th width="6%">&nbsp;</th>

					<th width="5%">&nbsp;</th>

					<th width="5%">&nbsp;</th>

				</tr>

				</thead>

				<tbody>

				 <?php

				 $sl_count = 0;

				if($general_task_plan_list["status"] == SUCCESS)

				{

					for($count = 0; $count < count($general_task_plan_list_data); $count++)

					{
						//Get Project Master

						// Project data
						$project_management_master_search_data = array("active"=>'1',"project_id"=>$general_task_plan_list_data[$count]["general_task_project"], "user_id"=>$user);
						$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
						if($project_management_master_list["status"] == SUCCESS)
						{
							$project_management_master_list_data = $project_management_master_list["data"];
							$project_name = $project_management_master_list_data[0]["project_master_name"];


						}
						else
						{
							$project_name = "";
						}


						if(($general_task_plan_list_data[$count]["general_task_end_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_planned_date"] != "1970-01-01") && ($search_status != '3'))

						{

							// Do nothing

						}

						else

						{

						if(get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00")

						{

							$end_date = date("Y-m-d");

						}

						else

						{

							$end_date = $general_task_plan_list_data[$count]["general_task_end_date"];

						}

						$start_date = $general_task_plan_list_data[$count]["general_task_planned_date"];



						if($general_task_plan_list_data[$count]["general_task_end_date"] == "0000-00-00")

						{

							$diff_end_date = date("Y-m-d");

						}

						else

						{

							$diff_end_date = $general_task_plan_list_data[$count]["general_task_end_date"];

						}



						$diff_data = get_date_diff($general_task_plan_list_data[$count]["general_task_added_on"],$diff_end_date);



						$css_class = "#000000";

						$sl_count++;

					?>

					<form method="post" id="edit_task_data" action="#">

					<input type="hidden" id="hd_task_id_<?php echo $sl_count; ?>" value="<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>" />

					<input type="hidden" id="hd_task_type_<?php echo $sl_count; ?>" value="<?php echo $general_task_plan_list_data[$count]["general_task_type"]; ?>" />

					<tr style="color:<?php echo $css_class; ?>">

						<td width="4%"><b><?php echo $sl_count; ?><b></td>

						<td width="30%" style="word-wrap:break-word;"><span id="task_details_disabled_<?php echo $sl_count; ?>"><b><?php echo $general_task_plan_list_data[$count]["general_task_details"]; ?></b></span><span id="task_details_enabled_<?php echo $sl_count; ?>" style="display:none;"><textarea id="txt_gen_task_details_<?php echo $sl_count; ?>" style="font-weight:bold;" disabled><?php echo $general_task_plan_list_data[$count]["general_task_details"]; ?></textarea></span></td>

						<td width="8%"><span id="task_department_disabled_<?php echo $sl_count; ?>"><b><?php echo $general_task_plan_list_data[$count]["general_task_department_name"]; ?></b></span><span id="task_department_enabled_<?php echo $sl_count; ?>" style="display:none;"><select id="ddl_department_<?php echo $sl_count; ?>" style="width:90px; font-weight:bold;" disabled>

						<?php

						for($dep_count = 0; $dep_count < count($department_list_data); $dep_count++)

						{?>

						<option style="font-weight:bold;" value="<?php echo $department_list_data[$dep_count]["general_task_department_id"]; ?>" <?php if($general_task_plan_list_data[$count]["general_task_department"] == $department_list_data[$dep_count]["general_task_department_id"]){ ?> selected="selected" <?php } ?>><?php echo $department_list_data[$dep_count]["general_task_department_name"]; ?></option>	<?php

						}?>

						</select></span></td>

						<td width="13%" style="word-wrap:break-word;"><b><?php echo $project_name; ?></b></td>

						<td width="7%"><span id="task_start_date_disabled_<?php echo $sl_count; ?>"><b><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_start_date"],"d-M-Y"); ?></b></span><span id="task_start_date_enabled_<?php echo $sl_count; ?>" style="display:none;"><input type="date" style="width:130px; font-weight:bold;" id="dt_gen_task_start_date_<?php echo $sl_count; ?>" value="<?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_start_date"],"Y-m-d"); ?>" disabled /></span></td>

						<td width="7%"><span id="task_end_date_disabled_<?php echo $sl_count; ?>"><b><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"d-M-Y"); ?></b></span><span id="task_end_date_enabled_<?php echo $sl_count; ?>" style="display:none;"><input type="date" style="width:130px; font-weight:bold;" id="dt_gen_task_end_date_<?php echo $sl_count; ?>" value="<?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d"); ?>" disabled /></span></td>

						<td width="13%" style="word-wrap:break-word;"><b><?php echo $general_task_plan_list_data[$count]["assigner"]; ?></b></td>

						<td width="13%" style="word-wrap:break-word;"><span id="task_assignee_disabled_<?php echo $sl_count; ?>"><b><?php echo $general_task_plan_list_data[$count]["assignee"]; ?></b></span><span id="task_assignee_enabled_<?php echo $sl_count; ?>" style="display:none;"><select id="ddl_assignee_<?php echo $sl_count; ?>" style="width:130px; font-weight:bold;" disabled>

						<?php

						for($user_count = 0; $user_count < count($user_list_data); $user_count++)

						{?>

						<option style="font-weight:bold;" value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($general_task_plan_list_data[$count]["general_task_user"] == $user_list_data[$user_count]["user_id"]){ ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>	<?php

						}?>

						</select></span></td>

						<td width="7%"><b><?php echo date("d-M-Y H:i:s",strtotime($general_task_plan_list_data[$count]["general_task_added_on"])); ?></b></td>
						<td width="7%"><b><?php echo date("d-M-Y H:i:s",strtotime($general_task_plan_list_data[$count]["general_task_planned_date"])); ?></b></td>

						<td width="7%"><?php if($diff_data["status"] != "2") {?><?php echo $diff_data["data"] + 1  ;?><?php } else { echo $diff_data["data"] ; } ?></td>

						<td width="7%"><?php if($view_perms_list["status"] == SUCCESS) {?><a href="view_gen_task_remarks.php?task=<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>" target="_blank"><span style="color:black; text-decoration: underline;">View Remarks</span></a><?php } ?></td>

						<td width="7%"><?php if($edit_perms_list["status"] == SUCCESS) {?><a href="#" onclick="return edit_submit(<?php echo $sl_count; ?>);">Submit</a><?php } ?></td>

						<td width="7%"><?php if($edit_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_edit_general_task('<?php echo $sl_count; ?>','<?php echo $search_department ;?>','<?php echo $search_project;?>','<?php echo $search_user ;?>');">Edit </a><?php }?><br /><?php if($delete_perms_list["status"] == SUCCESS) {?><a href="#" onclick="return confirm_deletion('<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>','<?php echo $search_department ;?>','<?php echo $search_project;?>','<?php echo $search_user ;?>');"><span style="color:black; text-decoration: underline;">Delete</span></a><?php } ?><br /><?php if($edit_perms_list["status"] == SUCCESS) {?><a href="#" onclick="return complete_task('<?php echo $sl_count; ?>','<?php echo date("Y-m-d"); ?>','<?php echo date("Y-m-d"); ?>','<?php echo $search_user ;?>','<?php echo $search_project ;?>');"><span style="color:black; text-decoration: underline;">Complete</span></a><?php } ?></td>



					</tr>

					</form>

					<script>

					document.getElementById('total_tasks').innerHTML = <?php echo $sl_count; ?>;

					</script>

					<?php

						}

					}

				}

				else

				{

				?>

				<td colspan="9">No tasks added yet!</td>

				<?php

				}

				?>

				<form method="post" name="add_gen_task" action="add_general_task.php">

				<input type="hidden" name="ddl_gen_task_type" value="1" />

				<input type="hidden" name="ddl_department" value="12" />

				<tr>

					<td><?php echo ++$sl_count; ?></td>

					<td colspan="2"><input type="text" name="txt_gen_task_details" placeholder="Enter Task Details" required /></td>

					<td colspan="2"><input type="date" name="dt_planned_end_date" /></td>

					<td colspan="3">

					<?php

					if($user != '143736679571416700')

					{

						?>

						<select name="ddl_assigned_to[]" multiple="multiple" required>

						<?php

						for($count = 0; $count < count($user_list_data); $count++)

						{

							?>

							<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $user)

							{

							?>

							selected="selected"

							<?php

							}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>

							<?php

						}

						?>

						</select>

						<?php

					}

					else

					{

						for($count = 0; $count < count($user_list_data); $count++)

						{

							if(($user_list_data[$count]["user_id"] == '144860117291627300') || ($user_list_data[$count]["user_id"] == '143841424055011700') || ($user_list_data[$count]["user_id"] == '144134480975277200') || ($user_list_data[$count]["user_id"] == '14636341281437900') || ($user_list_data[$count]["user_id"] == '143841429031701100') || ($user_list_data[$count]["user_id"] == '144102554647936900') || ($user_list_data[$count]["user_id"] == '14411073276815800') || ($user_list_data[$count]["user_id"] == '144110755226769100') || ($user_list_data[$count]["user_id"] == '144128013799301700') || ($user_list_data[$count]["user_id"] == '144134636241439400') || ($user_list_data[$count]["user_id"] == '144134651212194200') || ($user_list_data[$count]["user_id"] == '145862543096294000') || ($user_list_data[$count]["user_id"] == '145899173064360500') || ($user_list_data[$count]["user_id"] == '143620071466608200') || ($user_list_data[$count]["user_id"] == '143736679571416700')  || ($user_list_data[$count]["user_id"] == '14384120858785500') || ($user_list_data[$count]["user_id"] == '14388461769223300')  || ($user_list_data[$count]["user_id"] == '143884634583343100') || ($user_list_data[$count]["user_id"] == '143886021344160200')  || ($user_list_data[$count]["user_id"] == '143956187469638500') || ($user_list_data[$count]["user_id"] == '14410244536444700')  || ($user_list_data[$count]["user_id"] == '144103287161642300') || ($user_list_data[$count]["user_id"] == '144103293276609500')  || ($user_list_data[$count]["user_id"] == '144111481690966800') || ($user_list_data[$count]["user_id"] == '144127982324430100'))

							{

								?>

								<input type="checkbox" name="cb_assigned_to[]" value="<?php echo $user_list_data[$count]["user_id"]; ?>" />&nbsp;&nbsp;&nbsp;<?php echo $user_list_data[$count]["user_name"]; ?>&nbsp;&nbsp;&nbsp;<br />

								<?php

							}

						}

					}

					?>

					</td>

					<td colspan="4"><button type="submit" class="btn btn-primary" name="add_general_task_submit">Submit</button></td>

				</tr>

				</form>

                </tbody>

              </table>
			  <?php
			}
			?>

			  <br />

            </div>

            <!-- /widget-content -->

          </div>

          <!-- /widget -->



          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 -->

      </div>

      <!-- /row -->

    </div>

    <!-- /container -->

  </div>

  <!-- /main-inner -->

</div>









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function confirm_deletion(task_id,search_department,search_project,search_user)

{
	var ok = confirm("Are you sure you want to delete?")

	{

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					 window.location = "general_task_summary_desktop_view.php";

				}

			}



			xmlhttp.open("GET", "general_task_delete.php?task="+task_id +"&search_user="+search_user);   // file name where delete code is written

			xmlhttp.send();

		}

	}

}



function enable(sl_no)

{

	document.getElementById('task_details_disabled_'+sl_no).style.display = 'none';

	document.getElementById('task_details_enabled_'+sl_no).style.display = 'block';

	document.getElementById('txt_gen_task_details_'+sl_no).disabled = false;



	document.getElementById('task_department_disabled_'+sl_no).style.display = 'none';

	document.getElementById('task_department_enabled_'+sl_no).style.display = 'block';

	document.getElementById('ddl_department_'+sl_no).disabled = false;



	document.getElementById('task_start_date_disabled_'+sl_no).style.display = 'none';

	document.getElementById('task_start_date_enabled_'+sl_no).style.display = 'block';

	document.getElementById('dt_gen_task_start_date_'+sl_no).disabled = false;



	document.getElementById('task_end_date_disabled_'+sl_no).style.display = 'none';

	document.getElementById('task_end_date_enabled_'+sl_no).style.display = 'block';

	document.getElementById('dt_gen_task_end_date_'+sl_no).disabled = false;



	document.getElementById('task_assignee_disabled_'+sl_no).style.display = 'none';

	document.getElementById('task_assignee_enabled_'+sl_no).style.display = 'block';

	document.getElementById('ddl_assignee_'+sl_no).disabled = false;



	document.getElementById('edit_general_task_submit_'+sl_no).disabled = false;

}



function edit_submit(sl_no)

{

	task_id      = document.getElementById('hd_task_id_'+sl_no).value;

	task_type    = document.getElementById('hd_task_type_'+sl_no).value;

	task_details = document.getElementById('txt_gen_task_details_'+sl_no).value;

	start_date   = document.getElementById('dt_gen_task_start_date_'+sl_no).value;

	end_date     = document.getElementById('dt_gen_task_end_date_'+sl_no).value;

	assignee     = document.getElementById('ddl_assignee_'+sl_no).value;

	department   = document.getElementById('ddl_department_'+sl_no).value;



	/*

	if (window.XMLHttpRequest)

	{// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();

	}

	else

	{// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	}



	xmlhttp.onreadystatechange = function()

	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

		{

			 window.location = "edit_general_task.php";

		}

	}



	xmlhttp.open("GET", "edit_general_task.php?hd_task_id="+task_id+"&ddl_gen_task_type="+task_type+"&txt_gen_task_details="+task_details+"&dt_start_date="+start_date+"&dt_end_date="+end_date+"&ddl_assignee="+assignee+"&ddl_department="+department+"&edit_general_task_submit=submit");   // file name where delete code is written

	xmlhttp.send(); */



	window.location = "edit_general_task.php?hd_task_id="+task_id+"&ddl_gen_task_type="+task_type+"&txt_gen_task_details="+task_details+"&dt_start_date="+start_date+"&dt_end_date="+end_date+"&ddl_assignee="+assignee+"&ddl_department="+department+"&edit_general_task_submit=submit&source=desktop";

}



function complete_task(sl_no,start_date,end_date,search_user,search_project)
{

	task_id      = document.getElementById('hd_task_id_'+sl_no).value;

	task_type    = document.getElementById('hd_task_type_'+sl_no).value;

	task_details = document.getElementById('txt_gen_task_details_'+sl_no).value;

	assignee     = document.getElementById('ddl_assignee_'+sl_no).value;

	department   = document.getElementById('ddl_department_'+sl_no).value;



	/*

	if (window.XMLHttpRequest)

	{// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();

	}

	else

	{// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	}



	xmlhttp.onreadystatechange = function()

	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

		{

			 window.location = "edit_general_task.php";

		}

	}



	xmlhttp.open("GET", "edit_general_task.php?hd_task_id="+task_id+"&ddl_gen_task_type="+task_type+"&txt_gen_task_details="+task_details+"&dt_start_date="+start_date+"&dt_end_date="+end_date+"&ddl_assignee="+assignee+"&ddl_department="+department+"&edit_general_task_submit=submit");   // file name where delete code is written

	xmlhttp.send(); */



	window.location = "edit_general_task.php?hd_task_id="+task_id+"&ddl_gen_task_type="+task_type+"&txt_gen_task_details="+task_details+"&dt_start_date="+start_date+"&dt_end_date="+end_date+"&search_user="+search_user+"&search_project="+search_project+"&ddl_department="+department+"&edit_general_task_submit=submit&source=desktop";

}

function go_to_edit_general_task(sl_no,search_department,search_project,search_user)
{
	task_id      = document.getElementById('hd_task_id_'+sl_no).value;
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "edit_general_task_summary.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value","desktop");

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","search_department");
	hiddenField3.setAttribute("value",search_department);

	var hiddenField4 = document.createElement("input");
	hiddenField4.setAttribute("type","hidden");
	hiddenField4.setAttribute("name","search_project");
	hiddenField4.setAttribute("value",search_project);

	var hiddenField5 = document.createElement("input");
	hiddenField5.setAttribute("type","hidden");
	hiddenField5.setAttribute("name","search_user");
	hiddenField5.setAttribute("value",search_user);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	form.appendChild(hiddenField4);
	form.appendChild(hiddenField5);

	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


  </body>



</html>
