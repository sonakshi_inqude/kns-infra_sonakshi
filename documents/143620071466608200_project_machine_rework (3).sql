-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2017 at 12:33 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_12th_jan`
--

-- --------------------------------------------------------

--
-- Table structure for table `project_machine_rework`
--

CREATE TABLE `project_machine_rework` (
  `project_machine_rework_id` int(11) NOT NULL,
  `project_machine_rework_task_id` int(11) NOT NULL,
  `project_machine_rework_vendor_id` int(11) NOT NULL,
  `project_machine_rework_machine_id` int(11) NOT NULL,
  `project_machine_rework_start_date_time` datetime NOT NULL,
  `project_machine_rework_end_date_time` datetime NOT NULL,
  `project_machine_rework_plan_off_time` int(11) NOT NULL,
  `project_machine_rework_plan_additional_cost` float(9,2) NOT NULL,
  `project_machine_rework_number` varchar(200) NOT NULL,
  `project_machine_rework_fuel_charges` float(9,2) NOT NULL,
  `project_machine_rework_with_fuel_charges` float(9,2) NOT NULL,
  `project_machine_rework_bata` float(9,2) NOT NULL,
  `project_machine_rework_issued_fuel` float(9,2) NOT NULL,
  `project_machine_rework_display_status` enum('not approved','approved','pending payment') NOT NULL,
  `project_machine_rework_machine_completion` int(11) NOT NULL,
  `project_machine_rework_machine_type` enum('own','rent') NOT NULL,
  `project_machine_rework_check_status` int(11) NOT NULL,
  `project_machine_rework_active` int(11) NOT NULL,
  `project_machine_rework_remarks` varchar(500) NOT NULL,
  `project_machine_rework_checked_by` char(20) NOT NULL,
  `project_machine_rework_checked_on` datetime NOT NULL,
  `project_machine_rework_approved_by` char(20) NOT NULL,
  `project_machine_rework_approved_on` datetime NOT NULL,
  `project_machine_rework_added_by` char(20) NOT NULL,
  `project_machine_rework_added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_machine_rework`
--

INSERT INTO `project_machine_rework` (`project_machine_rework_id`, `project_machine_rework_task_id`, `project_machine_rework_vendor_id`, `project_machine_rework_machine_id`, `project_machine_rework_start_date_time`, `project_machine_rework_end_date_time`, `project_machine_rework_plan_off_time`, `project_machine_rework_plan_additional_cost`, `project_machine_rework_number`, `project_machine_rework_fuel_charges`, `project_machine_rework_with_fuel_charges`, `project_machine_rework_bata`, `project_machine_rework_issued_fuel`, `project_machine_rework_display_status`, `project_machine_rework_machine_completion`, `project_machine_rework_machine_type`, `project_machine_rework_check_status`, `project_machine_rework_active`, `project_machine_rework_remarks`, `project_machine_rework_checked_by`, `project_machine_rework_checked_on`, `project_machine_rework_approved_by`, `project_machine_rework_approved_on`, `project_machine_rework_added_by`, `project_machine_rework_added_on`) VALUES
(1, 709, 1, 22, '2017-09-06 11:00:00', '0000-00-00 00:00:00', 0, 12.00, '', 650.00, 650.00, 200.00, 0.00, '', 0, '', 0, 1, 'test', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '143620071466608200', '2017-09-06 08:30:40'),
(2, 709, 50, 22, '2017-09-06 11:00:00', '0000-00-00 00:00:00', 0, 12.00, '', 650.00, 650.00, 200.00, 0.00, '', 0, '', 0, 1, 'test', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '143620071466608200', '2017-09-06 08:36:22'),
(3, 709, 5, 78, '2017-09-06 23:00:00', '0000-00-00 00:00:00', 0, 12.00, '', 650.00, 650.00, 200.00, 0.00, '', 0, '', 0, 1, 'test', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '143620071466608200', '2017-09-06 09:08:13'),
(4, 709, 11, 22, '2017-09-06 11:00:00', '0000-00-00 00:00:00', 0, 76.00, '', 650.00, 650.00, 200.00, 0.00, '', 0, '', 0, 1, 'tst', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '143620071466608200', '2017-09-06 10:51:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `project_machine_rework`
--
ALTER TABLE `project_machine_rework`
  ADD PRIMARY KEY (`project_machine_rework_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `project_machine_rework`
--
ALTER TABLE `project_machine_rework`
  MODIFY `project_machine_rework_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
