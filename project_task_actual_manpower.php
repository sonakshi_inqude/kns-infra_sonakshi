<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

    $alert_type = -1;
    $alert = "";
if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    // Nothing

    if (isset($_GET['project_process_task_id'])) {
        $task_id = $_GET['project_process_task_id'];
    } else {
        $task_id   = $_POST["hd_task_id"];
    }
    if (isset($_GET['location_id'])) {
        $road_id = $_GET['location_id'];
    } else {
        $road_id                = $_POST["hd_road_id"];
    }
    if (isset($_GET['project_id'])) {
        $project_id = $_GET['project_id'];
    }else {
      $project_id = $_REQUEST['hd_project_id'];
    }


    //Get Regular Measurement
    $get_details_data = i_get_details('Regular', $task_id, $road_id);
    $reg_mp_msmrt = $get_details_data["mp_msmrt"];
    $planned_msmrt = $get_details_data["planned_msmrt"];
    $uom = $get_details_data["uom"];
    $reg_overall_msmrt = $get_details_data["overall_msmrt"];
    $road_name = $get_details_data["road"];
    $reg_mc_msmrt = $get_details_data["mc_msmrt"];
    $reg_cw_msmrt = $get_details_data["cw_msmrt"];
    $project = $get_details_data["project"];
    $process_name = $get_details_data["process"];
    $task_name = $get_details_data["task"];


    //Get Rework Measurement
    $get_details_data = i_get_details('Rework', $task_id, $road_id);
    $rework_mp_msmrt = $get_details_data["mp_msmrt"];
    $rework_mc_msmrt = $get_details_data["mc_msmrt"];
    $rework_cw_msmrt = $get_details_data["cw_msmrt"];
    $rework_overall_msmrt = $get_details_data["overall_msmrt"];


    // Get Projects Site Location Mapping added
    $project_site_location_mapping_master_search_data = array("active"=>'1',"road_id"=>$road_id);
    $project_site_location_mapping_master_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
    if ($project_site_location_mapping_master_list['status'] == SUCCESS) {
        $road_name = $project_site_location_mapping_master_list["data"][0]["project_site_location_mapping_master_name"];
    } else {
        $road_name = "No Roads";
    }

    //Get Man power List
    $man_power_search_data = array("task_id"=>$task_id,"active"=>'1',"work_type"=>"Regular");
    $man_power_actual_data = i_get_man_power_list($man_power_search_data);
    // $road_name = $man_power_actual_data["data"][0]["project_site_location_mapping_master_name"];
    if ($man_power_actual_data["status"] != SUCCESS) {
        $manpower_status = "NotStarted";
        $manpower_percent = "";
        $total_completed_msmrt = 0;
        for ($mp_count = 0 ; $mp_count < count($man_power_actual_data["data"]) ; $mp_count++) {
            $total_completed_msmrt = $total_completed_msmrt + $man_power_actual_data["data"][0]["project_task_actual_manpower_completed_msmrt"];
        }
    } else {
        $manpower_percent = $man_power_actual_data["data"][0]["project_task_actual_manpower_completion_percentage"];
        $manpower_status ="";
        $total_completed_msmrt = 0;
    }

    if (isset($_POST["add_tasks_submit"])) {
        // Capture all form data
        $task_id                = $_POST["hd_task_id"];
        $road_id                = $_POST["hd_road_id"];
        $project_id             = $_POST["hd_project_id"];
        $date                   = $_POST['date'];
        $men                    = $_POST['num_men'];
        $women                  = $_POST['num_women'];
        $mason                  = $_POST['num_mason'];
        $others                 = $_POST['num_others'];
        $agency                 = $_POST['ddl_agency'];
        $men_rate               = $_POST['hd_men_rate'];
        $women_rate             = $_POST['hd_women_rate'];
        $mason_rate             = $_POST['hd_mason_rate'];
        $work_type              = $_POST['work_type'];
        $remarks 	            = $_POST['txt_remarks'];

        $manpower_status = $get_details_data["manpower_status"];
        $machine_status = $get_details_data["machine_status"];
        $contract_status = $get_details_data["contract_status"];
        if (($manpower_status == "NotStarted") && ($machine_status == "NotStarted") && ($contract_status == "NotStarted")) {
            $project_process_task_update_data = array("actual_start_date"=>$date);
            $task_update_start_date_list = i_update_project_process_task($task_id, $project_process_task_update_data);
        } else {
            //Do nothing
        }

        if ($work_type == 1) {
            $task_iresult =  i_add_man_power($task_id, $road_id, $date, $men, $women, $mason, $others, $agency, $men_rate, $women_rate, $mason_rate, '', '', $remarks, $user, 'Regular', '');

            if ($task_iresult["status"] == SUCCESS) {
                $alert_type = 1;
                $alert = "Manpower Successfully Added";
            } else {
                //Do nothing
            }
        } elseif ($work_type == 2) {
            $manpower_iresult =  i_add_man_power($task_id, $road_id, $date, $men, $women, $mason, $others, $agency, $men_rate, $women_rate, $mason_rate, '', '', $remarks, $user, 'Rework', '');
            send_mail($project_id,$task_id,'manpower','',$loggedin_name,$road_id,$remarks);
            if ($manpower_iresult["status"] == SUCCESS) {
                $alert_type = 1;
                $alert = "Manpower Successfully Added";
            } else {
                //Do nothing
            }
            $rework_iresult =  i_add_project_manpower_rework($task_id, $road_id, $date, $men, $women, $mason, $agency, '', '', '', '', '', $remarks, $user);
        }
    } else {
        //
    }
    //Get Project Task Master modes already added
    // $project_task_master_search_data = array("active"=>'1',"task_id"=>$task_id);
    // $project_task_master_list = i_get_project_task_master($project_task_master_search_data);
    // if ($project_task_master_list['status'] == SUCCESS) {
    //     $project_task_master_list_data = $project_task_master_list['data'];
    //     $task_name = $project_task_master_list_data[0]["project_task_master_name"];
    // } else {
    //     $task_name = "";
    // }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
        $alert = $project_manpower_agency_list["data"];
        $alert_type = 0;
    }

    // Temp data
    $man_power_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>'1');
    $man_power_list = i_get_man_power_list($man_power_search_data);
    if ($man_power_list["status"] == SUCCESS) {
        $man_power_list_data = $man_power_list["data"];
    } else {
    }
} else {
    header("location:login.php");
}

function get_road_name($task_id)
{
    return 'Test '.$task_id;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50%;"> <i class="icon-th-list"></i>
              <h3>
                <span class="header-label">Project :</span> <?= $project; ?>
                <span class="header-label">Process :</span> <?= $process_name; ?>
                <span class="header-label">Task:</span>  <?= $task_name; ?>
              </h3>
              <h3 style="margin-left:25px">
                <span class="header-label">Road:</span>  <?= $road_name; ?>
                <span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
                <span class="header-label">UOM</span> <?= $uom; ?>
              </h3>
              <h3 >
                <span class="header-label">Regular</span> <?= $reg_overall_msmrt; ?> (MP : <?= $reg_mp_msmrt; ?>  MC : <?= $reg_mc_msmrt; ?> CW : <?= $reg_cw_msmrt; ?>)
                <span class="header-label">Rework</span> <?= $rework_overall_msmrt; ?> (MP : <?= $rework_mp_msmrt; ?>   MC : <?= $rework_mc_msmrt; ?> CW : <?= $rework_cw_msmrt; ?>)
                              </h3>
            </div>
            <!-- /widget-header -->


						<div class="widget-content">

							<div class="tabbable">
							<ul class="nav nav-tabs">
							  <li style="float:right">
							    <a href="#formcontrols" data-toggle="tab">Add Project</a>
							  </li>
							</ul>
							<br>
								<div class="control-group">
									<div class="controls">
									<?php
                                    if ($alert_type == 0) { // Failure?>
										<div class="alert">
	                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
	                                        <strong><?php echo $alert; ?></strong>
	                                    </div>
									<?php
                                    }
                                    ?>

									<?php
                                    if ($alert_type == 1) { // Success
                                    ?>
	                                    <div class="alert alert-success">
	                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
	                                        <?php echo $alert; ?>
	                                    </div>
									<?php
                                    }
                                    ?>
									</div> <!-- /controls -->
								</div> <!-- /control-group -->
								<div class="tab-content">
									<div class="tab-pane active" id="formcontrols">
										<form id="add_tasks" class="form-horizontal" method="post" action="project_task_actual_manpower.php">
											<input type="hidden" name="hd_task_id" id="hd_task_id" value="<?php echo $task_id; ?>" />
											<input type="hidden" name="hd_road_id" value="<?php echo $road_id; ?>" />
											<input type="hidden" name="hd_project_id" value="<?php echo $project_id; ?>" />
											<input type="hidden" name="hd_men_rate" id="hd_men_rate" value="">
											<input type="hidden" name="hd_women_rate" id="hd_women_rate" value="">
											<input type="hidden" name="hd_mason_rate" id="hd_mason_rate" value="">
										<fieldset>

														<div class="control-group">
														<label class="control-label" for="name">Date*</label>
														<div class="controls">
															<input type="date" style="width:21%" name="date" style="width:80%" id="date" value="<?php echo date('Y-m-d'); ?>">
														</div> <!-- /controls -->
													</div> <!-- /control-group -->

													<div class="control-group">
													<label class="control-label" for="name">Agency*</label>
													<div class="controls">
														<select name="ddl_agency" style="width:21%" id="ddl_agency" onchange="return manpower_rate()" required>
														<option>--Select vendor--</option>
														<?php
                                                        for ($count = 0; $count < count($project_manpower_agency_list_data); $count++) {
                                                            ?>
														<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>"><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
														<?php
                                                        }
                                                        ?></select>
													</div> <!-- /controls -->
												</div> <!-- /control-group -->

												<div class="control-group">
												<label class="control-label" for="num_men">Men*</label>
												<div class="controls">
													<input type="number" style="width:21%" name="num_men" style="width:80%" id="num_men" onkeyup='return display_man_days();'><span style="margin-left: 10px;" id="num_men_days"></span>&nbsp;&nbsp;<span id="hd_male_rate"></span>
												</div> <!-- /controls -->
											</div> <!-- /control-group -->


											<div class="control-group">
											<label class="control-label" for="num_women">Women*</label>
											<div class="controls">
												<input type="number"style="width:21%" name="num_women" style="width:80%" id="num_women" onkeyup='return display_woman_days();'><span style="margin-left: 10px;" id="num_women_days"></span>
												&nbsp;<span id="hd_female_rate">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
										<label class="control-label" for="num_mason">Mason*</label>
										<div class="controls">
											<input type="number" style="width:21%"  name="num_mason" style="width:80%" id="num_mason" onkeyup='return display_mason_days();'>
											<span style="margin-left: 10px;" id="num_mason_days"></span>
											<span id="hd_mason1_rate"></span>
										</div> <!-- /controls -->
									</div> <!-- /control-group -->

									<div class="control-group">
									<label class="control-label" for="num_others">Others*</label>
									<div class="controls">
										<input type="number"  style="width:21%" name="num_others" style="width:80%" id="num_others">
									</div> <!-- /controls -->
								</div> <!-- /control-group -->

									<div class="control-group">
									<label class="control-label" for="work_type">Worktype*</label>
									<div class="controls">
										<input type="radio" style="color:red" name="work_type" id="work_type"  <?php if ($reg_overall_msmrt >= $planned_msmrt) {
                                                            ?> disabled <?php
                                                        } ?> value="1">Regular Work<br>
										<input type="radio" name="work_type" id="work_type" <?php if ($reg_overall_msmrt <= 0) {
                                                            ?> disabled <?php
                                                        } ?> value="2"> Rework<br>
									</div> <!-- /controls -->
								</div> <!-- /control-group -->

									<div class="control-group">
									<label class="control-label" for="txt_remarks">Remarks*</label>
									<div class="controls">
										<input type="text" style="width:21%" name="txt_remarks" style="width:80%" id="txt_remarks">
									</div> <!-- /controls -->
								</div> <!-- /control-group -->
	                                                                                                                                                               										 <br />

											<div class="form-actions">
												<input type="submit" class="btn btn-primary" name="add_tasks_submit" id="add_tasks_submit" value="Submit" />
												<button type="reset" class="btn">Cancel</button>
											</div> <!-- /form-actions -->
										</fieldset>
									</form>
									</div>

								</div>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->


            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_actual_manpower(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_actual_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_actual_manpower(man_power_id,task_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_actual_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","man_power_id");
	hiddenField1.setAttribute("value",man_power_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","task_id");
	hiddenField2.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
function check_validity(total_payment_value,released_amount)
{
	var amount = parseInt(document.getElementById('amount').value);

	var cur_amount = parseInt(amount);
	var total_payment_value  = parseInt(total_payment_value);
	var released_amount      = parseInt(released_amount);
	if((cur_amount + released_amount) > total_payment_value)
	{
		document.getElementById('amount').value = 0;
		alert('Cannot release greater than total value');
	}
}

function display_man_days()
{
	var man_hrs  = document.getElementById('num_men').value;
	var men_rate = document.getElementById('hd_men_rate').value;

	if(man_hrs != '')
	{
		document.getElementById('num_men_days').innerHTML = (man_hrs/8) + ' - men';
		document.getElementById('hd_male_rate').innerHTML = 'Men Rate - ' + men_rate;
		if(men_rate == 0)
		{
			document.getElementById("add_tasks_submit").disabled = true;
		}
		else
		{
			document.getElementById("add_tasks_submit").disabled = false;
		}
	}
	else
	{
		document.getElementById('num_men_days').innerHTML = '';
	}
}

function display_woman_days()
{

	var woman_hrs  = document.getElementById('num_women').value;

	if(woman_hrs != '')
	{
		var women_rate = document.getElementById('hd_women_rate').value;
		document.getElementById('num_women_days').innerHTML = (woman_hrs/8) + ' - women';
		document.getElementById('hd_women_rate').value = women_rate;
		document.getElementById('hd_female_rate').innerHTML  = 'Women Rate - ' + women_rate;
		if(women_rate == 0)
		{
			document.getElementById("add_tasks_submit").disabled = true;
		}
		else
		{
			document.getElementById("add_tasks_submit").disabled = false;
		}
	}
	else
	{
		document.getElementById('num_women_days').innerHTML = '';
	}
}

function display_mason_days()
{
	var mason_hrs = document.getElementById('num_mason').value;

	if(mason_hrs > 0)
	{
		var mason_rate = document.getElementById('hd_mason_rate').value;
		document.getElementById('num_mason_days').innerHTML = (mason_hrs/8) + ' - mason';
		document.getElementById("hd_mason1_rate").innerHTML     = 'Mason Rate - ' + mason_rate;
		if(mason_rate == 0)
		{
			document.getElementById("add_tasks_submit").disabled = true;
		}
		else
		{
			document.getElementById("add_tasks_submit").disabled = false;
		}
	}
	else
	{
		document.getElementById('num_mason_days').innerHTML = '';
	}
}

function manpower_rate()
{

	var vendor_id = document.getElementById("ddl_agency").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			var object = JSON.parse(xmlhttp.responseText);
			document.getElementById('hd_men_rate').value = object.men_rate;
			document.getElementById('hd_women_rate').value  = object.women_rate;
			document.getElementById("hd_mason_rate").value     = object.mason_rate;
			men_rate    = object.men_rate;
			woman_rate  = object.women_rate;
			mason_rate  = object.mason_rate;
		}
	}

	xmlhttp.open("POST", "ajax/project_get_manpower_rate.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("vendor_id=" + vendor_id);
}
</script>

  </body>

</html>
