<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 07_Dec-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["estimate_id"]))
	{
		$estimate_id = $_GET["estimate_id"];
	}
	else
	{
		$estimate_id = "";
	}
	
	// Get Project Man Power Estimate modes already added
	$project_man_power_estimate_search_data = array("estimate_id"=>$estimate_id);
	$man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
	if($man_power_estimate_list['status'] == SUCCESS)
	{
		$man_power_estimate_list_data = $man_power_estimate_list['data'];
		$project = $man_power_estimate_list_data[0]["project_master_name"];
		$process = $man_power_estimate_list_data[0]["project_process_master_name"];
		$task = $man_power_estimate_list_data[0]["project_task_master_name"];
	}	
	
	// Capture the form data
	if(isset($_POST["edit_man_power_estimate_submit"]))
	{
		$estimate_id            = $_POST["hd_estimate_id"];
		$task_id                = $_POST["hd_task_id"];
		$no_of_men              = $_POST["no_of_men"];
		$no_of_women            = $_POST["no_of_women"];
		$no_of_mason            = $_POST["no_of_mason"];
		$no_of_others           = $_POST["no_of_others"];
		$remarks 	            = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($no_of_men != "") && ($no_of_women != "") && ($no_of_mason != "") && ($no_of_others != ""))
		{
			$project_man_power_estimate_update_data = array("no_of_men"=>$no_of_men,"no_of_women"=>$no_of_women,"no_of_mason"=>$no_of_mason,"no_of_others"=>$no_of_others,"remarks"=>$remarks);
			$project_man_power_estimate_iresult = i_update_project_man_power_estimate($estimate_id,$project_man_power_estimate_update_data);
			
			if($project_man_power_estimate_iresult["status"] == SUCCESS)
				
			{	
			$alert_type = 1;
		    
				header("location:project_man_power_estimate_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_man_power_estimate_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Project Man Power Rate modes already added
	$project_man_power_rate_search_data = array("active"=>'1');
	$project_man_power_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
	if($project_man_power_rate_list['status'] == SUCCESS)
	{
		$project_man_power_rate_list_data = $project_man_power_rate_list['data'];
	}
	else
	{ 
		$alert = $project_man_power_rate_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Man Power Agency Master modes already added
	$project_manpower_agency_search_data = array();
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
		
    }
     else
    {
		$alert = $project_manpower_agency_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Man Power Estimate modes already added
	$project_man_power_estimate_search_data = array("estimate_id"=>$estimate_id);
	$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
	if($project_man_power_estimate_list['status'] == SUCCESS)
	{
		$project_man_power_estimate_list_data = $project_man_power_estimate_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Man Power Estimate</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Man Power Estimate  &nbsp;&nbsp; Project : <?php echo $project ;?> &nbsp;&nbsp;Process :
						<?php echo  $process;?>&nbsp;&nbsp;Task : <?php echo $task;?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Man Power Estimate</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_edit_man_power_estimate_form" class="form-horizontal" method="post" action="project_edit_man_power_estimate.php">
								<input type="hidden" name="hd_estimate_id" value="<?php echo $estimate_id; ?>" />
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
									<fieldset>										
																
										
										
										<div class="control-group">											
											<label class="control-label" for="no_of_men">No of Men</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_men" placeholder="No of Men" value="<?php echo $project_man_power_estimate_list_data[0]["project_man_power_estimate_no_of_men"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_women">No of Women</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_women" placeholder="No of Women" value="<?php echo $project_man_power_estimate_list_data[0]["project_man_power_estimate_no_of_women"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_mason">No of Mason</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_mason" placeholder="No of Mason" value="<?php echo $project_man_power_estimate_list_data[0]["project_man_power_estimate_no_of_mason"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_others">No Of Others</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_others" placeholder="No of Others" value="<?php echo $project_man_power_estimate_list_data[0]["project_man_power_estimate_no_of_others"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_man_power_estimate_list_data[0]["project_man_power_estimate_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_man_power_estimate_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
