var planning_ids = [], data = {};
var $$ = function(id) {
    return document.getElementById(id);
  },
  container = $$('spreadsheet'),
  autosaveNotification,
  hot;
status1 = document.getElementById("hd_status").value;
hot = new Handsontable(container, {
  startRows: 10,
  startCols: 25,
  rowHeaders: true,
  colWidths: [10, 13, 0.1, 6, 6, 6, 10, 10, 6, 6, 6, 0.1, 10, 10, 0.1, 0.1, 0.1],
  colHeaders: ['Process', 'Task', 'ID', 'UOM', 'Quantity <br/> (Qty)', 'Road <br/> Number', 'Resource <br/> Type', 'Resource <br/> Name', 'No of <br/> Objects', 'Per Day <br/> Output', 'Total days <br/> (Duration)', '', 'Start Date', 'End Date', 'Planning ID', 'Task Master Id', 'Save Type'],
  search: { },
  columns: [{},
    {},
    {},
    {},
    {},
    {},
    {
      type: 'dropdown'
    },
    {
      type: 'dropdown'
    },
    {},
    {},
    {
      type: 'numeric'
    },
    {},
    {
      type: 'date',
      dateFormat: 'YYYY/MM/DD',
      correctFormat: true,
      defaultDate: '1970/01/01'
    },
    {
      type: 'date',
      dateFormat: 'YYYY/MM/DD',
      correctFormat: true,
      defaultDate: '1970/01/01'
    },
    {},
    {},
    {}
  ],
  // 0 :   "Process"
  // 1 :   "Task"
  // 2 :   "ID"
  // 3 :   "UOM"
  // 4 :   "MSRMT"
  // 5 :   "Road Number"
  // 6 :   "Object Type"
  // 7 :   "Object Name"
  // 8 :   "No of Objects"
  // 9 :   "Per Day Output"
  // 10 :  "Total Days"
  // 11 :  ""
  // 12 :  "Start Date"
  // 13 :  "End Date"
  // 14 :  "Planning ID"
  // 15 :  "Task Master Id"
  // 16 :  "Save Type"
  stretchH: 'all',
  beforeKeyDown: function(event) {

    if (event.keyCode === 46) {
      alert('Delete is not supported');
      Handsontable.Dom.stopImmediatePropagation(event);
    }
  },
  afterChange: function(change, source) {
    if (source === 'loadData' || source === 'dateValidator') {
      return; //don't save this change
    } else {
      if (!window.localStorage.getItem('public_holiday_list')) {
        get_holiday_list();
      }
      var changed_row = change[0][0];
      var changed_col = change[0][1];
      var old_value = change[0][2];
      var new_value = change[0][3];

      var measurement = hot.getDataAtCell(changed_row, 4);
      var object_name = hot.getDataAtCell(changed_row, 7);
      var task_id = hot.getDataAtCell(changed_row, 15);
      var total_days = hot.getDataAtCell(changed_row, 10);
      if ((total_days == 0) && (measurement != 0) && (changed_col == 10)) {
        // hot.setDataAtCell(changed_row, 10, old_value);
      }
      if ((changed_col == 4) && parseInt(data["data"][0][changed_row][4]) != 0 && (parseFloat(old_value) < parseFloat(new_value))) {
	       setPlanningId(changed_row);
       } else {
         console.log('planning_id ', planning_ids, old_value);
     }

      if ((changed_col == 6)) {
        if (old_value != new_value) {
          hot.setDataAtCell(changed_row, 7, '');
        }

        var instance = this;
        $.ajax({
          url: 'project_get_cw_list.php',
          data: "object_type=" + new_value + "&task_id=" + task_id,
          dataType: 'json',
          success: function(response) {
            var cell_properties = instance.getCellMeta(changed_row, 7);
            cell_properties.source = response;
            instance.render();
          }
        });
      }

      if ((changed_col == 7)) {
        var measurement = hot.getDataAtCell(changed_row, 4);
        var next_row = hot.getDataAtCell((changed_row - 1), 13);
        var object_type = hot.getDataAtCell(changed_row, 6);
        var object_name = encodeURIComponent(new_value);
        var start = hot.getDataAtCell(changed_row, 12);

        if (object_name != '') {
          $.ajax({
            type: "GET",
            data: "object_name=" + object_name + "&task_id=" + task_id + "&object_type=" + object_type,
            url: "project_get_object_details.php",
            cache: false,
            dataType: 'json',
            success: function(response) {
              hot.setDataAtCell(changed_row, 9, response[1]);
              hot.setDataAtCell(changed_row, 8, 1);
              var no_of_objects = hot.getDataAtCell(changed_row, 8);
              var total_days = Math.ceil(((measurement / response[1]) / no_of_objects));
              hot.setDataAtCell(changed_row, 10, total_days);
              hot.setDataAtCell(changed_row, 16, 'auto');

              if (changed_row == 0 || (next_row == '0000-00-00')) {
                var d = new Date();
                var start_date_display = get_formatted_date(d);
                var end_date = new Date(d);
              } else {
                var end_date = new Date(start);
                var start_date = new Date(next_row);
                start_date.setDate(new Date(next_row).getDate() + 1);
                start_date_display = get_formatted_date(start_date);
              }
              // hot.setDataAtCell(changed_row, 12, start_date_display);
              // var end_date = new Date(d);

              end_date.setDate(end_date.getDate() + (total_days - 1));
              var end_date_display = get_formatted_date(end_date);
              // hot.setDataAtCell(changed_row, 13, end_date_display);
              if (measurement != 0) {
                var newDates = process_dates(start_date_display, end_date_display);

                hot.setDataAtCell(changed_row, 12, newDates[0]);
                hot.setDataAtCell(changed_row, 13, newDates[1]);
              }
            }
          });
        }

        //$task_id = hot.getDataAtCell(changed_row,2);
        $object_type = hot.getDataAtCell(changed_row, 6);
      }

      // measurement column
      if (changed_col == 4) {
        var object_type = hot.getDataAtCell(changed_row, 6);
        var measurement = hot.getDataAtCell(changed_row, 4);
        var next_row = hot.getDataAtCell((changed_row - 1), 13);
        var no_of_objects = hot.getDataAtCell(changed_row, 8);
        var per_day_output = hot.getDataAtCell(changed_row, 9);

        var instance = this;
        $.ajax({
          url: 'project_get_object_type.php',
          data: "&task_id=" + task_id,
          dataType: 'json',
          success: function(response) {
            var temp = [];
            for (var name of response) {
              if (temp.indexOf(name) < 0) {
                temp.push(name)
              }
            }
            var cell_properties = instance.getCellMeta(changed_row, 6);
            cell_properties.source = temp;
            instance.render();
          }
        });

        if (Number(per_day_output) != 0.00 && no_of_objects != 0) {
          var no_of_objects = hot.getDataAtCell(changed_row, 8);
          var total_days = Math.ceil(((measurement / per_day_output) / no_of_objects));
          if (Number(per_day_output) != 0) {
            hot.setDataAtCell(changed_row, 10, total_days);
          } else {
            hot.setDataAtCell(changed_row, 10, 1)
          }
          hot.setDataAtCell(changed_row, 16, 'auto');

          if (changed_row == 0 && hot.getDataAtCell(0, 12) == '0000-00-00') {
            var d = new Date();
            var start_date_display = get_formatted_date(d);
          } else if (hot.getDataAtCell(changed_row, 12) != '0000-00-00') {
            start_date_display = hot.getDataAtCell(changed_row, 12);
            var end_date = new Date(start_date_display);
          } else {
            var start_date = new Date(next_row);
            var end_date = new Date(start);
            start_date.setDate(start_date.getDate() + 1);
            start_date_display = get_formatted_date(start_date);
          }
          // var end_date = new Date(d);
          end_date.setDate(end_date.getDate() + (total_days - 1));
          //var end_date =
          var end_date_display = get_formatted_date(end_date);

          if (measurement != 0) {
            var newDates = process_dates(start_date_display, end_date_display);
            hot.setDataAtCell(changed_row, 12, newDates[0]);
            hot.setDataAtCell(changed_row, 13, newDates[1]);
          }
        }
      }
      if (changed_col == 10) {
        var measurement = hot.getDataAtCell(changed_row, 4);
        var per_day_output = hot.getDataAtCell(changed_row, 9);
        var next_row = hot.getDataAtCell((changed_row - 1), 13);
        var updated_total_days = hot.getDataAtCell(changed_row, 10);
        if (new_value != 0) {
          var per_day_expected_output = measurement / new_value;
        } else {
          var per_day_expected_output = 0;
        }
        var object_type = hot.getDataAtCell(changed_row, 7);
        if (!object_type && !object_type.length) {
          hot.setDataAtCell(changed_row, 10, 0);
          return false;
        }
        if (updated_total_days == 0) {
          return false;
        }
        if (updated_total_days > Math.ceil(measurement / per_day_output)) {
          alert('Total days cannot be greater than measurement per number of objects');
          hot.setDataAtCell(changed_row, 10, old_value);
          return false;
        }

        if (per_day_output != 0) {
          total_objects = Math.ceil(per_day_expected_output / per_day_output);
        } else {
          total_objects = 1;
        }

        if (hot.getDataAtCell(changed_row, 16) != 'auto') {
          hot.setDataAtCell(changed_row, 8, total_objects);
        } else {
          hot.setDataAtCell(changed_row, 16, 'user');
        }

        if ((changed_row == 0 && hot.getDataAtCell(changed_row, 12) == '0000-00-00') || (next_row == '0000-00-00')) {
          var d = new Date();
          var start_date_display = get_formatted_date(d);
          var end_date = new Date();
        } else if (hot.getDataAtCell(changed_row, 12) != '0000-00-00') {
          start_date_display = hot.getDataAtCell(changed_row, 12);
          var end_date = new Date(start_date_display);
        } else {
          var start_date = new Date(next_row);
          start_date.setDate(start_date.getDate() + 1);
          start_date_display = get_formatted_date(start_date);
          var end_date = start_date;
        }

        end_date.setDate(end_date.getDate() + (updated_total_days - 1));
        var end_date_display = get_formatted_date(end_date);
        if (measurement != 0) {
          var newDates = process_dates(start_date_display, end_date_display);

          hot.setDataAtCell(changed_row, 12, newDates[0]);
          hot.setDataAtCell(changed_row, 13, newDates[1]);
        }
      }
      // if(changed_col == 12)
      if (changed_col == 12 && old_value != new_value) {
        var object_type = hot.getDataAtCell(changed_row, 6);
        var object_name = encodeURIComponent(new_value);
        var measurement = hot.getDataAtCell(changed_row, 4);
        var next_row = hot.getDataAtCell((changed_row - 1), 13);
        var no_of_objects = hot.getDataAtCell(changed_row, 8);
        var total_days = hot.getDataAtCell(changed_row, 10);
        var per_day_output = hot.getDataAtCell(changed_row, 9);

        if ((Number(measurement != 0)) && (Number(per_day_output) != 0.00)) {
          var no_of_objects = hot.getDataAtCell(changed_row, 8);
          var start = hot.getDataAtCell(changed_row, 12);
          var end_date = new Date(start);
          end_date.setDate(end_date.getDate() + (total_days - 1));
          var end_date_display = get_formatted_date(end_date);
          // hot.setDataAtCell(changed_row, 13, end_date_display);

          if (measurement != 0) {
            var newDates = process_dates(start, end_date_display);

            if (isSunday(start)) {
              alert('Start Date cannot be on Sunday ' + start);
              hot.setDataAtCell(changed_row, 12, newDates[0]);
            } else if (isPublicHoliday(start)) {
              alert('Start Date cannot be on Public Holiday ' + start);
              hot.setDataAtCell(changed_row, 12, newDates[0]);
            }
            hot.setDataAtCell(changed_row, 13, newDates[1]);
          }

        }

        if (compare_dates(start, $("#hd_start_date").val(), 'lt')) {
          $("#hd_start_date").val(start);
          $("#p_start_date").html(start);
          duration = get_date_difference($("#hd_start_date").val(), $("#hd_end_date").val());
          $("#duration_value").html(duration);
        }
      }

      // code to update next_row startDate
      // if (status1 != "Update") {
      //   if (changed_col == 13) {
      //     var prev_end_date = hot.getDataAtCell(changed_row, 13);
      //     var start_date = new Date(prev_end_date);
      //     start_date.setDate(start_date.getDate() + 1);
      //     var start_date_display = get_formatted_date(start_date);
      //     hot.setDataAtCell((changed_row + 1), 12, start_date_display);
      //     if (compare_dates(prev_end_date, $("#hd_end_date").val(), 'gt')) {
      //       $("#hd_end_date").val(prev_end_date);
      //       $("#p_end_date").html(prev_end_date);
      //       duration = get_date_difference($("#hd_start_date").val(), $("#hd_end_date").val());
      //       $("#duration_value").html(duration);
      //     }
      //   }
      // }
    }

    var planning_id = hot.getDataAtCell(changed_row, 14);

    // $.ajax({
    //   url: 'project_session_save_id.php',
    //   data: "planning_id=" + planning_id,
    //   dataType: 'json',
    //   success: function(response) {
    //     var cell_properties = instance.getCellMeta(changed_row, 7);
    //     //cell_properties.source = response;
    //     //instance.render();
    //   }
    // });
  }
});

searchFiled = document.getElementById('search_field');

Handsontable.dom.addEvent(searchFiled,'keyup', function (event) {
var self = this;
  setTimeout(function() {

    if(self.value == '' || self.value == ' ') {
      hot.render();
      return false;
    }

  var  queryResult = hot.search.query(self.value);

    if(queryResult.length) {
      hot.scrollViewportTo(queryResult[0].row, queryResult[0].col);
      document.getElementById('search_results').innerHTML = 'Total ' + queryResult.length + ' record(s) found';
    } else {
      document.getElementById('search_results').innerHTML = 'No records found';
    }
    hot.render();
  }, 2000);
 });


 $('#search_field').on('input', function(e) {
    if('' == this.value) {
    document.getElementById('search_results').innerHTML = '';
    hot.scrollViewportTo(0, 0);
    hot.search.query('');
    hot.render();
    }
});

var source = getUrlVars()["source"];
var project_id = getUrlVars()["project_id"];

$.ajax({
  type: "GET",
  data: "project_id=" + project_id,
  url: "project_get_task_planning_list.php",
  cache: false,
  success: function(result) {
    if (result) {

      data = JSON.parse(result);
      console.log('result ', data);
      hot.populateFromArray(0, 0, data.data[0], '', '', 'loadData');

      $("#p_start_date").html(data.data[2]["start_date"]);
      $("#p_end_date").html(data.data[2]["end_date"]);

      $("#hd_start_date").val(data.data[2]["hd_start_date"]);
      $("#hd_end_date").val(data.data[2]["hd_end_date"]);
      duration = get_date_difference(data.data[2]["hd_start_date"], data.data[2]["hd_end_date"]);
      $("#duration_value").html(duration);
      if (data.data[1] == 'no') {
        hot.updateSettings({
          cells: function(row, col, prop) {
            var cellProperties = {};
            if (status1 == "Plan") {
              if (col == 0 || col == 1 || col == 2 || col == 3 || col == 5 || col == 8 || col == 9) {
                cellProperties.readOnly = true;
              }
            } else if (status1 == "Update") {
              if (col == 0 || col == 1 || col == 2 || col == 3 || col == 4 || col == 5 || col == 6 || col == 7 || col == 8 || col == 9 || col == 11 || col == 12 || col == 13) {
                cellProperties.readOnly = true;
              }
            } else if (status1 == "Locked") {
              if (col == 0 || col == 1 || col == 2 || col == 3 || col == 4 || col == 5 || col == 6 || col == 7 || col == 8 || col == 9 || col == 10 || col == 11 || col == 12 || col == 13) {
                cellProperties.readOnly = true;
              }
            } else {
              //
            }

            return cellProperties;
          }
        })
      }
    }
  }
})
$('#add_task_plan_submit').click(function() {

  // Get token from the URL
  // Initialize data to be sent to server
  var params_to_send = JSON.stringify({
    data: hot.getData(),
    planning_ids : planning_ids,
    project_id : getUrlVars()["project_id"]
  });
  var source = getUrlVars()["source"];
  var project_id = getUrlVars()["project_id"];
  params_to_send = encodeURIComponent(params_to_send);
  $('#ajax_loading').show();
  $.ajax({

    type: "POST",
    data: "task_data=" + params_to_send,
    url: "project_edit_task_plan.php",
    success: function(data) {
		console.log(data);
      $('#next_button').removeClass('disabled_link');
      $('#span_last_updated').text(moment(data.updated).format('D MMM YY, H:mm'));
      $('#ajax_loading').hide();
      alert('Planning data saved successfully')
      //window.location = "project_task_planning_list.php?project_id=" + project_id;
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $('#ajax_loading').hide();
      console.log(XMLHttpRequest);
      if (XMLHttpRequest.status == 400) {
        $('#next_button').addClass('disabled_link');
      } else {
        swal("Oops, something went wrong", "Looks like there is an issue. Please try again after some time", "warning");

      }
    }
  })


});

function getUrlVars() {
  var vars = [],
    hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    if (hash[1] && hash[1].indexOf('#') > -1) {
      hash[1] = hash[1].replace('#', '');
    }
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }


  return vars;
}

function get_formatted_date(date) {
  /*var formatted_date =
  ("0" + date.getDate()).slice(-2) + "/" +
  ("0" + (date.getMonth() + 1)).slice(-2) + "/" +
  date.getFullYear();
  return formatted_date;*/

  // return date.toLocaleDateString('en-US');
  return moment(date).format('YYYY/MM/DD')
}

function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
  Handsontable.renderers.TextRenderer.apply(this, arguments);
  td.style.fontWeight = 'bold';
  // td.style.color = 'green';
  td.style.background = '#D3D3D3';
}

function secondRowRenderer(instance, td, row, col, prop, value, cellProperties) {
  Handsontable.renderers.TextRenderer.apply(this, arguments);
  td.style.fontWeight = 'bold';
  // td.style.color = 'green';
  td.style.background = 'red';
}

function compare_dates(date1, date2, type) {
  var d1 = new Date(date1);
  var d2 = new Date(date2);

  if (type == 'lt') {
    if (d1 < d2) {
      return true;
    } else {
      return false;
    }
  } else {
    if (d1 > d2) {
      return true;
    } else {
      return false;
    }
  }
}

function get_date_difference(start_date, end_date) {
  var date1 = new Date(start_date);
  var date2 = new Date(end_date);
  var diffDays = Math.abs(Math.round((date2 - date1) / (1000 * 60 * 60 * 24)));

  return (diffDays + 1);
}

// Returns an array of dates between the two dates
function enumerateDaysBetweenDates(startDate, endDate) {
  startDate = moment(startDate);
  endDate = moment(endDate);

  var now = startDate,
    dates = [];

  while (now.isBefore(endDate) || now.isSame(endDate)) {
    dates.push(now.format('YYYY/MM/DD'));
    now.add(1, 'days');
  }
  return dates;
};

function get_holiday_list() {

  $.ajax({
    url: 'data_access/da_hr_masters.php?f=db_get_holiday_list',
    data: {
      date_start: new Date().getFullYear() + '-01-01',
      date_end: new Date().getFullYear() + '-12-31'
    },
    dataType: 'json',
    success: function(response) {
      var temp = [];
      for (var date in response) {
        temp.push(response[date]['hr_holiday_date']);
      }
      window.localStorage.setItem('public_holiday_list', JSON.stringify(temp));
    }
  });
}

// Process startDate and endDate
function process_dates(startDate, endDate) {

  var count = 0;
  var all_dates = enumerateDaysBetweenDates(startDate, endDate);
  var public_holiday = getPublicHoliday();

  for (var day in all_dates) {
    if (new Date(day).getDay() == 0) {
      count++;
    }
  }

  for (var i in public_holiday) {
    for (var j in all_dates) {
      if (all_dates[j] == public_holiday[i] && new Date(public_holiday[i]).getDay() != 0) {
        count++;
      }
    }
  }

  // is startDate is Sunday
  // is startDate is Public Holiday
  if (new Date(startDate).getDay() == 0 || isPublicHoliday(startDate)) {
    startDate = add_days(startDate, 1);
  }

  endDate = add_days(endDate, count);

  //is endDate is Sunday
  // is endDate is Public Holiday
  if (new Date(endDate).getDay() == 0 || isPublicHoliday(endDate)) {
    endDate = add_days(endDate, 1);
  }

  return [startDate, endDate];
}

function add_days(date, count) {
  return moment(date).add(count, 'days').format('YYYY/MM/DD')
}

function isPublicHoliday(date) {
  var public_holiday = getPublicHoliday();
  var result = false;

  for (var i in public_holiday) {
    if (moment(date).format('') == moment(public_holiday[i]).format('')) {
      result = true;
    }
  }

  return result;
}

function isSunday(date) {
  if (new Date(date).getDay() == 0) {
    return true;
  } else {
    return false;
  }
}

function getPublicHoliday() {
  return JSON.parse(window.localStorage.getItem('public_holiday_list'));
}
function setPlanningId(changed_row){
    var planning_id = hot.getDataAtCell(changed_row, 14);
if(planning_ids.indexOf(planning_id) == -1){
  planning_ids.push(planning_id);
        }
    }
