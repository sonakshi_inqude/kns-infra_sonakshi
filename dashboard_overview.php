<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: dashboard_overview.php

CREATED ON	: 13-January-2017

CREATED BY	: Nitin Kashyap

PURPOSE     : Centralized Dashboard Access

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Query String Data

	// Nothing here	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>DASHBOARD</title>



	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes"> 

    

<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />



<link href="css/font-awesome.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    

<link href="css/style.css" rel="stylesheet" type="text/css">

<link href="css/pages/signin.css" rel="stylesheet" type="text/css">



</head>



<body>

	

<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



 <div style="width:70%; margin-left:auto; margin-right:auto;">

 

 <p>&nbsp;</p>

 

 <div class="row" style="padding-left:30px;">

 	<div class="span3" style="background-color:#4787DF; padding:10px; height:100px;">

		<div class="card" style="color:#fff;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-pie-chart" aria-hidden="true"></i>

			Marketing</h3>  

				<a href="dashboard_marketing_summary.php" class="card-link" style="color:black;"><b>Marketing Summary</b></a> <br />

				<a href="dashboard_marketing_daywise_summary.php" class="card-link" style="color:black;"><b>Daywise Summary</b></a>

		</div>

		</div>

	</div>





	<div class="span3" style="background-color:#DD4250; padding:10px; height:100px;">    

		<div class="card" style="color:#fff;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-line-chart" aria-hidden="true"></i>

			Sales</h3>   

				<a href="dashboard_sales_summary.php" class="card-link" style="color:black;"><b>Sales Summary</b></a> <br />

				<a href="dashboard_sales_employeewise.php" class="card-link" style="color:black;"><b>Daywise Summary</b></a><br />

				<a href="dashboard_sales_employee_monthwise.php" class="card-link" style="color:black;"><b>Monthwise Summary</b></a>

		</div>

		</div>

	</div>



	<div class="span3" style="background-color:#2DBD9A; padding:10px; height:100px;">

		<div class="card" style="color:#fff;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-user-circle-o" aria-hidden="true"></i>

			CRM</h3>   

				<a href="dashboard_crm_summary.php" class="card-link" style="color:black;"><b>CRM Summary</b></a> <br />

				<a href="dashboard_crm_projectwise.php" class="card-link" style="color:black;"><b>Projectwise Summary</b></a><br />

		</div>

		</div>

	</div>

</div>

<br />

<div class="row" style="padding-left:30px;">

 	<div class="span3" style="background-color:#F7BC30; padding:10px; height:100px;">

		<div class="card" style="color:#fff;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-graduation-cap" aria-hidden="true"></i>

			Legal</h3>  

				<a href="completed_process_report.php" class="card-link" style="color:black;"><b>Completed Process Report</b></a>

		</div>

		</div>

	</div>





	<div class="span3" style="background-color:#EB5638; padding:10px; height:100px;">    

		<div class="card" style="color:#fff;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-wpexplorer" aria-hidden="true"></i>

			Project Management</h3>   

				<a href="dashboard_projectmgmnt.php" class="card-link" style="color:black;"><b>Project Finance Report</b></a><br />
				<a href="project_completion_report.php" class="card-link" style="color:black;"><b>Project Completion Report</b></a>

		</div>

		</div>

	</div>

	<div class="span3" style="background-color:#8CC34B; padding:10px; height:100px;">

		<div class="card" style="color:#fff;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-binoculars" aria-hidden="true"></i>

			Survey Process</h3>  

				<a href="dashboard_survey_summary.php" class="card-link" style="color:black;"><b>Survey Summary</b></a> <br />

		</div>

		</div>

	</div>

	

</div>

<br />

<div class="row" style="padding-left:30px;">

 	<div class="span3" style="background-color:#CCD0DA; padding:10px; height:100px;">

		<div class="card" style="color:#030303;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-amazon" aria-hidden="true"></i>

			APF</h3>   

				<i>Coming Soon</i>

		</div>

		</div>

	</div>



	<div class="span3" style="background-color:#CCD0DA; padding:10px; height:100px;">    

		<div class="card" style="color:#030303;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-pie_chart" aria-hidden="true"></i>

			Liaison</h3>   

				<i>Coming Soon</i>

		</div>

		</div>

	</div>



	<div class="span3" style="background-color:#CCD0DA; padding:10px; height:100px;">

		<div class="card" style="color:#030303;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-list-ol" aria-hidden="true"></i>

			Tasks</h3>   

				<i>Coming Soon</i>

		</div>

		</div>

	</div>

</div>

<br />

<div class="row" style="padding-left:30px;">

 	<div class="span3" style="background-color:#CCD0DA; padding:10px; height:100px;">

		<div class="card" style="color:#030303;">

		<div class="card-block">

			<h3 class="card-title"><i class="fa fa-file-text-o" aria-hidden="true"></i>

			Khatha Process</h3>  

				<i>Coming Soon</i>

		</div>

		</div>

	</div>

</div>



</div>

 

 

</div>





<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>



<script src="js/signin.js"></script>

<script src="https://use.fontawesome.com/1c608bc16d.js"></script>

</body>



</html>

