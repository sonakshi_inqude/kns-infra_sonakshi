<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_method_planning_list.php
CREATED ON	: 14-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID','283');
/* DEFINES - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	$search_project   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}

	$search_process   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_process   = $_POST["search_process"];
	}

	$search_task   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_task   = $_POST["search_task"];
	}

	$search_doc_type   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_doc_type   = $_POST["search_doc_type"];
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1');
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	//Get Task Mehod Planning List
	$project_task_method_planning_search_data = array("active"=>'1',"process_id"=>$search_process,"project_id"=>$search_project,"task_id"=>$search_task,
	"plan_type"=>$search_doc_type);
	$project_task_method_planning_list = i_get_project_task_method_planning($project_task_method_planning_search_data);
	if($project_task_method_planning_list["status"] == SUCCESS)
	{
		$project_task_method_planning_list_data = $project_task_method_planning_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_method_planning_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Method Planning List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
			  <h3>Project Task Method Planning List</h3>
            </div>
            </div>
            <!-- /widget-header -->

			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_task_method_planning_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_doc_type">
			  <option value="">- - Select Document Type - -</option>
			  <option value="1" <?php if($search_doc_type == 1) { ?> selected="selected" <?php } ?>>Study Document</option>
			  <option value="2" <?php if($search_doc_type == 1) { ?> selected="selected" <?php } ?>>Check List</option>
			  <option value="3" <?php if($search_doc_type == 1) { ?> selected="selected" <?php } ?>>Cad Drawings</option>
			  </select>
			  </span>


			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>
            <div class="widget-content">
			<?php
			if($view_perms_list["status"] == SUCCESS)
			{
		    ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>Process</th>
					<th>Task Name</th>
					<th>Document</th>
					<th>Document Type</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
					<th colspan="4" style="text-align:center;">Actions</th>
				</tr>
				</thead>
				<tbody>
				<?php
					if($project_task_method_planning_list["status"] == SUCCESS)
					{
					$sl_no = 0;
					for($count = 0; $count < count($project_task_method_planning_list_data); $count++)
					{

						$sl_no++;
						$doc = $project_task_method_planning_list_data[$count]["project_task_method_planning_document"];
						$plan_type = $project_task_method_planning_list_data[$count]["project_task_method_planning_type"];
						if($plan_type == 1)
						{
							$plan_type = "Study Document";
						}
						elseif($plan_type == 2)
						{
							$plan_type = "Check List";
						}
						else
						{
							$plan_type = "Cad Drawings";
						}


					if($doc != 0)
					{
					?>
					<?php
					if(($edit_perms_list['status'] == SUCCESS) || ($project_task_method_planning_list_data[$count]["project_task_method_planning_added_by"] == $user))
					{
					 ?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["project_task_master_name"]; ?></td>
					<td><a href="documents/<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_document"]; ?>" target="_blank">DOWNLOAD</a></td>
					<td><?php echo $plan_type; ?></td>
					<td><a href="#" onclick="alert('<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_remarks"]; ?>');"><?php echo substr($project_task_method_planning_list_data[$count]["project_task_method_planning_remarks"],0,30); ?></a></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_method_planning_list_data[$count][
					"project_task_method_planning_added_on"])); ?></td>
					<td><?php if(($project_task_method_planning_list_data[$count]["project_task_method_planning_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return project_delete_method_plan('<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_id"]; ?>','<?php echo $task_id ;?>');">Delete</a><?php } ?></td>

					</tr>
					<?php
					}
					}
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
			    <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content -->
			    <script>
				  document.getElementById('total').innerHTML = '<?php echo $total; ?>';
				</script>
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_method_plan(planning_id,task_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_upload_documents.php?task_id=" +task_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_method_plan.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("planning_id=" + planning_id + "&action=0");
		}
	}
}

</script>

  </body>

</html>
