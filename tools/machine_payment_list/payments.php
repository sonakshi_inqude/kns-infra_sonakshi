    <?php
/* SESSION INITIATE - START */
//session_start();
/* SESSION INITIATE - END */

/*
FILE        : project_actual_contract_payment_list.php
CREATED ON    : 09-May-2017
CREATED BY    : Ashwini
PURPOSE     : List of project for actual contract payment
*/

/*
TBD:
*/

/* DEFINES - START */
//define('PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID', '277');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user            = $_SESSION["loggedin_user"];
    $role            = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    $alert_type = -1;
    $alert = "";

    // Get permission settings for this user for this page
    // $view_perms_list   = i_get_user_perms($user, '', PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID, '2', '1');
    // $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID, '3', '1');
    // $delete_perms_list = i_get_user_perms($user, '', PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID, '4', '1');
    // $add_perms_list    = i_get_user_perms($user, '', PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    if (isset($_GET["search_vendor"])) {
        $search_machine_vendor  = $_GET["search_vendor"];
    } else {
        $$search_machine_vendor = "";
    }

    // Get Project Actual Machine Payment modes already added
    $project_actual_machine_payment_search_data = array("active"=>'1',"status"=>"Accepted","vendor_id"=>$search_machine_vendor);
    $project_actual_machine_payment_list = i_get_project_payment_machine($project_actual_machine_payment_search_data);
    if ($project_actual_machine_payment_list['status'] == SUCCESS) {
        $project_actual_machine_payment_list_data = $project_actual_machine_payment_list['data'];
    } else {
        $alert = $alert."Alert: ".$project_actual_machine_payment_list["data"];
    }

    // Machine Vendor data
    $project_machine_vendor_search_data = array("active"=>'1');
    $project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_search_data);
    if ($project_machine_vendor_list["status"] == SUCCESS) {
        $project_machine_vendor_list_data = $project_machine_vendor_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_machine_vendor_list["data"];
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
} else {
    header("location:login.php");
}
?>



 <link rel="stylesheet" type="text/css" href="tools/datatables/css/jquery.dataTables.min.css">
 <link rel="stylesheet" type="text/css" href="tools/datatables/css/buttons.dataTables.min.css">

  <script src="tools/datatables/js/jquery.dataTables.min.js"></script>
  <script src="tools/datatables/js/dataTables.buttons.min.js"></script>
  <script src="tools/datatables/js/jszip.min.js"></script>
  <script src="tools/datatables/js/pdfmake.min.js"></script>
  <script src="tools/datatables/js/vfs_fonts.js"></script>
  <script src="tools/datatables/js/buttons.html5.min.js "></script>

<style type="text/css">
 .modal{
    width: 80%; /* respsonsive width */
    margin-left:-40%; /* width/2) */
}
.modal-open {
    overflow: hidden;
}

.modal {
  overflow-y: auto;
}
/* custom class to override .modal-open */
.modal-noscrollbar {
    margin-right: 0 !important;
}
#ajax_loading {
    width: 100%;
    height: 100%;
    background: url('tools/img/loader.gif') top center no-repeat rgba(0,0,0,0.15);
    background-position: 50% 50%;
    text-align: center;
    font-size: 20px;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 99999999;
    display: none;
}
</style>
<div id="ajax_loading"></div>
<div class="container">

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">


      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Payments Details</h4>
        </div>

        <div class="modal-body">
          <div id="details"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</div>


<script>

 $(document).ready(function() {


    $( ".vendor" ).click(function() {
    $('#ajax_loading').show();
    vendor_id = $(this).attr('data-id');
      $.ajax({
             url: "tools/machine_payment_list/getVenderPaymentDetails.php?search_vendor="+vendor_id,
             success: function(result){
                 $('#ajax_loading').hide();
                $("#details").html(result);
                $("#myModal").modal();
                    $('#example').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                        ]
                    } );
            }});
    });

} );
</script>
