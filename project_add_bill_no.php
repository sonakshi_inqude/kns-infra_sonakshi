<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	if(isset($_REQUEST['manpower_id']))
	{
		$manpower_id = $_REQUEST['manpower_id'];
	}
	else
	{
		$manpower_id = '';
	}
	
	// Capture the form data
	if(isset($_POST["add_bill_no_submit"]))
	{		
		$manpower_id  		  = $_POST["hd_manpower_id"];
		$billing_address      = $_POST["ddl_company"];
		$remarks      		  = $_POST["stxt_remarks"];
		$approved_by 		  = $user;
		$approved_on 		  = date("Y-m-d H:i:s");
		$project_actual_payment_manpower_update_data = array("billing_address"=>$billing_address,"approved_by"=>$user,"approved_on"=>$approved_on,"remarks"=>$remarks);
		$approve_payment_manpower_result = i_update_project_actual_payment_manpower($manpower_id,$project_actual_payment_manpower_update_data);
		if($approve_payment_manpower_result["status"] == SUCCESS)
		{
			header("location:project_task_approved_actual_manpower_list.php");
		}
	}

	//Get Bill No
	$project_actual_payment_manpower_search_data = array("manpower_id"=>$manpower_id,"start"=>'-1');

	$payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);

	if($payment_manpower_list["status"] == SUCCESS)

	{

		$payment_manpower_list_data = $payment_manpower_list["data"];
		$bill_no = $payment_manpower_list_data[0]["project_actual_payment_manpower_bill_no"];

	}

	else

	{ 

		$alert = $payment_manpower_list["data"];

		$alert_type = 0;

	}	
	
	//Get Company List

	$stock_company_master_search_data = array();

	$company_list = i_get_company_list($stock_company_master_search_data);

	if($company_list["status"] == SUCCESS)

	{

		$company_list_data = $company_list["data"];

	}

	else

	{ 

		$alert = $company_list["data"];

		$alert_type = 0;

	}	
		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Bill No</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Bill No</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_quotation_compare_form" class="form-horizontal" method="post" action="project_add_bill_no.php">
								<input type="hidden" name="hd_manpower_id" value="<?php echo $manpower_id; ?>" />
									<fieldset>	

												
												
										<div class="control-group">											
										<label class="control-label" for="stxt_remarks">Bill No</label>
											<div class="controls">
												<input type="text" name="stxt_remarks" disabled value="<?php echo $bill_no ;?>" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										
										<div class="control-group">		
											<label class="control-label" for="ddl_company">Billing Address*</label>

											<div class="controls">

												<select name="ddl_company" required>

												<option value="">- - -Select Billing Address- - -</option>

												<?php

												for($count = 0; $count < count($company_list_data); $count++)

												{

												?>

												<option value="<?php echo $company_list_data[$count]["stock_company_master_id"]; ?>"><?php echo $company_list_data[$count]["stock_company_master_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="stxt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="stxt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                                                                                                                                                               										 <br />
										
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_bill_no_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
										
									</fieldset>
								</form>
								</div>
							</div>
			
              
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
