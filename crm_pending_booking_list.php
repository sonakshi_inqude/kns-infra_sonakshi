<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_pending_boooking_list.php
CREATED ON	: 03-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of booking waiting for approval
*/
/* DEFINES - START */define('CRM_PENDING_BOOKING_LIST_FUNC_ID','95');define('CRM_BOOKING_APPROVE_LIST_FUNC_ID','314');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_PENDING_BOOKING_LIST_FUNC_ID,'1','1');		$view_perms_list   = i_get_user_perms($user,'',CRM_PENDING_BOOKING_LIST_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_PENDING_BOOKING_LIST_FUNC_ID,'3','1');	$delete_perms_list = i_get_user_perms($user,'',CRM_PENDING_BOOKING_LIST_FUNC_ID,'4','1');
	// Get permission settings for this user for this page	$add_perms_approve_list    = i_get_user_perms($user,'',CRM_BOOKING_APPROVE_LIST_FUNC_ID,'1','1');		$view_perms_approve_list   = i_get_user_perms($user,'',CRM_BOOKING_APPROVE_LIST_FUNC_ID,'2','1');	$edit_perms_approve_list   = i_get_user_perms($user,'',CRM_BOOKING_APPROVE_LIST_FUNC_ID,'3','1');	$delete_perms_approve_list = i_get_user_perms($user,'',CRM_BOOKING_APPROVE_LIST_FUNC_ID,'4','1');

	// Query String Data
	if(isset($_GET["msg"]))
	{	
		$alert_type = 0;
		$alert      = $_GET["msg"];
	}
	else
	{
		$alert_type = -1;
		$alert = "";
	}	

	// Temp data
	// Nothing here	
	
	if(isset($_POST["search_blocking_submit"]))
	{
		$project_id  = $_POST["ddl_project"];		
		if(($role == 1) || ($role == 5) || ($role == 10))
		{
			$added_by    = $_POST["ddl_user"];		
		}
		else
		{
			$added_by = '';
		}
		$site_number     = $_POST["stxt_site_num"];
	}
	else
	{
		$project_id  = "";		
		$added_by    = "";
		$site_number = "";
	}

	$booking_list = i_get_site_booking('',$project_id,'','','','0',$added_by,'','','','','','','','','',$site_number,'','','','site_no');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Booking Approval Site List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Booking - Pending List</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="pending_booked_list" action="crm_pending_booking_list.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="-1">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project_id == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_site_num" value="<?php echo $site_number; ?>" placeholder="Site Number" />
			  </span>			  					
			  <?php if(($role == 1) || ($role == 5) || ($role == 10))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{

					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($added_by == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_blocking_submit" />
			  </span>
			  </form>			  
            </div>			
            <!-- /widget-header -->
            <div class="controls">
			<?php 			
			if($alert_type == 0) // Failure
			{
			?>
				<div class="alert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong><?php echo $alert; ?></strong>
				</div>  
			<?php
			}
			?>
			
			<?php 
			if($alert_type == 1) // Success
			{
			?>								
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong><?php echo $alert; ?></strong>
				</div>
			<?php
			}
			?>
			</div> <!-- /controls -->
			<div class="widget-content">			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Project</th>
					<th>Site No.</th>					
					<th>Booked By</th>
					<th>Booked On</th>
					<th>Enquiry No</th>
					<th>Client Name</th>
					<th>Amount</th>
					<th>Rate per sq. ft</th>
					<th>Master Rate</th>
					<th>Premium Value</th>
					<th>Remarks</th>
					<?php if(($role == 1) || ($role == 5))
					{
					?>
					<th>&nbsp;</th>										
					<?php
					}
					?>
				</tr>
				</thead>
				<tbody>							
				<?php						
				if($booking_list["status"] == SUCCESS)
				{										
					$sl_no = 0;					
					for($count = 0; $count < count($booking_list_data); $count++)
					{
						// Get site cost
						$cost_details = i_get_site_cost($booking_list_data[$count]["project_id"],'',$booking_list_data[$count]["crm_booking_added_on"],'','','');
						$site_cost = $cost_details["data"][0]["crm_cost_value"];	

						// Get premium related data
						$premium_details = i_get_site_premium($booking_list_data[$count]["project_id"],$booking_list_data[$count]["crm_booking_premium_type"],'','','');
						if($premium_details["status"] == SUCCESS)
						{
							$premium_value = $premium_details["data"][0]["crm_premium_value"];
						}
						else
						{
							$premium_value = 0;
						}
						
						// Get max. allowed discount
						$discount_details = i_get_site_discount_perms($booking_list_data[$count]["project_id"],$role,'','','');						
						$max_discount = $discount_details["data"][0]["crm_discount_master_value"];
						
						// Calculate max. allowed for approval by this user
						$max_discounted_value = ($site_cost + $premium_value) - $max_discount;	
					
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $booking_list_data[$count]["project_name"]; ?></td>
					<td><?php echo $booking_list_data[$count]["crm_site_no"]; ?></td>					
					<td><?php echo $booking_list_data[$count]["user_name"]; ?></td>										
					<td><?php echo date("d-M-Y",strtotime($booking_list_data[$count]["crm_booking_added_on"])); ?></td>	
					<td><?php echo $booking_list_data[$count]["enquiry_number"]; ?></td>
					<td><?php echo $booking_list_data[$count]["name"]; ?></td>		
					<td><?php echo $booking_list_data[$count]["crm_booking_initial_amount"]; ?></td>
					<td><?php echo $booking_list_data[$count]["crm_booking_rate_per_sq_ft"]; ?></td>					
					<td><?php echo $site_cost; ?></td>
					<td><?php echo $premium_value; ?></td>
					<td><?php echo $booking_list_data[$count]["crm_booking_remarks"]; ?></td>								
					<td><?php if(($booking_list_data[$count]["crm_booking_rate_per_sq_ft"] >= $max_discounted_value) && ($edit_perms_profile_list['status'] == SUCCESS))
					{					?>
					<a href="crm_booking_action.php?booking=<?php echo $booking_list_data[$count]["crm_booking_id"]; ?>&action=1">Approve</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="crm_booking_action.php?booking=<?php echo $booking_list_data[$count]["crm_booking_id"]; ?>&action=0">Reject</a>
					<?php
					}
					else
					{?>
					You don't have rights for this approval. Please contact the management Or You may not authorized to Update
					<?php
					}?></td>			
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="7">No site booked yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>
