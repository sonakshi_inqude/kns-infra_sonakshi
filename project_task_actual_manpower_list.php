<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
$edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
$delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
$ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
$approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');

    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

    $search_vendor = "";
    if (isset($_REQUEST["search_vendor"])) {
        $search_vendor = $_REQUEST["search_vendor"];
    }

    $start_date = "";
    if (isset($_REQUEST["dt_start_date"])) {
        $start_date = $_REQUEST["dt_start_date"];
    }

    $end_date = "";
    if (isset($_REQUEST["dt_end_date"])) {
        $end_date = $_REQUEST["dt_end_date"];
    }

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

    // Project Manpower Vendor Master List
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_vendor_master_list["status"] == SUCCESS) {
        $project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
    }

    //Get Location List
    $stock_location_master_search_data = array();
    $location_list = i_get_stock_location_master_list($stock_location_master_search_data);
    if ($location_list["status"] == SUCCESS) {
        $location_list_data = $location_list["data"];
    } else {
        $alert = $location_list["data"];
        $alert_type = 0;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1',"project_id"=>$search_project, "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
		<script data-jsfiddle="common" src="js/handsontable-master/demo/js/moment/moment.js"></script>
    <link rel="stylesheet" type="text/css" href="tools/datatables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="tools/datatables/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="datatable.css">

     <script src="tools/datatables/js/jquery.dataTables.min.js"></script>
		 <script src="datatable/project_actual_manpower.js"></script>

  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
    ?>

  <div class="main">
    <div class="main-inner">
      <div class="container">
        <div class="row">

            <div class="span6" style="width:100%;">

            <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Project Task Actual Man Power List</h3>
              </div>
              <!-- /widget-header -->

              <div class="widget-content">

								<?php
                                if ($view_perms_list['status'] == SUCCESS) {
                                    ?>
								<div class="widget-header" style="height:80px; padding-top:10px;">
								  <form method="post" id="file_search_form" action="project_task_actual_manpower_list.php">

								  <span style="padding-left:20px; padding-right:20px;">
								  <select name="search_project">
								  <option value="">- - Select Project - -</option>
								  <?php
                                  for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                                      ?>
								  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                          ?> selected="selected" <?php
                                      } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
								  <?php
                                  } ?>
								  </select>
								  </span>

								  <span style="padding-left:20px; padding-right:20px;">
								  <select name="search_vendor">
								  <option value="">- - Select Vendor - -</option>
								  <?php
                                  for ($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++) {
                                      ?>
								  <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]) {
                                          ?> selected="selected" <?php
                                      } ?>><?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
								  <?php
                                  } ?>
								  </select>
								  </span>



								  <span style="padding-left:20px; padding-right:20px;">
								  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
								  </span>

								  <span style="padding-left:20px; padding-right:20px;">
								  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
								  </span>

								  <input type="submit" name="manpower_search_submit" />

									<script>
									var formElements = [];
									formElements['search_project'] = <?=  json_encode($search_project); ?>;
									formElements['search_vendor'] = <?=  json_encode($search_vendor) ; ?>;
									formElements['start_date'] = <?=  json_encode($start_date) ; ?>;
									formElements['end_date'] = <?=  json_encode($end_date) ; ?>;

									setVars(formElements);
									</script>

								  </form>
					            </div>
								 <?php
                                } else {
                                    echo 'You are not authorized to view this page';
                                }
                                ?>
                                <?php
                                      if ($view_perms_list['status'] == SUCCESS) {
                                          ?>
                <table class="table table-bordered" cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="example" aria-describedby="example_info">
                  <thead>
                <tr style="background-color: #f2f2f2">
                <th style="word-wrap:break-word;">#</th>
                <th style="word-wrap:break-word;">Project</th>
                <th style="word-wrap:break-word;" >Process</th>
                <th style="word-wrap:break-word;" >Task Name</th>
                <th style="word-wrap:break-word;" >Road Name</th>
                <th style="word-wrap:break-word;" >Work Type</th>
                <th style="word-wrap:break-word;" >%</th>
                <th style="word-wrap:break-word;" >Msrmt</th>
                <th style="word-wrap:break-word;" >Date</th>
                <th style="word-wrap:break-word;" >Agency</th>
                <th style="word-wrap:break-word;" >Men HR </th>
                <th style="word-wrap:break-word;" >Women HR </th>
                <th style="word-wrap:break-word;" >Mason HR </th>
                <th style="word-wrap:break-word;" >No of People</th>
                <th style="word-wrap:break-word;" >Amount</th>
                <th style="word-wrap:break-word;" >Remarks</th>
                <th style="word-wrap:break-word;" >Added By</th>
                <th style="word-wrap:break-word;" >Added On</th>
                <th style="word-wrap:break-word;" >Action</th>
                <th style="word-wrap:break-word;" >Action</th>
                <th style="word-wrap:break-word;" >Action</th>
                </tr>
                </thead>
                  </tbody>
                </table>
                <?php
                   } else {
                       echo 'You are not authorized to view this page';
                   }
                   ?>
              </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
