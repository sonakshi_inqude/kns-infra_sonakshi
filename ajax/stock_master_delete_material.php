<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
 
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$material_id = $_POST["material_id"];
	$active   	 = $_POST["action"];

	$material_master_update_data = array("material_active"=>$active);
	$delete_material_result = i_delete_material_list($material_id,$material_master_update_data);
	
	if($delete_material_result["status"] == FAILURE)
	{
		echo $delete_material_result["data"];
	}	
	else
	{
		echo "SUCCESS";
		$stock_location_master_search_data = array();
		$location_result_list = i_get_stock_location_master_list($stock_location_master_search_data);
		if($location_result_list["status"] == SUCCESS)
		{
			for($location_count = 0 ; $location_count < count($location_result_list["data"]) ; $location_count++)
			{
				$material_stock_update_data = array("active"=>$active);
				$delete_material_stock = i_update_material_stock($material_id,$location_result_list["data"][$location_count]["stock_location_id"],$material_stock_update_data);
			}
		}
	}
}
else
{
	header("location:login.php");
}
?>