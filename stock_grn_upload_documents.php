<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('QUOTATION_FUNC_ID','218');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'2','1');	
	$add_perms_list    = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'1','1');
	$edit_perms_list   = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String Data
	if(isset($_REQUEST['grn_id']))
	{
		$grn_id = $_REQUEST['grn_id'];
	}
	else
	{
		$grn_id = '';
	}
	
	// Capture the form data
	if(isset($_POST["add_quotation_compare_submit"]))
	{		

		$grn_id      = $_POST["hd_grn_id"];
		$doc 		 = upload("file_remarks_doc",$user);
		$remarks     = $_POST["stxt_remarks"];
		
		$task_iresult = i_add_stock_grn_document($grn_id,$doc,$remarks,$user);
		if($task_iresult["status"] == SUCCESS)
		{
			$alert_type = 1;
			$alert      = "Document Successfully Added";
		}
		else
		{
			$alert_type = 0;
		}
		
	}	
	//Get Grn Documnet List
	$stock_grn_document_search_data = array("active"=>'1',"grn_id"=>$grn_id);
	$stock_grn_document_list =  i_get_stock_grn_document_list($stock_grn_document_search_data);
	if($stock_grn_document_list["status"] == SUCCESS)
	{
		$stock_grn_document_list_data = $stock_grn_document_list["data"];
	}
	else
	{
		//
	}
		
}
else
{
	header("location:login.php");
}	

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add GRN Documnets</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Upload GRN Documents</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add GRN Documents</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_quotation_compare_form" class="form-horizontal" method="post" action="stock_grn_upload_documents.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_grn_id" value="<?php echo $grn_id; ?>" />
									<fieldset>	

										<div class="control-group">											
											<label class="control-label" for="file_remarks_doc">Document</label>
											<div class="controls">
											<input type="file" name="file_remarks_doc" id="file_remarks_doc" />
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="stxt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                                                                                                                                                               										 <br />
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_quotation_compare_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div> 
							
							<div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>GRN No</th>
					<th>Document</th>
					<th>Added By</th>				
					<th>Added On</th>									
					<th colspan="4" style="text-align:center;">Actions</th>									
				</tr>
				</thead>
				<tbody>	
				<?php
					if($stock_grn_document_list["status"] == SUCCESS)
					{
					$sl_no = 0;
					for($count = 0; $count < count($stock_grn_document_list_data); $count++)
					{
						$sl_no++;

					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $stock_grn_document_list_data[$count]["stock_grn_document_grn_id"]; ?></td>
					<td><a href="documents/<?php echo $stock_grn_document_list_data[$count]["stock_grn_document_name"]; ?>" target="_blank"><?php echo $stock_grn_document_list_data[$count]["stock_grn_document_name"]; ?></a></td>
					<td><?php echo $stock_grn_document_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($stock_grn_document_list_data[$count][
					"stock_grn_document_added_on"])); ?></td>
					<td><?php if(($stock_grn_document_list_data[$count]["stock_grn_document_active"] == "1")){?><a href="#" onclick="return delete_grn_document('<?php echo $stock_grn_document_list_data[$count]["stock_grn_document_id"]; ?>','<?php echo $grn_id ;?>');">Delete</a><?php } ?></td>
					
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No GRN Document condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_grn_document(grn_document_id,grn_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_grn_upload_documents.php?grn_id=" +grn_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/stock_delete_grn_document.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("grn_document_id=" + grn_document_id + "&action=0");
		}
	}	
}
</script>

  </body>

</html>
