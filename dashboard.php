<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */$_SESSION['module'] = 'Legal Reports';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */

	/* Get file count for each process */
	// Get process type list
	$process_type_list = i_get_process_type_list('','');
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Process Level Dashboard</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>File List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>Process Type</th>
					<th>Number of files</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<tr>
				<td>No more process</td>
				<td><span id="no_process_count" value="completed_file_count";></span></td>
				<td><a href="consolidated_report.php?completed=1">See all</a></td>
				</tr>
				<?php
				$completed_file_count = 0;
				if($process_type_list["status"] == SUCCESS)
				{					
					for($count = 0; $count < count($process_type_list["data"]); $count++)
					{ 	
						if($process_type_list["data"][$count]["process_is_bulk"] == '0')
						{
							$file_list = i_get_file_list('','','','','','','','','','');							
							
							if($file_list["status"] == SUCCESS)
							{
								$process_count 		  = 0;			
								
								for($file_count = 0; $file_count < count($file_list["data"]); $file_count++)
								{								
									$file_handover_data  = i_get_file_handover_details($file_list["data"][$file_count]["file_id"]);
									$completed_file_data = i_get_completed_process_plan_list($file_list["data"][$file_count]["file_id"],'','1');
									
									if($file_handover_data["status"] != SUCCESS)
									{
										$process_list_present = i_get_legal_process_plan_list('',$file_list["data"][$file_count]["file_id"],'','','','','','','','','');
										if(($process_list_present['status'] == FAILURE) && ($count == 0))
										{									
											$completed_file_count++;
										}
										
										if($completed_file_data["status"] != SUCCESS)
										{
											if(($process_type_list["data"][$count]["process_is_bulk"] == '0') && ($file_list["data"][$file_count]["process_type_id"] == $process_type_list["data"][$count]["process_master_id"]))
											{
												$process_count++;
											}										
										}
										else
										{										
											// Check if the latest bulk process is complete
											$legal_bulk_search_data = array("file_no"=>$file_list["data"][$file_count]["file_number"]);
											$legal_bulk_result = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
											if($legal_bulk_result['status'] == SUCCESS)
											{
												// Check if all bulk processes status is complete
												$bproc_completed = true;
												for($bcount = 0; $bcount < count($legal_bulk_result['data']); $bcount++)
												{
													if($legal_bulk_result['data'][$bcount]['legal_bulk_process_completed'] != '1')
													{
														$bproc_completed = false;
													}
												}
												
												if($bproc_completed == true)											
												{
													if($count == 0)
													{
														$completed_file_count++;
													}
												}
											}
											else
											{
												if($count == 0)
												{
													$completed_file_count++;
												}	
											}											
										}
									}
								}																										
							}
							else if($file_list["status"] == FAILURE)
							{
								$process_count = 0;
							}
							else
							{
								$process_count = 0;
							}												
						?>
						<tr>
							<td><?php echo $process_type_list["data"][$count]["process_name"]; ?></td>
							<td><?php echo $process_count; ?></td>
							<td><a href="consolidated_report.php?process=<?php echo $process_type_list["data"][$count]["process_master_id"]; ?>">See all</a></td>
						</tr>
					<?php 
						}
						else
						{
							// Get list of bulk process
							$legal_bulk_search_data = array("legal_bulk_process_type"=>$process_type_list["data"][$count]["process_master_id"]);
							$bulk_process_data = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
							$process_count = 0;
							if($bulk_process_data['status'] == SUCCESS)
							{
								for($bpcount = 0; $bpcount < count($bulk_process_data['data']); $bpcount++)
								{
									if($bulk_process_data['data'][$bpcount]['legal_bulk_process_completed'] != '1')
									{
										$process_count++;
									}
								}
							}
							?>
							<tr>
							<td><?php echo $process_type_list["data"][$count]["process_name"]; ?></td>
							<td><?php echo $process_count; ?></td>
							<td><a href="bulk_consolidated_report.php?process=<?php echo $process_type_list["data"][$count]["process_master_id"]; ?>">See all</a></td>
						</tr>
							<?php
						}
					}										
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
 document.getElementById("no_process_count").innerHTML = "<?php echo $completed_file_count ;?>";
</script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
