<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_plan_process_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */


	// Query String Data
	// Nothing

	$search_project   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}

	$search_user   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_user   = $_POST["search_user"];
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}


	// Temp data
	$project_plan_process_search_data = array();
	$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
	if($project_plan_process_list["status"] == SUCCESS)
	{
		$project_plan_process_list_data = $project_plan_process_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_plan_process_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Plan Process List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Plan Process List</h3>&nbsp;&nbsp;&nbsp;Total Man Power: <span id="total_man_power">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Total Machine Plan:</strong> <span id="total_machine_planning">
            </div>
            <!-- /widget-header -->

			<!--<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_plan_process_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_management_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$process_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$process_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$process_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>-->

            <div class="widget-content">

              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project Name</th>
					<th>Process Name</th>
					<th>Method</th>
					<th>Plan Start Date</th>
					<th>Plan End Date</th>
					<th>Actual Start Date</th>
					<th>Actual End Date</th>
					<th>Material</th>
					<th>Man Power Type planned</th>
					<th>Man Power Type Actual</th>
					<th>Machine Type Planned</th>
					<th>Machine Type Actual</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_plan_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_plan_process_list_data); $count++)
					{
						$man_power = 0;
						$machine_planning = 0;
						$total_man_power = 0;
						$actual_man_power = 0;
						$task_indent_item = 0;
						$indent_material = 0;
						$total_machine_planning = 0;
						$total_actual_man_power = 0;
						$total_actual_machine_plan = 0;
						$sl_no++;

						$plan_process_start_date = $project_plan_process_list_data[$count]["project_plan_process_start_date"];
						$plan_process_end_date = $project_plan_process_list_data[$count]["project_plan_process_end_date"];


						//Get Project Process Task
						$project_process_task_search_data = array("process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"]);
						$project_process_task = i_get_project_process_task($project_process_task_search_data);
						if($project_process_task["status"] == SUCCESS)
						{
							for($process_task_count = 0 ; $process_task_count < count($project_process_task["data"]) ; $process_task_count++)
							{
								$project_man_power_estimate_search_data = array("task_id"=>$project_process_task["data"][$process_task_count]["project_process_task_id"]);
								$project_man_power_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
								if($project_man_power_list["status"] == SUCCESS)
								{
									$project_man_power_list_data = $project_man_power_list["data"];
									$no_of_man_hours = $project_man_power_list_data[0]["project_man_power_estimate_no_of_man_hours"];
									$cost_per_hours = $project_man_power_list_data[0]["project_man_power_estimate_cost_per_hours"];
									$man_power = $man_power + ($no_of_man_hours * $cost_per_hours) ;
									//$total_man_power = $total_man_power + $man_power;
								}
								else
								{
									$man_power = "";
								}

								//Get Project Process Machine Planning
								$project_machine_planning_search_data = array("task_id"=>$project_process_task["data"][$process_task_count]["project_process_task_id"]);
								$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
								if($project_machine_planning_list["status"] == SUCCESS)
								{
									$total_machine_planning = 0;
									$project_machine_planning_data = $project_machine_planning_list["data"];
									for($mp_planning = 0 ;$mp_planning < count($project_machine_planning_data) ; $mp_planning++)
									{
										$no_of_hrs = $project_machine_planning_data[$mp_planning]["project_machine_planning_no_of_hours"];
										$machine_rate = $project_machine_planning_data[$mp_planning]["project_machine_planning_machine_rate"];
										$additional_cost = $project_machine_planning_data[$mp_planning]["project_machine_planning_additional_cost"];
										$machine_planning =  $machine_planning + (($no_of_hrs * $machine_rate) + $additional_cost);
									}
								}
								else
								{

								}

								//Get Project Process Actual Man power
								$man_power_search_data = array("task_id"=>$project_process_task["data"][$process_task_count]["project_process_task_id"],"work_type"=>"Regular");
								$project_actual_man_power_list = i_get_man_power_list($man_power_search_data);
								if($project_actual_man_power_list["status"] == SUCCESS)
								{
									$total_actual_man_power = 0;
									$project_actual_man_power_list_data = $project_actual_man_power_list["data"];
									for($mp_count = 0 ;$mp_count < count($project_actual_man_power_list_data) ; $mp_count++)
									{
										$no_of_people = $project_actual_man_power_list_data[$mp_count]["project_task_actual_manpower_no_of_people"];
										$no_of_man_hrs = $project_actual_man_power_list_data[$mp_count]["project_task_actual_manpower_no_of_man_hours"];
										$actual_man_power =  ($no_of_people * $no_of_man_hrs);
										$total_actual_man_power = $total_actual_man_power + $actual_man_power;
									}
								}
								else
								{

								}

								//Get Project Process Task Indent
								$project_task_indent_search_data = array("task_id"=>$project_process_task["data"][$process_task_count]["project_process_task_id"]);
								$project_task_indent_list = i_get_project_task_indent($project_task_indent_search_data);
								if($project_task_indent_list["status"] == SUCCESS)
								{
									$indent_material = 0;
									$project_task_indent_list_data = $project_task_indent_list["data"];
									for($task_indent_count = 0 ;$task_indent_count < count($project_task_indent_list_data) ; $task_indent_count++)
									{
										$indent_item_qty = $project_task_indent_list_data[$task_indent_count]["stock_indent_item_quantity"];
										$material_id = $project_task_indent_list_data[$task_indent_count]["stock_indent_item_material_id"];
										//Get Material Master List
										$stock_material_search_data = array("material_id"=>$material_id);
										$material_list = i_get_stock_material_master_list($stock_material_search_data);
										{
											$material_price = $material_list["data"][0]["stock_material_price"];
										}
										$task_indent_item = ($indent_item_qty * $material_price);
										$indent_material = $indent_material + $task_indent_item;
									}

								}
								else
								{

								}
								//Get Project Process Actual Machine Plan
								$actual_machine_plan_search_data = array("task_id"=>$project_process_task["data"][$process_task_count]["project_process_task_id"]);
								$project_actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
								if($project_actual_machine_plan_list["status"] == SUCCESS)
								{
									$total_actual_machine_plan =0;
									for($actual_mp_count = 0 ;$actual_mp_count < count($project_task_indent_list_data) ; $actual_mp_count ++)
									{
										$project_actual_machine_plan_list_data = $project_actual_machine_plan_list["data"];
										$machine_id = $project_actual_machine_plan_list_data[$actual_mp_count]["project_task_machine_id"];
										$project_machine_rate_master_search_data =array("machine_id"=>$machine_id);
										$machine_rate_master_list = i_get_project_machine_rate_master($project_machine_rate_master_search_data);
										{
											$machine_rate = $machine_rate_master_list["data"][0]["project_machine_rate"];
										}
										$start_date = $project_actual_machine_plan_list_data[$actual_mp_count]["project_task_actual_machine_plan_start_date_time"];
										$end_date = $project_actual_machine_plan_list_data[$actual_mp_count]["project_task_actual_machine_plan_end_date_time"];
										$additional_cost = $project_actual_machine_plan_list_data[$actual_mp_count]["project_task_actual_machine_plan_additional_cost"];

										$cal_min = get_datetime_diff($end_date,$start_date,'HRS');
										$actual_machine_plan = ($cal_min * $machine_rate);
										$total_actual_machine_plan = $total_actual_machine_plan + $actual_machine_plan;
									}
								}
								else
								{

								}
							}
						}

						//Get project plan
						$project_plan_search_data = array("plan_id"=>$project_plan_process_list_data[$count]["project_plan_process_plan_id"]);
						$project_plan_list = i_get_project_plan($project_plan_search_data);
						if($project_plan_list["status"] == SUCCESS)
						{
							$project_plan_list_data = $project_plan_list["data"];
							$project_id = $project_plan_list_data[0]["project_plan_project_id"];
						}

						//Get project Master
						$project_management_master_search_data = array("project_id"=>$project_id, "user_id"=>$user);
						$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
						if($project_management_master_list["status"] == SUCCESS)
						{
							$project_management_master_list_data = $project_management_master_list["data"];
							$project_name = $project_management_master_list_data[0]["project_master_name"];
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_name; ?></td>
					<td><?php echo $project_plan_process_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_plan_process_list_data[$count]["project_plan_process_method"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($project_plan_process_list_data[$count]["project_plan_process_start_date"] != "0000-00-00")
					{ ?><?php echo date("d-M-Y",strtotime($project_plan_process_list_data[$count][
					"project_plan_process_start_date"])); ?><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($project_plan_process_list_data[$count]["project_plan_process_end_date"] != "0000-00-00")
					{ ?><?php echo date("d-M-Y",strtotime($project_plan_process_list_data[$count][
					"project_plan_process_end_date"])); ?><?php } ?></td>
					<td></td>
					<td></td>
					<td><?php echo $indent_material ;?></td>
					<td><?php echo $man_power ;?></td>
					<td><?php echo $total_actual_man_power ;?></td>
					<td><?php echo $machine_planning ;?></td>
					<td><?php echo $total_actual_machine_plan  ;?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>
				 <script>
				 document.getElementById('total_man_power').innerHTML = <?php echo $total_man_power; ?>;
				 </script>
				 <script>
				 document.getElementById('total_machine_planning').innerHTML = <?php echo $total_machine_planning; ?>;
				 </script>

                </tbody>
              </table>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=0");
		}
	}
}
function project_approve_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}
}

function project_reject_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_reject_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}
}

function go_to_project_edit_project_plan_process(process_id,plan_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_project_plan_process.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_process_task(project_plan_process_id,plan_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_plan_process_task_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_plan_process_id");
	hiddenField1.setAttribute("value",project_plan_process_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
