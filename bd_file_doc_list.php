<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_doc_list.php
CREATED ON	: 13-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD documents of a file
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else
	{
		$file_id = "";
	}
	// Nothing here

	// Temp data
	$alert = "";
	
	// Get list of docs for the file
	$bd_doc_list = i_get_bd_file_docs('',$file_id,'');
	if($bd_doc_list["status"] == SUCCESS)
	{
		$bd_doc_list_data = $bd_doc_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_doc_list["data"];
	}	
	
	// Get list of files
	$bd_file_list = i_get_bd_files_list($file_id,'','','','','','','');	
	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
	}		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD Document List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>BD Document List</h3>			  
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="padding-left:15px;">
              Project: <?php echo $bd_file_list_data[0]["bd_project_name"]; ?>&nbsp;&nbsp;&nbsp;Survey No: <?php echo $bd_file_list_data[0]["bd_file_survey_no"]; ?>&nbsp;&nbsp;&nbsp;Owner: <?php echo $bd_file_list_data[0]["bd_file_owner"]; ?>&nbsp;&nbsp;&nbsp;Village: <?php echo $bd_file_list_data[0]["village_name"]; ?>&nbsp;&nbsp;&nbsp;			  
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    
				    <th>SL No</th>
					<th>Title</th>
					<th>Document</th>
					<th>Remarks</th>	
					<th>Added By</th>														
				</tr>
				</thead>
				<tbody>
				 <?php
				if($bd_doc_list["status"] == SUCCESS)
				{				
					$sl_no = 0;
					for($count = 0; $count < count($bd_doc_list_data); $count++)
					{			
						$sl_no++;
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_doc_list_data[$count]["bd_file_document_title"]; ?></td>
						<td style="word-wrap:break-word;"><a href="bd_files/<?php echo $bd_doc_list_data[$count]["bd_file_document_path"]; ?>" target="_blank">DOWNLOAD</a></td>
						<td style="word-wrap:break-word;"><?php echo $bd_doc_list_data[$count]["bd_file_document_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $bd_doc_list_data[$count]["user_name"]; ?></td>						
					</tr>
					<?php 						
					}
				}
				else
				{
				?>
				<td colspan="5">No Files added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
