<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 01-April-2017
CREATED BY	: Lakshmi
PURPOSE     : List of Task Plans for a particular process ID
*/
/*
TBD:
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

/* DEFINES - START */
define('PROJECT_TASK_PLANNING_LIST_ID', '359');
/* DEFINES - END */
$_SESSION['module'] = 'Projectmgmnt';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $add_perms_list     = i_get_user_perms($user, '', PROJECT_TASK_PLANNING_LIST_ID, '1', '1');
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_TASK_PLANNING_LIST_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_TASK_PLANNING_LIST_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_TASK_PLANNING_LIST_ID, '4', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_PLANNING_LIST_ID, '6', '1');

    if ($approve_perms_list["status"] == SUCCESS) {
        $permission = "yes";
    } else {
        $permission = "no";
    }

    if (isset($_REQUEST['project_id'])) {
        $project_id = $_REQUEST['project_id'];
    } else {
        $project_id = "";
    }

    $result = array();
    $result['data'] = array();
    $result['data'][0] = array();
    $result['data'][1] = $permission;

    $start_date = "0000-00-00";
    $end_date 	= "0000-00-00";
    // Get list of task plans for this process plan
    $project_task_planning_search_data = array("active"=>'1',"project_id"=>$project_id);
    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);

    if ($project_task_planning_list["status"] == SUCCESS) {
        $project_task_planning_list_data = $project_task_planning_list["data"];
        for ($task_count = 0 ; $task_count < count($project_task_planning_list_data); $task_count++) {
            $planned_start_date = $project_task_planning_list_data[$task_count]["project_task_planning_start_date"];
            $planned_end_date = $project_task_planning_list_data[$task_count]["project_task_planning_end_date"];

            if (((strtotime($planned_start_date) < strtotime($start_date)) && ($planned_start_date != '0000-00-00')) || ($start_date == "0000-00-00")) {
                $start_date = $planned_start_date;
                $test = 0;
            } else {
                $start_date = $start_date;
                $test = 1;
            }

            if ((strtotime($planned_end_date) > strtotime($end_date)) || ($end_date == "0000-00-00")) {
                $end_date = $planned_end_date;
            } else {
                $end_date = $end_date;
            }


            if (($project_task_planning_list_data[$task_count]["project_task_planning_object_type"] == "CW")) {
                $object_name = $project_task_planning_list_data[$task_count]["project_cw_master_name"];
            } elseif ($project_task_planning_list_data[$task_count]["project_task_planning_object_type"] == "MC") {
                $object_name = $project_task_planning_list_data[$task_count]["project_machine_type_master_name"];
            } else {
                $object_name = "";
            }

            if (($project_task_planning_list_data[$task_count]["project_task_planning_no_of_roads"] != "No Roads")) {
                $road_name = $project_task_planning_list_data[$task_count]["project_site_location_mapping_master_name"];
            } elseif ($project_task_planning_list_data[$task_count]["project_task_planning_no_of_roads"] == "No Roads") {
                $road_name = "No Roads";
            }

            // Get Already added Object Output
            $project_task_master_search_data = array("active"=>'1',"task_master_id"=>$project_task_planning_list_data[$task_count]["project_task_master_id"]);
            $project_task_master_list = i_get_project_task_master($project_task_master_search_data);
            if ($project_task_master_list["status"] == SUCCESS) {
                $project_task_master_list_data = $project_task_master_list["data"];
                $uom = $project_task_master_list_data[0]["project_uom_name"];
            } else {
                $uom = "" ;
            }

            $result['data'][0][$task_count][0] = $project_task_planning_list_data[$task_count]["project_process_master_name"];
            $result['data'][0][$task_count][1] = $project_task_planning_list_data[$task_count]["project_task_master_name"];
            $result['data'][0][$task_count][2] = $project_task_planning_list_data[$task_count]["project_task_planning_task_id"];
            $result['data'][0][$task_count][3] = $uom;
            $result['data'][0][$task_count][4] = $project_task_planning_list_data[$task_count]["project_task_planning_measurment"];
            $result['data'][0][$task_count][5] = $road_name;
            $result['data'][0][$task_count][6] = $project_task_planning_list_data[$task_count]["project_task_planning_object_type"];
            $result['data'][0][$task_count][7] = $object_name;
            $result['data'][0][$task_count][8] = $project_task_planning_list_data[$task_count]["project_task_planning_no_of_object"];
            $result['data'][0][$task_count][9] = $project_task_planning_list_data[$task_count]["project_task_planning_per_day_out"];
            $result['data'][0][$task_count][10] = $project_task_planning_list_data[$task_count]["project_task_planning_total_days"];
            $result['data'][0][$task_count][11] = "";
            $result['data'][0][$task_count][12] = $project_task_planning_list_data[$task_count]["project_task_planning_start_date"];
            $result['data'][0][$task_count][13] = $project_task_planning_list_data[$task_count]["project_task_planning_end_date"];
            $result['data'][0][$task_count][14] = $project_task_planning_list_data[$task_count]["project_task_planning_id"];
            $result['data'][0][$task_count][15] = $project_task_planning_list_data[$task_count]["project_task_master_id"];
        }

        if ($start_date == "0000-00-00") {
            $responded_start_date = 'NO PLAN';
            $hd_start_date = "9999/99/99";
        } else {
            $responded_start_date = date("Y/m/d", strtotime($start_date));
            $hd_start_date = date("Y/m/d/", strtotime($start_date));
        }

        if ($end_date == "0000-00-00") {
            $responded_end_date = 'NO PLAN';
            $hd_end_date = "0000/00/00";
        } else {
            $responded_end_date = date("Y/m/d", strtotime($end_date));
            $hd_end_date = date("Y/m/d", strtotime($end_date));
        }
        $result['data'][2] = array("start_date"=>$responded_start_date,"end_date"=>$responded_end_date,"hd_start_date"=>$hd_start_date,"hd_end_date"=>$hd_end_date);
    } else {
        //
    }

    echo(json_encode($result));
} else {
    header("location:login.php");
}
