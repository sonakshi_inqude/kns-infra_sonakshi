<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert	

	// Query String
	if(isset($_REQUEST["plan_id"]))
	{
		$plan_id = $_REQUEST["plan_id"];		
	}	
	else
	{
		$plan_id = "";
	}

	// Get process details
	$project_plan_search_data = array("plan_id"=>$plan_id,"active"=>'1');
	$project_plan_master_list = i_get_project_plan($project_plan_search_data);
	if($project_plan_master_list['status'] == SUCCESS)
	{
		
			$project_plan_name  = $project_plan_master_list["data"][0]["project_master_name"];
			$plan_user  		= $project_plan_master_list["data"][0]["project_plan_user_id"];
	}		
	else
	{
			$project_plan_name  = "";
	}
	
	if(isset($_POST["add_process_submit"]))
	{
		// Capture all form data
		$process_id             = $_POST["hd_process_id"];
		$plan_id                = $_POST["hd_plan_id"];
		$plan_user              = $_POST["hd_plan_user_id"];
		$project_process_id   	= $_POST["hd_process_id"];
		//$project_process_id   = $_POST["hd_process_id"];
		
		$process_type_array = $_POST["cb_process_type"];
		for($process_type_count = 0; $process_type_count < count($process_type_array); $process_type_count++)
		{
			$process_type = $process_type_array[$process_type_count];
			// Add task
			$process_iresult = i_add_project_plan_process($plan_id,$process_type,'','','','',$user,'',$plan_user);
			if($process_iresult["status"] == SUCCESS)
			{
				header("location:project_plan_process_list.php?plan_id=$plan_id");
				$alert =  "Process Successfully Added";
				$alert_type = 1;
			}
		}
	}

	// Get list of process for this process type	
	$project_process_master_search_data = array("plan_id"=>$plan_id,"active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project:  &nbsp;  &nbsp;<?php echo $project_plan_name; ?>&nbsp;&nbsp;&nbsp;&nbsp</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Process to this Plan</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
							
								<div class="tab-pane active" id="formcontrols">
								<form id="add_process" class="form-horizontal" method="post" action="project_add_process.php">
								<!--<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $project_process_name; ?>" />-->
								<input type="hidden" name="hd_plan_id" value="<?php echo $plan_id; ?>" />
								
								<input type="hidden" name="hd_plan_user_id" value="<?php echo $plan_user; ?>" />
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="process_type">Choose Process</label>
											<div class="controls">
												<?php 
												for($count = 0; $count < count($project_process_master_list_data); $count++)
												{
											   // Get already added process for this project
												$project_plan_process_search_data = array("plan_id"=>$plan_id,"name"=>$project_process_master_list_data[$count]["project_process_master_id"],"active"=>'1');
												$project_process = i_get_project_plan_process($project_plan_process_search_data);
												if($project_process['status'] != SUCCESS)
												
												{?>
												<input type="checkbox" name="cb_process_type[]" value="<?php echo $project_process_master_list_data[$count]["project_process_master_id"]; ?>" />&nbsp;&nbsp;&nbsp;<?php echo $project_process_master_list_data[$count]["project_process_master_name"]; ?>&nbsp;&nbsp;&nbsp;<br /><br />
												<?php
												 }
												}?>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->                                                                                                                       <br />										
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="add_process_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
                                								
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
