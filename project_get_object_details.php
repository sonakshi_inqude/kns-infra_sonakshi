<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing
	
	if(isset($_REQUEST['object_name']))
	{
		$object_name = $_REQUEST['object_name'];
	}
	else
	{
		$object_name = "";
	}
	if(isset($_REQUEST['task_id']))
	{
		$task_id = $_REQUEST['task_id'];
	}
	else
	{
		$task_id = "";
	}
	
	if(isset($_REQUEST['object_type']))
	{
		$object_type = $_REQUEST['object_type'];
	}
	else
	{
		$object_type = "";
	}
	if($object_type == "MC")
	{
		// Machine Type List
		$project_machine_type_master_search_data = array("active"=>'1',"name"=>$object_name);
		$project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
		if($project_machine_type_master_list["status"] == SUCCESS)
		{
			
			$project_machine_type_master_list_data = $project_machine_type_master_list["data"];
			$machine_type_id = $project_machine_type_master_list_data[0]["project_machine_type_master_id"];
			$project_object_output_search_data = array("active"=>'1',"reference_id"=>$machine_type_id,"task_id"=>$task_id,"object_type"=>"MC");
			$project_object_output_master_list = i_get_project_object_output_task($project_object_output_search_data);
			if($project_object_output_master_list["status"] == SUCCESS)
			{
				$project_object_output_master_list_data = $project_object_output_master_list["data"];
				$result[0] = "";
				$result[1] = $project_object_output_master_list_data[0]["project_object_output_obejct_per_hr"];
			}
		}
		
	}
	else if($object_type == "CW")
	{
		// Get Project CW Master List
		$project_cw_master_search_data = array("active"=>'1',"name"=>$object_name);
		$project_cw_master_list = i_get_project_cw_master($project_cw_master_search_data);
		if($project_cw_master_list['status'] == SUCCESS)
		{
			$project_cw_master_list_data = $project_cw_master_list["data"];
			$cw_id = $project_cw_master_list_data[0]["project_cw_master_id"];
			$project_object_output_search_data = array("active"=>'1',"reference_id"=>$cw_id,"task_id"=>$task_id,"object_type"=>"CW");
			$project_object_output_master_list = i_get_project_object_output_task($project_object_output_search_data);
			if($project_object_output_master_list["status"] == SUCCESS)
			{
				$project_object_output_master_list_data = $project_object_output_master_list["data"];
				$result[0] = "";
				$result[1] = $project_object_output_master_list_data[0]["project_object_output_obejct_per_hr"];
			}
		}
	}
	echo json_encode($result);
}
else
{
	header("location:login.php");
}	