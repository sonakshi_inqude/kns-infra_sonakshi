<?php
/**
 * @author Nitin
 * @copyright 2016
 */
 
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

/*
PURPOSE : To add new attendance
INPUT 	: Date, Employee, Shift, In Time, Out Time, Work Duration, OT Duration, Total Duration, Type, Status, Added By
OUTPUT 	: Attendance ID, success or failure message
BY 		: Nitin
*/
function db_add_attendance($attendance_date,$attendance_employee,$shift,$in_time,$out_time,$work_duration,$ot_duration,$total_duration,$type,$attendance_entered_by)
{
	// Query
    $attendance_iquery = "insert into hr_attendance (hr_attendance_id,hr_attendance_date,hr_attendance_employee,hr_attendance_shift,hr_attendance_in_time,hr_attendance_out_time,hr_attendance_work_duration,hr_attendance_ot_duration,hr_attendance_total_duration,hr_attendance_type,hr_attendance_status,hr_attendance_entered_by,hr_attendance_date_time) values (:attendance_id,:attendance_date,:attendance_employee,:shift,:in_time,:out_time,:work_duration,:ot_duration,:total_duration,:type,:status,:attendance_entered_by,:attendance_date_time)";    
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $user_istatement = $dbConnection->prepare($attendance_iquery);
        
        // Data
		$attendance_id = generate_unique_id();
        $attendance_idata = array(':attendance_id'=>$attendance_id,':attendance_date'=>$attendance_date,':attendance_employee'=>$attendance_employee,':shift'=>$shift,':in_time'=>$in_time,':out_time'=>$out_time,':work_duration'=>$work_duration,':ot_duration'=>$ot_duration,':total_duration'=>$total_duration,':type'=>$type,':status'=>'1',':attendance_entered_by'=>$attendance_entered_by,':attendance_date_time'=>date("Y-m-d H:i:s"));
    	$dbConnection->beginTransaction();
        $user_istatement->execute($attendance_idata);
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $attendance_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;

}

/*
PURPOSE : To get attendance list
INPUT 	: Attendance Filter Data: Employee ID, Date, Start Date (filter), End Date (filter), Status
OUTPUT 	: Attendance List
BY 		: Nitin
*/
function db_get_attendance_list($attendance_filter_data)
{
	$get_attendance_list_squery_base = "select * from hr_attendance HA inner join hr_employee HE on HE.hr_employee_id = HA.hr_attendance_employee inner join hr_attendance_type_master HASM on HASM.hr_attendance_type_id = HA.hr_attendance_type inner join users U on U.user_id = HE.hr_employee_user";
	
	$get_attendance_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_attendance_list_sdata = array();
	
	if(array_key_exists("attendance_id",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_id=:attendance_id";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_id=:attendance_id";				
		}
		
		// Data
		$get_attendance_list_sdata[':attendance_id']  = $attendance_filter_data["attendance_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_id",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_employee=:employee_id";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_employee=:employee_id";				
		}
		
		// Data
		$get_attendance_list_sdata[':employee_id']  = $attendance_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("manager",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where U.user_manager=:manager";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and U.user_manager=:manager";				
		}
		
		// Data
		$get_attendance_list_sdata[':manager']  = $attendance_filter_data["manager"];
		
		$filter_count++;
	}
	
	if(array_key_exists("attendance_date",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_date = :attendance_date";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_date = :attendance_date";				
		}
		
		// Data
		$get_attendance_list_sdata[':attendance_date']  = $attendance_filter_data["attendance_date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("attendance_start_date",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_date >= :attendance_start_date";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_date >= :attendance_start_date";				
		}
		
		// Data
		$get_attendance_list_sdata[':attendance_start_date']  = $attendance_filter_data["attendance_start_date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("attendance_end_date",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_date <= :attendance_end_date";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_date <= :attendance_end_date";				
		}
		
		// Data
		$get_attendance_list_sdata[':attendance_end_date']  = $attendance_filter_data["attendance_end_date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("attendance_status",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_status = :attendance_status";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_status = :attendance_status";				
		}
		
		// Data
		$get_attendance_list_sdata[':attendance_status']  = $attendance_filter_data["attendance_status"];
		
		$filter_count++;
	}		
	
	if(array_key_exists("attendance_type",$attendance_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." where hr_attendance_type = :attendance_type";								
		}
		else
		{
			// Query
			$get_attendance_list_squery_where = $get_attendance_list_squery_where." and hr_attendance_type = :attendance_type";				
		}
		
		// Data
		$get_attendance_list_sdata[':attendance_type']  = $attendance_filter_data["attendance_type"];
		
		$filter_count++;
	}		
	$get_attendance_list_squery_order = " order by hr_attendance_date";
	$get_attendance_list_squery = $get_attendance_list_squery_base.$get_attendance_list_squery_where.$get_attendance_list_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_attendance_list_sstatement = $dbConnection->prepare($get_attendance_list_squery);
		
		$get_attendance_list_sstatement -> execute($get_attendance_list_sdata);
		
		$get_attendance_list_sdetails = $get_attendance_list_sstatement -> fetchAll();
		
		if(FALSE === $get_attendance_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_attendance_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_attendance_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get attendance type list
INPUT 	: Attendance Type Filter Data: Attendance Type ID, Active
OUTPUT 	: Attendance Type List
BY 		: Nitin
*/
function db_get_attendance_type_list($attendance_type_filter_data)
{
	$get_attendance_type_list_squery_base = "select * from hr_attendance_type_master";
	
	$get_attendance_type_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_attendance_type_list_sdata = array();
	
	if(array_key_exists("type_id",$attendance_type_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_type_list_squery_where = $get_attendance_type_list_squery_where." where hr_attendance_type_id=:type_id";								
		}
		else
		{
			// Query
			$get_attendance_type_list_squery_where = $get_attendance_type_list_squery_where." and hr_attendance_type_id=:type_id";				
		}
		
		// Data
		$get_attendance_type_list_sdata[':type_id']  = $attendance_type_filter_data["type_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("active",$attendance_type_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_type_list_squery_where = $get_attendance_type_list_squery_where." where hr_attendance_type_active = :active";								
		}
		else
		{
			// Query
			$get_attendance_type_list_squery_where = $get_attendance_type_list_squery_where." and hr_attendance_type_active = :active";				
		}
		
		// Data
		$get_attendance_type_list_sdata[':active']  = $attendance_type_filter_data["active"];
		
		$filter_count++;
	}

	if(array_key_exists("leave_type",$attendance_type_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_attendance_type_list_squery_where = $get_attendance_type_list_squery_where." where hr_attendance_type_is_leave = :is_leave";								
		}
		else
		{
			// Query
			$get_attendance_type_list_squery_where = $get_attendance_type_list_squery_where." and hr_attendance_type_is_leave = :is_leave";				
		}
		
		// Data
		$get_attendance_type_list_sdata[':is_leave']  = $attendance_type_filter_data["leave_type"];
		
		$filter_count++;
	}
	
	$get_attendance_type_list_squery = $get_attendance_type_list_squery_base.$get_attendance_type_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_attendance_type_list_sstatement = $dbConnection->prepare($get_attendance_type_list_squery);
		
		$get_attendance_type_list_sstatement -> execute($get_attendance_type_list_sdata);
		
		$get_attendance_type_list_sdetails = $get_attendance_type_list_sstatement -> fetchAll();
		
		if(FALSE === $get_attendance_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_attendance_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_attendance_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update attendance details  
INPUT 	: Attendance ID, Attendance Data Array
OUTPUT 	: Attendance ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_attendance_details($attendance_id,$attendance_details)
{
	// Query
    $attendance_uquery_base = "update hr_attendance set";  
	
	$attendance_uquery_set = "";
	
	$attendance_uquery_where = " where hr_attendance_id=:attendance_id";
	
	$attendance_udata = array(":attendance_id"=>$attendance_id);
	
	$filter_count = 0;
	
	if(array_key_exists("shift",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_shift=:shift,";
		$attendance_udata[":shift"] = $attendance_details["shift"];
		$filter_count++;
	}
	
	if(array_key_exists("in_time",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_in_time=:in_time,";
		$attendance_udata[":in_time"] = $attendance_details["in_time"];
		$filter_count++;
	}
	
	if(array_key_exists("out_time",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_out_time=:out_time,";
		$attendance_udata[":out_time"] = $attendance_details["out_time"];
		$filter_count++;
	}
	
	if(array_key_exists("work_duration",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_work_duration=:work_duration,";
		$attendance_udata[":work_duration"] = $attendance_details["work_duration"];
		$filter_count++;
	}
	
	if(array_key_exists("ot",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_ot_duration=:ot,";
		$attendance_udata[":ot"] = $attendance_details["ot"];
		$filter_count++;
	}
	
	if(array_key_exists("total_duration",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_total_duration=:total_duration,";
		$attendance_udata[":total_duration"] = $attendance_details["total_duration"];
		$filter_count++;
	}
	
	if(array_key_exists("type",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_type=:type,";
		$attendance_udata[":type"] = $attendance_details["type"];
		$filter_count++;
	}
	
	if(array_key_exists("status",$attendance_details))
	{
		$attendance_uquery_set = $attendance_uquery_set." hr_attendance_status=:status,";
		$attendance_udata[":status"] = $attendance_details["status"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$attendance_uquery_set = trim($attendance_uquery_set,',');
	}
	
	$attendance_uquery = $attendance_uquery_base.$attendance_uquery_set.$attendance_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $attendance_ustatement = $dbConnection->prepare($attendance_uquery);		
        
        $attendance_ustatement -> execute($attendance_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $attendance_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new out pass
INPUT 	: Employee, Date, Out Time, Finish Time, Remarks, Out Pass Type, Added By
OUTPUT 	: Out Pass ID, success or failure message
BY 		: Nitin
*/
function db_add_out_pass_request($employee_id,$date,$out_time,$finish_time,$remarks,$type,$added_by)
{
	// Query
    $out_pass_iquery = "insert into hr_out_pass_applications (hr_out_pass_employee,hr_out_pass_date,hr_out_pass_out_time,hr_out_pass_finish_time,hr_out_pass_request_remarks,hr_out_pass_type,hr_out_pass_approval_status,hr_out_pass_approved_by,hr_out_pass_approved_on,hr_out_pass_request_added_by,hr_out_pass_request_added_on) values (:employee,:date,:out_time,:finish_time,:remarks,:type,:status,:approved_by,:approved_on,:added_by,:now)";    
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $out_pass_istatement = $dbConnection->prepare($out_pass_iquery);
        
        // Data		
        $out_pass_idata = array(':employee'=>$employee_id,':date'=>$date,':out_time'=>$out_time,':finish_time'=>$finish_time,':remarks'=>$remarks,':type'=>$type,':status'=>'0',':approved_by'=>'',':approved_on'=>'',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
    	$dbConnection->beginTransaction();
        $out_pass_istatement->execute($out_pass_idata);
		$out_pass_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $out_pass_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get out pass list
INPUT 	: Out Pass Filter Data: Out Pass ID, Employee ID, Date, Type, Status, Manager
OUTPUT 	: Attendance List
BY 		: Nitin
*/
function db_get_out_pass_list($out_pass_filter_data)
{
	$get_out_pass_list_squery_base = "select *,MU.user_name as manager from hr_out_pass_applications HOPA inner join hr_employee HE on HE.hr_employee_id = HOPA.hr_out_pass_employee inner join users U on U.user_id = HE.hr_employee_user left outer join users MU on MU.user_id = HOPA.hr_out_pass_approved_by";
	
	$get_out_pass_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_out_pass_list_sdata = array();
	
	if(array_key_exists("out_pass_id",$out_pass_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." where hr_out_pass_id=:out_pass_id";								
		}
		else
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." and hr_out_pass_id=:out_pass_id";				
		}
		
		// Data
		$get_out_pass_list_sdata[':out_pass_id']  = $out_pass_filter_data["out_pass_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_id",$out_pass_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." where hr_out_pass_employee=:employee_id";								
		}
		else
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." and hr_out_pass_employee=:employee_id";				
		}
		
		// Data
		$get_out_pass_list_sdata[':employee_id']  = $out_pass_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("date",$out_pass_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." where hr_out_pass_date = :out_pass_date";								
		}
		else
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." and hr_out_pass_date = :out_pass_date";				
		}
		
		// Data
		$get_out_pass_list_sdata[':out_pass_date']  = $out_pass_filter_data["date"];
		
		$filter_count++;
	}
	
	if(array_key_exists("status",$out_pass_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." where hr_out_pass_approval_status = :out_pass_status";								
		}
		else
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." and hr_out_pass_approval_status = :out_pass_status";				
		}
		
		// Data
		$get_out_pass_list_sdata[':out_pass_status']  = $out_pass_filter_data["status"];
		
		$filter_count++;
	}		
	
	if(array_key_exists("type",$out_pass_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." where hr_out_pass_type = :out_pass_type";								
		}
		else
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." and hr_out_pass_type = :out_pass_type";				
		}
		
		// Data
		$get_out_pass_list_sdata[':out_pass_type']  = $out_pass_filter_data["type"];
		
		$filter_count++;
	}

	if(array_key_exists("manager",$out_pass_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." where U.user_manager = :manager";								
		}
		else
		{
			// Query
			$get_out_pass_list_squery_where = $get_out_pass_list_squery_where." and U.user_manager = :manager";				
		}
		
		// Data
		$get_out_pass_list_sdata[':manager']  = $out_pass_filter_data["manager"];
		
		$filter_count++;
	}
	
	$get_out_pass_list_squery = $get_out_pass_list_squery_base.$get_out_pass_list_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_out_pass_list_sstatement = $dbConnection->prepare($get_out_pass_list_squery);
		
		$get_out_pass_list_sstatement -> execute($get_out_pass_list_sdata);
		
		$get_out_pass_list_sdetails = $get_out_pass_list_sstatement -> fetchAll();
		
		if(FALSE === $get_out_pass_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_out_pass_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_out_pass_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update out pass details  
INPUT 	: Out Pass ID, Out Pass Data Array
OUTPUT 	: Out Pass ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_out_pass_details($out_pass_id,$out_pass_details)
{
	// Query
    $out_pass_uquery_base = "update hr_out_pass_applications set";  
	
	$out_pass_uquery_set = "";
	
	$out_pass_uquery_where = " where hr_out_pass_id=:out_pass_id";
	
	$out_pass_udata = array(":out_pass_id"=>$out_pass_id);
	
	$filter_count = 0;
	
	if(array_key_exists("status",$out_pass_details))
	{
		$out_pass_uquery_set = $out_pass_uquery_set." hr_out_pass_approval_status=:status,";
		$out_pass_udata[":status"] = $out_pass_details["status"];
		$filter_count++;
	}
	
	if(array_key_exists("approver",$out_pass_details))
	{
		$out_pass_uquery_set = $out_pass_uquery_set." hr_out_pass_approved_by=:approver,";
		$out_pass_udata[":approver"] = $out_pass_details["approver"];
		$filter_count++;
	}
	
	if(array_key_exists("approved_on",$out_pass_details))
	{
		$out_pass_uquery_set = $out_pass_uquery_set." hr_out_pass_approved_on=:approved_on,";
		$out_pass_udata[":approved_on"] = $out_pass_details["approved_on"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$out_pass_uquery_set = trim($out_pass_uquery_set,',');
	}
	
	$out_pass_uquery = $out_pass_uquery_base.$out_pass_uquery_set.$out_pass_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $out_pass_ustatement = $dbConnection->prepare($out_pass_uquery);		
        
        $out_pass_ustatement -> execute($out_pass_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $out_pass_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>