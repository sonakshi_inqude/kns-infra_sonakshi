<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new project
INPUT 	: Project Name, Location, Start Date, GFS Month, Launch Month, Development Time, Sales Time, File Path, Fund Source, Added By
OUTPUT 	: Bank id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_bd_project($project_name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$fund_source,$added_by)
{
	// Query
    $project_iquery = "insert into bd_projects_master (bd_project_id,bd_project_name,bd_project_location,bd_project_start_date,bd_project_gfs_month,bd_project_launch_date,bd_project_development_time,bd_project_sales_time,bd_project_file_path,bd_project_fund_source,bd_project_status,bd_project_added_by,bd_project_added_on) values (:project_id,:project_name,:location,:start_date,:gfs_month,:launch_month,:dev_time,:sale_time,:file_path,:fund_source,:status,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $project_istatement = $dbConnection->prepare($project_iquery);
        
        // Data
		$project_id = generate_unique_id();
        $project_idata = array(':project_id'=>$project_id,':project_name'=>$project_name,':location'=>$location,':start_date'=>$start_date,':gfs_month'=>$gfs_month,':launch_month'=>$launch_month,':dev_time'=>$dev_time,':sale_time'=>$sale_time,':fund_source'=>$fund_source,':file_path'=>$file_path,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $project_istatement->execute($project_idata);		
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get project list
INPUT 	: Project ID, Project Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of projects
BY 		: Nitin Kashyap
*/
function db_get_bd_project_list($project_id,$project_name,$location,$fund_source,$added_by,$start_date,$end_date,$status='')
{
	$get_project_list_squery_base = "select * from bd_projects_master BPM inner join bd_fund_source_master BFSM on BFSM.bd_fund_source_id = BPM.bd_project_fund_source";
	
	$get_project_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_project_list_sdata = array();
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_id=:project_id";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_id=:project_id";				
		}
		
		// Data
		$get_project_list_sdata[':project_id']  = $project_id;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_name like :project_name";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_name like :project_name";				
		}
		
		// Data
		$get_project_list_sdata[':project_name']  = '%'.$project_name.'%';
		
		$filter_count++;
	}
	
	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_location=:location";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_location=:location";				
		}
		
		// Data
		$get_project_list_sdata[':location']  = $location;
		
		$filter_count++;
	}
	
	if($fund_source != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_fund_source=:fund_source";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_fund_source=:fund_source";				
		}
		
		// Data
		$get_project_list_sdata[':fund_source']  = $fund_source;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_added_by=:added_by";				
		}
		
		// Data
		$get_project_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_added_on >= :start_date";				
		}
		
		//Data
		$get_project_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_added_on <= :end_date";				
		}
		
		//Data
		$get_project_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where bd_project_status = :status";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and bd_project_status = :status";				
		}
		
		//Data
		$get_project_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	$get_project_list_squery_order = " order by bd_project_name asc";
	$get_project_list_squery = $get_project_list_squery_base.$get_project_list_squery_where.$get_project_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_project_list_sstatement = $dbConnection->prepare($get_project_list_squery);
		
		$get_project_list_sstatement -> execute($get_project_list_sdata);
		
		$get_project_list_sdetails = $get_project_list_sstatement -> fetchAll();
		
		if(FALSE === $get_project_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add survey to a bd project
INPUT 	: Project, Survey No, Owner, Owner Address, Owner Phone No, Extent, Owner Status, Cost, Process Status, JD Share Percent, Own Account, Remarks, Added By
OUTPUT 	: Project Survey Mapping id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_survey_to_bd_project($project_id,$survey_no,$sale_deed,$sale_deed_date,$owner,$owner_address,$owner_phone_no,$village,$extent,$owner_status,$cost,$brokerage_amount,$broker_name,$broker_address,$process_status,$jd_share_percent,$own_account,$remarks,$added_by)
{
	// Query
    $survey_project_iquery = "insert into bd_project_files (bd_mapped_project_id,bd_file_survey_no,bd_file_survey_sale_deed,bd_file_survey_sale_deed_date,bd_file_owner,bd_file_owner_address,bd_file_owner_phone_no,bd_file_village,bd_file_extent,bd_file_owner_status,bd_file_land_cost,bd_file_brokerage_amount,bd_file_broker_name,bd_file_broker_address,bd_file_process_status,bd_project_file_jda_share_percent,bd_file_own_account,bd_project_file_remarks,bd_project_file_mapping_active,bd_project_file_mapping_added_by,bd_project_file_mapping_added_on) values (:project_id,:survey_no,:sale_deed,:sale_deed_date,:owner,:owner_address,:owner_phone,:village,:extent,:owner_status,:cost,:brokerage_amount,:broker_name,:broker_address,:process_status,:share_percent,:own_account,:remarks,:active,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_project_istatement = $dbConnection->prepare($survey_project_iquery);
        
        // Data
        $survey_project_idata = array(':project_id'=>$project_id,':survey_no'=>$survey_no,':sale_deed'=>$sale_deed,':sale_deed_date'=>$sale_deed_date,':owner'=>$owner,':owner_address'=>$owner_address,':owner_phone'=>$owner_phone_no,':village'=>$village,':extent'=>$extent,':owner_status'=>$owner_status,':cost'=>$cost,':brokerage_amount'=>$brokerage_amount,':broker_name'=>$broker_name,':broker_address'=>$broker_address,':process_status'=>$process_status,':share_percent'=>$jd_share_percent,':own_account'=>$own_account,':remarks'=>$remarks,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		
		$dbConnection->beginTransaction();
        $survey_project_istatement->execute($survey_project_idata);
		$survey_project_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_project_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get list of files mapped to a project
INPUT 	: File ID, Project ID, Survey no, Owner, Village, Owner Status, Process Status, Active, Added By, Start Date, End Date
OUTPUT 	: List of files
BY 		: Nitin Kashyap
*/
function db_get_mapped_survey_list($file_id,$project_id,$survey_no,$owner,$village,$owner_status,$process_status,$active,$added_by,$start_date,$end_date,$sale_deed="")
{
	$get_mapped_survey_list_squery_base = "select * from bd_project_files BPF inner join bd_projects_master BPM on BPM.bd_project_id = BPF.bd_mapped_project_id inner join village_master VM on VM.village_id = BPF.bd_file_village left outer join bd_file_owner_status FOS on FOS.bd_file_owner_status_id = BPF.bd_file_owner_status left outer join process_master PM on PM.process_master_id = BPF.bd_file_process_status inner join users U on U.user_id = BPF.bd_project_file_mapping_added_by left outer join bd_own_account_master BOAM on BOAM.bd_own_accunt_master_id = BPF.bd_file_own_account";
	
	$get_mapped_survey_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_mapped_survey_list_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_project_file_id=:file";								
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_project_file_id=:file";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':file']  = $file_id;
		
		$filter_count++;
	}
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_mapped_project_id=:project";								
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_mapped_project_id=:project";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':project']  = $project_id;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_file_survey_no=:survey_no";								
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_file_survey_no=:survey_no";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':survey_no']  = $survey_no;
		
		$filter_count++;
	}		if($sale_deed != "")	{		if($filter_count == 0)		{			// Query			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_file_survey_sale_deed=:sale_deed";										}		else		{			// Query			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_file_survey_sale_deed=:sale_deed";						}				// Data		$get_mapped_survey_list_sdata[':sale_deed']  = $sale_deed;				$filter_count++;	}
	
	if($owner != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_file_owner=:owner";								
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_file_owner=:owner";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':owner']  = $owner;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_file_village=:village";								
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_file_village=:village";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':village']  = $village;
		
		$filter_count++;
	}
	
	if($owner_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_file_owner_status=:owner_status";						
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_file_owner_status=:owner_status";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':owner_status']  = $owner_status;
		
		$filter_count++;
	}
	
	if($process_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_file_process_status=:process_status";				
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_file_process_status=:process_status";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':process_status']  = $process_status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_project_file_mapping_active=:active";					
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_project_file_mapping_active=:active";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_project_file_mapping_added_by=:added_by";				
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_project_file_mapping_added_by=:added_by";				
		}
		
		// Data
		$get_mapped_survey_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_project_file_mapping_added_on >= :start_date";			
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_project_file_mapping_added_on >= :start_date";				
		}
		
		//Data
		$get_mapped_survey_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." where bd_project_file_mapping_added_on <= :end_date";				
		}
		else
		{
			// Query
			$get_mapped_survey_list_squery_where = $get_mapped_survey_list_squery_where." and bd_project_file_mapping_added_on <= :end_date";				
		}
		
		//Data
		$get_mapped_survey_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}		
	
	$get_mapped_survey_list_squery = $get_mapped_survey_list_squery_base.$get_mapped_survey_list_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_mapped_survey_list_sstatement = $dbConnection->prepare($get_mapped_survey_list_squery);
		
		$get_mapped_survey_list_sstatement -> execute($get_mapped_survey_list_sdata);
		
		$get_mapped_survey_list_sdetails = $get_mapped_survey_list_sstatement -> fetchAll();
		
		if(FALSE === $get_mapped_survey_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_mapped_survey_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_mapped_survey_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add profitability calculation settings
INPUT 	: Project, Expected Rate, Stamp Duty, Development Cost, Admin Charges, Finance Rate, Added By
OUTPUT 	: Profitability Calculation id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_prof_calc_settings($project_id,$expected_rate,$stamp_duty,$dev_cost,$admin_charges,$finance_rate,$added_by)
{
	// Query
    $prof_calc_settings_iquery = "insert into bd_profitability_calculation_settings (bd_profitability_calc_setting_project,bd_profitability_calc_setting_expected_rate,bd_profitability_calc_setting_stamp_duty_charges,bd_profitability_calc_setting_dev_cost_rate,bd_profitability_calc_setting_admin_charges,bd_profitability_calc_setting_fianance_rate,bd_profitability_calc_setting_added_by,bd_profitability_calc_setting_added_on) values (:project_id,:expected_rate,:stamp_duty,:dev_cost,:admin_charges,:finance_rate,:added_by,:now)";  	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $prof_calc_settings_istatement = $dbConnection->prepare($prof_calc_settings_iquery);
        
        // Data
        $prof_calc_settings_idata = array(':project_id'=>$project_id,':expected_rate'=>$expected_rate,':stamp_duty'=>$stamp_duty,':dev_cost'=>$dev_cost,':admin_charges'=>$admin_charges,':finance_rate'=>$finance_rate,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		
		$dbConnection->beginTransaction();
        $prof_calc_settings_istatement->execute($prof_calc_settings_idata);
		$prof_calc_settings_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $prof_calc_settings_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get profitability calculation settings of a project
INPUT 	: Calculation ID, Project ID, Added By, Start Date, End Date
OUTPUT 	: List of calculation settings
BY 		: Nitin Kashyap
*/
function db_get_prof_calc_settings($calc_id,$project_id,$added_by,$start_date,$end_date)
{
	$get_prof_calc_list_squery_base = "select * from bd_profitability_calculation_settings BPCF inner join bd_projects_master BPM on BPM.bd_project_id = BPCF.bd_profitability_calc_setting_project inner join users U on U.user_id = BPCF.bd_profitability_calc_setting_added_by";
	
	$get_prof_calc_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_prof_calc_list_sdata = array();
	
	if($calc_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." where bd_profitability_calc_setting_id=:calc_id";						
		}
		else
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." and bd_profitability_calc_setting_id=:calc_id";				
		}
		
		// Data
		$get_prof_calc_list_sdata[':calc_id']  = $calc_id;
		
		$filter_count++;
	}
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." where bd_profitability_calc_setting_project=:project";					
		}
		else
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." and bd_profitability_calc_setting_project=:project";				
		}
		
		// Data
		$get_prof_calc_list_sdata[':project']  = $project_id;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." where bd_profitability_calc_setting_added_on >= :start_date";			
		}
		else
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." and bd_profitability_calc_setting_added_on >= :start_date";				
		}
		
		//Data
		$get_prof_calc_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." where bd_profitability_calc_setting_added_on <= :end_date";				
		}
		else
		{
			// Query
			$get_prof_calc_list_squery_where = $get_prof_calc_list_squery_where." and bd_profitability_calc_setting_added_on <= :end_date";				
		}
		
		//Data
		$get_prof_calc_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}		
	
	$get_prof_calc_list_squery = $get_prof_calc_list_squery_base.$get_prof_calc_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_prof_calc_list_sstatement = $dbConnection->prepare($get_prof_calc_list_squery);
		
		$get_prof_calc_list_sstatement -> execute($get_prof_calc_list_sdata);
		
		$get_prof_calc_list_sdetails = $get_prof_calc_list_sstatement -> fetchAll();
		
		if(FALSE === $get_prof_calc_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_prof_calc_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_prof_calc_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update BD file details
INPUT 	: File ID, Project ID, Survey No, Owner, Owner Address, Owner Phone, Village, Extent, Owner Status, Cost, Process Status, JD Share Percent, Own Account, Remarks, Active/Inactive
OUTPUT 	: File ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_bd_file($file_id,$project_id,$survey_no,$owner,$owner_address,$owner_phone,$village,$extent,$owner_status,$cost,$broker_name,$broker_address,$process_status,$jd_share_percent,$own_account,$remarks,$active,$brokerage_amount,$sale_deed,$sale_deed_date)
{
	// Query
    $bd_file_uquery_base = "update bd_project_files set";  
	
	$bd_file_uquery_set = "";
	
	$bd_file_uquery_where = " where bd_project_file_id=:file_id";
	
	$bd_file_udata = array(":file_id"=>$file_id);
	
	$filter_count = 0;
	
	if($project_id != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_mapped_project_id=:project_id,";
		$bd_file_udata[":project_id"] = $project_id;
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_survey_no=:survey_no,";
		$bd_file_udata[":survey_no"] = $survey_no;
		$filter_count++;
	}		if($sale_deed != "")	{		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_survey_sale_deed=:sale_deed,";		$bd_file_udata[":sale_deed"] = $sale_deed;		$filter_count++;	}		if($sale_deed_date != "")	{		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_survey_sale_deed_date=:sale_deed_date,";		$bd_file_udata[":sale_deed_date"] = $sale_deed_date;		$filter_count++;	}
	
	if($owner != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_owner=:owner,";
		$bd_file_udata[":owner"] = $owner;
		$filter_count++;
	}
	
	if($owner_address != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_owner_address=:owner_address,";
		$bd_file_udata[":owner_address"] = $owner_address;
		$filter_count++;
	}

	if($owner_phone != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_owner_phone_no=:owner_phone,";
		$bd_file_udata[":owner_phone"] = $owner_phone;
		$filter_count++;
	}
	
	if($village != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_village=:village,";
		$bd_file_udata["village"] = $village;
		$filter_count++;
	}
	
	if($extent != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_extent=:extent,";
		$bd_file_udata["extent"] = $extent;
		$filter_count++;
	}
	if($owner_status != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_owner_status=:owner_status,";
		$bd_file_udata["owner_status"] = $owner_status;
		$filter_count++;
	}
	
	if($cost != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_land_cost=:cost,";
		$bd_file_udata["cost"] = $cost;
		$filter_count++;
	}
	
	if($broker_name != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_broker_name=:broker_name,";
		$bd_file_udata["broker_name"] = $broker_name;
		$filter_count++;
	}
	
	if($broker_address != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_broker_address=:broker_address,";
		$bd_file_udata["broker_address"] = $broker_address;
		$filter_count++;
	}
	
	if($process_status != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_process_status=:process_status,";
		$bd_file_udata["process_status"] = $process_status;
		$filter_count++;
	}
	
	if($jd_share_percent != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_project_file_jda_share_percent=:jd_share_percent,";
		$bd_file_udata["jd_share_percent"] = $jd_share_percent;
		$filter_count++;
	}
	
	if($own_account != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_own_account=:own_account,";
		$bd_file_udata["own_account"] = $own_account;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_project_file_remarks=:remarks,";
		$bd_file_udata["remarks"] = $remarks;
		$filter_count++;
	}
	
	if($active != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_project_file_mapping_active=:active,";
		$bd_file_udata["active"] = $active;
		$filter_count++;
	}
	
	if($brokerage_amount != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_file_brokerage_amount=:brokerage_amount,";
		$bd_file_udata["brokerage_amount"] = $brokerage_amount;
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$bd_file_uquery_set = trim($bd_file_uquery_set,',');
	}
	
	$bd_file_uquery = $bd_file_uquery_base.$bd_file_uquery_set.$bd_file_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $bd_file_ustatement = $dbConnection->prepare($bd_file_uquery);		
        
        $bd_file_ustatement -> execute($bd_file_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update BD project details
INPUT 	: Project ID, Name, Location, Start Date, GFS Month, Launch Month, Dev Time, Sale Time, File Path, Source
OUTPUT 	: File ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_bd_project($project_id,$status,$name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$source)
{
	// Query
    $bd_project_uquery_base = "update bd_projects_master set";  
	
	$bd_project_uquery_set = "";
	
	$bd_project_uquery_where = " where bd_project_id=:project_id";
	
	$bd_project_udata = array(":project_id"=>$project_id);
	
	$filter_count = 0;
	
	if($status != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_status=:status,";
		$bd_project_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($name != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_name=:name,";
		$bd_project_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($location != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_location=:location,";
		$bd_project_udata[":location"] = $location;
		$filter_count++;
	}
	
	if($start_date != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_start_date=:start_date,";
		$bd_project_udata[":start_date"] = $start_date;
		$filter_count++;
	}
	
	if($gfs_month != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_gfs_month=:gfs_month,";
		$bd_project_udata[":gfs_month"] = $gfs_month;
		$filter_count++;
	}
	
	if($launch_month != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_launch_date=:launch_month,";
		$bd_project_udata["launch_month"] = $launch_month;
		$filter_count++;
	}
	
	if($dev_time != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_development_time=:dev_time,";
		$bd_project_udata["dev_time"] = $dev_time;
		$filter_count++;
	}
	
	if($sale_time != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_sales_time=:sale_time,";
		$bd_project_udata["sale_time"] = $sale_time;
		$filter_count++;
	}
	
	if($file_path != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_file_path=:file_path,";
		$bd_project_udata["file_path"] = $file_path;
		$filter_count++;
	}
	
	if($source != "")
	{
		$bd_project_uquery_set = $bd_project_uquery_set." bd_project_fund_source=:source,";
		$bd_project_udata["source"] = $source;
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$bd_project_uquery_set = trim($bd_project_uquery_set,',');
	}
	
	$bd_project_uquery = $bd_project_uquery_base.$bd_project_uquery_set.$bd_project_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $bd_project_ustatement = $dbConnection->prepare($bd_project_uquery);		
        
        $bd_project_ustatement -> execute($bd_project_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new file to a document
INPUT 	: File ID, Doc Title, Doc Path, Remarks, Added By
OUTPUT 	: Doc id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_bd_file_doc($file_id,$title,$path,$remarks,$added_by)
{
	// Query
    $file_doc_iquery = "insert into bd_file_documents (bd_file_document_file_id,bd_file_document_title,bd_file_document_path,bd_file_document_remarks,bd_file_document_added_by,bd_file_document_added_on) values (:file_id,:doc_title,:doc_path,:doc_remarks,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $file_doc_istatement = $dbConnection->prepare($file_doc_iquery);
        
        // Data		
        $file_doc_idata = array(':file_id'=>$file_id,':doc_title'=>$title,':doc_path'=>$path,':doc_remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $file_doc_istatement->execute($file_doc_idata);	
		$file_id = $dbConnection -> lastInsertId();		
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get docs 
INPUT 	: Doc ID, File ID, Added By, Start Date, End Date
OUTPUT 	: List of files
BY 		: Nitin Kashyap
*/
function db_get_file_docs($doc_id,$file_id,$added_by,$start_date,$end_date)
{
	$get_doc_list_squery_base = "select * from bd_file_documents BFD inner join users U on U.user_id = BFD.bd_file_document_added_by";
	
	$get_doc_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_doc_list_sdata = array();
	
	if($doc_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." where bd_file_document_id=:doc_id";						
		}
		else
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." and bd_file_document_id=:doc_id";				
		}
		
		// Data
		$get_doc_list_sdata[':doc_id']  = $doc_id;
		
		$filter_count++;
	}
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." where bd_file_document_file_id=:file_id";					
		}
		else
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." and bd_file_document_file_id=:file_id";				
		}
		
		// Data
		$get_doc_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." where bd_file_document_added_by=:added_by";					
		}
		else
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." and bd_file_document_added_by=:added_by";				
		}
		
		// Data
		$get_doc_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." where bd_file_document_added_on >= :start_date";			
		}
		else
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." and bd_file_document_added_on >= :start_date";				
		}
		
		//Data
		$get_doc_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." where bd_file_document_added_on <= :end_date";				
		}
		else
		{
			// Query
			$get_doc_list_squery_where = $get_doc_list_squery_where." and bd_file_document_added_on <= :end_date";				
		}
		
		//Data
		$get_doc_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}		
	
	$get_doc_list_squery = $get_doc_list_squery_base.$get_doc_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_doc_list_sstatement = $dbConnection->prepare($get_doc_list_squery);
		
		$get_doc_list_sstatement -> execute($get_doc_list_sdata);
		
		$get_doc_list_sdetails = $get_doc_list_sstatement -> fetchAll();
		
		if(FALSE === $get_doc_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_doc_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_doc_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add project edit history
INPUT 	: Project ID,Project Name, Location, Start Date, GFS Month, Launch Month, Development Time, Sales Time, Fund Source, Added By
OUTPUT 	: History id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_bd_project_history($project_id,$project_name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$fund_source,$added_by)
{
	// Query
    $project_history_iquery = "insert into bd_projects_history (bd_project_id_history,bd_project_name_history,bd_project_location_history,bd_project_start_date_history,bd_project_gfs_month_history,bd_project_launch_date_history,bd_project_development_time_history,bd_project_sales_time_history,bd_project_fund_source_history,bd_project_history_added_by,bd_project_history_added_on) values (:project_id,:project_name,:location,:start_date,:gfs_month,:launch_month,:dev_time,:sale_time,:fund_source,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $project_history_istatement = $dbConnection->prepare($project_history_iquery);
        
        // Data		
        $project_history_idata = array(':project_id'=>$project_id,':project_name'=>$project_name,':location'=>$location,':start_date'=>$start_date,':gfs_month'=>$gfs_month,':launch_month'=>$launch_month,':dev_time'=>$dev_time,':sale_time'=>$sale_time,':fund_source'=>$fund_source,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			
		$dbConnection->beginTransaction();
        $project_history_istatement->execute($project_history_idata);		
		$project_history_id = $dbConnection -> lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_history_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To ADD DELAY REASONS
INPUT 	: FILE ID, REASON MASTER ID, IS SUB TASK, REMARKS, ADDED BY
OUTPUT 	: REASON ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_bd_delay_reasons($delay_reasons_file_id,$delay_reason_id,$delay_is_task,$delay_remarks,$delay_added_by)
{
	// Query
    $delay_reasons_iquery = "insert into bd_delay_reasons (bd_delay_reasons_file_id,bd_delay_reason_id,bd_delay_reason_is_task,bd_delay_remarks,bd_delay_added_by,bd_delay_added_on) values (:delay_reasons_file_id,:delay_reason_id,:delay_is_task,:delay_remarks,:delay_added_by,:delay_added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $delay_reasons_istatement = $dbConnection->prepare($delay_reasons_iquery);
        
        // Data
        $delay_reasons_idata = array(':delay_reasons_file_id'=>$delay_reasons_file_id,':delay_reason_id'=>$delay_reason_id,':delay_is_task'=>$delay_is_task,':delay_remarks'=>$delay_remarks,':delay_added_by'=>$delay_added_by,':delay_added_on'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $delay_reasons_istatement->execute($delay_reasons_idata);
		$delay_reasons_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $delay_reasons_id;		
	}
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get REASONS
INPUT 	: Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Nitin Kashyap
*/
function db_get_bd_delay_reasons($reasons_search_data)
{
	// Extract all input parameters
	if(array_key_exists("delay_reasons_id",$reasons_search_data))
	{
		$delay_reasons_id = $reasons_search_data["delay_reasons_id"];
	}
	else
	{
		$delay_reasons_id = "";
	}
	
	if(array_key_exists("delay_reason_file_id",$reasons_search_data))
	{
		$delay_reason_file_id = $reasons_search_data["delay_reason_file_id"];
	}
	else
	{
		$delay_reason_file_id = "";
	}
	
	if(array_key_exists("delay_reason_id",$reasons_search_data))
	{
		$delay_reason_id = $reasons_search_data["delay_reason_id"];
	}
	else
	{
		$delay_reason_id = "";
	}
	
	if(array_key_exists("delay_added_by",$reasons_search_data))
	{
		$delay_added_by = $reasons_search_data["delay_added_by"];
	}
	else
	{
		$delay_added_by = "";
	}
	
		if(array_key_exists("delay_added_on",$reasons_search_data))
	{
		$delay_added_on = $reasons_search_data["delay_added_on"];
	}

	
	$get_reasons_squery_base = "select * from bd_delay_reasons BDR inner join bd_delay_reason_master BDRM on BDRM.bd_delay_reason_master_id = BDR.bd_delay_reason_id left outer join users U on U.user_id = BDR.bd_delay_added_by";
	
	$get_reasons_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_reasons_sdata = array();
	
	if($delay_reasons_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." where bd_delay_reasons_id = :delay_reasons_id";								
		}
		else
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." and bd_delay_reasons_id = :delay_reasons_id";				
		}
		
		// Data
		$get_reasons_sdata[':delay_reasons_id']  = $delay_reasons_id;
		
		$filter_count++;
	}
	
	if($delay_reason_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." where bd_delay_reason_id = :delay_reason_id";								
		}
		else
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." and bd_delay_reason_id = :delay_reason_id";				
		}
		
		// Data
		$get_reasons_sdata[':delay_reason_id']  = $delay_reason_id;
		
		$filter_count++;
	}
	
	if($delay_reason_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." where bd_delay_reasons_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." and bd_delay_reasons_file_id = :file_id";				
		}
		
		// Data
		$get_reasons_sdata[':file_id']  = $delay_reason_file_id;
		
		$filter_count++;
	}
	
	if($delay_added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." where bd_delay_added_by = :delay_added_by";								
		}
		else
		{
			// Query
			$get_reasons_squery_where = $get_reasons_squery_where." and bd_delay_added_by = :delay_added_by";				
		}
		
		// Data
		$get_reasons_sdata[':delay_added_by']  = $delay_added_by;
		
		$filter_count++;
	}
	
	$get_reasons_squery_order = ' order by bd_delay_added_on desc';
	$get_reasons_squery = $get_reasons_squery_base.$get_reasons_squery_where.$get_reasons_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reasons_sstatement = $dbConnection->prepare($get_reasons_squery);
		
		$get_reasons_sstatement -> execute($get_reasons_sdata);
		
		$get_reason_sdetails = $get_reasons_sstatement -> fetchAll();
		
		if(FALSE === $get_reason_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reason_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reason_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update financial workings of a project
INPUT 	: Project ID, Finance Working Data
OUTPUT 	: Project ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_prof_calc_settings($project_id,$finance_working_data)
{
	// Query
    $prof_calc_uquery_base = "update bd_profitability_calculation_settings set";  
	
	$prof_calc_uquery_set = "";
	
	$prof_calc_uquery_where = " where bd_profitability_calc_setting_project = :project";
	
	$prof_calc_udata = array(":project"=>$project_id);
	
	$filter_count = 0;
	
	if(array_key_exists('rate',$finance_working_data))
	{
		$prof_calc_uquery_set = $prof_calc_uquery_set." bd_profitability_calc_setting_expected_rate = :rate,";
		$prof_calc_udata[":rate"] = $finance_working_data['rate'];
		$filter_count++;
	}
	
	if(array_key_exists('stamp_duty',$finance_working_data))
	{
		$prof_calc_uquery_set = $prof_calc_uquery_set." bd_profitability_calc_setting_stamp_duty_charges = :stamp_duty,";
		$prof_calc_udata[":stamp_duty"] = $finance_working_data['stamp_duty'];
		$filter_count++;
	}
	
	if(array_key_exists('dev_cost',$finance_working_data))
	{
		$prof_calc_uquery_set = $prof_calc_uquery_set." bd_profitability_calc_setting_dev_cost_rate = :dev_cost,";
		$prof_calc_udata[":dev_cost"] = $finance_working_data['dev_cost'];
		$filter_count++;
	}
	
	if(array_key_exists('admin_charges',$finance_working_data))
	{
		$prof_calc_uquery_set = $prof_calc_uquery_set." bd_profitability_calc_setting_admin_charges = :admin_charges,";
		$prof_calc_udata[":admin_charges"] = $finance_working_data['admin_charges'];
		$filter_count++;
	}
	
	if(array_key_exists('finance_rate',$finance_working_data))
	{
		$prof_calc_uquery_set = $prof_calc_uquery_set." bd_profitability_calc_setting_fianance_rate = :finance_rate,";
		$prof_calc_udata[":finance_rate"] = $finance_working_data['finance_rate'];
		$filter_count++;
	}		
	
	if($filter_count > 0)
	{
		$prof_calc_uquery_set = trim($prof_calc_uquery_set,',');
	}
	
	$prof_calc_uquery = $prof_calc_uquery_base.$prof_calc_uquery_set.$prof_calc_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $prof_calc_ustatement = $dbConnection->prepare($prof_calc_uquery);		
        
        $prof_calc_ustatement -> execute($prof_calc_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Land Status
INPUT 	: File ID, Land Status, Date, Added By
OUTPUT 	: Land Status id, success or failure message
BY 		: Punith 
*/
function db_add_land_status($file_id,$land_status,$date,$added_by)
{
	// Query
    $land_type_iquery = "insert into bd_file_land_status (bd_file_land_status_file_id,bd_file_land_status,bd_file_land_status_date,bd_file_land_status_added_by,bd_file_land_status_added_on) values (:file_id,:land_status,:date,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $land_type_istatement = $dbConnection->prepare($land_type_iquery);
        
        // Data
        $land_type_idata = array(':file_id'=>$file_id,':land_status'=>$land_status,':date'=>$date,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $land_type_istatement->execute($land_type_idata);
		$land_status_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $land_status_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Land Status Details
INPUT 	: File ID, Land Status, Date, Added By, Start Date, End Date
OUTPUT 	: List of File Land Status
BY 		: Punith
*/
function db_get_land_status($file_id,$land_status,$date,$added_by,$start_date,$end_date)
{
	$get_land_status_squery_base = "select * from bd_file_land_status FLS left outer join bd_file_owner_status BFOS on BFOS.bd_file_owner_status_id = FLS.bd_file_land_status inner join users U on U.user_id = FLS.bd_file_land_status_added_by";
	
	$get_land_status_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_land_status_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." where bd_file_land_status_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." and bd_file_land_status_file_id = :file_id";				
		}
		
		// Data
		$get_land_status_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($land_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." where bd_file_land_status = :land_status";								
		}
		else
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." and bd_file_land_status = :land_status";				
		}
		
		// Data
		$get_land_status_sdata[':land_status']  = $land_status;
		
		$filter_count++;
	}
	
	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." where bd_file_land_status_date = :date";								
		}
		else
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." and bd_file_land_status_date = :date";				
		}
		
		// Data
		$get_land_status_sdata[':date']  = $date;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." where bd_file_land_status_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." and bd_file_land_status_added_by = :added_by";				
		}
		
		// Data
		$get_land_status_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." where bd_file_land_status_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." and bd_file_land_status_added_on >= :start_date";				
		}
		
		//Data
		$get_land_status_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." where bd_file_land_status_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_land_status_squery_where = $get_land_status_squery_where." and bd_file_land_status_added_on <= :end_date";				
		}
		
		//Data
		$get_land_status_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_land_status_squery_order = ' order by bd_file_land_status_added_on desc';
	$get_land_status_squery = $get_land_status_squery_base.$get_land_status_squery_where.$get_land_status_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_land_status_sstatement = $dbConnection->prepare($get_land_status_squery);
		
		$get_land_status_sstatement -> execute($get_land_status_sdata);
		
		$get_land_status_sdetails = $get_land_status_sstatement -> fetchAll();
		
		if(FALSE === $get_land_status_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_land_status_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_land_status_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update BD Delay Reason
INPUT 	: Delay Reason ID, Delay Reason Data
OUTPUT 	: Delay Reason ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_bd_delay_reason($delay_reason_id,$delay_reason_data)
{
	// Query
    $delay_reason_uquery_base = "update bd_delay_reasons set";  
	
	$delay_reason_uquery_set = "";
	
	$delay_reason_uquery_where = " where bd_delay_reasons_id = :delay_reason_id";
	
	$delay_reason_udata = array(":delay_reason_id"=>$delay_reason_id);
	
	$filter_count = 0;
	
	if(array_key_exists('task_action',$delay_reason_data))
	{
		$delay_reason_uquery_set = $delay_reason_uquery_set." bd_delay_reason_is_task = :action,";
		$delay_reason_udata[":action"] = $delay_reason_data['task_action'];
		$filter_count++;
	}		
	
	if($filter_count > 0)
	{
		$delay_reason_uquery_set = trim($delay_reason_uquery_set,',');
	}
	
	$delay_reason_uquery = $delay_reason_uquery_base.$delay_reason_uquery_set.$delay_reason_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $delay_reason_ustatement = $dbConnection->prepare($delay_reason_uquery);		
        
        $delay_reason_ustatement -> execute($delay_reason_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $delay_reason_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new borrow details
INPUT 	: Custody, File ID, Loan value, Relased Date, Added By
OUTPUT 	: borrow id, success or failure message
BY 		: Sonakshi D
*/
function db_add_bd_file_borrow_details($custody,$file_id,$bank,$mortgage,$recieved_loan,$name,$released_date,$remarks,$added_by)
{
	// Query
    $borrow_iquery = "insert into bd_file_borrow_details
	(bd_file_borrow_custody,bd_file_id,bd_file_borrow_bank,bd_file_borrow_mortgage,bd_file_borrow_recieved_loan_value,bd_file_borrower,
	bd_file_borrow_released_date,bd_file_borrow_active,bd_file_borrow_remarks,bd_file_borrow_added_by,bd_file_borrow_added_on) 
	values(:custody,:file_id,:bank,:mortgage,:recieved_loan,:name,:released_date,:active, :remarks,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $borrow_istatement = $dbConnection->prepare($borrow_iquery);
        
        // Data
        $borrow_idata = array(':custody'=>$custody,':file_id'=>$file_id,':bank'=>$bank,':mortgage'=>$mortgage,':recieved_loan'=>$recieved_loan,':name'=>$name,':released_date'=>$released_date,':remarks'=>$remarks,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $borrow_istatement->execute($borrow_idata);		
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $custody;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get borrow list
INPUT 	: Borrow ID, Custody, File ID, Bank, Active, Added By, Village, Added on
OUTPUT 	: List of borrow Details
BY 		: Sonakshi
*/
function db_get_bd_file_borrow_list($custody,$file_id,$bank,$mortgage,$loan_value,$released_date,$active,$added_by,$village,$added_on,$borrow_id='',$mort_start_date='',$mort_end_date='')
{
	$get_borrow_list_squery_base = "select * from bd_file_borrow_details BFBD inner join bd_project_files BPF on BPF.bd_project_file_id = BFBD.bd_file_id left outer join crm_bank_master CBM on BFBD.bd_file_borrow_bank = CBM.crm_bank_master_id left outer join village_master VM on VM.village_id = BPF.bd_file_village left outer join bd_own_account_master BOAM on BOAM.bd_own_accunt_master_id = BPF.bd_file_own_account";
	
	$get_borrow_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_borrow_list_sdata = array();
	
	if($custody != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_custody=:custody";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_custody=:custody";				
		}
		
		// Data
		$get_borrow_list_sdata[':custody']  = $custody;
		
		$filter_count++;
	}
	
	if($borrow_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_id = :borrow_id";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_id = :borrow_id";				
		}
		
		// Data
		$get_borrow_list_sdata[':borrow_id']  = $borrow_id;
		
		$filter_count++;
	}
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_id=:file_id";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_id=:file_id";				
		}
		
		// Data
		$get_borrow_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($bank != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_bank=:bank";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_bank=:bank";				
		}
		
		// Data
		$get_borrow_list_sdata[':bank']  = $bank;
		
		$filter_count++;
	}
	if($mortgage != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_mortgage=:mortgage";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_mortgage=:mortgage";				
		}
		
		// Data
		$get_borrow_list_sdata[':mortgage']  = $mortgage;
		
		$filter_count++;
	}
	
	
	if($loan_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_recieved_loan_value=:loan_value";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_recieved_loan_value=:loan_value";				
		}
		
		// Data
		$get_borrow_list_sdata[':loan_value']  = $loan_value;
		
		$filter_count++;
	}

	if($released_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_released_date=:released_date";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_released_date=:released_date";				
		}
		
		// Data
		$get_borrow_list_sdata[':released_date']  = $released_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_active=:active";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_active=:active";				
		}
		
		//Data
		$get_borrow_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where BPF.bd_file_village = :village";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and BPF.bd_file_village = :village";				
		}
		
		//Data
		$get_borrow_list_sdata[':village']  = $village;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_added_by=:added_by";				
		}
		
		// Data
		$get_borrow_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." where bd_file_borrow_added_on <= :added_on";								
		}
		else
		{
			// Query
			$get_borrow_list_squery_where = $get_borrow_list_squery_where." and bd_file_borrow_added_on <= :added_on";				
		}
		
		//Data
		$get_borrow_list_sdata[':added_on']  = $added_on;
		
		$filter_count++;
	}
	
	if($mort_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_file_borrow_mortgage >= :mort_start_date";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_file_borrow_mortgage >= :mort_start_date";				
		}
		
		//Data
		$get_bd_land_bank_list_sdata[':mort_start_date']  = $mort_start_date;
		
		$filter_count++;
	}

	if($mort_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_file_borrow_mortgage <= :mort_end_date";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_file_borrow_mortgage <= :mort_end_date";				
		}
		
		//Data
		$get_bd_land_bank_list_sdata['mort_end_date']  = $mort_end_date;
		
		$filter_count++;
	}
	
	$get_borrow_list_squery_order = " order by bd_file_borrow_added_on desc";
	$get_borrow_list_squery = $get_borrow_list_squery_base.$get_borrow_list_squery_where.$get_borrow_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_borrow_list_sstatement = $dbConnection->prepare($get_borrow_list_squery);
		
		$get_borrow_list_sstatement -> execute($get_borrow_list_sdata);
		
		$get_borrow_list_sdetails = $get_borrow_list_sstatement -> fetchAll();
		
		if(FALSE === $get_borrow_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_borrow_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_borrow_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update Borrow Files
INPUT 	: Borrow ID, Custody, File ID, Bank, Mortgage Date, Loan Value, Released Date, Active
OUTPUT 	: Borrow ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_bd_file_borrow($borrow_id,$custody,$file_id,$bank,$mortgage,$loan_value,$borrower,$released_date,$active,$remarks)
{
	$bd_file_borrow_uquery_base = "update bd_file_borrow_details set";  
	
	$bd_file_borrow_set = "";
	
	$bd_file_uquery_where = " where bd_file_borrow_id=:borrow_id";
	
	$bd_file_borrow_udata = array(":borrow_id"=>$borrow_id);
	
	$filter_count = 0;
	
	if($custody != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_custody=:custody,";
		$bd_file_borrow_udata[":custody"] = $custody;
		$filter_count++;
	}
	
	if($file_id != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_id=:file_id,";
		$bd_file_borrow_udata[":file_id"] = $file_id;
		$filter_count++;
	}
	
	if($bank != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_bank = :bank,";
		$bd_file_borrow_udata[":bank"] = $bank;
		$filter_count++;
	}
	
	if($mortgage != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_mortgage=:mortgage,";
		$bd_file_borrow_udata[":mortgage"] = $mortgage;
		$filter_count++;
	}
	
	if($loan_value != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_recieved_loan_value=:loan_value,";
		$bd_file_borrow_udata[":loan_value"] = $loan_value;
		$filter_count++;
	}		if($borrower != "")	{		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrower=:borrower,";		$bd_file_borrow_udata[":borrower"] = $borrower;		$filter_count++;	}

	if($released_date != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_released_date=:released_date,";
		$bd_file_borrow_udata[":released_date"] = $released_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_active=:active,";
		$bd_file_borrow_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$bd_file_borrow_set = $bd_file_borrow_set." bd_file_borrow_remarks=:remarks,";
		$bd_file_borrow_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	if($filter_count > 0)
	{
		$bd_file_borrow_set = trim($bd_file_borrow_set,',');
	}
	
	$bd_file_borrow_uquery = $bd_file_borrow_uquery_base.$bd_file_borrow_set.$bd_file_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $bd_file_borrow_ustatement = $dbConnection->prepare($bd_file_borrow_uquery);		
        
        $bd_file_borrow_ustatement -> execute($bd_file_borrow_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $borrow_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new BD Land Bank
INPUT 	: File ID, Remarks, Added By
OUTPUT 	: BD Land Bank ID, success or failure message
BY 		: Lakshmi
*/
function db_add_bd_land_bank($file_id,$remarks,$added_by)
{
	// Query
   $bd_land_bank_iquery = "insert into bd_land_bank
   (bd_land_bank_file_id,bd_land_bank_active,bd_land_bank_remarks,bd_land_bank_added_by,bd_land_bank_added_on) values(:file_id,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $bd_land_bank_istatement = $dbConnection->prepare($bd_land_bank_iquery);
        
        // Data
        $bd_land_bank_idata = array(':file_id'=>$file_id,':remarks'=>$remarks,':active'=>'1',':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $bd_land_bank_istatement->execute($bd_land_bank_idata);
		$bd_land_bank_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $bd_land_bank_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get BD Land Bank list
INPUT 	: BD Land Bank ID, File ID, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of BD Land Bank
BY 		: Lakshmi
*/
function db_get_bd_land_bank_list($bd_land_bank_search_data)
{  
	if(array_key_exists("bank_id",$bd_land_bank_search_data))
	{
		$bank_id = $bd_land_bank_search_data["bank_id"];
	}
	else
	{
		$bank_id= "";
	}
	
	if(array_key_exists("file_id",$bd_land_bank_search_data))
	{
		$file_id= $bd_land_bank_search_data["file_id"];
	}
	else
	{
		$file_id = "";
	}
	
	if(array_key_exists("active",$bd_land_bank_search_data))
	{
		$active= $bd_land_bank_search_data["active"];
	}
	else
	{
		$active= "";
	}
	
	if(array_key_exists("added_by",$bd_land_bank_search_data))
	{
		$added_by= $bd_land_bank_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}
	
	if(array_key_exists("start_date",$bd_land_bank_search_data))
	{
		$start_date= $bd_land_bank_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$bd_land_bank_search_data))
	{
		$end_date= $bd_land_bank_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	if(array_key_exists("survey_no",$bd_land_bank_search_data))
	{
		$survey_no = $bd_land_bank_search_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	if(array_key_exists("village",$bd_land_bank_search_data))
	{
		$village = $bd_land_bank_search_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("status",$bd_land_bank_search_data))
	{
		$status = $bd_land_bank_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("mort_start_date",$bd_land_bank_search_data))
	{
		$mort_start_date= $bd_land_bank_search_data["mort_start_date"];
	}
	else
	{
		$mort_start_date= "";
	}
	
	if(array_key_exists("mort_end_date",$bd_land_bank_search_data))
	{
		$mort_end_date= $bd_land_bank_search_data["mort_end_date"];
	}
	else
	{
		$mort_end_date= "";
	}
	
	if(array_key_exists("sort",$bd_land_bank_search_data))
	{
		$sort = $bd_land_bank_search_data["sort"];
	}
	else
	{
		$sort = "";
	}
	
	$get_bd_land_bank_list_squery_base = "select * from bd_land_bank BLB inner join bd_project_files BPF on BPF.bd_project_file_id = BLB.bd_land_bank_file_id inner join village_master VM on VM.village_id = BPF.bd_file_village left outer join bd_file_owner_status FOS on FOS.bd_file_owner_status_id = BPF.bd_file_owner_status left outer join bd_own_account_master BOAM on BOAM.bd_own_accunt_master_id = BPF.bd_file_own_account";
	
	$get_bd_land_bank_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_bd_land_bank_list_sdata = array();
	
	if($bank_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_land_bank_id = :bank_id";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_land_bank_id = :bank_id";				
		}
		
		// Data
		$get_bd_land_bank_list_sdata[':bank_id'] = $bank_id;
		
		$filter_count++;
	}
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_land_bank_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_land_bank_file_id = :file_id";				
		}
		
		// Data
		$get_bd_land_bank_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where BPF.bd_file_survey_no = :survey_no";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and BPF.bd_file_survey_no = :survey_no";				
		}
		
		// Data
		$get_bd_land_bank_list_sdata[':survey_no']  = $survey_no;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where BPF.bd_file_village = :village";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and BPF.bd_file_village = :village";				
		}
		
		// Data
		$get_bd_land_bank_list_sdata[':village']  = $village;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where BPF.bd_file_owner_status = :status";
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and BPF.bd_file_owner_status = :status";				
		}
		
		// Data
		$get_bd_land_bank_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_land_bank_active = :active";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_land_bank_active = :active";				
		}
		
		// Data
		$get_bd_land_bank_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_land_bank_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_land_bank_added_by = :added_by";				
		}
		
		//Data
		$get_bd_land_bank_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_land_bank_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_land_bank_added_on >= :start_date";				
		}
		
		//Data
		$get_bd_land_bank_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." where bd_land_bank_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_bd_land_bank_list_squery_where = $get_bd_land_bank_list_squery_where." and bd_land_bank_added_on <= :end_date";				
		}
		
		//Data
		$get_bd_land_bank_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($sort != "")
	{
		$get_bd_land_bank_list_squery_order = ' order by bd_land_bank_added_on desc';
	}
	else
	{
		$get_bd_land_bank_list_squery_order = '';
	}
	
	$get_bd_land_bank_list_squery = $get_bd_land_bank_list_squery_base.$get_bd_land_bank_list_squery_where.$get_bd_land_bank_list_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_bd_land_bank_list_sstatement = $dbConnection->prepare($get_bd_land_bank_list_squery);
		
		$get_bd_land_bank_list_sstatement -> execute($get_bd_land_bank_list_sdata);
		
		$get_bd_land_bank_list_sdetails = $get_bd_land_bank_list_sstatement -> fetchAll();
		
		if(FALSE === $get_bd_land_bank_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_bd_land_bank_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_bd_land_bank_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update BD Land Bank
INPUT 	: BD Land Bank ID, BD Land Bank Update Array
OUTPUT 	: BD Land Bank ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_bd_land_bank($bank_id,$bd_land_bank_update_data)
{
	if(array_key_exists("file_id",$bd_land_bank_update_data))
	{	
		$file_id = $bd_land_bank_update_data["file_id"];
	}
	else
	{
		$file_id = "";
	}
	
	if(array_key_exists("active",$bd_land_bank_update_data))
	{	
		$active = $bd_land_bank_update_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("remarks",$bd_land_bank_update_data))
	{	
		$remarks = $bd_land_bank_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$bd_land_bank_update_data))
	{	
		$added_by = $bd_land_bank_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$bd_land_bank_update_data))
	{	
		$added_on = $bd_land_bank_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $bd_land_bank_update_uquery_base = "update bd_land_bank set";  
	
	$bd_land_bank_update_uquery_set = "";
	
	$bd_land_bank_update_uquery_where = " where bd_land_bank_id = :bank_id";
	
	$bd_land_bank_update_udata = array(":bank_id"=>$bank_id);
	
	$filter_count = 0;
	
	if($file_id != "")
	{
		$bd_land_bank_update_uquery_set = $bd_land_bank_update_uquery_set." bd_land_bank_file_id = :file_id,";
		$bd_land_bank_update_udata[":file_id"] = $file_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$bd_land_bank_update_uquery_set = $bd_land_bank_update_uquery_set." bd_land_bank_active = :active,";
		$bd_land_bank_update_udata[":active"] = $active;		
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$bd_land_bank_update_uquery_set = $bd_land_bank_update_uquery_set." bd_land_bank_remarks = :remarks,";
		$bd_land_bank_update_udata[":remarks"] = $remarks;		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$bd_land_bank_update_uquery_set = $bd_land_bank_update_uquery_set." bd_land_bank_added_by = :added_by,";
		$bd_land_bank_update_udata[":added_by"] = $added_by;		
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$bd_land_bank_update_uquery_set = $bd_land_bank_update_uquery_set." bd_land_bank_added_on = :added_on,";
		$bd_land_bank_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$bd_land_bank_update_uquery_set = trim($bd_land_bank_update_uquery_set,',');
	}
	
	$bd_land_bank_update_uquery = $bd_land_bank_update_uquery_base.$bd_land_bank_update_uquery_set.$bd_land_bank_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $bd_land_bank_update_ustatement = $dbConnection->prepare($bd_land_bank_update_uquery);		
        
        $bd_land_bank_update_ustatement -> execute($bd_land_bank_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $bank_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>