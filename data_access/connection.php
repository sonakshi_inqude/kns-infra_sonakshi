<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

date_default_timezone_set("Asia/Kolkata");

function get_conn_handle()
{
	$db     = 'kns_erp_live';
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';                      

	try
	{
		$db_conn_handle = new PDO('mysql:host='.$dbhost.';dbname='.$db, $dbuser, $dbpass);
    }
    catch (PDOException $e)
	{
		$db_conn_handle = DB_CONN_FAILURE;
		$error = $e->getMessage();
	}

	return $db_conn_handle;
}
?>
