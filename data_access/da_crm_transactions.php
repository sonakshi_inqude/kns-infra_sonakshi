<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new enquiry
INPUT 	: Site ID, Customer Name, Mobile, Email, Company, Location, Source of Enquiry, Remarks, Interest Status, Follow Up Date, Assigned To, Added By
OUTPUT 	: Enquiry id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_enquiry($enq_no,$project_id,$name,$cell,$email,$company,$location,$source,$remarks,$interest_status,$walk_in,$follow_up_date,$assigned_to,$added_by)
{
	// Query
    $enquiry_iquery = "insert into crm_enquiry (enquiry_number,project_id,site_id,name,cell,email,company,location,source,remarks,interest_status,walk_in,follow_up_date,assigned_to,assigned_on,added_by,added_on) values (:enq_no,:project_id,:site_id,:name,:cell,:email,:company,:location,:source,:remarks,:status,:walk_in,:follow_up_date,:assigned_to,:assigned_on,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $enquiry_istatement = $dbConnection->prepare($enquiry_iquery);
        
        // Data
        $enquiry_idata = array(':enq_no'=>$enq_no,':project_id'=>$project_id,':site_id'=>'',':name'=>$name,':cell'=>$cell,':email'=>$email,':company'=>$company,':location'=>$location,':source'=>$source,':remarks'=>$remarks,':status'=>$interest_status,':walk_in'=>$walk_in,':follow_up_date'=>$follow_up_date,':assigned_to'=>$assigned_to,':assigned_on'=>date("Y-m-d H:i:s"),':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $enquiry_istatement->execute($enquiry_idata);
		$enquiry_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $enquiry_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get project list
INPUT 	: Enquiry ID, Enquiry No, Project ID, Site ID, Name, Cell, Email, Company, Source, Interest Status, Follow Up Date, Assigned To, Added By, Start Date, End date, Order, Number of Records required, Source Type, Project User Mapped User
OUTPUT 	: List of enquiries
BY 		: Nitin Kashyap
REMARKS : Email and Company are accepted as inputs. Processing to be done later.
*/
function db_get_enquiry_list($enq_id,$enq_number,$project,$site_id,$name,$cell,$email,$company,$source,$interest_status,$follow_up_date,$assigned_to,$added_by,$start_date,$end_date,$assigned_start_date,$assigned_end_date,$order,$limit_start,$limit_count,$source_type='',$project_user='')
{
	$get_enquiry_list_squery_base = "select * from crm_enquiry CE left outer join crm_project_master CPM on CPM.project_id = CE.project_id inner join crm_enquiry_source_master ESM on ESM.enquiry_source_master_id = CE.source inner join crm_customer_interest_status CIS on CIS.crm_cust_interest_status_id = CE.interest_status inner join users U on U.user_id = CE.assigned_to";
	
	$get_enquiry_list_squery_where = "";
	
	$get_enquiry_list_squery_order = "";
	
	$get_enquiry_list_squery_limit = "";
	
	$filter_count = 0;
	
	// Data
	$get_enquiry_list_sdata = array();
	
	if($enq_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where enquiry_id=:enquiry_id";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and enquiry_id=:enquiry_id";				
		}
		
		// Data
		$get_enquiry_list_sdata[':enquiry_id']  = $enq_id;
		
		$filter_count++;
	}
	
	if($enq_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where enquiry_number=:enq_number";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and enquiry_number=:enq_number";				
		}
		
		// Data
		$get_enquiry_list_sdata[':enq_number']  = $enq_number;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where CE.project_id=:project";
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and CE.project_id=:project";
		}
		// Data
		$get_enquiry_list_sdata[':project']  = $project;
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";										}		else		{			// Query			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";						}		// Data		$get_enquiry_list_sdata[':project_user_id']  = $project_user;		$filter_count++;	}
	
	if($site_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where site_id=:site_id";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and site_id=:site_id";				
		}
		
		// Data
		$get_enquiry_list_sdata[':site_id']  = $site_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where name=:name";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and name=:name";				
		}
		
		// Data
		$get_enquiry_list_sdata[':name']  = $name;
		
		$filter_count++;
	}
	
	if($cell != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where cell like :cell";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and cell like :cell";				
		}
		
		// Data
		$get_enquiry_list_sdata[':cell']  = '%'.$cell.'%';
		
		$filter_count++;
	}
	
	if($source != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where source=:source";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and source=:source";				
		}
		
		// Data
		$get_enquiry_list_sdata[':source']  = $source;
		
		$filter_count++;
	}
	
	if($source_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where ESM.enquiry_source_master_type = :source_type";
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and ESM.enquiry_source_master_type = :source_type";				
		}
		
		// Data
		$get_enquiry_list_sdata[':source_type']  = $source_type;
		
		$filter_count++;
	}
	
	if($interest_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where interest_status=:interest_status";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and interest_status=:interest_status";				
		}
		
		// Data
		$get_enquiry_list_sdata[':interest_status']  = $interest_status;
		
		$filter_count++;
	}
	
	if($follow_up_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where follow_up_date=:follow_up_date";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and follow_up_date=:follow_up_date";				
		}
		
		// Data
		$get_enquiry_list_sdata[':follow_up_date']  = $follow_up_date;
		
		$filter_count++;
	}
	
	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where assigned_to=:assigned_to";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and assigned_to=:assigned_to";				
		}
		
		// Data
		$get_enquiry_list_sdata[':assigned_to']  = $assigned_to;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where added_by=:added_by";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and added_by=:added_by";				
		}
		
		// Data
		$get_enquiry_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and added_on >= :start_date";				
		}
		
		//Data
		$get_enquiry_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and added_on <= :end_date";				
		}
		
		//Data
		$get_enquiry_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($assigned_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where assigned_on >= :assigned_start_date";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and assigned_on >= :assigned_start_date";				
		}
		
		//Data
		$get_enquiry_list_sdata[':assigned_start_date']  = $assigned_start_date;
		
		$filter_count++;
	}

	if($assigned_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." where assigned_on <= :assigned_end_date";								
		}
		else
		{
			// Query
			$get_enquiry_list_squery_where = $get_enquiry_list_squery_where." and assigned_on <= :assigned_end_date";				
		}
		
		//Data
		$get_enquiry_list_sdata[':assigned_end_date']  = $assigned_end_date;
		
		$filter_count++;
	}
	
	if($order == "entry_date_desc")
	{
		$get_enquiry_list_squery_order = " order by added_on desc";
	}
	else if($order == "assign_date_desc")
	{
		$get_enquiry_list_squery_order = " order by assigned_on desc";
	}
	else if($order != "")
	{
		$get_enquiry_list_squery_order = " order by enquiry_id ".$order;
	}
	
	if($limit_count != "")
	{	
		if($limit_start == "")
		{
			$limit_start = 0;
		}
		$get_enquiry_list_squery_limit = " limit ".$limit_start.",".$limit_count;
	}
	
	$get_enquiry_list_squery = $get_enquiry_list_squery_base.$get_enquiry_list_squery_where.$get_enquiry_list_squery_order.$get_enquiry_list_squery_limit;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_list_sstatement = $dbConnection->prepare($get_enquiry_list_squery);
		
		$get_enquiry_list_sstatement -> execute($get_enquiry_list_sdata);
		
		$get_enquiry_list_sdetails = $get_enquiry_list_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add enquiry follow up
INPUT 	: Enquiry ID, Remarks, Interest Status, Follow up Date, Added By
OUTPUT 	: Enquiry Follow Up id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_enquiry_follow_up($enquiry_id,$remarks,$interest_status,$follow_up_date,$added_by)
{
	// Query
    $enquiry_fup_iquery = "insert into crm_enquiry_follow_up (enquiry_id_for_follow_up,enquiry_follow_up_remarks,enquiry_follow_up_int_status,enquiry_follow_up_date,enquiry_follow_up_added_by,enquiry_follow_up_added_on) values (:enq_id,:remarks,:status,:follow_up_date,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $enquiry_fup_istatement = $dbConnection->prepare($enquiry_fup_iquery);
        
        // Data
        $enquiry_fup_idata = array(':enq_id'=>$enquiry_id,':remarks'=>$remarks,':status'=>$interest_status,':follow_up_date'=>$follow_up_date,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $enquiry_fup_istatement->execute($enquiry_fup_idata);
		$enquiry_fup_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $enquiry_fup_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get enquiry follow up list
INPUT 	: Enquiry ID, Follow Up Date, Added By, Relationship between assigner and assignee, Start Date, End date, Order, Number of Records required, Relationship between assigner and assignee, Follow Up Date, Follow Up Start, Project Mapped User
OUTPUT 	: List of enquiry follow up 
BY 		: Nitin Kashyap
*/
function db_get_enquiry_fup_list($enq_id,$follow_up_date,$added_by,$start_date,$end_date,$order,$limit_start,$limit_count,$relationship="",$fup_date_start="",$fup_date_end="",$project_user="")
{
	$get_enquiry_fup_list_squery_base = "select * from crm_enquiry_follow_up CIF inner join crm_enquiry CE on CE.enquiry_id = CIF.enquiry_id_for_follow_up inner join crm_project_master PM on PM.project_id = CE.project_id inner join crm_customer_interest_status CIS on CIS.crm_cust_interest_status_id = CIF.enquiry_follow_up_int_status inner join users U on U.user_id = CIF.enquiry_follow_up_added_by";
	
	$get_enquiry_fup_list_squery_where = "";
	
	$get_enquiry_fup_list_squery_order = "";
	
	$get_enquiry_fup_list_squery_limit = "";
	
	$filter_count = 0;
	
	// Data
	$get_enquiry_fup_list_sdata = array();
	
	if($enq_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_id_for_follow_up=:enquiry_id";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_id_for_follow_up=:enquiry_id";				
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':enquiry_id']  = $enq_id;
		
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";		}		else		{			// Query			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";						}				// Data		$get_enquiry_fup_list_sdata[':project_user_id']  = $project_user;		$filter_count++;	}
	if($follow_up_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_follow_up_date=:follow_up_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_follow_up_date=:follow_up_date";				
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':follow_up_date']  = $follow_up_date;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			switch($relationship)
			{
				case "assignee_or_assigner":
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where (enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;

				case "assignee_only":
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CE.assigned_to=:added_by";
				break;			
				
				case "assigner_only":
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_follow_up_added_by=:added_by";
				break;			
				
				default:
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where (enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;
			}
		}
		else
		{
			// Query
			switch($relationship)
			{
				case "assignee_or_assigner":
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and (enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;

				case "assignee_only":
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CE.assigned_to=:added_by";
				break;			
				
				case "assigner_only":
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_follow_up_added_by=:added_by";
				break;			
				
				default:
				$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and (enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;
			}		
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_follow_up_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_follow_up_added_on >= :start_date";				
		}
		
		//Data
		$get_enquiry_fup_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_follow_up_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_follow_up_added_on <= :end_date";				
		}
		
		//Data
		$get_enquiry_fup_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($fup_date_start != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_follow_up_date >= :fup_start_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_follow_up_date >= :fup_start_date";				
		}
		
		//Data
		$get_enquiry_fup_list_sdata[':fup_start_date']  = $fup_date_start;
		
		$filter_count++;
	}

	if($fup_date_end != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where enquiry_follow_up_date <= :fup_end_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and enquiry_follow_up_date <= :fup_end_date";				
		}
		
		//Data
		$get_enquiry_fup_list_sdata[':fup_end_date']  = $fup_date_end;
		
		$filter_count++;
	}
	
	if($order == "added_date_desc")
	{
		$get_enquiry_fup_list_squery_order = " order by enquiry_follow_up_added_on desc";
	}
	else if($order != "")
	{
		$get_enquiry_fup_list_squery_order = " order by enquiry_follow_up_date ".$order;
	}
	
	if($limit_count != "")
	{	
		if($limit_start == "")
		{
			$limit_start = 0;
		}
		$get_enquiry_fup_list_squery_limit = " limit ".$limit_start.",".$limit_count;
	}
	
	$get_enquiry_fup_list_squery = $get_enquiry_fup_list_squery_base.$get_enquiry_fup_list_squery_where.$get_enquiry_fup_list_squery_order.$get_enquiry_fup_list_squery_limit;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_fup_list_sstatement = $dbConnection->prepare($get_enquiry_fup_list_squery);
		
		$get_enquiry_fup_list_sstatement -> execute($get_enquiry_fup_list_sdata);
		
		$get_enquiry_fup_list_sdetails = $get_enquiry_fup_list_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_fup_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_fup_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_fup_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get enquiry follow up list
INPUT 	: Enquiry ID, Follow Up Date, Status, Status Operation, Assigned To, Added By, Start Date, End date, Order, Relationship, Follow Up Date - Start, Follow Up Date - End, Source, Project Mapped User, Status Filter Negation, Is Not Booked, Is Not unqualified, Start, Limit
OUTPUT 	: List of enquiry follow up 
BY 		: Nitin Kashyap
*/
function db_get_enquiry_fup_latest($enq_id,$follow_up_date,$status,$status_operation,$assigned_to,$added_by,$start_date,$end_date,$order,$relationship,$follow_up_date_start,$follow_up_date_end,$source='',$project_user='',$status_filter_out='',$is_booked='',$is_unqualified='',$start='',$limit='',$mobile='')
{
	$get_enquiry_fup_latest_squery_base = "select PT.*,CIS.*,CE.* from crm_enquiry_follow_up PT left outer join crm_enquiry_follow_up TT on (PT.enquiry_id_for_follow_up = TT.enquiry_id_for_follow_up and PT.enquiry_follow_up_date < TT.enquiry_follow_up_date) inner join crm_enquiry CE on CE.enquiry_id = PT.enquiry_id_for_follow_up inner join crm_customer_interest_status CIS on CIS.crm_cust_interest_status_id = PT.enquiry_follow_up_int_status where TT.enquiry_id_for_follow_up is NULL";
	
	$get_enquiry_fup_latest_squery_where = "";
	
	$get_enquiry_fup_latest_squery_order = "";	
	
	$filter_count = 1;
	
	// Data
	$get_enquiry_fup_latest_sdata = array();
	
	if($enq_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_id_for_follow_up=:enquiry_id";								
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_id_for_follow_up=:enquiry_id";				
		}
		
		// Data
		$get_enquiry_fup_latest_sdata[':enquiry_id']  = $enq_id;
		
		$filter_count++;
	}
	
	if($follow_up_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where date(PT.enquiry_follow_up_date)=:follow_up_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and date(PT.enquiry_follow_up_date)=:follow_up_date";				
		}
		
		// Data
		$get_enquiry_fup_latest_sdata[':follow_up_date']  = $follow_up_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			if($status_operation == "exclude")
			{
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_int_status!=:status";
			}
			else
			{
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_int_status=:status";
			}
		}
		else
		{
			// Query
			if($status_operation == "exclude")
			{
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_int_status!=:status";				
			}
			else			
			{
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_int_status=:status";
			}
		}
		
		// Data
		$get_enquiry_fup_latest_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where CE.assigned_to=:assigned_to";								
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and CE.assigned_to=:assigned_to";				
		}
		
		// Data
		$get_enquiry_fup_latest_sdata[':assigned_to']  = $assigned_to;
		
		$filter_count++;
	}	if($mobile != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where CE.cell=:mobile_no";										}		else		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and CE.cell=:mobile_no";						}				// Data		$get_enquiry_fup_latest_sdata[':mobile_no']  = $mobile;				$filter_count++;	}
	
	if($source != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where CE.source = :source";								
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and CE.source = :source";				
		}
		
		// Data
		$get_enquiry_fup_latest_sdata[':source']  = $source;
		
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";										}		else		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";						}				// Data		$get_enquiry_fup_latest_sdata[':project_user_id']  = $project_user;				$filter_count++;	}
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			switch($relationship)
			{
				case "assignee_or_assigner":
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where (PT.enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;

				case "assignee_only":
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where CE.assigned_to=:added_by";
				break;			
				
				case "assigner_only":
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_added_by=:added_by";
				break;			
				
				default:
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where (PT.enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;
			}
		}
		else
		{
			// Query
			switch($relationship)
			{
				case "assignee_or_assigner":
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and (PT.enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;

				case "assignee_only":
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and CE.assigned_to=:added_by";
				break;			
				
				case "assigner_only":
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_added_by=:added_by";
				break;			
				
				default:
				$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where (PT.enquiry_follow_up_added_by=:added_by or CE.assigned_to=:added_by)";
				break;
			}		
		}
		
		// Data
		$get_enquiry_fup_latest_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_added_on >= :start_date";				
		}
		
		//Data
		$get_enquiry_fup_latest_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_added_on <= :end_date";				
		}
		
		//Data
		$get_enquiry_fup_latest_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($follow_up_date_start != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_date >= :fup_start_date";					
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_date >= :fup_start_date";				
		}
		
		//Data
		$get_enquiry_fup_latest_sdata[':fup_start_date']  = $follow_up_date_start;
		
		$filter_count++;
	}
	
	if($follow_up_date_end != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_date <= :fup_end_date";					
		}
		else
		{
			// Query
			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_date <= :fup_end_date";				
		}
		
		//Data
		$get_enquiry_fup_latest_sdata[':fup_end_date']  = $follow_up_date_end;
		
		$filter_count++;
	}	if($status_filter_out != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_follow_up_int_status != :status_filter_out";		}		else		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_follow_up_int_status != :status_filter_out";		}				// Data		$get_enquiry_fup_latest_sdata[':status_filter_out'] = $status_filter_out;				$filter_count++;	}		if($is_booked != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_id_for_follow_up NOT IN (select CE.enquiry_id from crm_booking CB inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry where CB.crm_booking_status != 2 and CB.crm_booking_status != 3)";		}		else		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_id_for_follow_up NOT IN (select CE.enquiry_id from crm_booking CB inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry where CB.crm_booking_status != 2 and CB.crm_booking_status != 3)";		}								$filter_count++;	}		if($is_unqualified != "")	{		if($filter_count == 0)		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." where PT.enquiry_id_for_follow_up NOT IN (select UL.crm_unqualified_lead_enquiry_id from crm_unqualified_leads UL inner join crm_enquiry CE on CE.enquiry_id = UL.crm_unqualified_lead_enquiry_id where UL.crm_unqualified_lead_status = 1)";		}		else		{			// Query			$get_enquiry_fup_latest_squery_where = $get_enquiry_fup_latest_squery_where." and PT.enquiry_id_for_follow_up NOT IN (select UL.crm_unqualified_lead_enquiry_id from crm_unqualified_leads UL inner join crm_enquiry CE on CE.enquiry_id = UL.crm_unqualified_lead_enquiry_id where UL.crm_unqualified_lead_status = 1)";		}								$filter_count++;	}
	
	if($order != "")
	{
		$get_enquiry_fup_latest_squery_order = " order by enquiry_follow_up_date ".$order;
	}
	if($start != '')	{		$get_enquiry_fup_latest_squery_limit = " limit $start,$limit";	}
	
	$get_enquiry_fup_latest_squery = $get_enquiry_fup_latest_squery_base.$get_enquiry_fup_latest_squery_where.$get_enquiry_fup_latest_squery_order.$get_enquiry_fup_latest_squery_limit;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_fup_latest_sstatement = $dbConnection->prepare($get_enquiry_fup_latest_squery);
		
		$get_enquiry_fup_latest_sstatement -> execute($get_enquiry_fup_latest_sdata);
		
		$get_enquiry_fup_latest_sdetails = $get_enquiry_fup_latest_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_fup_latest_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_fup_latest_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_fup_latest_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update enquiry  
INPUT 	: Enquiry ID, User
OUTPUT 	: Enquiry id, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_enquiry_assignee($enquiry_id,$assigned_to)
{
	// Query
    $enquiry_uquery = "update crm_enquiry set assigned_to=:assigned_to,assigned_on=:assigned_on where enquiry_id=:enquiry_id";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $enquiry_ustatement = $dbConnection->prepare($enquiry_uquery);
        
        // Data
        $enquiry_udata = array(':assigned_to'=>$assigned_to,':assigned_on'=>date("Y-m-d H:i:s"),':enquiry_id'=>$enquiry_id);		
        
        $enquiry_ustatement -> execute($enquiry_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $enquiry_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new site visit plan
INPUT 	: Enquiry ID, Enquiry Date, Call Time, Project, Pickup Location, Added By
OUTPUT 	: Site Visit Plan id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_visit_plan($enquiry_id,$site_visit_date,$site_visit_time,$site_project,$confirm_status,$drive_type,$pickup_location,$added_by)
{
	// Query
    $site_visit_plan_iquery = "insert into crm_site_visit_plan (crm_site_visit_plan_enquiry,crm_site_visit_plan_date,crm_site_visit_plan_time,crm_site_visit_plan_project,crm_site_visit_plan_confirmation,crm_site_visit_plan_drive,crm_site_visit_plan_status,crm_site_visit_pickup_location,crm_site_visit_plan_added_by,crm_site_visit_plan_added_on) values (:enquiry_id,:date,:time,:project,:confirm,:drive,:status,:location,:added_by,:now)"; 	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_visit_plan_istatement = $dbConnection->prepare($site_visit_plan_iquery);
        
        // Data
        $site_visit_plan_idata = array(':enquiry_id'=>$enquiry_id,':date'=>$site_visit_date,':time'=>$site_visit_time,':project'=>$site_project,':confirm'=>$confirm_status,':drive'=>$drive_type,':status'=>'1',':location'=>$pickup_location,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $site_visit_plan_istatement->execute($site_visit_plan_idata);
		$site_visit_plan_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_visit_plan_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To update site visit plan status
INPUT 	: Enquiry ID, Status
OUTPUT 	: Enquiry ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_site_visit_plan($enquiry_id,$status)
{
	// Query
    $site_visit_plan_uquery = "update crm_site_visit_plan set crm_site_visit_plan_status = :status where crm_site_visit_plan_enquiry = :enquiry"; 	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_visit_plan_ustatement = $dbConnection->prepare($site_visit_plan_uquery);
        
        // Data
        $site_visit_plan_udata = array(':enquiry'=>$enquiry_id,':status'=>$status);
		$dbConnection->beginTransaction();
        $site_visit_plan_ustatement->execute($site_visit_plan_udata);		
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $enquiry_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get site visit plan list
INPUT 	: Enquiry ID, Date, Project, Status, Added By, Start Date, End date, Order in which entries have to be sorted (Default: Descending), Site Visit Plan ID, Plan Date End, Project Mapped User
OUTPUT 	: List of site visit plans
BY 		: Nitin Kashyap
*/
function db_get_site_visit_plan_list($enquiry,$date,$project,$status,$added_by,$start_date,$end_date,$order,$svp_id="",$project_user="")
{
	$get_site_visit_plan_list_squery_base = "select *,U.user_name as assigner,U1.user_name as assignee from crm_site_visit_plan CSVP inner join crm_enquiry CE on CE.enquiry_id = CSVP.crm_site_visit_plan_enquiry inner join crm_project_master PM on PM.project_id = CSVP.crm_site_visit_plan_project inner join users U on U.user_id = CSVP.crm_site_visit_plan_added_by inner join users U1 on U1.user_id = CE.assigned_to";
	
	$get_site_visit_plan_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_visit_plan_list_sdata = array();
	
	if($enquiry != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_enquiry=:enquiry_id";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_enquiry=:enquiry_id";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':enquiry_id']  = $enquiry;
		
		$filter_count++;
	}
	
	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_date=:date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_date=:date";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':date']  = $date;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_project=:project";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_project=:project";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':project']  = $project;
		
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_project IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";						}		else		{			// Query			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_project IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";						}		// Data		$get_site_visit_plan_list_sdata[':project_user_id']  = $project_user;		$filter_count++;	}
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_status=:status";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_status=:status";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_added_by=:added_by";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_added_on >= :start_date";				
		}
		
		//Data
		$get_site_visit_plan_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_added_on <= :end_date";				
		}
		
		//Data
		$get_site_visit_plan_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($svp_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_id = :svp_id";				
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_id = :svp_id";				
		}
		
		//Data
		$get_site_visit_plan_list_sdata[':svp_id']  = $svp_id;
		
		$filter_count++;
	}
	
	if($order == "asc")
	{
		$get_site_visit_plan_list_squery_order = " order by crm_site_visit_plan_added_on asc";
	}
	else
	{
		$get_site_visit_plan_list_squery_order = " order by crm_site_visit_plan_added_on desc";
	}	
	$get_site_visit_plan_list_squery = $get_site_visit_plan_list_squery_base.$get_site_visit_plan_list_squery_where.$get_site_visit_plan_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_visit_plan_list_sstatement = $dbConnection->prepare($get_site_visit_plan_list_squery);
		
		$get_site_visit_plan_list_sstatement -> execute($get_site_visit_plan_list_sdata);
		
		$get_site_visit_plan_list_sdetails = $get_site_visit_plan_list_sstatement -> fetchAll();
		
		if(FALSE === $get_site_visit_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_visit_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_visit_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get site visit plan list
INPUT 	: Enquiry ID, Plan Filter Start Date, Plan Filter End Date, Project, Added By, Start Date, End date
OUTPUT 	: Filtered List of site visits
BY 		: Nitin Kashyap
*/
function db_get_site_visit_plan_filtered_list($enquiry,$sv_plan_start_date,$sv_plan_end_date,$project,$added_by,$start_date,$end_date,$order)
{
	$get_site_visit_plan_list_squery_base = "select *,U.user_name as assigner,U1.user_name as assignee from crm_site_visit_plan CSVP inner join crm_enquiry CE on CE.enquiry_id = CSVP.crm_site_visit_plan_enquiry inner join crm_project_master PM on PM.project_id = CSVP.crm_site_visit_plan_project inner join users U on U.user_id = CSVP.crm_site_visit_plan_added_by inner join users U1 on U1.user_id = CE.assigned_to";
	
	$get_site_visit_plan_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_visit_plan_list_sdata = array();
	
	if($enquiry != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_enquiry=:enquiry_id";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_enquiry=:enquiry_id";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':enquiry_id']  = $enquiry;
		
		$filter_count++;
	}
	
	if($sv_plan_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_date >= :svp_start_date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_date >= :svp_start_date";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':svp_start_date']  = $sv_plan_start_date;
		
		$filter_count++;
	}
	
	if($sv_plan_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_date <= :svp_end_date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_date <= :svp_end_date";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':svp_end_date']  = $sv_plan_end_date;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_project=:project";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_project=:project";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':project']  = $project;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_added_by=:added_by";				
		}
		
		// Data
		$get_site_visit_plan_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_added_on >= :start_date";				
		}
		
		//Data
		$get_site_visit_plan_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." where crm_site_visit_plan_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_visit_plan_list_squery_where = $get_site_visit_plan_list_squery_where." and crm_site_visit_plan_added_on <= :end_date";				
		}
		
		//Data
		$get_site_visit_plan_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($order == "asc")
	{
		$get_site_visit_plan_list_squery_order = " order by crm_site_visit_plan_date asc";
	}
	else
	{
		$get_site_visit_plan_list_squery_order = " order by crm_site_visit_plan_date desc";
	}	
	$get_site_visit_plan_list_squery = $get_site_visit_plan_list_squery_base.$get_site_visit_plan_list_squery_where.$get_site_visit_plan_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_visit_plan_list_sstatement = $dbConnection->prepare($get_site_visit_plan_list_squery);
		
		$get_site_visit_plan_list_sstatement -> execute($get_site_visit_plan_list_sdata);
		
		$get_site_visit_plan_list_sdetails = $get_site_visit_plan_list_sstatement -> fetchAll();
		
		if(FALSE === $get_site_visit_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_visit_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_visit_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add an unqualified enquiry
INPUT 	: Enquiry ID, Remarks, Reason, Added By
OUTPUT 	: Unqualified Enquiry id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_unqualified_enquiry($enquiry_id,$remarks,$reason,$added_by)
{
	// Query
    $unq_enquiry_iquery = "insert into crm_unqualified_leads (crm_unqualified_lead_enquiry_id,crm_unqualified_lead_remarks,crm_unqualified_lead_reason,crm_unqualified_lead_status,crm_unqualified_lead_added_by,crm_unqualified_lead_added_on) values (:enquiry_id,:remarks,:reason,:status,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $unq_enquiry_istatement = $dbConnection->prepare($unq_enquiry_iquery);
        
        // Data
        $unq_enquiry_idata = array(':enquiry_id'=>$enquiry_id,':remarks'=>$remarks,':reason'=>$reason,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $unq_enquiry_istatement->execute($unq_enquiry_idata);
		$unq_enquiry_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $unq_enquiry_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get unqualified leads
INPUT 	: Unqualified Enquiry ID, Enquiry ID, Reason, Enquiry Added By, Added By, Start Date, End date, Enquiry No, Mobile No, Unqualified Status, STM
OUTPUT 	: List of unqualified enquiries
BY 		: Nitin Kashyap
*/
function db_get_unqualified_leads($unq_enq_id,$enquiry_id,$reason,$enquiry_added_by,$added_by,$start_date,$end_date,$enq_start_date,$enq_end_date,$enquiry_no='',$cell_no='',$status='1',$assigned_to='',$project_user='')
{
	$get_unq_enq_list_squery_base = "select * from crm_unqualified_leads UL inner join crm_enquiry CE on CE.enquiry_id = UL.crm_unqualified_lead_enquiry_id inner join crm_reason_master RM on RM.reason_id = UL.crm_unqualified_lead_reason inner join users U on U.user_id = UL.crm_unqualified_lead_added_by inner join crm_project_master PM on PM.project_id = CE.project_id inner join crm_enquiry_source_master ESM on ESM.enquiry_source_master_id = CE.source";
	
	$get_unq_enq_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_unq_enq_list_sdata = array();
	
	if($unq_enq_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_id=:unq_enq_id";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_id=:unq_enq_id";				
		}
		
		// Data
		$get_unq_enq_list_sdata[':unq_enq_id']  = $unq_enq_id;
		
		$filter_count++;
	}
	
	if($enquiry_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_enquiry_id=:enquiry_id";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_enquiry_id=:enquiry_id";				
		}
		
		// Data
		$get_unq_enq_list_sdata[':enquiry_id']  = $enquiry_id;
		
		$filter_count++;
	}
	
	if($reason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_reason=:reason";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_reason=:reason";				
		}
		
		// Data
		$get_unq_enq_list_sdata[':reason']  = $reason;
		
		$filter_count++;
	}
	
	if($enquiry_added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.added_by=:enq_added_by";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.added_by=:enq_added_by";				
		}
		
		// Data
		$get_unq_enq_list_sdata[':enq_added_by']  = $enquiry_added_by;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_added_by=:added_by";				
		}
		
		// Data
		$get_unq_enq_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_added_on >= :start_date";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_added_on <= :end_date";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($enq_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.added_on >= :enq_start_date";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.added_on >= :enq_start_date";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':enq_start_date']  = $enq_start_date;
		
		$filter_count++;
	}

	if($enq_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.added_on <= :enq_end_date";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.added_on <= :enq_end_date";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':enq_end_date']  = $enq_end_date;
		
		$filter_count++;
	}
	
	if($enquiry_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.enquiry_number = :enquiry_no";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.enquiry_number = :enquiry_no";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':enquiry_no']  = $enquiry_no;
		
		$filter_count++;
	}
	
	if($cell_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.cell = :cell_no";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.cell = :cell_no";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':cell_no']  = $cell_no;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where crm_unqualified_lead_status = :status";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and crm_unqualified_lead_status = :status";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.assigned_to = :stm";								
		}
		else
		{
			// Query
			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.assigned_to = :stm";				
		}
		
		//Data
		$get_unq_enq_list_sdata[':stm']  = $assigned_to;
		
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." where CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";		}		else		{			// Query			$get_unq_enq_list_squery_where = $get_unq_enq_list_squery_where." and CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";		}		// Data		$get_unq_enq_list_sdata[':project_user_id']  = $project_user;		$filter_count++;	}
	$get_unq_enq_list_squery = $get_unq_enq_list_squery_base.$get_unq_enq_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_unq_enq_list_sstatement = $dbConnection->prepare($get_unq_enq_list_squery);
		
		$get_unq_enq_list_sstatement -> execute($get_unq_enq_list_sdata);
		
		$get_unq_enq_list_sdetails = $get_unq_enq_list_sstatement -> fetchAll();
		
		if(FALSE === $get_unq_enq_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_unq_enq_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_unq_enq_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new propsective client profile
INPUT 	: Enquiry ID, Occupation, Other Income, No. of dependants, Annual Income, Loan Types, EMI, Rejected, Currently Living, Rent, Buying Interest, Funding Source, Loan Eligibility, Loan Tenure, Duration, Tentative Closure Date, Remarks, Added By
OUTPUT 	: Enquiry id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_prospective_client($enq_no,$occupation,$other_income,$num_dependents,$annual_income,$loan_types,$emi,$rejected,$current_living,$rent,$buying_interest,$funding_source,$loan_eligibility,$loan_tenure,$duration,$tentative_closure_date,$remarks,$added_by)
{
	// Query
    $prospective_iquery = "insert into crm_prospective_profile (crm_prospective_profile_enquiry,crm_prospective_profile_occupation,crm_prospective_profile_other_income,crm_prospective_profile_num_dependents,crm_prospective_profile_annual_income,crm_prospective_profile_loan_type,crm_prospective_profile_total_emi,crm_prospective_profile_loan_reject,crm_prospective_profile_current_living,crm_prospective_profile_rent,crm_prospective_profile_buying_interest,crm_prospective_profile_funding_source,crm_prospective_profile_loan_eligibility,crm_prospective_profile_loan_tenure,crm_prospective_profile_buy_duration,crm_prospective_profile_tentative_closure_date,crm_prospective_profile_remarks,crm_prospective_profile_added_by,crm_prospective_profile_added_on) values (:enq_no,:occupation,:other_income,:num_dependents,:annual_income,:loan_types,:emi,:rejected,:current_living,:rent,:buying_interest,:funding_source,:loan_eligibility,:loan_tenure,:duration,:tentative_closure,:remarks,:added_by,:now)";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $prospective_istatement = $dbConnection->prepare($prospective_iquery);
        
        // Data
        $prospective_idata = array(':enq_no'=>$enq_no,':occupation'=>$occupation,':other_income'=>$other_income,':num_dependents'=>$num_dependents,':annual_income'=>$annual_income,':loan_types'=>$loan_types,':emi'=>$emi,':rejected'=>$rejected,':current_living'=>$current_living,':rent'=>$rent,':buying_interest'=>$buying_interest,':funding_source'=>$funding_source,':loan_eligibility'=>$loan_eligibility,':loan_tenure'=>$loan_tenure,':duration'=>$duration,':tentative_closure'=>$tentative_closure_date,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			
		$dbConnection->beginTransaction();
        $prospective_istatement->execute($prospective_idata);
		$prospective_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $prospective_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get unqualified leads
INPUT 	: Profile ID, Enquiry ID, Added By, Start Date, End Date, Project Mapped User
OUTPUT 	: List of prospective clients
BY 		: Nitin Kashyap
*/
function db_get_prospective_client_list($profile_id,$enquiry_id,$added_by,$start_date,$end_date,$project_user='')
{
	$get_prospective_client_squery_base = "select *,U.user_name as stm,AU.user_name as added_by from crm_prospective_profile PP inner join crm_enquiry E on E.enquiry_id = PP.crm_prospective_profile_enquiry inner join crm_project_master PM on PM.project_id = E.project_id inner join crm_enquiry_source_master EM on enquiry_source_master_id = E.source inner join users U on U.user_id = E.assigned_to inner join users AU on AU.user_id = PP.crm_prospective_profile_added_by";
	
	$get_prospective_client_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_prospective_client_sdata = array();
	
	if($profile_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." where crm_prospective_profile_id=:profile_id";								
		}
		else
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." and crm_prospective_profile_id=:profile_id";				
		}
		
		// Data
		$get_prospective_client_sdata[':profile_id']  = $profile_id;
		
		$filter_count++;
	}
	
	if($enquiry_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." where crm_prospective_profile_enquiry=:enquiry_id";								
		}
		else
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." and crm_prospective_profile_enquiry=:enquiry_id";				
		}
		
		// Data
		$get_prospective_client_sdata[':enquiry_id']  = $enquiry_id;
		
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_prospective_client_squery_where = $get_prospective_client_squery_where." where E.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";		}		else		{			// Query			$get_prospective_client_squery_where = $get_prospective_client_squery_where." and E.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";		}		// Data		$get_prospective_client_sdata[':project_user_id']  = $project_user;		$filter_count++;	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." where crm_prospective_profile_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." and crm_prospective_profile_added_by=:added_by";				
		}
		
		// Data
		$get_prospective_client_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." where crm_prospective_profile_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." and crm_prospective_profile_added_on >= :start_date";				
		}
		
		//Data
		$get_prospective_client_sdata[':start_date']  = $start_date." 00:00:00";
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." where crm_prospective_profile_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_prospective_client_squery_where = $get_prospective_client_squery_where." and crm_prospective_profile_added_on <= :end_date";				
		}
		
		//Data
		$get_prospective_client_sdata[':end_date']  = $end_date." 23:59:59";
		
		$filter_count++;
	}
	
	$get_prospective_client_squery = $get_prospective_client_squery_base.$get_prospective_client_squery_where;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_prospective_client_sstatement = $dbConnection->prepare($get_prospective_client_squery);
		
		$get_prospective_client_sstatement -> execute($get_prospective_client_sdata);
		
		$get_prospective_client_sdetails = $get_prospective_client_sstatement -> fetchAll();
		
		if(FALSE === $get_prospective_client_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_prospective_client_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_prospective_client_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update enquiry  
INPUT 	: Enquiry ID, Name, Cell, Email, Company, Location, Source, Walk In
OUTPUT 	: Enquiry id, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_enquiry($enquiry_id,$name,$cell,$email,$company,$location,$source,$walk_in)
{
	// Query
    $enquiry_uquery = "update crm_enquiry set name=:name,cell=:cell,email=:email,company=:company,location=:location,source=:source,walk_in=:walk_in where enquiry_id=:enquiry_id";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $enquiry_ustatement = $dbConnection->prepare($enquiry_uquery);
        
        // Data
        $enquiry_udata = array(':name'=>$name,':cell'=>$cell,':email'=>$email,':company'=>$company,':location'=>$location,':source'=>$source,':walk_in'=>$walk_in,':enquiry_id'=>$enquiry_id);
        
        $enquiry_ustatement -> execute($enquiry_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $enquiry_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update unqualified enquiry  
INPUT 	: Unqualified Enquiry ID, Status
OUTPUT 	: Unqualified Enquiry id, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_unqualified_enquiry($unq_enquiry_id,$status)
{
	// Query
    $unq_enquiry_uquery = "update crm_unqualified_leads set crm_unqualified_lead_status = :status where crm_unqualified_lead_id = :unq_enq_id";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $unq_enquiry_ustatement = $dbConnection->prepare($unq_enquiry_uquery);
        
        // Data
        $unq_enquiry_udata = array(':status'=>$status,':unq_enq_id'=>$unq_enquiry_id);
        
        $unq_enquiry_ustatement -> execute($unq_enquiry_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $unq_enquiry_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update prospective client profile  
INPUT 	: Profile ID, Tentative Closure Date
OUTPUT 	: Profile id, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_prospective_profile($profile_id,$closure_date)
{
	// Query
    $pros_client_uquery = "update crm_prospective_profile set crm_prospective_profile_tentative_closure_date = :closure_date where crm_prospective_profile_id=:profile_id";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $pros_client_ustatement = $dbConnection->prepare($pros_client_uquery);
        
        // Data
        $pros_client_udata = array(':closure_date'=>$closure_date,':profile_id'=>$profile_id);		
        
        $pros_client_ustatement -> execute($pros_client_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $profile_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To get distinct enquiries
INPUT 	: Assigner, Assignee, Follow Up Date (start filter), Follow Up Date (end filter), Added on (start filter), Added on (end filter), Order, Limit Starting Point, Records
OUTPUT 	: List of distinct enquiry follow up 
BY 		: Nitin Kashyap
*/
function db_get_distinct_enquiries($added_by,$assigned_to,$follow_up_start_date,$follow_up_end_date,$start_date,$end_date,$order,$limit_start,$limit_count)
{
	$get_enquiry_fup_list_squery_base = "select distinct(enquiry_id_for_follow_up),CE.assigned_to,CE.assigned_on,CE.added_by,CE.added_on,CIF.enquiry_follow_up_date,CE.enquiry_id,CIF.enquiry_follow_up_int_status,CE.walk_in from crm_enquiry_follow_up CIF inner join crm_enquiry CE on CE.enquiry_id = CIF.enquiry_id_for_follow_up";
	
	$get_enquiry_fup_list_squery_where = "";
	
	$get_enquiry_fup_list_squery_order = "";
	
	$get_enquiry_fup_list_squery_limit = "";
	
	$filter_count = 0;
	
	// Data
	$get_enquiry_fup_list_sdata = array();
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CIF.enquiry_follow_up_added_by=:added_by";					
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CIF.enquiry_follow_up_added_by=:added_by";				
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CE.assigned_to=:assigned_to";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CE.assigned_to=:assigned_to";				
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':assigned_to']  = $assigned_to;
		
		$filter_count++;
	}
	
	if($follow_up_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query			
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CIF.enquiry_follow_up_date >= :fup_start_date";
		}
		else
		{
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CIF.enquiry_follow_up_date >= :fup_start_date";
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':fup_start_date']  = $follow_up_start_date;
		
		$filter_count++;
	}
	
	if($follow_up_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query			
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CIF.enquiry_follow_up_date <= :fup_end_date";
		}
		else
		{
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CIF.enquiry_follow_up_date <= :fup_end_date";
		}
		
		// Data
		$get_enquiry_fup_list_sdata[':fup_end_date']  = $follow_up_end_date;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CIF.enquiry_follow_up_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CIF.enquiry_follow_up_added_on >= :start_date";				
		}
		
		//Data
		$get_enquiry_fup_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." where CIF.enquiry_follow_up_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_enquiry_fup_list_squery_where = $get_enquiry_fup_list_squery_where." and CIF.enquiry_follow_up_added_on <= :end_date";				
		}
		
		//Data
		$get_enquiry_fup_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($order == "added_date_desc")
	{
		$get_enquiry_fup_list_squery_order = " order by enquiry_follow_up_added_on desc";
	}
	else if($order != "")
	{
		$get_enquiry_fup_list_squery_order = " order by enquiry_follow_up_date ".$order;
	}
	
	if($limit_count != "")
	{	
		if($limit_start == "")
		{
			$limit_start = 0;
		}
		$get_enquiry_fup_list_squery_limit = " limit ".$limit_start.",".$limit_count;
	}
	
	$get_enquiry_fup_list_squery = $get_enquiry_fup_list_squery_base.$get_enquiry_fup_list_squery_where.$get_enquiry_fup_list_squery_order.$get_enquiry_fup_list_squery_limit;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_fup_list_sstatement = $dbConnection->prepare($get_enquiry_fup_list_squery);
		
		$get_enquiry_fup_list_sstatement -> execute($get_enquiry_fup_list_sdata);
		
		$get_enquiry_fup_list_sdetails = $get_enquiry_fup_list_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_fup_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_fup_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_fup_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get the no of leads
INPUT 	: Enquiry Filter Array
OUTPUT 	: Count of enquiries
BY 		: Nitin Kashyap
*/
function db_get_enquiries_count($enquiry_filter)
{
	$enquiry_count_squery_base = "select count(*) as enq_count from crm_enquiry CE left outer join crm_project_master CPM on CPM.project_id = CE.project_id inner join crm_enquiry_source_master ESM on ESM.enquiry_source_master_id = CE.source inner join crm_customer_interest_status CIS on CIS.crm_cust_interest_status_id = CE.interest_status inner join users U on U.user_id = CE.assigned_to";
	
	$enquiry_count_squery_where = '';
	
	$enquiry_count_sdata = array();	
	if(array_key_exists('start_date',$enquiry_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$enquiry_count_squery_where = $enquiry_count_squery_where." where CE.added_on >= :start_date";								
		}
		else
		{
			// Query
			$enquiry_count_squery_where = $enquiry_count_squery_where." and CE.added_on >= :start_date";				
		}
		
		//Data
		$enquiry_count_sdata[':start_date']  = $enquiry_filter['start_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('end_date',$enquiry_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$enquiry_count_squery_where = $enquiry_count_squery_where." where CE.added_on <= :end_date";								
		}
		else
		{
			// Query
			$enquiry_count_squery_where = $enquiry_count_squery_where." and CE.added_on <= :end_date";				
		}
		
		//Data
		$enquiry_count_sdata[':end_date']  = $enquiry_filter['end_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('added_by',$enquiry_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$enquiry_count_squery_where = $enquiry_count_squery_where." where CE.added_by = :added_by";								
		}
		else
		{
			// Query
			$enquiry_count_squery_where = $enquiry_count_squery_where." and CE.added_by = :added_by";				
		}
		
		//Data
		$enquiry_count_sdata[':added_by']  = $enquiry_filter['added_by'];
		
		$filter_count++;
	}
	
	$enquiry_count_squery = $enquiry_count_squery_base.$enquiry_count_squery_where;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$enquiry_count_sstatement = $dbConnection->prepare($enquiry_count_squery);
		
		$enquiry_count_sstatement -> execute($enquiry_count_sdata);
		
		$get_enquiry_count_sdetails = $enquiry_count_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_count_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_count_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_count_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get the no of site visits
INPUT 	: Site Visit Filter Array
OUTPUT 	: Count of site visits
BY 		: Nitin Kashyap
*/
function db_get_sv_count($sv_filter)
{
	$sv_count_squery_base = "select count(*) as sv_count from crm_site_visit_plan CSVP inner join crm_enquiry CE on CE.enquiry_id = CSVP.crm_site_visit_plan_enquiry inner join crm_project_master PM on PM.project_id = CSVP.crm_site_visit_plan_project inner join users U on U.user_id = CSVP.crm_site_visit_plan_added_by inner join users U1 on U1.user_id = CE.assigned_to";
	
	$sv_count_squery_where = '';
	
	$sv_count_sdata = array();	
	if(array_key_exists('start_date',$sv_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$sv_count_squery_where = $sv_count_squery_where." where CSVP.crm_site_visit_plan_date >= :start_date";								
		}
		else
		{
			// Query
			$sv_count_squery_where = $sv_count_squery_where." and CSVP.crm_site_visit_plan_date >= :start_date";				
		}
		
		//Data
		$sv_count_sdata[':start_date']  = $sv_filter['start_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('end_date',$sv_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$sv_count_squery_where = $sv_count_squery_where." where CSVP.crm_site_visit_plan_date <= :end_date";								
		}
		else
		{
			// Query
			$sv_count_squery_where = $sv_count_squery_where." and CSVP.crm_site_visit_plan_date <= :end_date";				
		}
		
		//Data
		$sv_count_sdata[':end_date']  = $sv_filter['end_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('added_by',$sv_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$sv_count_squery_where = $sv_count_squery_where." where CSVP.crm_site_visit_plan_added_by = :added_by";								
		}
		else
		{
			// Query
			$sv_count_squery_where = $sv_count_squery_where." and CSVP.crm_site_visit_plan_added_by = :added_by";				
		}
		
		//Data
		$sv_count_sdata[':added_by']  = $sv_filter['added_by'];
		
		$filter_count++;
	}
	
	$sv_count_squery = $sv_count_squery_base.$sv_count_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$sv_count_sstatement = $dbConnection->prepare($sv_count_squery);
		
		$sv_count_sstatement -> execute($sv_count_sdata);
		
		$sv_count_sdetails = $sv_count_sstatement -> fetchAll();
		
		if(FALSE === $sv_count_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($sv_count_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $sv_count_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get enquiry follow up list
INPUT 	: Enquiry ID, Follow Up Date, Status, Status Operation, Assigned To, Added By, Start Date, End date, Order, Relationship, Follow Up Date - Start, Follow Up Date - End, Source
OUTPUT 	: List of enquiry follow up 
BY 		: Nitin Kashyap
*/
function db_get_enquiry_fup_count($fup_filter_data)
{
	$get_enquiry_fup_squery_base = "select count(*) as fup_count from crm_enquiry_follow_up CEFU inner join crm_enquiry CE on CE.enquiry_id = CEFU.enquiry_id_for_follow_up";
	
	$get_enquiry_fup_squery_where = "";
	
	$get_enquiry_fup_squery_order = "";	
	
	$filter_count = 1;
	
	// Data
	$get_enquiry_fup_sdata = array();		
	
	if(array_key_exists('fup_start_date',$fup_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." where enquiry_follow_up_date >= :fup_start_date";
		}
		else
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." and enquiry_follow_up_date >= :fup_start_date";
		}
		
		// Data
		$get_enquiry_fup_sdata[':fup_start_date']  = $fup_filter_data['fup_start_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('fup_end_date',$fup_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." where enquiry_follow_up_date <= :fup_end_date";
		}
		else
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." and enquiry_follow_up_date <= :fup_end_date";
		}
		
		// Data
		$get_enquiry_fup_sdata[':fup_end_date']  = $fup_filter_data['fup_end_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('assigned_to',$fup_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." where CE.assigned_to=:assigned_to";								
		}
		else
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." and CE.assigned_to=:assigned_to";				
		}
		
		// Data
		$get_enquiry_fup_sdata[':assigned_to']  = $fup_filter_data['assigned_to'];
		
		$filter_count++;
	}

	if(array_key_exists('fupped_start_date',$fup_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." where enquiry_follow_up_added_on >= :fupped_start_date";
		}
		else
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." and enquiry_follow_up_added_on >= :fupped_start_date";
		}
		
		// Data
		$get_enquiry_fup_sdata[':fupped_start_date']  = $fup_filter_data['fupped_start_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('fupped_end_date',$fup_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." where enquiry_follow_up_added_on <= :fupped_end_date";
		}
		else
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." and enquiry_follow_up_added_on <= :fupped_end_date";
		}
		
		// Data
		$get_enquiry_fup_sdata[':fupped_end_date']  = $fup_filter_data['fupped_end_date'];
		
		$filter_count++;
	}
	
	if(array_key_exists('added_by',$fup_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." where CEFU.enquiry_follow_up_added_by = :added_by";
		}
		else
		{
			// Query
			$get_enquiry_fup_squery_where = $get_enquiry_fup_squery_where." and CEFU.enquiry_follow_up_added_by = :added_by";				
		}
		
		// Data
		$get_enquiry_fup_sdata[':added_by']  = $fup_filter_data['added_by'];
		
		$filter_count++;
	}
	
	$get_enquiry_fup_squery = $get_enquiry_fup_squery_base.$get_enquiry_fup_squery_where;			

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_enquiry_fup_sstatement = $dbConnection->prepare($get_enquiry_fup_squery);
		
		$get_enquiry_fup_sstatement -> execute($get_enquiry_fup_sdata);
		
		$get_enquiry_fup_sdetails = $get_enquiry_fup_sstatement -> fetchAll();
		
		if(FALSE === $get_enquiry_fup_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_enquiry_fup_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_enquiry_fup_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
?>
