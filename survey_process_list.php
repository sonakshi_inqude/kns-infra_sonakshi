<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: survey_details_list.php
CREATED ON	: 05-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('SURVEY_PROCESS_LIST_FUNC_ID','202');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
//include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',SURVEY_PROCESS_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',SURVEY_PROCESS_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',SURVEY_PROCESS_LIST_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',SURVEY_PROCESS_LIST_FUNC_ID,'1','1');

	// Query String Data
	// Nothing
	
	if(isset($_GET['survey_id']))
	{
		$survey_id = $_GET['survey_id'];
	}
	else
	{
		$survey_id = "";
	}
	
	if(isset($_GET['source']))
	{
		$source = $_GET['source'];
	}
	else
	{
		$source = "";
	}
	
	if(isset($_GET['survey_process_id']))
	{
		$survey_process_id = $_GET['survey_process_id'];
	}
	else
	{
		$survey_process_id = "";
	}
	
	if($survey_id == '')
	{
		$search_process = "";
	}
	else
	{
		$search_process = '';
	}
	$search_village      = "";
	$search_project      = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_process    = $_POST["search_process"];
		$search_village    = $_POST["search_village"];
		$search_project    = $_POST["search_project"];
		$survey_id         = $_POST["hd_survey_id"];
	}
	
	// Get process details
	$survey_details_search_data = array("active"=>'1',"survey_id"=>$survey_id);
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list['status'] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list["data"];
		// Get process details
		$survey_file_search_data = array("survey_id"=>$survey_details_list_data[0]["survey_details_id"]);
		$survey_file_list = i_get_survey_file($survey_file_search_data);
		if($survey_file_list['status'] == SUCCESS)
		{
		
		$village_name  = $survey_file_list["data"][0]["village_name"];
		$survey_no     = $survey_file_list["data"][0]["survey_master_survey_no"];
	    }		
		else
		{
			$village_name  = "<em>PLEASE ADD FILE</em>";
			$survey_no  = "<em>PLEASE ADD FILE</em>";
		}	
	}
	
	else
	{
		$village_name  = "";
		$survey_no     = "";
	}
	
	// Get Survey Process Master modes already added
	$survey_process_master_search_data = array("active"=>'1',,"process_type"=>'1');
	$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
	if($survey_process_master_list['status'] == SUCCESS)
	{
		$survey_process_master_list_data = $survey_process_master_list['data'];
	}	

	else
	{
		$alert = $survey_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey already added
	$survey_project_master_search_data = array("active"=>'1');
	$survey_project_master_list = i_get_survey_project_master($survey_project_master_search_data);
	if($survey_project_master_list['status'] == SUCCESS)
	{
		$survey_project_master_list_data = $survey_project_master_list['data'];
	}
	else
	{
		$alert = $survey_project_master_list["data"];
		$alert_type = 0;
	}
	
	 // Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey Process already added
	$survey_process_search_data = array("active"=>'1',"survey_id"=>$survey_id,"survey_process_id"=>$survey_process_id,"village"=>$search_village,"process_name"=>$search_process,"project"=>$search_project);
	$survey_process_list = i_get_survey_process($survey_process_search_data);
	if($survey_process_list['status'] == SUCCESS)
	{
		$survey_process_list_data = $survey_process_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$survey_process_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey Process List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
             <h3>APF Process List</h3><?php if($add_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;">
			  <?php if($source == "view") { ?> <a href="survey_add_process.php?survey_id=<?php echo $survey_id ;?> ">Add Process</a><?php } ?></span><?php } ?>Village:&nbsp;&nbsp;&nbsp;<?php echo $village_name; ?>&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Survey No:&nbsp;&nbsp;&nbsp;<?php echo $survey_no; ?>
            </div>
            <!-- /widget-header -->
			<?php 
			if($survey_id == '')
			{?>
			<div class="widget-header" style="height:50px; padding-top:10px;">		
			<form method="post" id="file_search_form" action="survey_process_list.php">
			<input type="hidden" name="hd_survey_id" value="<?php echo $survey_id; ?>" />
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($survey_project_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $survey_project_master_list_data[$project_count]["survey_project_master_id"]; ?>" <?php if($search_project == $survey_project_master_list_data[$project_count]["survey_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $survey_project_master_list_data[$project_count]["survey_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($survey_process_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $survey_process_master_list_data[$apf_count]["survey_process_master_id"]; ?>" <?php if($search_process == $survey_process_master_list_data[$apf_count]["survey_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $survey_process_master_list_data[$apf_count]["survey_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_village">
			  <option value="">- - Select Village - -</option>
			  <?php
			  for($village_count = 0; $village_count < count($village_master_list_data); $village_count++)
			  {
			  ?>
			  <option value="<?php echo $village_master_list_data[$village_count]["village_id"]; ?>" <?php if($search_village == $village_master_list_data[$village_count]["village_id"]) { ?> selected="selected" <?php } ?>><?php echo $village_master_list_data[$village_count]["village_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
			  <?php
			}
			?>
            <div class="widget-content">
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
		    ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Process</th>
					<th>Village</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Leadtime</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="6" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($survey_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($survey_process_list_data); $count++)
					{
						$sl_no++;
						
						// Get Survey Process Master modes already added
						$survey_process_master_search_data = array("master_id"=>$survey_process_list_data[$count]["process_id"]);
						$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
						if($survey_process_master_list['status'] == SUCCESS)
						{
							$survey_process_master_list_data = $survey_process_master_list['data'];
							$process_name = $survey_process_master_list_data[0]["survey_process_master_name"];
						}	

						else
						{
							$alert = $survey_process_master_list["data"];
							$alert_type = 0;
						}
						
						    $process_start_date = $survey_process_list_data[$count]["survey_process_start_date"];
							$process_end_date = $survey_process_list_data[$count]["survey_process_end_date"];
							$leadtime = get_date_diff($process_start_date,$process_end_date);
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $process_name; ?></td>
					<td><?php echo $survey_process_list_data[$count]["village_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_start_date,"d-M-Y"); ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_end_date,"d-M-Y"); ?></td>
					<td><?php echo $leadtime["data"] ;?></td>
					<td><?php echo $survey_process_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_process_list_data[$count][
					"survey_process_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_survey_edit_process('<?php echo $survey_process_list_data[$count]["survey_process_id"]; ?>','<?php echo $survey_process_list_data[$count]["survey_process_survey_id"]; ?>');">Edit</a><?php } ?></td>
					<td><?php if(($survey_process_list_data[$count]["survey_process_active"] == "1") && ($delete_perms_list["status"] == SUCCESS)){ ?><a href="#" onclick="return survey_delete_process('<?php echo $survey_process_list_data[$count]["survey_process_id"]; ?>)','<?php echo $survey_process_list_data[$count]["survey_process_survey_id"]; ?>');">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($add_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_delay_reason('<?php echo $survey_process_list_data[$count]["survey_process_id"]; ?>');">Delay Reason</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_survey_documents('<?php echo $survey_process_list_data[$count]["process_id"]; ?>','<?php echo $survey_process_list_data[$count]["survey_process_id"]; ?>');">Upload Documents</a><br /><br /><a style="padding-right:10px" href="#" onclick="return go_to_survey_download('<?php echo $survey_process_list_data[$count]["process_id"]; ?>','<?php echo $survey_process_list_data[$count]["survey_process_id"]; ?>');">Download</a><br /><br /><?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_query_response('<?php echo $survey_process_list_data[$count]["process_id"]; ?>');">Add Query</a><?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_survey_user_process('<?php echo $survey_process_list_data[$count]["process_id"]; ?>');">User Mapping</a><?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if($view_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_process_task('<?php echo $survey_process_list_data[$count]["process_id"]; ?>','<?php echo $survey_process_list_data[$count]["survey_process_id"]; ?>');">Task</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Survey Process added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <?php
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function survey_delete_process(survey_process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "survey_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/survey_delete_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("survey_process_id=" + survey_process_id + "&action=0");
		}
	}	
}
function go_to_survey_edit_process(survey_id,survey_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_edit_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","survey_process_id");
	hiddenField2.setAttribute("value",survey_process_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_delay_reason(process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_add_process_delay.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_documents(process_id,survey_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_upload_documents.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","survey_process_id");
	hiddenField2.setAttribute("value",survey_process_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_download(process_id,apf_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_document_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","apf_process_id");
	hiddenField2.setAttribute("value",apf_process_id);
	
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_query_response(process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_add_query.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_user_process(process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_add_process_user.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_process_task(survey_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_add_process_task.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_process_id");
	hiddenField1.setAttribute("value",survey_process_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>