<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_user_mapping_list.php
CREATED ON	: 28-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID','254');
/* DEFINES - END */

/*
TBD: 
*/
$_SESSION['module'] = 'Projectmgmnt';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID,'4','1');
	$ok_perms_list   	= i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID,'5','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_ACTUAL_MACHINE_PLANNING_LIST_FUNC_ID,'6','1');
	
	// Query String Data
	// Nothing
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}

	// Machine Planning data
	$actual_machine_plan_search_data = array("active"=>'1',"percentage_pending"=>"1","process_id"=>$process_id);
	$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
	if($actual_machine_plan_list["status"] == SUCCESS)
	{
		$actual_machine_plan_list_data = $actual_machine_plan_list["data"];
	}
	else
	{
		$alert = $actual_machine_plan_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Actual Machine Pending List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Task Actual Machine Pending List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Project</th>
				    <th style="word-wrap:break-word;">Process</th>
					<th style="word-wrap:break-word;">Task Name</th>
					<th style="word-wrap:break-word;">%</th>
					<th style="word-wrap:break-word;">Machine Type</th>
					<th style="word-wrap:break-word;">Machine Vendor</th>
					<th style="word-wrap:break-word;">Machine</th>
					<th style="word-wrap:break-word;">Actual Start Date Time</th>
					<th style="word-wrap:break-word;">Actual End date Tme</th>
					<th style="word-wrap:break-word;">Hours Worked</th>
					<th style="word-wrap:break-word;">Off Time</th>
					<th style="word-wrap:break-word;">Machine Rate</th>
					<th style="word-wrap:break-word;">Additional Cost</th>
					<th style="word-wrap:break-word;">Machine Bata</th>
					<th style="word-wrap:break-word;">Machine Issued Fuel</th>
					<th style="word-wrap:break-word;">Total Amount</th>					
                    <th style="word-wrap:break-word;">Remarks</th>					
                    <th style="word-wrap:break-word;">Added By</th>									
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($actual_machine_plan_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($actual_machine_plan_list_data); $count++)
					{
						if($actual_machine_plan_list_data[$count]["project_task_actual_machine_display_status"] == "not approved")
						{
						$sl_no++;
						
						$machine_rate = $actual_machine_plan_list_data[$count]['project_task_actual_machine_fuel_charges'];
						
						//get stock quantity
						$project_machine_rate_master_search_data = array("machine_id"=>$actual_machine_plan_list_data[$count]["project_task_machine_id"]);
						$project_machine_rate_master_data =  i_get_project_machine_rate_master($project_machine_rate_master_search_data);
						if($project_machine_rate_master_data["status"] == SUCCESS)
						{							
							$vendor_machine_number = $project_machine_rate_master_data["data"][0]["project_machine_master_id_number"];
							$machine_type = $project_machine_rate_master_data["data"][0]["project_machine_type"];
						}
						else
						{
							$vendor_machine_number = 'NA';
							$machine_type = 'NA';
						}
						
						//get stock quantity
						$machine_vendor = $actual_machine_plan_list_data[$count]['project_machine_vendor_master_name'];
						
						//No of hrs worked
						if($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_end_date_time"] != "0000-00-00 00:00:00")
						{
							$start_date_time = strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_start_date_time"]);
							$end_date_time = strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_end_date_time"]);
							$date_diff = $end_date_time - $start_date_time;
							$no_hrs_worked = $date_diff/3600;
							
							$end_date_display = date('d-M-Y H:i:s',strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_end_date_time"]));
						}
						else
						{
							$start_date_time = strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_start_date_time"]);
							$end_date_time = strtotime(date("Y-m-d H:i:s"));
							$date_diff = $end_date_time - $start_date_time;
							$no_hrs_worked = $date_diff/3600;
							
							$end_date_display = '';
						}
						
						// Off time related calculation
						$off_time = $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_off_time"]/60;
						$eff_hrs = $no_hrs_worked - $off_time;						
			
						$cost = ($machine_rate * $eff_hrs) + $actual_machine_plan_list_data[$count]["project_task_actual_machine_bata"] + $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_additional_cost"] - $actual_machine_plan_list_data[$count]["project_task_actual_machine_issued_fuel"];
			
						
					?>
					<tr>  
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_process_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_completion"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $machine_type; ?></td>
					<td style="word-wrap:break-word;"><?php echo $machine_vendor; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_machine_master_name"]; ?>-<?php echo $vendor_machine_number; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($actual_machine_plan_list_data[$count][
					"project_task_actual_machine_plan_start_date_time"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $end_date_display; ?></td>					
					<td style="word-wrap:break-word;"><?php echo round($no_hrs_worked,2) ; ?></td>	
					<td style="word-wrap:break-word;"><?php echo round($off_time,2) ; ?></td>						
					<td style="word-wrap:break-word;"><?php echo $machine_rate ; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_additional_cost"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_bata"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_issued_fuel"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo round($cost,2); ?></td>					
				
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["user_name"]; ?></td>	
					</tr>
					<?php
						}
					}
					
				}
				else
				{
				?>
				<td colspan="22">No machine details added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>