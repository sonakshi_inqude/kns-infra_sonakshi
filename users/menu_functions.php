 <?php
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
?>
 <link href="bootstrap_aku.min.css" rel="stylesheet" type="text/css">
  <link href="https://use.fontawesome.com/releases/v4.7.0/css/font-awesome-css.min.css" rel="stylesheet">

<script src="js/jquery-1.7.2.min1.js"></script>
 <script type="text/javascript">
          $(document).ready(function(){
			  if ( ($(window).width() <= 767) && ($.trim($(".btm").html())=='') ){
			  }else{
			  $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(500);
        $(this).toggleClass('open');
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(500);
        $(this).toggleClass('open');
      });
			  }

        });
        </script>

  <div id="menu112">

<div class="container">
     <nav class="navbar navbar-fixed-top navbar-inverse ">
     <div class="container">
      <div class="navbar-header">
      	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">

  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  		</button>
  		<a class="navbar-brand" href="dashboard_overview.php" style="color:#ffffff;">KNS Infra</a>
        <div class="clearfix"></div>
  	</div>

  	<div class="collapse navbar-collapse js-navbar-collapse">

    <ul class="nav navbar-nav">
        <?php
                // Load menu based on user permissions

                $perms_list = i_get_user_perms($user, 'Dashboard', '', '2', '1');

                if ($perms_list['status'] == SUCCESS) {
                    ?>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dashboard<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>
            </ul>
          </li>
          <?php
                }?>

                        <?php

                $perms_list1 = i_get_user_perms($user, 'Legal', '', '2', '1');
                $perms_list2 = i_get_user_perms($user, 'Tasks', '', '2', '1');
                $perms_list3 = i_get_user_perms($user, 'Process', '', '2', '1');
                $perms_list4 = i_get_user_perms($user, 'Legal Masters', '', '2', '1');
                $perms_list5 = i_get_user_perms($user, 'Reason', '', '2', '1');
                $perms_list6 = i_get_user_perms($user, 'Legal Reports', '', '2', '1');
                $perms_list7 = i_get_user_perms($user, 'Dashboard Legal', '', '2', '1');
                $perms_list8 = i_get_user_perms($user, 'Registration', '', '2', '1');
                $perms_list9 = i_get_user_perms($user, 'Katha_Transfer', '', '2', '1');
                $perms_list9 = i_get_user_perms($user, 'Registration', '', '2', '1');
                if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS  || $perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS  || $perms_list5['status'] == SUCCESS  || $perms_list6['status'] == SUCCESS  || $perms_list7['status'] == SUCCESS  || $perms_list8['status'] == SUCCESS  || $perms_list9['status'] == SUCCESS) {
                    ?>

          	<li class="dropdown mega-dropdown">
  				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Legal - Liaison<span class="caret"></span></a>
          <ul class="dropdown-menu mega-dropdown-menu">


  					<li class="col-sm-3">
  						<ul>

                                <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Legal', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
                 <li class="dropdown-header"><b class="fa fa-legal">&nbsp;</b>Legal</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>

                                     <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Tasks', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
                 <li class="divider"></li>
  							<li class="dropdown-header"><b class="fa fa-list-alt">&nbsp;</b>Tasks</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>


           						</ul>
  					</li>
                                          					<li class="col-sm-3">
  						<ul>

                                <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Legal Masters', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
                <li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Legal Masters</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>


  						           <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Process', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
               <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-list">&nbsp;</b>Process</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>



  						           <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Reason', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
                <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-commenting">&nbsp;</b>Reason</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>




  						</ul>
  					</li>
                                          					<li class="col-sm-3">
  						<ul>

                            <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Legal Reports', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
               <li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Legal Reports</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>

                   <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Dashboard Legal', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
               <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-dashboard">&nbsp;</b>Dashboard</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>

                 <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Katha_Transfer_Buy', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
               <li class="divider"></li>
               <li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Katha Transfer -Buy
</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                    } ?>

  						</ul>
  					</li>
       <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Registration', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
<li class="col-sm-3">
  						<ul>


             <li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Registration</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>


  						</ul>
  					</li>

            <?php
                    } ?>


  				</ul>
  			</li>

 <?php
                }?>


 <?php
 $perms_list1 = i_get_user_perms($user, 'Land Bank', '', '2', '1');
 $perms_list2 = i_get_user_perms($user, 'BD', '', '2', '1');
 $perms_list3 = i_get_user_perms($user, 'BD Masters', '', '2', '1');
 $perms_list4 = i_get_user_perms($user, 'survey', '', '2', '1');
 $perms_list5 = i_get_user_perms($user, 'APF', '', '2', '1');
 $perms_list6 = i_get_user_perms($user, 'Court Case', '', '2', '1');
    if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS  || $perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS  || $perms_list5['status'] == SUCCESS  || $perms_list6['status'] == SUCCESS) {
        ?>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">BD<span class="caret"></span></a>
          <ul class="dropdown-menu mega-dropdown-menu">
         <?php if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS) {
            ?>
 <li class="col-sm-3">
                <ul>

                  <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Land Bank', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>
             <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Land Bank</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>


                <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'BD', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>
             <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>BD</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>




                                </ul></li>
                                <?php
        } ?>
                 <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'survey', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>
                <li class="col-sm-3">
                <ul>

           <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Survey</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>

                                </ul>
                </li>

                <?php
        } ?>
                   <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'BD Masters', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>
                <li class="col-sm-3">
                <ul>


           <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>BD Masters</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>


                  </ul>

                  </li>

                <?php
        } ?>
                <?php if ($perms_list5['status'] == SUCCESS  || $perms_list6['status'] == SUCCESS) {
            ?>
                 <li class="col-sm-3">
                <ul>

                   <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Court Case', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>

           <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Court Case</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>

                 <?php
            } ?>
                    <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'APF', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>
            <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>APF</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>

                  </ul>
                  </li>
                  <?php
        } ?>
          </ul>
        </li>

  <?php
    }?>


  <?php
  $perms_list1 = i_get_user_perms($user, 'CRM Masters', '', '2', '1');
  $perms_list2 = i_get_user_perms($user, 'CRM Projects', '', '2', '1');
  $perms_list3 = i_get_user_perms($user, 'CRM Transactions', '', '2', '1');
  $perms_list4 = i_get_user_perms($user, 'Site Visit', '', '2', '1');
    $perms_list6 = i_get_user_perms($user, 'CRM Reports', '', '2', '1');
  $perms_list5 = i_get_user_perms($user, 'Misc', '', '2', '1');
    if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS  || $perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS  || $perms_list5['status'] == SUCCESS || $perms_list6['status'] == SUCCESS) {
        ?>


        <li class="dropdown mega-dropdown">
      		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sales-CRM<span class="caret"></span></a>
  				<ul class="dropdown-menu mega-dropdown-menu">
                   				        <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'CRM Masters', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>

                                	<li class="col-sm-3">
      					<ul>

          <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>CRM Masters</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>


  						</ul>
  					</li>

                    <?php
        } ?>
                                <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'CRM Projects', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>

                                            					<li class="col-sm-3">
  						<ul>

         <li class="dropdown-header"><b class="fa fa-check-circle-o">&nbsp;</b>CRM Projects</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>


  						</ul>
  					</li>
                    <?php
        } ?>

                    <?php if ($perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS) {
            ?>
                                           					<li class="col-sm-3">
  						<ul>
                          <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'CRM Transactions', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>
       	<li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>CRM Transactions</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>

                       <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Site Visit', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>
       <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-taxi">&nbsp;</b>Site Visit</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>



  						</ul>
  					</li>
                    <?php
        } ?>
                          <?php if ($perms_list5['status'] == SUCCESS  || $perms_list6['status'] == SUCCESS) {
            ?>
                                <li class="col-sm-3">
      					<ul>

                           <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'CRM Reports', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>
     <li class="dropdown-header"><b class="fa fa-bar-chart">&nbsp;</b>CRM Reports</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>

                 <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Misc', '', '2', '1');
            if ($perms_list['status'] == SUCCESS) {
                ?>


                <li class="dropdown-header"><b class="fa fa-hashtag">&nbsp;</b>Misc.</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
            } ?>


              </ul>
  					</li>
                    <?php
        } ?>
  				</ul>
  			</li>


         <?php
    }?>


         <?php
         $perms_list1 = i_get_user_perms($user, 'Asset', '', '2', '1');
         $perms_list2 = i_get_user_perms($user, 'Stock Masters', '', '2', '1');
         $perms_list3 = i_get_user_perms($user, 'Stock Transactions', '', '2', '1');
        $perms_list4 = i_get_user_perms($user, 'Stock Reports', '', '2', '1');
            if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS  || $perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS) {
                ?>
            <li class="dropdown mega-dropdown">
      		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Procurement<span class="caret"></span></a>
  				<ul class="dropdown-menu mega-dropdown-menu">

                       <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Asset', '', '2', '1');
                if ($perms_list['status'] == SUCCESS) {
                    ?>
                                  <li class="col-sm-3">
                <ul>



                <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Asset</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>





					                  </ul>
                  </li>
                       <?php
                } ?>
                    <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Stock Masters', '', '2', '1');
                if ($perms_list['status'] == SUCCESS) {
                    ?>

            <li class="col-sm-3">
                <ul>



                <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Stock Masters</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>

                            </ul>
            </li>
                 <?php
                } ?>
                   <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Stock Transactions', '', '2', '1');
                if ($perms_list['status'] == SUCCESS) {
                    ?>
                                        <li class="col-sm-3">
                <ul>

  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Stock Transactions</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>


                </ul>
            </li>

               <?php
                } ?>
                  <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Stock Reports', '', '2', '1');
                if ($perms_list['status'] == SUCCESS) {
                    ?>
                                    <li class="col-sm-3">
                <ul>

  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Stock Reports</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>



					                     </ul>
            </li>
            <?php
                } ?>

			 <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Stock Transfer', '', '2', '1');
                if ($perms_list['status'] == SUCCESS) {
                    ?>
                                    <li class="col-sm-3">
                <ul>

  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Stock Transfer</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>



					                     </ul>
            </li>
            <?php
                } ?>
                                    </ul>

                </li>

            <?php
            }?>



            <?php
                $perms_list1 = i_get_user_perms($user, 'PM Masters', '', '2', '1');
                $perms_list2 = i_get_user_perms($user, 'Contracts', '', '2', '1');
                $perms_list3 = i_get_user_perms($user, 'Manpower', '', '2', '1');
                $perms_list4 = i_get_user_perms($user, 'Machine', '', '2', '1');
                if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS  || $perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS) {
                    ?>
         <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Project Management<span class="caret"></span></a>
          <ul class="dropdown-menu mega-dropdown-menu">
          <?php 	if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS) {
                        ?>
            <li class="col-sm-3">
                <ul>
                   <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'PM Masters', '', '2', '1');
                        if ($perms_list['status'] == SUCCESS) {
                            ?>
   <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Masters</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                        } ?>

                </ul>
            </li>
            <?php
                    } ?>
   <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Manpower', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
            <li class="col-sm-3">
                <ul>

    <li class="dropdown-header"><b class="fa fa-users">&nbsp;</b>Manpower</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>

				<?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Contracts', '', '2', '1');
                        if ($perms_list['status'] == SUCCESS) {
                            ?>
   <li class="divider"></li>
                  <li class="dropdown-header"><b class="fa fa-newspaper-o">&nbsp;</b>Contracts</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    }
                        } ?>



                </ul>
            </li>
            <?php
                    } ?>
                <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Machine', '', '2', '1');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
            <li class="col-sm-3">
                <ul>

   <li class="dropdown-header"><b class="fa fa-truck">&nbsp;</b>Machine</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>





                </ul>
            </li>
            <?php
                    } ?>
    <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Projectmgmnt', '', '2', '1','0');
                    if ($perms_list['status'] == SUCCESS) {
                        ?>
            <li class="col-sm-3">
                <ul>


                  <li class="dropdown-header"><b class="fa fa-building">&nbsp;</b>Projects</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>



                </ul>
            </li>
<?php
                    } ?>

          </ul>
        </li>

<?php
                }?>

<?php
$perms_list1 = i_get_user_perms($user, 'HR', '', '2', '1');
$perms_list2 = i_get_user_perms($user, 'General Task', '', '2', '1');
$perms_list3 = i_get_user_perms($user, 'Users', '', '2', '1');
$perms_list4 = i_get_user_perms($user, 'Meeting', '', '2', '1');
    if ($perms_list1['status'] == SUCCESS || $perms_list2['status'] == SUCCESS  || $perms_list3['status'] == SUCCESS  || $perms_list4['status'] == SUCCESS) {
        ?>
        <li class="dropdown mega-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">HR<span class="caret"></span></a>
  <ul class="dropdown-menu mega-dropdown-menu">
     <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'HR', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>
    <li class="col-sm-3">
      <ul>


                  <li class="dropdown-header"><b class="fa fa-shopping-basket">&nbsp;</b>Current HR Menu</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>



                  </ul>
    </li>
    <?php
        } ?>
       <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'General Task', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>
    <li class="col-sm-3">
      <ul>



                  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>General Tasks</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>



            </ul>
    </li>
    <?php
        } ?>
        <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Users', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>
    <li class="col-sm-3">
      <ul>

 <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Users</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>




                  </ul>
    </li>
    <?php
        } ?>
       <?php
                // Load menu based on user permissions
                $perms_list = i_get_user_perms($user, 'Meeting', '', '2', '1');
        if ($perms_list['status'] == SUCCESS) {
            ?>
        <li class="col-sm-3">
      <ul>


  <li class="dropdown-header"><b class="fa fa-handshake-o">&nbsp;</b>Meeting</li>
                           <?php

                    for ($count = 0; $count < count($perms_list['data']); $count++) {
                        ?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>

					<?php
                    } ?>


            </ul>
    </li><?php
        } ?>
  </ul>
</li>


      <?php
    }?>

  		</ul>

          <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $loggedin_name; ?><span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a  href="javascript:;">Help</a></li>
              <li><a href="#">Profile</a></li>
              <li class="divider"></li>
              <li><a href="out.php" style="color: red;">Logout</a></li>
            </ul>
          </li>
        </ul>
  	</div><!-- /.nav-collapse -->
    </div>
    </nav>
  </div>
  </div>


  <style type="text/css">

.table th {

  position:relative;
  z-index:999;
	/*min-width:100px;*/
}
.table td {
	min-width: 100px;
	}
	.table {
		background:#FFF;
	}
@media only screen and (max-width:1199px)
	{
.table {
	width:auto !important;

	}
	}

.dropdown-menu{

    overflow-y: auto !important;
	`}
  </style>


<button data-toggle="collapse" data-target="#demo">Collapsible</button>

<div id="demo" class="collapse">


<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>

<script type='text/javascript'>
function hide_th(number){

       $('.table').find("th:nth-child("+number+")").toggle();
  $('.table').find("td:nth-child("+number+")").toggle();

	}
//<![CDATA[
$(window).load(function(){
/*	 var colspanned = $('[colspan]','table thead');

$(colspanned).each(function(idx,el){
    var colSpanSize = parseInt($(el).attr('colspan'));
    if (colSpanSize <= 1)
    {
        return;
    }

    colSpanSize = colSpanSize - 1; //Delta
    for (i = 0; i < colSpanSize; i++) {
        $(el).closest('tr').append('<th style="word-wrap:break-word;">Action</th>');
    }
	$(el).attr('colspan','1');
});

	 var colspanned12 = $('[colspan]','table tbody');

$(colspanned12).each(function(idx,el){
    var colSpanSize456 = parseInt($(el).attr('colspan'));
    if (colSpanSize456 <= 1)
    {
        return;
    }

    colSpanSize456 = colSpanSize456 - 1; //Delta
    for (i = 0; i < colSpanSize456; i++) {
        $(el).closest('tr').append('<td ></td>');
    }
	$(el).attr('colspan','1');
});*/
setTimeout(function(){ $(document).ready(function () {
	var rowCount = $('table tr').length;

$('#loader-wrapper').fadeOut('slow');
if(rowCount>1){

	// $('table').DataTable();

	$(".dropdown-menu").css('max-height',($(window).height()-80)+'px');

	$(".widget-header").height('auto');
	 var cellText='<div class="dropdown col-sm-1"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-th" aria-hidden="true"></i><span class="caret"></span></button><div class="column-list dropdown-menu " ></div></div>';

	$( cellText ).insertBefore( ".widget-header form" );
//	$('.dataTables_wrapper .col-sm-6:nth-child(2)').removeClass('col-sm-6').addClass('col-sm-5');
(function ( $ ) {
    'use strict';
     var path = window.location.pathname;
var page = path.split("/").pop();

    $.fn.columnFilter = function( options ) {
        var settings = $.extend({
            columnCheckboxsContainer: '.column-list',
            localStorageNamespace: page,
            headerCell: 'TH'
        }, options );

        var columnList = '';
        var that = this;
        var checked='';
        $(that).find('* > tr').each(function(){
            $(this).children().each(function(index){
                if( localStorage.getItem(settings.localStorageNamespace + index) == 'hide' ) {
                    $(this).hide();
                    checked='';
                } else {
                    $(this).show();
                    checked='checked';
                }
                if( $(this).context.nodeName == settings.headerCell ) {
                    var label = $.trim($(this).text());
                    var checkbox = '<input type="checkbox" '+ checked +' data-label="'+ label +'">';
                    columnList = $.fn.columnFilter.format( checkbox, label );
                    $(settings.columnCheckboxsContainer).append('<div class="col-sm-12">'+columnList+'</div>');
                }
            });
        });

		var options = [];

$( '.widget-header .dropdown-menu .col-sm-12' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.find( 'input' ).val(),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();

   console.log( options );
   return false;
});
        $(settings.columnCheckboxsContainer).find('input').bind('click', function(){
            var label = $(this).attr('data-label');

            var index = $(that).find('thead tr th').filter(function() {
                return $.trim($(this).text()) === label;
            }).index();

            if( !$(this).is(':checked') ) {
                localStorage.setItem(settings.localStorageNamespace + index, 'hide');
                $(that).find('thead').children().each(function(i){
                    $(this).children().eq(index).hide();
                });

                $(that).find('tbody').children().each(function(i){
                    $(this).children().eq(index).hide();
                });
            } else {
                localStorage.setItem(settings.localStorageNamespace + index, 'show');
                $(that).find('thead').children().each(function(i){
                    $(this).children().eq(index).show();
                });

                $(that).find('tbody').children().each(function(i){
                    $(this).children().eq(index).show();
                });
            }
        });
        return;
    };

    $.fn.columnFilter.format = function(checkbox,column) {
        return checkbox + '<label>' + column + '</label>';
    };
}( jQuery ));



$('table').columnFilter();
    var lastScrollLeft = 0;
    $(window).scroll(function () {
		var scroll = $(window).scrollTop();

		  if (scroll <= $('.navbar-header').height()) {
        var x = $(window).scrollTop();
        $('.table th').css({
            top: x,
        });
		  }else{
			  var x = $(window).scrollTop()-$('.table').offset().top+$('.navbar-header').height();
        $('.table th').css({
            top: x,
        });
			  }
    });
	}


});}, 500);

});//]]>

</script>
</div>
<style>
/*.dataTables_wrapper .row{
	margin-left:0px !important;
	margin-bottom: 20px;
    margin-top: 13px;
	}
.dataTables_wrapper	.input-sm{
	margin-left:15px !important;
		}*/

.column-list input{
	float:left;
	margin-right:10px;
	}
	.column-list label{
	float:left;
	margin-right:0px;
	font-size:11px;
	}
	.column-list{
		width:800px !important;
		column-count: 3;
		}
	@charset "utf-8";
.chromeframe {
	margin: 0.2em 0;
	background: #ccc;
	color: #000;
	padding: 0.2em 0;
}
p {
	line-height: 1.33em;
	color: #7E7E7E;
}
#loader-wrapper {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1000;
	background-color: #fff;
}
#loader {
	display: block;
	position: relative;
	left: 50%;
	top: 50%;
	width: 150px;
	height: 150px;
	margin: -75px 0 0 -75px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #3498db;
	-webkit-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
}
#loader:before {
	content: "";
	position: absolute;
	top: 5px;
	left: 5px;
	right: 5px;
	bottom: 5px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #e74c3c;
	-webkit-animation: spin 3s linear infinite;
	animation: spin 3s linear infinite;
}
#loader:after {
	content: "";
	position: absolute;
	top: 15px;
	left: 15px;
	right: 15px;
	bottom: 15px;
	border-radius: 50%;
	border: 3px solid transparent;
	border-top-color: #f9c922;
	-webkit-animation: spin 1.5s linear infinite;
	animation: spin 1.5s linear infinite;
}
@-webkit-keyframes spin {
0% {
-webkit-transform:rotate(0deg);
-ms-transform:rotate(0deg);
transform:rotate(0deg);
}
100% {
-webkit-transform:rotate(360deg);
-ms-transform:rotate(360deg);
transform:rotate(360deg);
}
}
@keyframes spin {
0% {
-webkit-transform:rotate(0deg);
-ms-transform:rotate(0deg);
transform:rotate(0deg);
}
100% {
-webkit-transform:rotate(360deg);
-ms-transform:rotate(360deg);
transform:rotate(360deg);
}
}
</style>

<div id="loader-wrapper">
  <div id="loader"></div>
</div>

<!-- /navbar -->
<div style=" margin:50px auto;"></div>
<script type="text/javascript" src="https://use.fontawesome.com/1c608bc16d.js"></script>
