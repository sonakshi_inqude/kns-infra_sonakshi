<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */

    $user_data = $_POST['user_data'];
    $user_data_array = json_decode($user_data,true);
    $user_ids = $user_data_array['user_ids'];
    $project_id = $user_data_array['project_id'];
    $dep_id = $user_data_array['dep_id'];
    for($id_count = 0 ; $id_count < (count($user_ids)) ; $id_count++)
  	{
        $project_user_iresults = i_add_project_user_mapping($project_id, $user_ids[$id_count], 'remarks', $user);
        if($project_user_iresults["status"] == SUCCESS)
        {
          echo "Mapping Successfully Added";
        }
        else {
          echo "";
        }
    }
} else {
    header("location:login.php");
}
?>
