<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID','289');
/* DEFINES - END */

/*
TBD:
*/
$_SESSION['module'] = 'Projectmgmnt';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   	= i_get_user_perms($user,'',PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID,'2','1');
	$edit_perms_list   	= i_get_user_perms($user,'',PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID,'3','1');
	$delete_perms_list 	= i_get_user_perms($user,'',PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID,'4','1');
	$ok_perms_list   	= i_get_user_perms($user,'',PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID,'5','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID,'6','1');

	// Query String Data
	// Nothing
	$alert_type = -1;
	$alert = "";

	$search_project = "";
	if(isset($_REQUEST["search_project"]))
	{
		$search_project   = $_REQUEST["search_project"];
	}

	$search_vendor = "";
	if(isset($_REQUEST["search_vendor"]))
	{
		$search_vendor   = $_REQUEST["search_vendor"];
	}

	$search_task = "";
	if(isset($_REQUEST["search_task"]))
	{
		$search_task   = $_REQUEST["search_task"];
	}

	$search_process = "";
	if(isset($_REQUEST["search_process"]))
	{
		$search_process   = $_REQUEST["search_process"];
	}

	$search_work_type = "";
	if(isset($_REQUEST["search_work_type"]))
	{
		$search_work_type   = $_REQUEST["search_work_type"];
	}

	$start_date = "";
	if(isset($_REQUEST["dt_start_date"]))
	{
		$start_date = $_REQUEST["dt_start_date"];
	}

	$end_date = "";
	if(isset($_REQUEST["dt_end_date"]))
	{
		$end_date = $_REQUEST["dt_end_date"];
	}


	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Project Manpower Vendor Master List
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_vendor_master_list["status"] == SUCCESS)
	{
		$project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
	}

	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}

	// Process Master
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
	}
	// Temp data
	$man_power_search_data = array("active"=>'1',"display_status"=>"approved","process_id"=>$process_id,"project"=>$search_project,"agency"=>$search_vendor,"work_start_date"=>$start_date,"work_end_date"=>$end_date,"task_master"=>$search_task,"process"=>$search_process,"work_type"=>$search_work_type);
	$man_power_list = i_get_man_power_list($man_power_search_data);
	if($man_power_list["status"] == SUCCESS)
	{
		$man_power_list_data = $man_power_list["data"];
		// var_dump($man_power_list_data[0]);
	}
	else
	{
		$alert = $alert."Alert: ".$man_power_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power Pending List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Task Actual Man Power List</h3>&nbsp;&nbsp;&nbsp;&nbsp; Grand Total : <span id="grand_total"><i>Calculating</i></span>
            </div>
            <!-- /widget-header -->
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_approved_manpower_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if($search_vendor == $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_work_type">
			  <option value="">- - Select Work Type - -</option>
			  <option value="Regular" <?php if ($search_work_type == "Regular") { ?> selected="selected" <?php } ?>>- - Regular Work - -</option>
			  <option value="Rework" <?php if ($search_work_type == "Rework") { ?> selected="selected" <?php } ?>>- - Rework - -</option>

			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>

			  <input type="submit" name="manpower_search_submit" />
			  </form>
            </div>
			 <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">SL No</th>
				    <th style="word-wrap:break-word;">Project</th>
				    <th style="word-wrap:break-word;">Process</th>
					<th style="word-wrap:break-word;">Task Name</th>
					<th style="word-wrap:break-word;">Road Name</th>
					<th style="word-wrap:break-word;">Work Type</th>
					<th style="word-wrap:break-word;">%</th>
					<th style="word-wrap:break-word;">Msrmnt</th>
					<th style="word-wrap:break-word;">Date</th>
					<th style="word-wrap:break-word;">Agency</th>
					<th style="word-wrap:break-word;">Men Hr</th>
					<th style="word-wrap:break-word;">Women Hr</th>
					<th style="word-wrap:break-word;">Mason Hr</th>
					<th style="word-wrap:break-word;">No of People</th>
					<th style="word-wrap:break-word;">Amount</th>
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Added By</th>
					<th style="word-wrap:break-word;">Added On</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($man_power_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$man_power_men_rate = 0;
					$man_power_women_rate = 0;
					$man_power_mason_rate = 0;
					$total_cost = 0;
					$grand_total = 0;
					for($count = 0; $count < count($man_power_list_data); $count++)
					{
						$sl_no++;

						$man_power_men_rate = $man_power_list_data[$count]["project_task_actual_manpower_men_rate"];
						$man_power_women_rate = $man_power_list_data[$count]["project_task_actual_manpower_women_rate"];
						$man_power_mason_rate = $man_power_list_data[$count]["project_task_actual_manpower_mason_rate"];


						/*$project_man_power_rate_search_data = array("vendor_id"=>$man_power_list_data[$count]["project_task_actual_manpower_agency"],"active"=>'1');
						$manpower_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);

						if($manpower_rate_list["status"] == SUCCESS)
						{
							for($rate_count =0 ; $rate_count < count($manpower_rate_list["data"]) ; $rate_count++ )
							{
								$manpower_rate_list_data = $manpower_rate_list["data"];
								if($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 1)
								{
									$man_power_men_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
								}
								if($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 2)
								{
									$man_power_women_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
								}
								if($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 3)
								{
									$man_power_mason_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
								}
							}
						}*/
						$total_men_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] * $man_power_men_rate;
						$total_women_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] * $man_power_women_rate;
						$total_mason_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] * $man_power_mason_rate;
						$total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;

						$men_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"];
						$women_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"];
						$mason_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"];

						$no_of_men = $men_hrs/8;
						$no_of_women = $women_hrs/8;
						$no_of_mason = $mason_hrs/8;

						$total_hrs = $men_hrs + $women_hrs + $mason_hrs;
						$no_of_people = $total_hrs/8;
						$grand_total = $grand_total + $total_cost;

					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_process_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_site_location_mapping_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $work_type = $man_power_list_data[$count]["project_task_actual_manpower_work_type"]; ?></td>
					<td style="word-wrap:break-word;"><?php
					if($work_type == "Regular")
					{
						echo $man_power_list_data[$count]["project_task_actual_manpower_completion_percentage"];
					}
					elseif($work_type == "Rework")
					{
						echo $man_power_list_data[$count]["project_task_actual_manpower_rework_completion"];
					}?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_completed_msmrt"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($man_power_list_data[$count][
					"project_task_actual_manpower_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"]; ?>
					  &nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_men_rate ;?>
					  Men :<?php echo $no_of_men;?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"]; ?>
					&nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_women_rate ;?> Women :<?php echo $no_of_women;?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"]; ?>
					 &nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_mason_rate ;?> Mason :<?php echo $no_of_mason ;?></td>
					<td style="word-wrap:break-word;"><?php echo $no_of_people ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $total_cost ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($man_power_list_data[$count][
					"project_task_actual_manpower_added_on"])); ?></td>

					</tr>

					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
			  <script>
			  document.getElementById('grand_total').innerHTML = '<?php echo $grand_total; ?>';
			  </script>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


  </body>

</html>
