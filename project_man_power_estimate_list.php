<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_man_power_estimate_list.php
CREATED ON	: 07-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID','260');
//define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID','');
/* DEFINES - END */


/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	$search_project = "";

	if(isset($_POST["search_project"]))
	{
		$search_project   = $_POST["search_project"];
	}

	$search_vendor   	 = "";

	if(isset($_POST["search_vendor"]))
	{
		$search_vendor   = $_POST["search_vendor"];
	}

    $search_task   	 = "";
    if(isset($_POST["search_task"]))
	{
		$search_task   = $_POST["search_task"];
	}

	$search_process   	 = "";

	if(isset($_POST["search_process"]))
	{
		$search_process   = $_POST["search_process"];
	}


	// Get Project Man Power Estimate modes already added
	$project_man_power_estimate_search_data = array("active"=>'1',"project"=>$search_project,"agency"=>$search_vendor,"process"=>$search_process,"task"=>$search_task);
	$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
	if($project_man_power_estimate_list['status'] == SUCCESS)
	{
		$project_man_power_estimate_list_data = $project_man_power_estimate_list['data'];
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Process Master
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
	}

	// Task Master
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list["status"] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_master_list["data"];
	}

	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_manpower_agency_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Man Power Estimate List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Man Power Estimate List </h3><span style="float:right; padding-right:20px;"><!--<a href="project_add_man_power_estimate.php">Project Add Man Power Estimate</a>--></span>

            </div>

            <!-- /widget-header -->
            <div class="widget-content">
			&nbsp;&nbsp;&nbsp;&nbsp; Total Men hrs : <span id="total_men_hrs"><i>Calculating</i></span>
			&nbsp;&nbsp;&nbsp;&nbsp;Total No of Men : <span id="total_men"><i>Calculating</i></span>
			&nbsp;&nbsp;&nbsp;&nbsp;Total Men Rate : <span id="total_men_rate"><i>Calculating</i></span>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Women hrs : <span id="total_women_hrs"><i>Calculating</i></span>
			&nbsp;&nbsp;&nbsp;&nbsp;Total No of Women : <span id="total_women"><i>Calculating</i></span>
			&nbsp;&nbsp;&nbsp;&nbsp;Total Women Rate : <span id="total_women_rate"><i>Calculating</i></span>


			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Mason hrs : <span id="total_mason_hrs"><i>Calculating</i></span>
			&nbsp;&nbsp;&nbsp;&nbsp;Total No of Mason : <span id="total_mason"><i>Calculating</i></span>
			&nbsp;&nbsp;&nbsp;&nbsp;Total Mason Rate : <span id="total_mason_rate"><i>Calculating</i></span>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Amount : <span id="grand_total"><i>Calculating</i></span>

			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>

			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_man_power_estimate_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process" onchange="this.form.submit();">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>


			  <input type="submit" name="file_search_submit" />
			  </form>

            </div>

			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
				    <th>Process</th>
					<th>Task Name</th>
					<th>No of Men</th>
					<th>No Of Women</th>
					<th>No of Mason</th>
					<th>No of Others</th>
					<th>Total</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
					<th colspan="3" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_man_power_estimate_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$man_power_men_rate = 0;
					$man_power_women_rate = 0;
					$man_power_mason_rate = 0;

					$project_man_power_rate_search_data = array("active"=>'1',"power_type_id"=>'1');
					$manpower_men_rate_list = i_get_project_man_power_max_rate_master($project_man_power_rate_search_data);
					if($manpower_men_rate_list["status"] == SUCCESS)
					{
						$manpower_men_rate_list_data = $manpower_men_rate_list["data"];
						$man_power_men_rate = $manpower_men_rate_list_data[0]["max_rate"];

					}
					$project_man_power_rate_search_data = array("active"=>'1',"power_type_id"=>'2');
					$manpower_women_rate_list = i_get_project_man_power_max_rate_master($project_man_power_rate_search_data);
					if($manpower_women_rate_list["status"] == SUCCESS)
					{
						$manpower_women_rate_list_data = $manpower_women_rate_list["data"];
						$man_power_women_rate = $manpower_women_rate_list_data[0]["max_rate"];

					}

					$project_man_power_rate_search_data = array("active"=>'1',"power_type_id"=>'3');
					$manpower_mason_rate_list = i_get_project_man_power_max_rate_master($project_man_power_rate_search_data);
					if($manpower_mason_rate_list["status"] == SUCCESS)
					{
						$manpower_mason_rate_list_data = $manpower_mason_rate_list["data"];
						$man_power_mason_rate = $manpower_mason_rate_list_data[0]["max_rate"];

					}
					$total_men_hrs = 0;
					$total_womrn_hrs = 0;
					$total_mason_hrs = 0;

					for($count = 0; $count < count($project_man_power_estimate_list_data); $count++)
					{
						$sl_no++;

						$total_men_cost = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_men"] * $man_power_men_rate;
						$total_women_cost = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_women"] * $man_power_women_rate;
						$total_mason_cost = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_mason"] * $man_power_mason_rate;
						$total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;

						$men_hrs = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_men"];
						$women_hrs = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_women"];
						$mason_hrs = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_mason"];


						$no_of_men = $men_hrs/8;
						$no_of_women = $women_hrs/8;
						$no_of_mason = $mason_hrs/8;

						//Total No of Man hours
						$total_men_hrs = $total_men_hrs  + $men_hrs ;
						$total_women_hrs = $total_women_hrs  + $women_hrs ;
						$total_mason_hrs = $total_mason_hrs  + $mason_hrs ;

						//Total no of Man
						$total_no_of_men = $total_men_hrs/8;
						$total_no_of_women = $total_women_hrs/8;
						$total_no_of_mason = $total_mason_hrs/8;

						$total_men_rate = $total_men_hrs * $man_power_men_rate;
						$total_women_rate = $total_women_hrs * $man_power_women_rate;
						$total_mason_rate = $total_mason_hrs * $man_power_mason_rate;

						 $grand_total_cost = $total_men_rate + $total_women_rate + $total_mason_rate;


					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_task_master_name"]; ?></td>

					<td><?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_men"]; ?>&nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_men_rate ;?>
					  Men :<?php echo $no_of_men;?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_women"]; ?>&nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_women_rate ;?> Women :<?php echo $no_of_women;?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_mason"]; ?>&nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_mason_rate ;?> Mason :<?php echo $no_of_mason ;?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_others"]; ?></td>

					<td><?php echo round($total_cost); ?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_remarks"]; ?></td>
					<td><?php echo $project_man_power_estimate_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_man_power_estimate_list_data[$count][
					"project_man_power_estimate_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_man_power_estimate('<?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($project_man_power_estimate_list_data[$count]["project_man_power_estimate_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)) { ?> <a href="#" onclick="return go_to_project_delete_man_power_estimate('<?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_id"]; ?>');">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if(($edit_perms_list['status'] == SUCCESS) && ($project_man_power_estimate_list_data[$count]["project_man_power_estimate_active"] != "2")){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_plan_cancelation('<?php echo $project_man_power_estimate_list_data[$count]["project_man_power_estimate_task_id"]; ?>','manpower');">Cancel Plan </a><?php } ?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
			   <script>
			  document.getElementById('total_men_hrs').innerHTML = '<?php echo $total_men_hrs; ?>';
			  document.getElementById('total_women_hrs').innerHTML = <?php echo $total_women_hrs; ?>;
			  document.getElementById('total_mason_hrs').innerHTML = <?php echo $total_mason_hrs; ?>;
			  document.getElementById('total_men').innerHTML = <?php echo $total_no_of_men; ?>;
			  document.getElementById('total_women').innerHTML = <?php echo $total_no_of_women; ?>;
			  document.getElementById('total_mason').innerHTML = <?php echo $total_no_of_mason; ?>;
			  document.getElementById('total_men_rate').innerHTML = <?php echo $total_men_rate; ?>;
			  document.getElementById('total_women_rate').innerHTML = <?php echo $total_women_rate; ?>;
			  document.getElementById('total_mason_rate').innerHTML = <?php echo $total_mason_rate; ?>;
			  document.getElementById('grand_total').innerHTML = <?php echo round($grand_total_cost); ?>;
			  </script>
			  <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_project_delete_man_power_estimate(estimate_id)
{
	alert("hbj");
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{
			alert("h");
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_man_power_estimate_list.php";
					}
				}
			}

			xmlhttp.open("POST","project_delete_man_power_estimate.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("estimate_id=" + estimate_id + "&action=0");
		}
	}
}
function go_to_project_edit_man_power_estimate(estimate_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_man_power_estimate.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","estimate_id");
	hiddenField1.setAttribute("value",estimate_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_cancelation(task_id,source)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_plan_add_cancelation.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
