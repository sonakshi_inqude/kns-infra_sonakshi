<?php
/**
 * @author Nitin kashyap
 * @copyright 2016
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'meetings'.DIRECTORY_SEPARATOR.'meeting_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_meetings.php');

/*
PURPOSE : To add meeting
INPUT 	: Meeting No, Department, Agenda, Doc Path, Date, Time, Venue, Details, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_meeting($department,$agenda,$doc_path,$date,$time,$venue,$details,$added_by)
{	
	$meeting_no = p_generate_meeting_id();
    $meeting_iresult = db_add_meeting($meeting_no,$department,$agenda,$doc_path,$date,$time,$venue,$details,$added_by);
	
	if($meeting_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $meeting_iresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To generate a meeting ID
INPUT 	: <No input>
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function p_generate_meeting_id()
{	
	$meeting_data = array("order"=>"meeting_no","limit_start"=>'0',"limit_count"=>'1');
    $meeting_sresult = db_get_meetings($meeting_data);	
	if($meeting_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		// Increment from the last known ID
		$meeting_no_array = explode('/',$meeting_sresult["data"][0]["meeting_no"]);
		$meeting_no = $meeting_no_array[2] + 1;;
	}
	else
	{
		// No previous meeting no. Start from 1
		$meeting_no = 1;
	}
	
	// Append company info and file info
	$meeting_no = COMPANY_NAME.'/'.MTNG_NO_TYPE.'/'.$meeting_no;
		
	return $meeting_no;
}

/*
PURPOSE : To add meeting participant
INPUT 	: Participant ID, Meeting ID, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_meeting_participant($participant_id,$meeting_id,$added_by)
{	
    $meeting_participant_iresult = db_add_meeting_participant($participant_id,$meeting_id,$added_by);
	
	if($meeting_participant_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $meeting_participant_iresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get meeting list
INPUT   : Meeting List Data Array
OUTPUT 	: Meeting List, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_meetings($meeting_data)
{	
    $meeting_sresult = db_get_meetings($meeting_data);
	
	if($meeting_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["data"]   = $meeting_sresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get meeting participant list
INPUT   : Meeting Participant List Data Array
OUTPUT 	: Meeting Participant List, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_meeting_participants($participant_data)
{	
    $participant_sresult = db_get_meeting_participants($participant_data);
	
	if($participant_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["data"]   = $participant_sresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "No meeting participant added!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get meeting MOM list
INPUT   : Meeting MOM List Data Array
OUTPUT 	: Meeting MOM List, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_meeting_mom($mom_data)
{	
    $mom_sresult = db_get_mom($mom_data);
	
	if($mom_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["data"]   = $mom_sresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add meeting MOM
INPUT   : Meeting ID, MOM Item, Description, Target Date, Task ID, Added By
OUTPUT 	: Meeting MOM List, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_meeting_mom($meeting_id,$mom_item,$description,$target_date,$task_id,$added_by)
{	
    $mom_iresult = db_add_mom_item($meeting_id,$mom_item,$description,$target_date,$task_id,$added_by);
	
	if($mom_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $mom_iresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To update meeting MOM
INPUT   : Meeting ID, MOM Item, Description, Target Date, Task ID, Added By
OUTPUT 	: Meeting MOM List, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_meeting_mom($mom_id,$mom_data)
{	
    $mom_uresult = db_update_mom($mom_id,$mom_data);
	
	if($mom_uresult['status'] == SUCCESS)
	{
		$return["data"]   = $mom_uresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
?>