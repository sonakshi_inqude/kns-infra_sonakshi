<?php
include_once('general_includes.php');
include_once('PHPMailer/class.smtp.php');
include_once('PHPMailer/class.phpmailer.php');

function create_cart($ip_address)
{
	$cart_reference = generate_random_string(cart_ref_size);
	$now            = date("Y-m-d H:i:s");
	
	$cart_unique = false;
	while(!($cart_unique))
	{
		$cart_uni = check_cart($cart_reference);
		if($cart_uni == false)
		{
			$cart_reference = generate_random_string(cart_ref_size);
			$cart_unique = false;
		}
		else
		{
			$cart_unique = true;
		}
	}

	$cart_iquery = "insert into bfc_shopping_cart (bfc_shopping_cart_cookie_reference,bfc_shopping_cart_ip_address,bfc_shopping_cart_created_date_time,bfc_shopping_cart_status) values ('$cart_reference','$ip_address','$now','0')";
	$cart_iresult = mysql_query($cart_iquery);
	
	if(FALSE == $cart_iresult)
	{
		$return["cart_id"] = -1;
		$return["cart_ref"] = -1;
	}
	else
	{
		$return["cart_id"] = mysql_insert_id();
		$return["cart_ref"] = $cart_reference;
	}
	
	return $return;
}

function check_cart($ref)
{
	$cart_ref_squery  = "select * from bfc_shopping_cart where bfc_shopping_cart_cookie_reference='$ref'";
	$cart_ref_sresult = mysql_query($cart_ref_squery);
	
	$cart_ref_scount = mysql_num_rows($cart_ref_sresult);
	
	if($cart_ref_scount > 0)
	{
		$return = false;
	}
	else
	{
		$return = true;
	}
	
	return $return;
}

function add_products_to_cart($cart_id,$cart_item_id,$cart_item_qty,$cart_item_price,$cart_item_other_details,$size,$squad,$tname,$tnum)
{
	$now = date("Y-m-d H:i:s");

	$cart_item_iquery = "insert into bfc_shopping_cart_items (bfc_shopping_cart_id,bfc_shopping_cart_item,bfc_shopping_cart_item_qty,bfc_shopping_cart_item_other_details,bfc_shopping_cart_item_price,bfc_size,bfc_squad,bfc_name,bfc_num,bfc_shopping_cart_item_status,bfc_shopping_cart_item_date_time) values ('$cart_id','$cart_item_id','$cart_item_qty','$cart_item_other_details','$cart_item_price','$size','$squad','$tname','$tnum','1','$now')";
	$cart_item_iresult = mysql_query($cart_item_iquery);
	
	if(FALSE == $cart_item_iresult)
	{
		$return = -1;
	}
	else
	{
		$return = mysql_insert_id();
	}
	
	return $return;
}

function delete_products_from_cart($cart_item_id)
{
	$cart_item_uquery = "update bfc_shopping_cart_items set bfc_shopping_cart_item_status='0' where bfc_shopping_cart_item_id='$cart_item_id'";
	$cart_item_uresult = mysql_query($cart_item_uquery);
	
	if(FALSE != $cart_item_uresult)
	{
		$return = $cart_item_uresult;
	}
	else
	{
		$return = -1;
	}
	
	return $cart_item_uquery;
}

function clear_cart($cart_id)
{
	$cart_clear_uquery = "update bfc_shopping_cart set bfc_shopping_cart_status='2' where bfc_shopping_cart_id='$cart_id'";
	$result = mysql_query($cart_clear_uquery);

	if(FALSE != $result)
	{
		$return = update_cart("",$cart_id,0,"");
	}
	else
	{
		$return = -1;
	}
	
	return $return;
}

function update_cart($bfc_shopping_cart_id,$bfc_shopping_cart_item,$item_status,$item_qty)
{
	if($bfc_shopping_cart_item != "")
	{
		$cart_item_uquery = "update bfc_shopping_cart_items set bfc_shopping_cart_item_qty='$item_qty',bfc_shopping_cart_item_status='$item_status' where bfc_shopping_cart_id='$bfc_shopping_cart_id' and bfc_shopping_cart_item='$bfc_shopping_cart_item'";
		$cart_item_uresult = mysql_query($cart_item_uquery);
	}
	else
	{
		$cart_item_uquery = "update bfc_shopping_cart_items set bfc_shopping_cart_item_status='$item_status' where bfc_shopping_cart_id='$bfc_shopping_cart_id'";
		$cart_item_uresult = mysql_query($cart_item_uquery);
	}
	
	if(FALSE == $cart_item_uresult)
	{
		$return = -1;
	}
	else
	{
		$return = 1;
	}
	
	return $return;
}

function get_cart_items($cart_reference)
{
	$cart_id = get_cart_id($cart_reference);

	$cart_squery = "select * from bfc_shopping_cart_items BSI inner join bfc_products BFP on BFP.bfc_product_id=BSI.bfc_shopping_cart_item where BSI.bfc_shopping_cart_id='$cart_id' and BSI.bfc_shopping_cart_item_status='1'";
	$cart_sresult = mysql_query($cart_squery);
	
	if(FALSE == $cart_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $cart_sresult;
	}
	
	return $return;
}

function get_cart_value($cart_reference)
{
	$cart_id = get_cart_id($cart_reference);

	$cart_squery = "select * from bfc_shopping_cart_items BSI inner join bfc_products BFP on BFP.bfc_product_id=BSI.bfc_shopping_cart_item where BSI.bfc_shopping_cart_id='$cart_id' and BSI.bfc_shopping_cart_item_status='1'";
	$cart_sresult = mysql_query($cart_squery);
	
	if(FALSE == $cart_sresult)
	{
		$return = -1;
	}
	else
	{
		$value = 0;
		while($cart_sdata = mysql_fetch_array($cart_sresult))
		{
			$value = $value + ($cart_sdata["bfc_shopping_cart_item_price"] * $cart_sdata["bfc_shopping_cart_item_qty"]);
		}
		
		$return = $value;
	}
	
	return $return;
}

function get_cart_id($cart_reference)
{
	$cart_ref_squery = "select * from bfc_shopping_cart where bfc_shopping_cart_cookie_reference='$cart_reference'";
	$cart_ref_sresult = mysql_query($cart_ref_squery);
	
	$cart_ref_sdata = mysql_fetch_array($cart_ref_sresult);
	
	return $cart_ref_sdata["bfc_shopping_cart_id"];
}

function place_order($cart_ref,$name,$email,$address,$city,$pincode,$phone,$other_cost)
{
	$now = date("Y-m-d H:i:s");
	
	// Create user
	$user_iquery = "insert into bfc_order_user (bfc_order_user_name,bfc_order_user_email,bfc_order_user_phone,bfc_order_user_address,bfc_order_user_city,bfc_order_user_pincode,bfc_order_user_date_time) values ('$name','$email','$phone','$address','$city','$pincode','$now')";
	mysql_query($user_iquery);
	
	$user_id = mysql_insert_id();
	
	if($user_id >= 1)
	{
		// Create order
		// payment mode 1 => payment gateway
		// order reference number to be passed to payment gw; its a time stamp
		$order_ref_num = date("YmdHis");
		
		$order_iquery = "insert into bfc_orders (bfc_order_reference_num,bfc_order_user,bfc_order_date_time,bfc_order_pay_mode,bfc_order_cart_reference,bfc_order_payment_reference) values ('$order_ref_num','$user_id','$now','1','$cart_ref','')";
		mysql_query($order_iquery);
		
		$order_id = mysql_insert_id();
		
		// Get cart items
		$cart_sresult = get_cart_items($cart_ref);
		
		$product_total_cost = 0;
		while($cart_sdata = mysql_fetch_array($cart_sresult))
		{
			// Insert into order item table from cart table
			$product_id = $cart_sdata["bfc_shopping_cart_item"];
			$quantity   = $cart_sdata["bfc_shopping_cart_item_qty"];
			$other_det  = $cart_sdata["bfc_shopping_cart_item_other_details"];
			$price      = $cart_sdata["bfc_shopping_cart_item_price"];
			
			$sub_total = ($price * $quantity);
			
			$product_total_cost = $product_total_cost + $sub_total;
					
			$order_item_iquery = "insert into bfc_orders_items (bfc_order_id,bfc_order_item_product_id,bfc_order_item_other_details,bfc_order_item_qty,bfc_order_item_subtotal) values ('$order_id','$product_id','$other_det','$quantity','$sub_total')";
			mysql_query($order_item_iquery);
		}
		
		// Other cost
		$shipping    = $other_cost["shipping"];
		$vat         = $other_cost["vat"];
		$service_tax = $other_cost["stax"];
		$others      = $other_cost["others"];
		
		$other_cost_iquery = "insert into bfc_orders_other_costs (bfc_order_order_id,bfc_order_other_cost_element,bfc_order_other_cost_amount) values ('$order_id','Shipping Charges','$shipping')";
		mysql_query($other_cost_iquery);
		
		$other_cost_iquery = "insert into bfc_orders_other_costs (bfc_order_order_id,bfc_order_other_cost_element,bfc_order_other_cost_amount) values ('$order_id','VAT','$vat')";
		mysql_query($other_cost_iquery);
		
		$other_cost_iquery = "insert into bfc_orders_other_costs (bfc_order_order_id,bfc_order_other_cost_element,bfc_order_other_cost_amount) values ('$order_id','Service Tax','$service_tax')";
		mysql_query($other_cost_iquery);
		
		$other_cost_iquery = "insert into bfc_orders_other_costs (bfc_order_order_id,bfc_order_other_cost_element,bfc_order_other_cost_amount) values ('$order_id','Other Charges','$others')";
		mysql_query($other_cost_iquery);
		
		// Prepare return data
		$return["amount"] = $product_total_cost + $shipping + $vat + $service_tax + $others;
		$return["order_ref"] = $order_ref_num;
	}
	else
	{
		$return["amount"] = 0;
		$return["order_ref"] = -1;
	}
	
	return $return;
}

function update_order_transaction_id($cart,$pay_ref)
{
	$order_uquery = "update bfc_orders set bfc_order_payment_reference='$pay_ref' where bfc_order_cart_reference='$cart'";
	mysql_query($order_uquery);
	
	$order_squery = "select * from bfc_orders where bfc_order_cart_reference='$cart'";
	$order_sresult = mysql_query($order_squery);
	
	$order_sdata = mysql_fetch_array($order_sresult);
	
	update_order_transaction($order_sdata["bfc_order_id"],"1","");
	update_stock($order_sdata["bfc_order_id"]);
	
	return $order_sdata["bfc_order_id"];
}

function update_order_transaction($order_id,$status,$remarks)
{
	$date_time = date("Y-m-d H:i:s");
	$order_iquery = "insert into bfc_orders_status (bfc_order_id,bfc_order_status_value,bfc_order_status_remarks,bfc_order_date_time) values ('$order_id','$status','$remarks','$date_time')";
	mysql_query($order_iquery);
}

function get_deal_data($deal_id)
{
	$deal_squery = "select * from bfc_deals where bfc_deal_id='$deal_id'";
	$deal_sresult = mysql_query($deal_squery);
	
	return $deal_sresult;
}

function get_deal_items($deal_id)
{
	$deal_items_squery = "select * from bfc_deals_items DL inner join bfc_products PR on PR.bfc_product_id=DL.bfc_deal_item where bfc_deal_deal_id='$deal_id'";
	$deal_items_sresult = mysql_query($deal_items_squery);
	
	return $deal_items_sresult;
}

function get_products($param)
{
	$type = $param[0]; // Possible Values - list, entry, filtered
	$data = $param[1]; // Possible Values - Depends on type
	
	switch($type)
	{
	case "list":
	if($data == "active")
	{
		$product_squery = "select * from bfc_products where bfc_product_active='1'";
	}	
	else
	{
		$product_squery = "select * from bfc_products";
	}
	break;
	
	case "entry":
	$product_squery = "select * from bfc_products where bfc_product_id='$data'";
	break;
	
	case "filtered":
	$sub_cat  = $data[0];
	$price    = $data[1];
	
	$product_squery_base = "select * from bfc_products";
	
	$filter_count = 0;
	if($sub_cat != "")
	{
		if($filter_count == 0)
		{
			$product_squery_where = " where bfc_product_subcategory='$sub_cat'";
		}
		else
		{
			$product_squery_where = " and bfc_product_subcategory='$sub_cat'";
		}
	}

	if($price != "")
	{
		if($filter_count == 0)
		{
			$product_squery_where = " where (bfc_product_discount - bfc_product_cost) <= '$price'";
		}
		else
		{
			$product_squery_where = " and (bfc_product_discount - bfc_product_cost) <= '$price'";
		}
	}	
	
	$product_squery = $product_squery_base.$product_squery_where." and bfc_product_active='1'";
	break;
	
	default:
	$product_squery = "select * from bfc_products where bfc_product_active='1'";
	}
	
	$product_sresult = mysql_query($product_squery);
	
	if(FALSE == $product_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $product_sresult;
	}
	
	return $return;
}

// $data to indicate active inactive in case of list, city ID in case of entry
function get_cities($type,$data)
{
	switch($type)
	{
	case "list":
	$city_squery = "select * from bfc_cities where bfc_city_active='$data'";	
	break;
	
	case "entry":
	$city_squery = "select * from bfc_cities where bfc_city_id='$data'";
	break;
	
	default:
	$city_squery = "select * from bfc_cities where bfc_city_active='$data'";	
	break;
	}
	
	$city_sresult = mysql_query($city_squery);
	if($city_sresult == FALSE)
	{
		$return = -1;
	}
	else
	{
		$return = $city_sresult;
	}
	
	return $city_sresult;
}

function get_city_name($city_id)
{
	$city_squery = "select * from bfc_cities where bfc_city_id='$city_id'";
	$city_sresult = mysql_query($city_squery);
	$city_sdata = mysql_fetch_array($city_sresult);
	
	return $city_sdata["bfc_city_name"];
}

function get_size($type,$data)
{
	switch($type)
	{
	case "list":
	$size_squery = "select * from bfc_size_stock where bfc_item='$data'";
	break;
	
	case "entry":
	$size_squery = "select * from bfc_size_stock where bfc_size_stock_id='$data'";
	break;
	
	default:
	$size_squery = "select * from bfc_size_stock where bfc_item='$data'";
	break;
	}
	
	$size_sresult = mysql_query($size_squery);
	
	if($size_sresult == FALSE)
	{
		$return = -1;
	}
	else
	{
		$return = $size_sresult;
	}
	
	return $return;
}

function get_categories($status)
{
	switch($status)
	{
	case "all":
	$cat_squery = "select * from bfc_categories";
	break;
	
	case "active":
	$cat_squery = "select * from bfc_categories where bfc_category_active='1'";
	break;
	
	case "inactive":
	$cat_squery = "select * from bfc_categories where bfc_category_active='0'";
	break;
	}
	
	$cat_sresult = mysql_query($cat_squery);
	
	if(FALSE == $cat_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $cat_sresult;
	}
	
	return $return;
}

function get_subcategories($status,$cat)
{
	switch($status)
	{
	case "all":
	$subcat_squery = "select * from bfc_subcategories where bfc_subcategory_cat='$cat'";
	break;
	
	case "active":
	$subcat_squery = "select * from bfc_subcategories where bfc_subcategory_active='1' and bfc_subcategory_cat='$cat'";
	break;
	
	case "inactive":
	$subcat_squery = "select * from bfc_subcategories where bfc_subcategory_active='0' and bfc_subcategory_cat='$cat'";
	break;
	}
	
	$subcat_sresult = mysql_query($subcat_squery);
	
	if(FALSE == $subcat_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $subcat_sresult;
	}
	
	return $return;
}

function get_featured_products($count)
{
	$feat_squery = "select * from bfc_products where bfc_product_featured='1' and bfc_product_active='1' order by bfc_product_added_date_time desc limit 0,$count";
	$feat_sresult = mysql_query($feat_squery);
	
	if(FALSE == $feat_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $feat_sresult;
	}
	
	return $return;
}

function get_latest_products($count)
{
	$latest_squery = "select * from bfc_products where bfc_product_active='1' order by bfc_product_added_date_time limit 0,$count";
	$latest_sresult = mysql_query($latest_squery);
	
	if(FALSE == $latest_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $latest_sresult;
	}
	
	return $return;
}

function get_bestsellers($count)
{
	$best_squery = "select bfc_order_item_product_id,count(bfc_order_item_product_id) as tot_orders from bfc_order_items group by bfc_order_item_product_id order by tot_orders desc limit 0,$count";
	$best_sresult = mysql_query($best_squery);
	
	if(FALSE == $best_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $best_sresult;
	}
	
	return $return;
}

function add_review($name,$product_id,$content,$date_time)
{
	$content = mysql_real_escape_string($content);
	$review_iquery = "insert into bfc_product_reviews (bfc_product_reviews_name,bfc_product_reviews_product_id,bfc_product_reviews_content,bfc_product_reviews_date_time,bfc_product_reviews_active) values ('$name','$product_id','$content','$date_time','1')";
	$review_iresult = mysql_query($review_iquery);
	
	if(FALSE == $review_iresult)
	{
		$return = -1;
	}
	else
	{
		$return = $review_iresult;
	}
	
	return $return;
}

function get_reviews($product_id)
{
	$review_squery = "select * from bfc_product_reviews where bfc_product_reviews_product_id='$product_id'";
	
	$review_sresult = mysql_query($review_squery);
	
	if(FALSE == $review_sresult)
	{
		$return = -1;
	}
	else
	{
		$return = $review_sresult;
	}
	
	return $return;
}

function send_email($to,$subject,$message)
{
	$to    = $to;
	$from  = 'bengalurufc1@gmail.com';
	$fName = 'BengaluruFC';
	$lName = 'Online Shop';
	$subject = $subject;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->Host       = "smtp.gmail.com"; // SMTP server
	$mail->SMTPDebug  = 0;                       // enables SMTP debug information (for testing)
	$mail->SMTPAuth   = true;                    // enable SMTP authentication
	$mail->Port       = 465;                     // set the SMTP port for the OUTLOOK server
	$mail->SMTPSecure = 'ssl';
	$mail->Username   = "bengalurufc1@gmail.com";    // SMTP account username
	$mail->Password   = "BengaluruFc123";
	$mail->SetFrom($from, $fName.' '.$lName);
	$mail->Subject = $subject;
	$mail->AddAddress($to);
	$mail->MsgHTML($message);

	if(!$mail->Send()) 
	{
		// Log crash
		$return = $mail->ErrorInfo;
	}
	else
	{
		$return = 1;
	}
	
	return $return;
}

/* Private Functions */
function generate_random_string($size)
{
	$characters = 'bcdfghjkmnpqrstvwxz0123456789';
	
	$random_string = '';
	for ($i = 0; $i < $size; $i++) 
	{
		$random_string = $random_string.$characters[rand(0, strlen($characters) - 1)];
	}
	
	return $random_string;
}

function get_product_name($product_id)
{
	$pname_squery = "SELECT bfc_product_name FROM bfc_products WHERE bfc_product_id = '$product_id' ";
	$pname_res_squery = mysql_query($pname_squery);
	
	if(FALSE == $pname_res_squery)
	{
		$return = -1;
	}
	else
	{
		$return = $pname_res_squery;
	}
	
	return $return;
}

function get_product_size($size_id)
{
	$pname_squery = " SELECT bfc_size FROM bfc_size_stock WHERE bfc_size_stock_id = '$size_id' ";
	$pname_res_squery = mysql_query($pname_squery);
	
	if(FALSE == $pname_res_squery)
	{
		$return = -1;
	}
	else
	{
		$return = $pname_res_squery;
	}
	
	return $return;
}

function get_city_name2($city_id)
{
	$pname_squery = " SELECT * FROM bfc_cities WHERE bfc_city_id = '$city_id' ";
	$pname_res_squery = mysql_query($pname_squery);
	
	if(FALSE == $pname_res_squery)
	{
		$return = -1;
	}
	else
	{
		$return = $pname_res_squery;
	}
	
	return $return;
}

function update_stock($order_id)
{
	$order_squery  = "select * from bfc_shopping_cart_items BSC inner join bfc_shopping_cart BC on BC.bfc_shopping_cart_id = BSC.bfc_shopping_cart_id inner join bfc_orders BO on BO.bfc_order_cart_reference=BC.bfc_shopping_cart_cookie_reference where BO.bfc_order_id='$order_id' and BSC.bfc_shopping_cart_item_status='1'";
	$order_sresult = mysql_query($order_squery);
	
	while($order_sdata = mysql_fetch_array($order_sresult))
	{
		if($order_sdata["bfc_size"] != 0)
		{
			$size = $order_sdata["bfc_size"];
			$qty = $order_sdata[""];
			$stock_uquery = "update bfc_size_stock set bfc_stock = bfc_stock - $qty where bfc_size_stock_id='$size'";
			mysql_query($stock_uquery);
		}	
	}
}

// To encrypt password
function encrypt_password($password)
{
	$salt = "fcblore";
	$enc_password = md5($password.$salt);
	
	return $enc_password;
}

/* ~~ Karthik ~~ 2/11/2014 ~~
*  Admin Login
*  parms - admin email, admin password
*  return - email and password
*/
function check_login($name,$password)
{
	//$enc_pw = encrypt_password($password);

	$check_squery = "SELECT * FROM bfc_admin WHERE email = '$name' and password ='$password'";
	$exec_check_squery = mysql_query($check_squery);
	return $exec_check_squery;
}

/* ~~ Karthik ~~  2/11/2014 ~~
*  add new user to admin table
*  params - name, email, password
*  return success/failure msg
*/
function add_user($name,$email,$password)
{
	$enc_pw = encrypt_password($password);

	$add_user_iquery = "INSERT INTO bfc_admin(name, password, email ) VALUES ('$name','$password','$email')";
	$exec_add_user_iquery = mysql_query($add_user_iquery);
	
	if(FALSE == $exec_add_user_iquery)
	{
		$return = "Internal error! Please try again.";
	}
	else
	{
		$return = "New user added successfully.";
	}
	
	return $return;
}

/* ~~ Karthik ~~  2/18/2014 ~~
*  add new product
*  params - prod_name, avail, stock_size, prod_price, up_image
*  return success/failure msg
*/
function add_new_product($bfc_product_name,$bfc_product_subcategory,$bfc_product_cost,$bfc_product_discount,$bfc_product_description,$bfc_product_size_cust,$bfc_product_image_path,$bfc_product_active,$bfc_product_featured,$bfc_product_added_date_time)
{
	
	$add_new_product_iquery = "INSERT INTO bfc_products(bfc_product_name, bfc_product_subcategory, bfc_product_cost, bfc_product_discount, bfc_product_description, bfc_product_size_cust, bfc_product_image_path, bfc_product_active, bfc_product_featured, bfc_product_added_date_time) VALUES ('$bfc_product_name','$bfc_product_subcategory','$bfc_product_cost','$bfc_product_discount','$bfc_product_description','$bfc_product_size_cust','$bfc_product_image_path','$bfc_product_active','$bfc_product_featured','$bfc_product_added_date_time')";
	$exec_add_new_product_iquery = mysql_query($add_new_product_iquery);
	
	if(FALSE == $exec_add_new_product_iquery)
	{
		$return = "Internal error! Please try again.";
	}
	else
	{
		$return = "New product added successfully.";
	}
	
	return $return;
}

/* ~~ Karthik ~~  2/21/2014 ~~
*  To update Stock
*  params - size, item_id, stock
*  return success/failure msg
*/
function updatestock($bfc_size, $bfc_item, $bfc_stock)
{
	
	$check_stock_squery = "SELECT * FROM bfc_size_stock WHERE bfc_item = $bfc_item";
	$check_stock_squery_exec = mysql_query($check_stock_squery);
	
	if(FALSE == $check_stock_squery_exec)
	{
		$return = "Internal error! Please try again.";
	}
	else
	{
		$stock_count = mysql_num_rows($check_stock_squery_exec);
		if($stock_count > 0)
			{
				$update_stock_uquery  = "UPDATE bfc_size_stock SET bfc_stock = '$bfc_stock' WHERE bfc_item = '$bfc_item' and bfc_size = '$bfc_size' ";
				$update_stock_uquery_exec = mysql_query($update_stock_uquery);
				
				if(FALSE == $update_stock_uquery_exec)
				{
					$return = "Stock update Failed. Please try again.";
				}
				else
				{
					$return = "Stock Updated Successfully!";
				}
				
			}
			else
			{	
				$insert_stock_iquery  = "INSERT INTO bfc_size_stock(bfc_size, bfc_item, bfc_stock) VALUES ('$bfc_size', '$bfc_item', '$bfc_stock')";
				$insert_stock_uquery_exec = mysql_query($insert_stock_iquery);
				
				if(FALSE == $insert_stock_uquery_exec)
				{
					$return = "Stock insert Failed. Please try again.";
				}
				else
				{
					$return = "Stock Inserted Successfully!";
				}
			}
	}
	
	return $return;
}


?>