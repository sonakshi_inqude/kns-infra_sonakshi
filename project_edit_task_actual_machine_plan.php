<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('PROJECT_UPDATE_ACTUAL_MACHINE_DETAILS_FUNC_ID', '340');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_UPDATE_ACTUAL_MACHINE_DETAILS_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_UPDATE_ACTUAL_MACHINE_DETAILS_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_UPDATE_ACTUAL_MACHINE_DETAILS_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_UPDATE_ACTUAL_MACHINE_DETAILS_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_UPDATE_ACTUAL_MACHINE_DETAILS_FUNC_ID, '6', '1');

    if (isset($_GET["plan_id"])) {
        $plan_id = $_GET["plan_id"];
    } else {
        $plan_id = $_POST["hd_plan_id"];
    }

    if (isset($_GET['task_id'])) {
        $task_id = $_GET['task_id'];
    } else {
        $task_id = $_POST["hd_task_id"];
    }

    if (isset($_REQUEST['road_id'])) {
        $road_id = $_REQUEST['road_id'];
    } else {
        $road_id = "";
    }
    $allow_edit = false;

    // Machine Actual data
    $actual_machine_plan_search_data = array("plan_id"=>$plan_id,"task_id"=>$task_id);
    $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
    if ($actual_machine_plan_list["status"] == SUCCESS) {
        $actual_machine_plan_list_data = $actual_machine_plan_list["data"];
        $actual_machine_completion = $actual_machine_plan_list_data[count($actual_machine_plan_search_data) - 1]['project_task_actual_machine_completion'];
        $m_percenatge = $actual_machine_plan_list_data[0]["project_task_actual_machine_completion"];
        $work_type = $actual_machine_plan_list_data[0]["project_task_actual_machine_work_type"];
        $process_master_id = $actual_machine_plan_list_data[0]["project_process_master_id"];
        $start_date = date("Y-m-d h:i:s", strtotime($actual_machine_plan_list_data[0]["project_task_actual_machine_plan_start_date_time"]));
    } else {
        $alert = $actual_machine_plan_list["data"];
        $alert_type = 0;
    }
    //Get Regular Measurement
    $get_details_data = i_get_details('Regular', $task_id, $road_id);
    $reg_mp_msmrt = $get_details_data["mp_msmrt"];
    $planned_msmrt = $get_details_data["planned_msmrt"];
    $uom = $get_details_data["uom"];
    $reg_overall_msmrt = $get_details_data["overall_msmrt"];
    $road_name = $get_details_data["road"];
    $reg_mc_msmrt = $get_details_data["mc_msmrt"];
    $reg_cw_msmrt = $get_details_data["cw_msmrt"];
    $project = $get_details_data["project"];
    $process_name = $get_details_data["process"];
    $task = $get_details_data["task"];

    //Get Rework Measurement
    $get_details_data = i_get_details('Rework', $task_id, $road_id);
    $rework_mp_msmrt = $get_details_data["mp_msmrt"];
    $rework_mc_msmrt = $get_details_data["mc_msmrt"];
    $rework_cw_msmrt = $get_details_data["cw_msmrt"];
    $rework_overall_msmrt = $get_details_data["overall_msmrt"];
    // Capture the form data
    if (isset($_POST["edit_project_task_actual_machine_submit"])) {
        $plan_id             = $_POST["hd_plan_id"];
        $work_type					 = $_POST["hd_work_type"];
        $task_id             = $_POST["hd_task_id"];
        $machine_id          = $_POST["hd_machine_id"];
        $start_date_time     = $_POST['hd_start_date_time'];
        $end_date_time       = date('Y-m-d H:i:s', strtotime($_POST['end_date_time']));
        $off_time	           = $_POST['num_off_time'];
        $fuel_cost		       = $_POST['num_fuel_cost'];
        $bata			           = $_POST['num_bata'];
        $additional_cost     = $_POST['additional_cost'];
        $msmrt	             = $_POST['num_msmrt'];
        $completion_percent	 = $_POST["completion_percent"];
        $remarks		         = $_POST['txt_remarks'];

        if ($completion_percent == 100) {
            $actual_end_date = date("Y-m-d H:i:s");
        } else {
            $actual_end_date = "0000-00-00";
        }

        //Get Machine List
        $actual_machine_plan_search_data = array("plan_id"=>$plan_id,"task_id"=>$task_id,"work_type"=>"Regular");
        $machine_list = i_get_machine_planning_list($actual_machine_plan_search_data);
        if ($machine_list["status"] == SUCCESS) {
            $mc_percenatge = $machine_list["data"][0]["project_task_actual_machine_completion"];
        } else {
            $mc_percenatge = "";
        }
        $project_process_task_search_data = array("active"=>'1',"task_id"=>$task_id);
        $project_task_list = i_get_project_process_task($project_process_task_search_data);
        if ($project_task_list["status"] == SUCCESS) {
            $project_task_list_data = $project_task_list["data"];
            $task_manpower_completion = $project_task_list_data[0]["project_process_task_completion"];
            $task_machine_completion = $project_task_list_data[0]["project_process_machine_task_completion"];
            $task_contract_completion = $project_task_list_data[0]["project_process_contract_task_completion"];
        } else {
            $task_manpower_completion = 0;
            $task_machine_completion = 0;
            $task_contract_completion = 0;
        }
        if (($edit_perms_list["status"] == SUCCESS)) {
            if (($process_master_id == 32) || ($process_master_id == 33)) {
                if (($completion_percent > 0) && ($completion_percent <= 99)) {
                    $allow_edit = true;
                }
            }
            // else {
            //     var_dump($task_machine_completion);
            //     if (($work_type == "Regular") && ($edit_perms_list["status"] == SUCCESS)) {
            //         if (($completion_percent > 0) && ($completion_percent > $task_machine_completion) && ($completion_percent <= 100)) {
            //             $allow_edit = true;
            //         }
            //     } elseif (($work_type == "Rework") && ($edit_perms_list["status"] == SUCCESS)) {
            //         if (($completion_percent > 0) && ($completion_percent <= 100)) {
            //             $allow_edit = true;
            //         }
            //     } else {
            //         $allow_edit = false;
            //     }
            // }
        } else {
            $allow_edit = true;
        }

        // Check for mandatory fields
        if ($allow_edit = true) {
            if (strtotime($end_date_time) <= strtotime(date('Y-m-d H:i:s'))) {
                if (strtotime($end_date_time) >= strtotime($start_date_time)) {
                    if ($work_type == "Regular") {
                        //Update Regular completion percentage
                        $actual_machine_plan_update_data = array("start_date_time"=>$start_date_time,"end_date_time"=>$end_date_time,"additional_cost"=>$additional_cost,"msmrt"=>$msmrt,'completion'=>$completion_percent,"off_time"=>$off_time,"machine_issued_fuel"=>$fuel_cost,"machine_id"=>$machine_id,"remarks"=>$remarks,"machine_bata"=>$bata);
                        $machine_iresult = i_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data);

                        if ($machine_iresult["status"] == SUCCESS) {
                            $alert_type = 1;
                            $project_process_task_update_data = array("machine_completion"=>$completion_percent,"actual_end_date"=>$actual_end_date);
                            $task_update_list = i_update_project_process_task($task_id, $project_process_task_update_data);
                            header("location:project_actual_machine_planning_list.php");
                        } else {
                            $alert = "Project Machine Already Exists";
                            $alert_type = 0;
                        }
                    } elseif ($work_type == "Rework") {
                        //Update Rework completion percentage
                        $actual_machine_plan_update_data = array("start_date_time"=>$start_date_time,"end_date_time"=>$end_date_time,"additional_cost"=>$additional_cost,'msmrt'=>$msmrt,'rework_completion'=>$completion_percent,"off_time"=>$off_time,"machine_issued_fuel"=>$fuel_cost,"machine_id"=>$machine_id,"remarks"=>$remarks,"machine_bata"=>$bata);
                        $machine_iresult = i_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data);

                        if ($machine_iresult["status"] == SUCCESS) {
                            $alert_type = 1;
                            header("location:project_actual_machine_planning_list.php");
                        } else {
                            $alert = "Project Machine Already Exists";
                            $alert_type = 0;
                        }
                    }

                    $alert = $machine_iresult["data"];
                } else {
                    $alert = "Machine end time cannot be lesser than start time";
                    $alert_type = 0;
                }
            } else {
                $alert = "Machine end time cannot be greater than current time";
                $alert_type = 0;
            }
        } else {
            $alert = "Please Enter Completion Percentage";
            $alert_type = 0;
        }
    }



    // Task Master data
    // $project_task_master_search_data = array("active"=>'1');
    // $task_master_list = i_get_project_task_master($project_task_master_search_data);
    // if ($task_master_list["status"] == SUCCESS) {
    //     $task_master_list_data = $task_master_list["data"];
    //     $task_name  = $task_master_list_data[0]["project_task_master_name"];
    // } else {
    //     $alert = $task_master_list["data"];
    //     $alert_type = 0;
    // }

    // Get project_machine_vendor_master modes already added
    $project_machine_vendor_master_search_data = array("active"=>'1');
    $project_machine_vendor_master_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
    if ($project_machine_vendor_master_list['status'] == SUCCESS) {
        $project_machine_vendor_master_list_data = $project_machine_vendor_master_list['data'];
    } else {
        $alert = $project_machine_vendor_master_list["data"];
        $alert_type = 0;
    }


    // Get Project Machine Master modes already added
    $project_machine_master_search_data = array("active"=>'1');
    $project_machine_master_list = i_get_project_machine_master($project_machine_master_search_data);
    if ($project_machine_master_list['status'] == SUCCESS) {
        $project_machine_master_list_data = $project_machine_master_list["data"];
    } else {
        $alert = $project_machine_master_list["data"];
        $alert_type = 0;
    }

    // Get list of task plans for this process plan
    $task_manpower_completion = 0;
    $task_machine_completion = 0;
    $task_contract_completion = 0;
    $avg_count = 0;
    $project_process_task_search_data = array("active"=>'1',"task_id"=>$task_id);
    $project_process_task_list = i_get_project_process_task($project_process_task_search_data);
    if ($project_process_task_list["status"] == SUCCESS) {
        $project_process_task_list_data = $project_process_task_list["data"];
        $task_manpower_completion = $project_process_task_list_data[0]["project_process_task_completion"];
        $task_machine_completion = $project_process_task_list_data[0]["project_process_machine_task_completion"];
        $task_contract_completion = $project_process_task_list_data[0]["project_process_contract_task_completion"];

        $project_process_task_list_data = $project_process_task_list["data"];
        //$no_of_task = count($project_process_task_list_data);

        // Actual Manpower data
        $man_power_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
        $man_power_list1 = i_get_man_power_list($man_power_search_data);
        if ($man_power_list1["status"] == SUCCESS) {
            $avg_count = $avg_count + 1;
        } else {
            // Get Project Man Power Estimate modes already added
            $project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$task_id);
            $project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
            if ($project_man_power_estimate_list['status'] == SUCCESS) {
                $avg_count = $avg_count + 1;
            } else {
                //Do not Inncrement
            }
        }

        // Machine Planning data
        $actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
        $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
        if ($actual_machine_plan_list["status"] == SUCCESS) {
            $avg_count = $avg_count + 1;
        } else {
            //Machine Planning
            $project_machine_planning_search_data = array("active"=>'1',"task_id"=>$task_id);
            $project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
            if ($project_machine_planning_list["status"] == SUCCESS) {
                $avg_count = $avg_count + 1;
            } else {
                //Do not Increment
            }
        }
        // Actual Contract  Data
        $project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
        $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
        if ($project_task_boq_actual_list['status'] == SUCCESS) {
            $avg_count = $avg_count + 1;
        } else {
            $project_task_boq_search_data = array("active"=>'1',"task_id"=>$task_id);
            $project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
            if ($project_task_boq_list['status'] == SUCCESS) {
                $avg_count = $avg_count + 1;
            } else {
                ///Do not Increment
            }
        }
    } else {
        //
    }
    $overall_percentage = $task_manpower_completion + $task_machine_completion +$task_contract_completion ;
    if ($avg_count != 0) {
        $avg_overall_percentage = $overall_percentage/$avg_count;
    } else {
        $avg_overall_percentage = 0;
    }
    $overall_pending_percentage = 100 - $avg_overall_percentage;
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Edit Project Task Actual Machine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header custom-header">
	      				<i class="icon-user"></i>
                <h3>
									<span class="header-label">Project :</span> <?= $project; ?>
									<span class="header-label">Process :</span> <?= $process_name; ?>
									<span class="header-label">Task:</span>  <?= $task; ?>
								</h3>
								<h3 style="margin-left:25px">
									<span class="header-label">Road:</span>  <?= $road_name; ?>
									<span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
									<span class="header-label">UOM</span> <?= $uom; ?>
								</h3>
								<h3 >
									<span class="header-label">Regular</span> <?= $reg_overall_msmrt; ?> (MP : <?= $reg_mp_msmrt; ?>  MC : <?= $reg_mc_msmrt; ?> CW : <?= $reg_cw_msmrt; ?>)
									<span class="header-label">Rework</span> <?= $rework_overall_msmrt; ?> (MP : <?= $rework_mp_msmrt; ?>   MC : <?= $rework_mc_msmrt; ?> CW : <?= $rework_cw_msmrt; ?>)
                								</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Task Actual Machine</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
                                if ($alert_type == 0) { // Failure?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>

								<?php
                                if ($alert_type == 1) { // Success?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_edit_task_actual_machine_plan_form" class="form-horizontal" method="post" action="project_edit_task_actual_machine_plan.php">
								<input type="hidden" name="hd_plan_id" value="<?php echo $plan_id; ?>" />
								<input type="hidden" name="hd_work_type" value="<?php echo $work_type; ?>" />
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_machine_id" value="<?php echo $actual_machine_plan_list_data[0]["project_task_machine_id"]; ?>" />
								<input type="hidden" name="hd_start_date_time" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_plan_start_date_time"]; ?>" />
									<fieldset>

											<?php
                              $start_date = date("Y-m-d h:i:s", strtotime($actual_machine_plan_list_data[0]["project_task_actual_machine_plan_start_date_time"]));

                              if ($actual_machine_plan_list_data[0]["project_task_actual_machine_plan_end_date_time"] == '0000-00-00 00:00:00') {
                                  $end_day = date('Y-m-d H:i:s');
                              } else {
                                  $end_day = $actual_machine_plan_list_data[0]["project_task_actual_machine_plan_end_date_time"];
                              }

                              $end_date = date("Y-m-d H:i:s", strtotime($end_day));

                              $start_date_array = explode(" ", $start_date);
                              $start_date_array_date = $start_date_array[0];
                              $start_date_array_time = $start_date_array[1];
                              $s_date = $start_date_array_date .'T'.$start_date_array_time;

                              $end_date_array = explode(" ", $end_date);
                              $end_date_array_date = $end_date_array[0];
                              $end_date_array_time = $end_date_array[1];
                              $e_date = $end_date_array_date .'T'.$end_date_array_time;

                              if ($actual_machine_plan_list_data[0]["project_task_actual_machine_plan_end_date_time"] == '0000-00-00 00:00:00') {
                                  $e_date = '';
                              }
                              ?>


												<div class="control-group">
											<label class="control-label" for="work_type">Work Type</label>
											<div class="controls">
												<input style="font-size:20px;" type="text" name="work_type" placeholder="Bata" min="0" step="0.01" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_work_type"] ;?>" disabled>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
											<div class="control-group">
											<label class="control-label" for="ddl_machine_id">Machine*</label>
											<div class="controls">
												<select class="span6" name="ddl_machine_id" disabled required>
												<option>- - Select Machine - -</option>
												<?php
                                                for ($count = 0; $count < count($project_machine_master_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_machine_master_list_data[$count]["project_machine_master_id"]; ?>" <?php if ($project_machine_master_list_data[$count]["project_machine_master_id"] == $actual_machine_plan_list_data[0]["project_task_machine_id"]) {
                                                        ?> selected="selected" <?php
                                                    } ?>><?php echo $project_machine_master_list_data[$count]["project_machine_master_name"].' - '.$project_machine_master_list_data[$count]["project_machine_master_id_number"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="start_date_time">Start Date Time</label>
											<div class="controls">
												<input type="datetime-local" name="start_date_time" disabled placeholder="Start Date" value="<?php echo $s_date; ?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="end_date_time">End Date Time</label>
											<div class="controls">
												<input type="datetime-local" name="end_date_time" placeholder="End Date" value="<?php echo $e_date ;?>" <?php if (($actual_machine_plan_list_data[0]["project_task_actual_machine_plan_end_date_time"] != '0000-00-00 00:00:00') && ($delete_perms_list['status'] != SUCCESS)) {
                                                    ?> readOnly <?php
                                                } ?>>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="off_time">Off Time (in mins)</label>
											<div class="controls">
												<input type="number" name="num_off_time" placeholder="Off duration" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_plan_off_time"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_bata">Bata</label>
											<div class="controls">
												<input type="number" name="num_bata" placeholder="Bata" min="0" step="0.01" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_bata"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="fuel_cost">Fuel Cost</label>
											<div class="controls">
												<input type="number" name="num_fuel_cost" min="0" step="0.01" placeholder="Cost" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_issued_fuel"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="additional_cost">Additional Cost</label>
											<div class="controls">
												<input type="number" name="additional_cost" placeholder="Cost" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_plan_additional_cost"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_msmrt">Measurment</label>
											<div class="controls">
												<input type="number" name="num_msmrt" id="num_msmrt" placeholder="Measurement" value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_msmrt"] ;?>" onchange="return check_validity('<?php echo $reg_overall_msmrt ;?>','<?php echo $planned_msmrt ; ?>','<?php echo $work_type ;?>','<?php echo $rework_mc_msmrt ;?>');">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

                    <input type="hidden" name="completion_percent"  id="completion_percent" value="" />
										<?php if ($edit_perms_list["status"] == SUCCESS) {
                                                    ?>
										<div class="control-group">
											<label class="control-label" for="completion_percent1">Completion Percent</label>
											<div class="controls">
												<input type="number" name="completion_percent1" id="completion_percent1" placeholder="Percent" required disabled value="<?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_completion"] ; ?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
										<?php
                                                }
                                        ?>

										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" rows="4" cols="50" placeholder="Remarks"><?php echo $actual_machine_plan_list_data[0]["project_task_actual_machine_plan_remarks"] ;?></textarea>
											</div> <!-- /controls
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" id="edit_project_task_actual_machine_submit" name="edit_project_task_actual_machine_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function check_validity(overall_msmrt,planned_msmrt,work_type,rework_msmrt)
{
    if(!planned_msmrt || planned_msmrt == 0) {
      document.getElementById('edit_project_task_actual_machine_submit').disabled = true;
      alert('Kindly plan the measurement');
      return false;
    }
    var cur_msmrt = parseFloat(document.getElementById('num_msmrt').value);
    var total_msmrt = parseFloat(overall_msmrt);
    var planned_msmrt  = parseFloat(planned_msmrt);

    if(cur_msmrt == 0) {
      alert("Enter valid measurement");
      document.getElementById('edit_project_task_actual_machine_submit').disabled = true;
    }

    if((cur_msmrt + total_msmrt) > planned_msmrt)
    {
        document.getElementById('num_msmrt').value = 0;
          alert('Actual msmrt can not be greater than planned');
    }
    else if ((work_type == "Rework") && (cur_msmrt > total_msmrt)) {
      document.getElementById('num_msmrt').value = 0;
      alert('Rework msmrt can not be greater than completed');
    }
		else
		{
			auto_percentage = ((cur_msmrt + total_msmrt) / planned_msmrt) * 100 ;
			 document.getElementById('completion_percent').value = parseFloat(auto_percentage).toFixed(2);
			 document.getElementById('completion_percent1').value = parseFloat(auto_percentage).toFixed(2);
       document.getElementById('edit_project_task_actual_machine_submit').disabled = false;
		}

}

</script>

  </body>

</html>
