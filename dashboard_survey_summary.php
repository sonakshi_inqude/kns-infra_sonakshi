<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 13th Jan 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$process_master_array = array();
	// Get process master
	$survey_process_master_search_data = array('active'=>'1');
	$process_master_sresult = i_get_survey_process_master($survey_process_master_search_data);
	if($process_master_sresult['status'] == SUCCESS)
	{
		$process_master_list_data = $process_master_sresult['data'];
		for($count = 0; $count < count($process_master_list_data); $count++)
		{
			$process_master_array[$process_master_list_data[$count]['survey_process_master_id']]['process_name'] = $process_master_list_data[$count]['survey_process_master_name'];
			$process_master_array[$process_master_list_data[$count]['survey_process_master_id']]['process_id'] = $process_master_list_data[$count]['survey_process_master_id'];
			$process_master_array[$process_master_list_data[$count]['survey_process_master_id']]['pcount'] = 0;
		}
	}
		
	// Get survey details
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list['status'] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list['data'];
		for($count = 0; $count < count($survey_details_list_data); $count++)
		{			
			// Get list of incomplete processes in ascending order
			$survey_process_search_data = array('survey_id'=>$survey_details_list_data[$count]['survey_details_id'],'process_end_date'=>'0000-00-00','active'=>'1');
			$survey_process_list = i_get_survey_process($survey_process_search_data);			
			if($survey_process_list['status'] == SUCCESS)
			{								
				$process_master_array[$survey_process_list['data'][0]['process_id']]['pcount'] = $process_master_array[$process_master_list_data[$count]['survey_process_master_id']]['count'] + 1;
			}
		}
	}
		
	array_sort_conditional($process_master_array,'sort_on_number') ;
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Survey - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
	
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span6">                        
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Survey - Dashboard</h3>
                            </div>
                            <!-- /widget-header -->							
                            <div class="widget-content">
                                <!-- Tables to go here -->
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>									
									<th>PROCESS</th>
									<th>NO OF FILES</th>									
								  </tr>								  
								</thead>
								<tbody>
								<?php 
								if($survey_details_list['status'] == SUCCESS)
								{									
									for($count = 0; $count < count($process_master_array); $count++)
									{
								?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $process_master_array[$count]['process_name']; ?></td>
									<td style="word-wrap:break-word;"><a href="survey_details_list.php?process_master=<?php echo $process_master_array[$count]['process_id']; ?>"><?php echo $process_master_array[$count]['pcount']; ?></a></td>
									</tr>
									<?php
									}
								}
								else
								{
									?>
									<tr>
									<td colspan="2">No survey added</td>
									</tr>
									<?php
								}
								?>
								</tbody>
								</table>
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    <div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2016
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/excanvas.min.js"></script>
    <script src="js/chart.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/base.js"></script>
<script>
function go_to_enquiry_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_enquiry_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date_entry_form");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date_entry_form");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","enquiry_search_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_booking_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_overall_report.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","search_report_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
</body>
</html>