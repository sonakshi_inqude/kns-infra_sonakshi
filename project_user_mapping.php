<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 24-Aug-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */

    // Get list of Department*
    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
        $alert_type = 0;
    } ?>
	<script>
		console.log('dep ', <?= json_encode($department_list_data); ?>);
	</script>
	<?php


    // Get Users modes already added
    $user_list = i_get_user_list('', '', '', '', '1');
    if ($user_list['status'] == SUCCESS) {
        $user_list_data = $user_list['data'];
    } else {
        $alert = $user_list["data"];
        $alert_type = 0;
    }

    // Get Project Management Master modes already added
    $project_management_master_search_data = array("active"=>'1', "status"=>"all");
    $project_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_master_list['status'] == SUCCESS) {
        $project_master_list_data = $project_master_list['data'];
    } else {
        $alert = $project_master_list["data"];
        $alert_type = 0;
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
<style>

.custom-view{
	width: 500px !important;
	margin-top: -100px;
	margin-left: 50% !important;
	border-bottom-color:#003366;
}

.draggable-list {
	background-color: #f9f6f1;
	list-style: none;
	margin: 0;
	/*min-height: 70px;*/
	padding: 10px;
	width:200px;
	text-align: center;
}

#sel_user {
    overflow-y: auto;
		overflow-x: hidden;
    max-height: 300px;
}

.panel-success {
    border-color: #3d82bc;
    text-align: center;
    margin-bottom: auto;
    background-color: #54bb8c !important;
    color: white;
    font-size: 18px;
    margin-bottom: 0px !important;
}

.selected-panel {
  max-width: 199px !important;
    margin-left: 60%;
    margin-top: -56px;
}

.table {
    width: 50% !important;
    margin-bottom: 20px;
    background-color: #eeeeef !important;
}

#sel_user div {
	background-color: #FFF;
	border: 1px solid #c7c7c7;
	cursor: move;
	display: block;
	font-weight: bold;
	color:#3f3d40;
	/*padding-bottom:  20px;*/
	margin: 5px;
  position: relative;
  display: block;
  padding: 10px 10px 10px 10px;
  margin-bottom: -1px;
  border: 1px solid #dddddd;
	/*text-align: center;*/
}
.draggable-list.ui-sortable div{
	background-color: #FFF;
	border: 1px solid #000;
	cursor: move;
	display: block;
	font-weight: bold;
	color:#CC0033;
	/*padding-bottom:  20px;*/
	margin: 5px;
	/*text-align: center;*/
  position: relative;
  display: block;
  padding: 10px 10px 10px 10px;
  margin-bottom: -1px;
  border: 1px solid #dddddd;
}

.draggable-list.ui-sortable {
	overflow-x: hidden;
	overflow-y: auto;
	/*max-height: 300px;*/
}

</style>
    <meta charset="utf-8">
    <title>User - Add  Project Mapping</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

	<?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
    ?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header custom-header">
	      				<i class="icon-user"></i>
	      				<h3>User - Add  Project Mapping</h3>
								<!-- <span style="float:right; padding-right:20px;"><a href="crm_user_project_mapping_list.php">User Project Mapping List</a></span> -->
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<!-- <ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">User - Add  Project Mapping</a>
						  </li>
						</ul> -->
						<!-- <br> -->
							<div class="control-group">
								<div class="controls">
								<?php
                                if ($alert_type == 0) { // Failure?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><span id="msg"></span></strong>
                                    </div>
								<?php
                                }
                                ?>

								<?php
                                if ($alert_type == 1) { // Success
                                ?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <span id="msg"></span>
                                    </div>
								<?php
                                }
                                ?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="user_project_mapping_add_form" class="form-horizontal" action="#">
									<fieldset>

										<div class="control-group">
											<label class="control-label" for="ddl_project_id">Project*</label>
											<div class="controls">
												<select name="ddl_project_id" id="ddl_project_id" required>
												<option value="0">- - Select project - -</option>
												<?php
                            for ($count = 0; $count < count($project_master_list_data); $count++) {
                          ?>
												<option value="<?php echo $project_master_list_data[$count]["project_management_master_id"]; ?>"><?php echo $project_master_list_data[$count]["project_master_name"]; ?></option>
												<?php
                      }
                    ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

											<div class="control-group">
											<label class="control-label" for="ddl_department_id">Department*</label>
											<div class="controls">
												<select id="ddl_department_id" name="ddl_department_id">
												<option value='0'>- - Select Department - -</option>

												<?php
                                                for ($count = 0; $count < count($department_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										 <!-- <div class="control-group">
											<label class="control-label" for="ddl_user_id">User*</label>
											<div class="controls">
												<select id="sel_user" name="ddl_user_id" required>
													<option value="0">- Select User -</option>
												</select>
											</div>
										</div> -->

                  </fieldset>

										<div class="container custom-view">
                      <div class="panel panel-success" style="max-width: 249px; max-height: 100px">Users</div>
                      <div id="sel_user" class="table draggable-list" style="float:left">
                       <p>Add users</p>
                      </div>
                      <div class="panel panel-success selected-panel">Mapped users</div>
										  <div id="selected_user" class="draggable-list" style="float:right">
										  </div>
										</div>


                  <div class="form-actions">
          					<input type="submit" class="btn btn-primary" id="map_user" name="add_user_project_mapping_submit" value="Submit" onclick="mapUsers()"/>
          					<button type="reset" class="btn">Cancel</button>
          				</div> <!-- /form-actions -->
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 .
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>
<script>
var selectedUserIds = [];
var   IDs  = [];

function mapUsers() {
  IDs = [];
  $("#selected_user").find("div").each(function(){
    if(IDs.indexOf(this.id) == -1) {
      IDs.push(this.id);
    }
  });
var user_ids_to_send = JSON.stringify({
  user_ids : IDs,
  project_id : document.getElementById("ddl_project_id").value,
  dep_id : document.getElementById("ddl_department_id").value
});
  console.log('ids ', IDs);
    $.ajax({
      type: "POST",
      data: "user_data=" + user_ids_to_send,
      url: "add_project_user_mapping.php",
      success: function(data) {
        alert(data);
        document.getElementById("msg").innerHTML = data ;
      }

    })
}
$(document).ready(function() {

$('.container .draggable-list').sortable({
connectWith: '.container .draggable-list'
});

$("#selected_user").droppable({
    drop: function(event, ui) {
        var id = ui.draggable.attr("id");
        if(selectedUserIds.indexOf(id) == -1) {
          selectedUserIds.push(id);
        } else {
          ui.draggable.remove();
          alert('User already in mapping list');
        }
    }
});

	$("#ddl_department_id").change(function() {
		var department_id = $(this).val();
		console.log('department_id ', department_id);
		if(department_id == 0) {
			$("#sel_user").empty();
			return false;
		}

		$.ajax({
			url: 'ajax/getUsers.php',
			data: {department_id: department_id},
			dataType: 'json',
			success: function(response) {
				console.log('hope i got all users ', response);

				$("#sel_user").empty();
				for(var i=0; i< response.length; i++) {
					var id = response[i]['user_id'];
					var name = response[i]['user_name'];
          // var class = 'list-group-item';

					// $("#sel_user").append("<option value='"+id+"'>"+name+"</option>");
					$("#sel_user").append("<div id='"+id+"'>"+name+"</div>");
				}
			}
		})
	})
})

</script>
  </body>

</html>
