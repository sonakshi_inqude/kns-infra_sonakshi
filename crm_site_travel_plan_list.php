<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_site_travel_plan_list.php
CREATED ON	: 12-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Site Travel Plans
*/
define('CRM_SITE_TRAVEL_PLAN_LIST_FUNC_ID','109');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_SITE_TRAVEL_PLAN_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_SITE_TRAVEL_PLAN_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_SITE_TRAVEL_PLAN_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_SITE_TRAVEL_PLAN_LIST_FUNC_ID,'4','1');
	
	if(isset($_GET["site_plan_travel_search_submit"]))
	{		
		$project    = $_GET["ddl_search_project"];
		if(isset($_GET["dt_st_plan_date"]))
		{
			$date    = $_GET["dt_st_plan_date"];
		}
		else		
		{
			$date = "";
		}
		$status     = $_GET["ddl_search_status"];
		if(($role == 1) || ($role == 5) || ($role == 8) || ($role == 9))
		{
			$planned_by   = $_GET["ddl_search_planned_by"];
			$assigned_to  = $_GET["ddl_search_stm"];
			$relationship = "planned_by";
		}
		else		
		{
			$planned_by   = $user;
			$assigned_to  = "";
			$if_planned_by_me = $_GET["cb_planned_by_me"];
			if(isset($if_planned_by_me))
			{
				$relationship = "planned_by";
			}
			else			
			{
				$relationship = "all";
			}
		}
	}
	else
	{		
		$project     = "";
		$date        = "";
		$status      = "1";
		$assigned_to = "";
		if(($role == 1) || ($role == 5) || ($role == 8) || ($role == 9))
		{
			$planned_by = "";
		}
		else
		{
			$planned_by = $user;
		}	
		$relationship = "all";
	}

	// Temp data
	$alert = "";	
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"project_active"=>'1',"active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Cab List
	$cab_list = i_get_cab_list('','');
	if($cab_list["status"] == SUCCESS)
	{
		$cab_list_data = $cab_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$cab_list["data"];
		$alert_type = 0; // Failure
	}
	
	$site_travel_plan_list = i_get_site_travel_plan_list('','','','',$project,$date,$status,$assigned_to,$planned_by,$relationship,'','',$user);
	if($site_travel_plan_list["status"] == SUCCESS)
	{
		$site_visit_plan_list_data = $site_travel_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_travel_plan_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Site Travel Plan List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Site Travel Plan List<?php if($site_travel_plan_list["status"] == SUCCESS){?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Count: <?php echo count($site_visit_plan_list_data); }
			  else { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo "Count: 0";}?></h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="get" id="site_travel_search_form" action="crm_site_travel_plan_list.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($count = 0; $count < count($project_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project == $project_list_data[$count]["project_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  
			  <?php
			  if(($role == 1) || ($role == 5) || ($role == 8) || ($role == 9))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_planned_by">
			  <option value="">- - Select Planned By - -</option>
			  <?php
			  for($count = 0; $count < count($user_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($planned_by == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_stm">
			  <option value="">- - Select STM - -</option>
			  <?php
			  for($count = 0; $count < count($user_list_data); $count++)
			  {?>
			  
			  <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($assigned_to == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
			  <?php			  
			  }
			  ?>
			  </select>
			  </span>
			  <?php
			  }			
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_st_plan_date" value="<?php echo $date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_status">
			  <option value="">- - Select Status - -</option>
			  <option value="1" <?php if($status == "1") { ?> selected="selected" <?php }?>>Pending</option>
			  <option value="2" <?php if($status == "2") { ?> selected="selected" <?php }?>>Completed</option>
			  <option value="3" <?php if($status == "3") { ?> selected="selected" <?php }?>>Customer Cancelled</option>
			  <option value="4" <?php if($status == "4") { ?> selected="selected" <?php }?>>Driver did not come</option>
			  <option value="5" <?php if($status == "5") { ?> selected="selected" <?php }?>>Company Cancelled</option>
			  </select>
			  </span>			  
			  <?php
			  if(!(($role == 1) || ($role == 5) || ($role == 8) || ($role == 9)))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px; width: 100px;">
			  Planned By Me&nbsp;&nbsp;<input type="checkbox" name="cb_planned_by_me" value="1" <?php if($if_planned_by_me == "1"){ ?> checked <?php } ?> />
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="site_plan_travel_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th style="word-wrap:break-word;">SL No</th>	
					<th>Enquiry No.</th>	
					<th>Project</th>					
					<th>Date Time</th>
					<th>Drive Type</th>
					<th>Confirmation Status</th>
					<th>Status</th>					
					<th style="word-wrap:break-word;">Customer Details</th>
					<th>Assigned To</th>										<th>Vehicle Remarks</th>
					<th>Enquiry Added By</th>
					<th>Site Visit Planned By</th>	
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($site_travel_plan_list["status"] == SUCCESS)
				{
					$sl_no = 0;					
					for($count = 0; $count < count($site_visit_plan_list_data); $count++)
					{
						$sl_no++;						
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $site_visit_plan_list_data[$count]["enquiry_number"]; ?></td>
					<td><?php echo $site_visit_plan_list_data[$count]["project_name"]; ?></td>					
					<td><?php echo date("d-M-Y H:i",strtotime($site_visit_plan_list_data[$count]["crm_site_travel_plan_date"])); ?></td>					
					<td><?php if($site_visit_plan_list_data[$count]['crm_site_visit_plan_drive'] == '1'){echo 'SELF DRIVE';}					else{echo $site_visit_plan_list_data[$count]["crm_cab_travels"];} ?></td>
					<td><?php if($site_visit_plan_list_data[$count]["crm_site_visit_plan_confirmation"] == "0")
					{
						echo "Tentative";
					}
					else
					{
						echo "Confirmed";
					}?></td>
					<td><?php switch($site_visit_plan_list_data[$count]["crm_site_travel_plan_status"])
					{
					case "1":
					echo "Pending";
					break;
					
					case "2":
					echo "Completed";
					break;
					
					case "3":
					echo "Customer Cancelled";
					break;
					
					case "4":
					echo "Driver did not come";
					break;
					
					case "5":
					echo "Company Cancelled";
					break;
					
					default:
					echo "Pending";
					break;
					}										?></td>					
					<td><?php echo $site_visit_plan_list_data[$count]["name"].",".$site_visit_plan_list_data[$count]["cell"]; ?></td>
					<td><?php $user_details = i_get_user_list($site_visit_plan_list_data[$count]["assigned_to"],'','','');
					echo $user_details["data"][0]["user_name"];?></td>										<td><?php echo $site_visit_plan_list_data[$count]["crm_site_travel_actual_cab"] ;?></td>
					<td><?php $user_details = i_get_user_list($site_visit_plan_list_data[$count]["added_by"],'','','');
					echo $user_details["data"][0]["user_name"];?></td>
					<td><?php $svp_planned_by_details = i_get_user_list($site_visit_plan_list_data[$count]["crm_site_visit_plan_added_by"],'','','');
					echo $svp_planned_by_details["data"][0]["user_name"];?></td>
					<td><?php if(($role == 1) || ($role == 5) || (($site_visit_plan_list_data[$count]["crm_site_travel_plan_status"] == "1") && ($user == $site_visit_plan_list_data[$count]["assigned_to"]))) {?><a href="crm_update_site_travel_plan.php?travel_plan_id=<?php echo $site_visit_plan_list_data[$count]["crm_site_travel_plan_id"]; ?>">Update Status</a><?php } else { echo "Can't Update!"; } ?></td>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="7">No site travel plan!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>