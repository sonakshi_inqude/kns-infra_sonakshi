<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_grn_accont_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of grn account for customer withdrawals

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Query String Data	

	// Nothing

	

	if(isset($_POST['stock_search_submit']))

	{
		if(isset($_POST["cb_closing_stock"]))
		{
			$closing_stock_value   = '0';
		}
		else
		{
			$closing_stock_value   = '-1';
		}
		$project   = $_POST["ddl_project"];
	}

	else

	{
		$project = '-1';
		$closing_stock_value = '-1';
	}

	//Meterail Search
	if(isset($_POST["hd_material_id"]))
	{
		if($_POST["stxt_material"] != '')
		{
			$search_material 	  = $_POST["hd_material_id"];
			$search_material_name = $_POST["stxt_material"];
         }
		else
		{
			$search_material 	  = "-1";
			$search_material_name = "";	
		}		
	}
	else
	{				
		if(isset($_GET['hd_material_id']))
		{
			if($_GET["material_name"] != '')
			{
				$search_material 	  = $_GET["hd_material_id"];
				$search_material_name = $_GET["material_name"];	
			}
			else
			{
				$search_material 	  = "-1";
				$search_material_name = "";
			}
		}
		else
		{
			$search_material 	  = "-1";
			$search_material_name = "";
		}
	}

	//Get Material Master List

	$stock_material_search_data = array('material_active'=>'1',"material_id"=>$search_material);

	$material_master_list = i_get_stock_material_master_list($stock_material_search_data);

	if($material_master_list["status"] == SUCCESS)

	{

		$material_master_list_data  = $material_master_list["data"];
	}

	
	// Get Project List
	$stock_project_search_data = array();
	$project_result_list = i_get_project_list($stock_project_search_data);
	if($project_result_list['status'] == SUCCESS)
	{
		$project_result_list_data = $project_result_list['data'];		
	}	

	else

	{
		$alert = $project_result_list["data"];
		$alert_type = 0;		

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Stock Report</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Stock Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="span_total_value"></span></h3>

            </div>

            <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_statement_report.php">	
				<input type="hidden" name="hd_material_id" id="hd_material_id" value="" />	
				
				<span style="padding-right:20px;">										

					<input type="text" name="stxt_material" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search Material by name or code" />

					<div id="search_results" class="dropdown-content"></div>

			  </span>	
			  
			  <span style="padding-right:20px;">
			  <select name="ddl_project">
				<option value="">- - Select Project - -</option>
				<?php
				for($count = 0; $count < count($project_result_list_data); $count++)
				{
				?>
				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>
				<?php
				}
				?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Show Only Greater than '0'  Closing Stock&nbsp;&nbsp;&nbsp;<input type="checkbox" value="<?php echo $search_material_name; ?>" name="cb_closing_stock" id="cb_closing_stock" <?php if($closing_stock_value == '0') { ?> checked <?php } ?> />
			  </span>
			  	
			  <input type="submit" name="stock_search_submit" />

			  </form>			  

            </div>

            <div class="widget-content">

			

              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>SL No</th>

					<th>Material Name</th>

					<th>Material Code</th>
					
					<th>UOM</th>

					<th>Opening Stock</th>		

					<th>Receipts</th>	

					<th>Issues</th>	

					<th>Closing Stock</th>	

					<th>Rate</th>	

					<th>Value</th>										

				</tr>

				</thead>

				<tbody>							

				<?php

				$total_value = 0;

				if($material_master_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($material_master_list_data); $count++)

					{

						$sl_no ++;

											
					// Get Stock History for Opening
					$stock_history_search_data = array("transaction_type"=>'Opening',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
					$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

					if($stock_history_list['status'] == SUCCESS)

					{

						$stock_history_list_data = $stock_history_list['data'];

						$opening_qty = $stock_history_list_data[0]["total_stock_history_quantity"];

					}	

					else

					{

						$opening_qty  = "0";

					}

					

					// Get Stock history for Issue
					$stock_history_search_data = array("transaction_type"=>'Issue',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
					$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

					if($stock_history_list['status'] == SUCCESS)

					{

						$stock_history_list_data = $stock_history_list['data'];

						$issue_qty = $stock_history_list_data[0]["total_stock_history_quantity"];

					}	

					else

					{

						$issue_qty  = "0";

					}

				
					$stock_history_search_data = array("transaction_type"=>'Transfer Out',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
					$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

					if($stock_history_list['status'] == SUCCESS)

					{

						$stock_history_list_data = $stock_history_list['data'];

						$issue_qty = $issue_qty + $stock_history_list_data[0]["total_stock_history_quantity"];

					}	

					else

					{

						$issue_qty  = $issue_qty + 0;;

					}

					

					

					// Get Stock history for Purchase
					$stock_history_search_data = array("transaction_type"=>'Purchase',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
					$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

					if($stock_history_list['status'] == SUCCESS)

					{

						$stock_history_list_data = $stock_history_list['data'];

						$purchase_qty = $stock_history_list_data[0]["total_stock_history_quantity"];

					}	

					else

					{

						$purchase_qty  = "0";

					}

					
					
					$stock_history_search_data = array("transaction_type"=>'Transfer In',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
					$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

					if($stock_history_list['status'] == SUCCESS)

					{

						$stock_history_list_data = $stock_history_list['data'];

						$purchase_qty = $purchase_qty + $stock_history_list_data[0]["total_stock_history_quantity"];

					}	

					else

					{

						$purchase_qty  = $purchase_qty + 0;

					}


					

					$rate = $material_master_list_data[$count]["stock_material_price"];

					$closing_stock = ($opening_qty + $purchase_qty - $issue_qty);

					$value = ($rate * $closing_stock);

					$total_value = $total_value + $value;
					
					if($closing_stock > $closing_stock_value)
						
					{

					?>

					<tr>

					<td><?php echo $sl_no; ?></td>

					<td><a target="_blank" href="stock_material_report.php?material_id=<?php echo  $material_master_list_data[$count]["stock_material_id"];?>&ddl_project=<?php echo $project ;?>"><?php echo $material_master_list_data[$count]["stock_material_code"]; ?> </a></td>

					<td><?php echo $material_master_list_data[$count]["stock_material_name"];?></td>
					
					<td><?php echo $material_master_list_data[$count]["stock_unit_name"];?></td>
					

					<td><?php echo $opening_qty; ?></td>

					<td><?php echo $purchase_qty; ?></td>

					<td><?php echo $issue_qty; ?></td>

					<td><?php echo $closing_stock; ?></td>

					<td><?php echo $rate; ?></td>

					<td><?php echo $value; ?></td>

					</tr>

					<?php									

					}
					}

				}

				else

				{

				?>

				<td colspan="9"></td>

				<?php

				}

				 ?>	



                </tbody>

              </table>

			  <script>

			  document.getElementById('span_total_value').innerHTML = 'Total Value: ' + '<?php echo $total_value; ?>';

			  </script>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_account(account_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_grn_account_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_account.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("account_id=" + account_id + "&action=0");

		}

	}	

}

function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;

	

	document.getElementById('search_results').style.display = 'none';

}

</script>



  </body>



</html>

