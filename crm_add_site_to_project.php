<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* DEFINES - START */
define('CRM_ADD_SITE_TO_PROJECT_FUNC_ID','86');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'CRM Projects';


/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',CRM_ADD_SITE_TO_PROJECT_FUNC_ID,'1','1');	
	$view_perms_list   = i_get_user_perms($user,'',CRM_ADD_SITE_TO_PROJECT_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if((!(isset($_POST["add_site_project_submit"]))) && (isset($_POST["ddl_project"])))
	{
		$project_id = $_POST["ddl_project"];
	}
	else if(isset($_POST["add_site_project_submit"]))
	{
		$project_id      = $_POST["ddl_project"];
		$site_number     = $_POST["stxt_site_no"];
		$block           = $_POST["stxt_block"];
		$wing            = $_POST["stxt_wing"];
		$survey_no_one   = $_POST["ddl_survey_no_one"];
		$survey_no_two   = $_POST["ddl_survey_no_two"];
		$survey_no_three = $_POST["ddl_survey_no_three"];
		$survey_no_four  = $_POST["ddl_survey_no_four"];
		$dimension       = $_POST["ddl_dimension"];
		$area            = $_POST["stxt_area"];		
		$site_type       = $_POST["ddl_site_type"];
		$remarks         = $_POST["txt_remarks"];
		$rel_status      = $_POST["ddl_release_status"];
		$site_status     = $_POST["ddl_site_status"];
		$bank            = $_POST["ddl_bank"];
		
		// Check for mandatory fields
		if(($project_id !="") && ($site_number !="") && ($dimension !="") && ($survey_no_one !="") && ($site_type !="") && ($rel_status !="") && ($site_status !="") && ($bank !="") && ($block !="") && ($wing !=""))
		{					
			$site_project_iresult = i_add_site_to_project($project_id,$site_number,$block,$wing,$dimension,$area,'',$site_type,$remarks,$site_status,$rel_status,$bank,$user);
			
			if($site_project_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Survey Successfully Mapped to Site";
				
				if($survey_no_one != "")
				{
					$site_survey_mapping = i_add_survey_to_site($survey_no_one,$site_project_iresult["data"],$user);
				}
				if($survey_no_two != "")
				{
					$site_survey_mapping = i_add_survey_to_site($survey_no_two,$site_project_iresult["data"],$user);
				}	
				if($survey_no_three != "")
				{
					$site_survey_mapping = i_add_survey_to_site($survey_no_three,$site_project_iresult["data"],$user);
				}	
				if($survey_no_four != "")
				{
					$site_survey_mapping = i_add_survey_to_site($survey_no_four,$site_project_iresult["data"],$user);
				}	
			}
			else
			{
				$alert_type = 0;
				$alert = $site_project_iresult["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	else
	{
		$project_id = "";
	}
	
	// Get project list
	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');
	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get site type list
	$site_type_list = i_get_site_type_list('','1');
	if($site_type_list["status"] == SUCCESS)
	{
		$site_type_list_data = $site_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_type_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get dimension list
	$dimension_list = i_get_dimension_list('','','','','1');
	if($dimension_list["status"] == SUCCESS)
	{
		$dimension_list_data = $dimension_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$dimension_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get site status list
	$site_status_list = i_get_site_status_list('','1');
	if($site_status_list["status"] == SUCCESS)
	{
		$site_status_list_data = $site_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_status_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get release status list
	$rel_status_list = i_get_release_status('','1');
	if($rel_status_list["status"] == SUCCESS)
	{
		$rel_status_list_data = $rel_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$rel_status_list["data"];
		$alert_type = 0; // Failure
	}
		
	// Get bank list
	$bank_list = i_get_bank_list('','1');
	if($rel_status_list["status"] == SUCCESS)
	{
		$bank_list_data = $bank_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bank_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get survey number list
	$survey_no_list = i_get_survey_number_list('',$project_id,'1');
	if($survey_no_list["status"] == SUCCESS)
	{
		$survey_no_list_data = $survey_no_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$survey_no_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Site to Project</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Site to Project</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_site_status" class="form-horizontal" method="post" action="crm_add_site_to_project.php">
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="ddl_project">Project*</label>
											<div class="controls">
												<select name="ddl_project" required onchange="this.form.submit();">
												<option value="-1">- - Select Project - -</option>
												<?php
												for($count = 0; $count < count($project_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project_id == $project_list_data[$count]["project_id"]){ ?>selected="selected"<?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_site_no">Site No*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_site_no" placeholder="Site Number of the site" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_block">Block*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_block" placeholder="Block to which site belongs" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_wing">Wing*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_wing" placeholder="Wing to which site belongs" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_site_no">Survey No 1*</label>
											<div class="controls">
												<select name="ddl_survey_no_one" required>
												<option value="">- - Select Survey Number - -</option>
												<?php
												for($count = 0; $count < count($survey_no_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_no_list_data[$count]["crm_survey_master_id"]; ?>"><?php echo $survey_no_list_data[$count]["crm_survey_master_survey_no"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_site_no">Survey No 2</label>
											<div class="controls">
												<select name="ddl_survey_no_two">
												<option value="">- - Select Survey Number - -</option>
												<?php
												for($count = 0; $count < count($survey_no_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_no_list_data[$count]["crm_survey_master_id"]; ?>"><?php echo $survey_no_list_data[$count]["crm_survey_master_survey_no"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_site_no">Survey No 3</label>
											<div class="controls">											
												<select name="ddl_survey_no_three">
												<option value="">- - Select Survey Number - -</option>
												<?php
												for($count = 0; $count < count($survey_no_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_no_list_data[$count]["crm_survey_master_id"]; ?>"><?php echo $survey_no_list_data[$count]["crm_survey_master_survey_no"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_site_no">Survey No 4</label>
											<div class="controls">												
												<select name="ddl_survey_no_four">
												<option value="">- - Select Survey Number - -</option>
												<?php
												for($count = 0; $count < count($survey_no_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_no_list_data[$count]["crm_survey_master_id"]; ?>"><?php echo $survey_no_list_data[$count]["crm_survey_master_survey_no"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_dimension">Site Dimension*</label>
											<div class="controls">
												<select name="ddl_dimension" required>
												<option value="">- - Select Dimension - -</option>
												<?php
												for($count = 0; $count < count($dimension_list_data); $count++)
												{
												?>
												<option value="<?php echo $dimension_list_data[$count]["crm_dimension_id"]; ?>"><?php echo $dimension_list_data[$count]["crm_dimension_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_dimension">Site Area</label>
											<div class="controls">
												<input type="text" name="stxt_area" onblur="dimensions(this.value);" placeholder="Total area of the site in sq.ft (for non-standard dimensions only)"> sq. ft&nbsp;&nbsp;&nbsp; (<span id="area_sq_m"></span> sq. m)
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																																														
										<div class="control-group">											
											<label class="control-label" for="ddl_site_type">Facing/Site Type*</label>
											<div class="controls">
												<select name="ddl_site_type" required>
												<?php
												for($count = 0; $count < count($site_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $site_type_list_data[$count]["crm_site_type_id"]; ?>"><?php echo $site_type_list_data[$count]["crm_site_type_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_site_status">Site Status*</label>
											<div class="controls">
												<select name="ddl_site_status" required>
												<?php
												for($count = 0; $count < count($site_status_list_data); $count++)
												{
												?>
												<option value="<?php echo $site_status_list_data[$count]["status_id"]; ?>"><?php echo $site_status_list_data[$count]["status_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_release_status">Release Status*</label>
											<div class="controls">
												<select name="ddl_release_status" required>
												<?php
												for($count = 0; $count < count($rel_status_list_data); $count++)
												{
												?>
												<option value="<?php echo $rel_status_list_data[$count]["release_status_id"]; ?>"><?php echo $rel_status_list_data[$count]["release_status"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_bank">Mortgage*</label>
											<div class="controls">
												<select name="ddl_bank" required>
												<?php
												for($count = 0; $count < count($bank_list_data); $count++)
												{
												?>
												<option value="<?php echo $bank_list_data[$count]["crm_bank_master_id"]; ?>"><?php echo $bank_list_data[$count]["crm_bank_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_cost">Remarks</label>
											<div class="controls">
												<textarea class="span6" name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
										
											<?php

										if($add_perms_list['status'] == SUCCESS)

										{

										?>
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_site_project_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
											<?php

										}

										else

										{

											?>

											<div class="form-actions">

												You are not authorized to add Site

											</div> <!-- /form-actions -->

											<?php

										}

										?>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script>
function dimensions(area_sq_ft)
{	
	var area_sq_m  = area_sq_ft*(0.092903);	
	area_sq_m = area_sq_m.toFixed(2);
		
	document.getElementById("area_sq_m").innerHTML = area_sq_m;
}
</script>

<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


  </body>

</html>
