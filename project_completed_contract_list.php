<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

/* DEFINES - START */
define('PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID','');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID,'4','1');
	$ok_perms_list   	= i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID,'5','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_TASK_PROJECT_BOQ_ACTUALS_FUNC_ID,'6','1');
	
	$alert_type = -1;
	$alert = "";

	// Query String Data
	// Nothing
	
	$search_project = '';
	$search_vendor  = '';
	$start_date     = '';
	$end_date	    = '';
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	// Get Project Task BOQ modes already added
	$project_task_boq_actual_search_data = array("active"=>'1',"percentage_completion"=>"1","process_id"=>$process_id);
	$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
	if($project_task_boq_actual_list['status'] == SUCCESS)
	{
		$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_boq_actual_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Pending contract List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Pending contract Work List </h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
					<th>Task</th>
					<th>%</th>
					<th>Contract Process</th>
					<th>Contract Task</th>
					<th>Location</th>
					<th>Vendor</th>
					<th>Date</th>
					<th>UOM</th>
					<th>Number</th>
					<th>Length</th>
					<th>Breadth</th>
					<th>Depth</th>
					<th>Total Measurement</th>
					<th>Rate</th>
					<th>Total</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>								
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_task_boq_actual_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_task_boq_actual_list_data); $count++)
					{
						$sl_no++;
						$measurement = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"];
						$numbers = $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"];
						$rate = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"];
						/*if($numbers != 0)
						{
							$total = $measurement * $numbers * $rate;
						}
						else
						{
							$total = $measurement * $rate;
						}*/
						$actual_total = $measurement * $rate;
						$total = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_lumpsum"];
						
						$total_amount = $total_amount + $total;
					?>
					<tr <?php if(round($total) != round($actual_total)){ ?> style="color:red;" <?php } ?>>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_process_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_contract_rate_master_work_task"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_completion"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_site_location_mapping_master_name"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo get_formatted_date($project_task_boq_actual_list_data[$count]["project_task_actual_boq_date"],"d-M-Y"); ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["stock_unit_name"]; ?></td>
					
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_boq_actual_number"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_length"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_breadth"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_depth"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"]; ?></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"]; ?></td>
					<td><?php echo  $total ; ?></td>
					<td style="word-wrap:break-word;"><a href="#" onclick="alert('<?php echo $project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"]; ?>');"><?php echo substr($project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"],0,30); ?></a></td>
					<td><?php echo $project_task_boq_actual_list_data[$count]["added_by"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_boq_actual_list_data[$count][
					"project_task_actual_boq_added_on"])); ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>   <?php 
		    } 
			?>
			   <script>
			  document.getElementById('total_amount').innerHTML = '<?php echo $total_amount; ?>';
			   </script>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_boq(boq_actual_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_task_boq_actuals_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_boq_actuals.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_actual_id=" + boq_actual_id + "&action=0");
		}
	}	
}
function go_to_project_edit_task_boq(boq_actual_id,task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_boq_actuals.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","boq_actual_id");
	hiddenField1.setAttribute("value",boq_actual_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","task_id");
	hiddenField2.setAttribute("value",task_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function project_check_task_actual_contract(boq_id,task_id,search_project,search_vendor)
{
	var ok = confirm("Are you sure you want to Ok?")
	{         
		if (ok)
		{			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_task_boq_actuals_list.php?search_project=" +search_project;
					}
				}
			}

			xmlhttp.open("POST", "project_check_contract_work.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&task_id=" +task_id+ "&action=approved");
		}
	}	
}
function project_approve_task_actual_contract(boq_id,task_id,search_project,search_vendor)
{
	var ok = confirm("Are you sure you want to Approve?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					  window.location = "project_task_boq_actuals_list.php?search_project=" +search_project;
					}
				}
			}

			xmlhttp.open("POST", "project_approve_contract_work.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("boq_id=" + boq_id + "&task_id=" +task_id+ "&action=approved");
		}
	}	
}
</script>

  </body>

</html>