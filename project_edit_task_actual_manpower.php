<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */

    if (isset($_REQUEST["man_power_id"])) {
        $man_power_id = $_REQUEST["man_power_id"];
    } else {
        $man_power_id = $_POST["hd_man_power_id"];
    }

    if (isset($_REQUEST["task_id"])) {
        $task_id = $_REQUEST["task_id"];
    } else {
        $task_id = "";
    }

    if (isset($_REQUEST["road_id"])) {
        $road_id = $_REQUEST["road_id"];
    } else {
        $road_id = "";
    }

    // Get Project Task Actual Man Power modes already added
    $man_power_search_data = array("man_power_id"=>$man_power_id);
    $man_power_list = i_get_man_power_list($man_power_search_data);
    if ($man_power_list['status'] == SUCCESS) {
        $man_power_list_data = $man_power_list['data'];
        $process_master_id = $man_power_list_data[0]["project_process_master_id"];
        $work_type = $man_power_list_data[0]["project_task_actual_manpower_work_type"];
        if ($work_type == "Rework") {
            $rework_percentage = $man_power_list_data[0]["project_task_actual_manpower_rework_completion"];
        }
    }

    //Get Regular Measurement
    $get_details_data = i_get_details('Regular', $task_id, $road_id);
    $reg_mp_msmrt = $get_details_data["mp_msmrt"];
    $planned_msmrt = $get_details_data["planned_msmrt"];
    $uom = $get_details_data["uom"];
    $reg_overall_msmrt = $get_details_data["overall_msmrt"];
    $road_name = $get_details_data["road"];
    $reg_mc_msmrt = $get_details_data["mc_msmrt"];
    $reg_cw_msmrt = $get_details_data["cw_msmrt"];
    $project = $get_details_data["project"];
    $process_name = $get_details_data["process"];
    $task_name = $get_details_data["task"];

    //Get Rework Measurement
    $get_details_data = i_get_details('Rework', $task_id, $road_id);
    $rework_mp_msmrt = $get_details_data["mp_msmrt"];
    $rework_mc_msmrt = $get_details_data["mc_msmrt"];
    $rework_cw_msmrt = $get_details_data["cw_msmrt"];
    $rework_overall_msmrt = $get_details_data["overall_msmrt"];

    // Capture the form data
    if (isset($_POST["edit_project_task_actual_manpower_submit"])) {
        $task_id				         = $_POST["hd_man_task_id"];
        $work_type			         = $_POST["hd_work_type"];
        $man_power_id            = $_POST["hd_man_power_id"];
        $men                     = $_POST['num_men'];
        $women                   = $_POST['num_women'];
        $mason                   = $_POST['num_mason'];
        $others                  = $_POST['num_others'];
        $msmrt                   = $_POST['num_msmrt'];
        $completion_percent      = $_POST["completion_percent"];
        $remarks 	               = $_POST["txt_remarks"];

        if ($completion_percent == 100) {
            $actual_end_date = date("Y-m-d H:i:s");
        } else {
            $actual_end_date = "0000-00-00";
        }

        //Get Actual Manpower List
        $man_power_search_data = array("man_power_id"=>$man_power_id,"work_type"=>"Regular");
        $manpower_list = i_get_man_power_list($man_power_search_data);
        if ($manpower_list["status"] == SUCCESS) {
            $manpower_list_data = $manpower_list["data"];
            $mp_percentage = $manpower_list_data[0]["project_task_actual_manpower_completion_percentage"];
            $road_name = $manpower_list_data[0]["project_site_location_mapping_master_name"];
        } else {
            $mp_percentage = "";
        }
        $project_process_task_search_data = array("active"=>'1',"task_id"=>$task_id);
        $project_task_list = i_get_project_process_task($project_process_task_search_data);
        if ($project_task_list["status"] == SUCCESS) {
            $project_task_list_data = $project_task_list["data"];
            $task_manpower_completion = $project_task_list_data[0]["project_process_task_completion"];
            $task_machine_completion = $project_task_list_data[0]["project_process_machine_task_completion"];
            $task_contract_completion = $project_task_list_data[0]["project_process_contract_task_completion"];
        } else {
            $task_manpower_completion = 0;
            $task_machine_completion = 0;
            $task_contract_completion = 0;
        }
        // $allow_edit = false;
        // if (($process_master_id == 32) || ($process_master_id == 33)) {
        //     if (($completion_percent > 0) && ($completion_percent <=99)) {
        //         $allow_edit = true;
        //     }
        // } else {
        //     if ($work_type == "Regular") {
        //         if (($completion_percent > 0) && ($completion_percent <= 100)) {
        //             $allow_edit = true;
        //         }
        //     } elseif ($work_type == "Rework") {
        //         $allow_edit = true;
        //     } else {
        //         $allow_edit = false;
        //     }
        // }

        // if ($allow_edit == true) {
            if ($work_type == "Regular") {
                $man_power_update_data = array("men"=>$men,"women"=>$women,"mason"=>$mason,"others"=>$others,
                "msmrt"=>$msmrt,"completion_percent"=>$completion_percent,"remarks"=>$remarks);
                $man_power_iresult = i_update_man_power($man_power_id, $task_id, $man_power_update_data);

                if ($man_power_iresult["status"] == SUCCESS) {
                    $alert_type = 1;
                    $project_process_task_update_data = array("completion"=>$completion_percent,"actual_end_date"=>$actual_end_date);
                    $task_update_list = i_update_project_process_task($task_id, $project_process_task_update_data);
                    header("location:project_task_actual_manpower_list.php?project_process_task_id=$task_id");
                } else {
                    $alert = "Project Already Exists";
                    $alert_type = 0;
                }
            } elseif ($work_type == "Rework") {
                $man_power_update_data = array("men"=>$men,"women"=>$women,"mason"=>$mason,"others"=>$others,
                "msmrt"=>$msmrt,"rework_completion"=>$completion_percent,"remarks"=>$remarks);
                $man_power_iresult = i_update_man_power($man_power_id, $task_id, $man_power_update_data);

                if ($man_power_iresult["status"] == SUCCESS) {
                    $alert_type = 1;
                    header("location:project_task_actual_manpower_list.php?project_process_task_id=$task_id");
                } else {
                    $alert = "Project Already Exists";
                    $alert_type = 0;
                }
            }

            $alert = $man_power_iresult["data"];
        // } else {
        //     $alert = "Please Enter Completion Percentage";
        //     $alert_type = 0;
        // }
    }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
        $alert = $project_manpower_agency_list["data"];
        $alert_type = 0;
    }

    // Get list of task plans for this process plan
    $task_manpower_completion = 0;
    $task_machine_completion = 0;
    $task_contract_completion = 0;
    $avg_count = 0;
    $project_process_task_search_data = array("active"=>'1',"task_id"=>$task_id);
    $project_process_task_list = i_get_project_process_task($project_process_task_search_data);
    if ($project_process_task_list["status"] == SUCCESS) {
        $project_process_task_list_data = $project_process_task_list["data"];
        $task_manpower_completion = $project_process_task_list_data[0]["project_process_task_completion"];
        $task_machine_completion = $project_process_task_list_data[0]["project_process_machine_task_completion"];
        $task_contract_completion = $project_process_task_list_data[0]["project_process_contract_task_completion"];

        $project_process_task_list_data = $project_process_task_list["data"];

        // Actual Manpower data
        $man_power_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
        $man_power_list1 = i_get_man_power_list($man_power_search_data);
        if ($man_power_list1["status"] == SUCCESS) {
            $avg_count = $avg_count + 1;
        } else {
            // Get Project Man Power Estimate modes already added
            $project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$task_id);
            $project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
            if ($project_man_power_estimate_list['status'] == SUCCESS) {
                $avg_count = $avg_count + 1;
            } else {
                //Do not Inncrement
            }
        }

        // Machine Planning data
        $actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
        $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
        if ($actual_machine_plan_list["status"] == SUCCESS) {
            $avg_count = $avg_count + 1;
        } else {
            //Machine Planning
            $project_machine_planning_search_data = array("active"=>'1',"task_id"=>$task_id);
            $project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
            if ($project_machine_planning_list["status"] == SUCCESS) {
                $avg_count = $avg_count + 1;
            } else {
                //Do not Increment
            }
        }
        // Actual Contract  Data
        $project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$task_id,"work_type"=>"Regular");
        $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
        if ($project_task_boq_actual_list['status'] == SUCCESS) {
            $avg_count = $avg_count + 1;
        } else {
            $project_task_boq_search_data = array("active"=>'1',"task_id"=>$task_id);
            $project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
            if ($project_task_boq_list['status'] == SUCCESS) {
                $avg_count = $avg_count + 1;
            } else {
                ///Do not Increment
            }
        }
    } else {
        //
    }
    $overall_percentage = $task_manpower_completion + $task_machine_completion +$task_contract_completion ;
    if ($avg_count != 0) {
        $avg_overall_percentage = $overall_percentage/$avg_count;
    } else {
        $avg_overall_percentage = 0;
    }
    $overall_pending_percentage = 100 - $avg_overall_percentage;
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Edit Project Task Actual Man Power</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header custom-header">

                <h3>
                  <span class="header-label">Project :</span> <?= $project; ?>
                  <span class="header-label">Process :</span> <?= $process_name; ?>
                  <span class="header-label">Task:</span>  <?= $task_name; ?>
                </h3>
                <h3 style="margin-left:25px">
                  <span class="header-label">Road:</span>  <?= $road_name; ?>
                  <span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
                  <span class="header-label">UOM</span> <?= $uom; ?>
                </h3>
                <h3 >
                  <span class="header-label">Regular</span> <?= $reg_overall_msmrt; ?> (MP : <?= $reg_mp_msmrt; ?>  MC : <?= $reg_mc_msmrt; ?> CW : <?= $reg_cw_msmrt; ?>)
                  <span class="header-label">Rework</span> <?= $rework_overall_msmrt; ?> (MP : <?= $rework_mp_msmrt; ?>   MC : <?= $rework_mc_msmrt; ?> CW : <?= $rework_cw_msmrt; ?>)
                                </h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="right">
						    <a href="#formcontrols" data-toggle="tab">Edit Project Task Actual Man Power</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
                                if ($alert_type == 0) { // Failure?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>

								<?php
                                if ($alert_type == 1) { // Success
                                ?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_edit_task_actual_manpower_form" class="form-horizontal" method="post" action="project_edit_task_actual_manpower.php">
								<input type="hidden" name="hd_man_power_id" value="<?php echo $man_power_id; ?>" />
								<input type="hidden" name="hd_man_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_work_type" value="<?php echo $work_type; ?>" />
									<fieldset>


										<div class="control-group">
											<label class="control-label" for="date">Work Type</label>
											<div class="controls">
												<input type="text" style="font-size:20px;" name="date" placeholder=" Actual Date" disabled value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_work_type"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="date">Date</label>
											<div class="controls">
												<input type="date" name="date" placeholder=" Actual Date" disabled value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_date"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_agency">Manpower Agency*</label>
											<div class="controls">
												<select name="ddl_agency" disabled required>
												<option>- - Select Agency - -</option>
												<?php
                                                for ($count = 0; $count < count($project_manpower_agency_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>" <?php if ($project_manpower_agency_list_data[$count]["project_manpower_agency_id"] == $man_power_list_data[0]["project_task_actual_manpower_agency"]) {
                                                        ?> selected="selected" <?php
                                                    } ?>><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_men">Men</label>
											<div class="controls">
												<input type="number" name="num_men" placeholder="NO of Men" value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_no_of_men"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_women">Women</label>
											<div class="controls">
												<input type="number" name="num_women" placeholder="NO of Women" value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_no_of_women"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_mason">Mason</label>
											<div class="controls">
												<input type="number" name="num_mason" placeholder="NO of Manson" value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_no_of_mason"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_others">Others</label>
											<div class="controls">
												<input type="number" name="num_others" placeholder="NO of Others" value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_no_of_others"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="num_msmrt">Completed Measurment*</label>
											<div class="controls">
												<input type="number" min="1" required name="num_msmrt" id="num_msmrt" placeholder="Measurment" onchange="return check_validity('<?php echo $reg_overall_msmrt ;?>','<?php echo $planned_msmrt ; ?>','<?php echo $work_type ;?>','<?php echo $rework_overall_msmrt ;?>');" value="<?php echo $man_power_list_data[0]["project_task_actual_manpower_completed_msmrt"] ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<?php if ($work_type == "Regular") {
                                                    $completion_percenatge =  $man_power_list_data[0]["project_task_actual_manpower_completion_percentage"] ;
                                                } else {
                                                    $completion_percenatge = $man_power_list_data[0]["project_task_actual_manpower_rework_completion"] ;
                                                }
                                        ?>
										<input type="hidden" name="completion_percent"  id="completion_percent" value="" />
										<div class="control-group">
											<label class="control-label" for="completion_percent1">Completion Percent</label>
											<div class="controls">
												<input type="number" name="completion_percent1" disabled  id="completion_percent1"  required placeholder="Percent" value="<?php echo $completion_percenatge ;?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" rows="4" cols="50" placeholder="Remarks"><?php echo $man_power_list_data[0]["project_task_actual_manpower_remarks"] ;?></textarea>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" id="edit_project_task_actual_manpower_submit" name="edit_project_task_actual_manpower_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function check_validity(total_msmrt,planned_msmrt,work_type,total_rework_msmrt)
{
    if(!planned_msmrt || planned_msmrt == 0) {
      document.getElementById('edit_project_task_actual_manpower_submit').disabled = true;
      alert('Kindly Plan the measurement');
      return false;
    }
    
    var cur_msmrt = parseFloat(document.getElementById('num_msmrt').value);
    var total_msmrt = parseFloat(total_msmrt);
    var planned_msmrt  = parseFloat(planned_msmrt);
    var total_rework_msmrt  = parseFloat(total_rework_msmrt);

    if((work_type == "Regular") && (cur_msmrt + total_msmrt) > planned_msmrt)
    {
        document.getElementById('num_msmrt').value = 0;
          alert('Actual msmrt can not be greater than planned');
    }
    else if ((work_type == "Rework") && (cur_msmrt > total_msmrt)) {
      document.getElementById('num_msmrt').value = 0;
      alert('Rework msmrt can not be greater than completed');
    }
		else
		{
      if(work_type == "Regular"){
        auto_percentage = ((cur_msmrt + total_msmrt) / planned_msmrt) * 100 ;
      }
      else if (work_type == "Rework") {
        auto_percentage = 1 ;
      }
			 document.getElementById('completion_percent').value = parseFloat(auto_percentage).toFixed(2);
			 document.getElementById('completion_percent1').value = parseFloat(auto_percentage).toFixed(2);
       document.getElementById('edit_project_task_actual_manpower_submit').disabled = false;
		}

}

</script>



  </body>

</html>
