<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_site_visit_plan_submit"]))
	{
		$enquiry_id      = $_POST["hd_enquiry"];
		$site_visit_date = $_POST["dt_site_visit"];
		$site_visit_time = $_POST["stxt_visit_time"];
		$location        = $_POST["stxt_pickup_location"];
		$site_project    = $_POST["ddl_project"];
		$confirm_status  = $_POST["rd_confirm_status"];
		$drive_type      = $_POST["rd_drive_type"];				
		
		// Check for mandatory fields
		if(($enquiry_id !="") && ($site_visit_date !="") && ($site_project !="") && ($site_visit_time != ""))
		{
			$date_valid_array = get_date_diff($site_visit_date,date("Y-m-d"));
		
			if($date_valid_array["status"] == 0)
			{
				$site_visit_plan_iresult = i_add_site_visit_plan($enquiry_id,$site_visit_date,$site_visit_time,$site_project,$confirm_status,$drive_type,$location,$user);
				
				if($site_visit_plan_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;					
					if(($confirm_status == "1") && ($drive_type == "1"))
					{
						$site_travel_iresult = i_add_site_travel_plan($site_visit_plan_iresult["data"],'1',$site_project,$site_visit_date,$user);
						
						if($site_travel_iresult["status"] == SUCCESS)
						{
							$svp_uresult = i_update_site_visit_plan($enquiry_id,$status);
							if($svp_uresult["status"] == SUCCESS)
							{
								header("location:crm_enquiry_list.php");
							}
							else
							{
								$alert_type = 0;
								$alert      = "Travel Plan added successfully, but with minor issues. Please contact the admin!";
							}
						}
						else
						{
							$alert_type = 0;
							$alert      = "Tentative Site Visit Plan added successfully, but travel plan not added. Please contact the admin!";
						}												
					}										
				}
				else
				{
					$alert_type = 0;
					$alert      = $site_visit_plan_iresult["data"];
				}							
			}
			else
			{
				$alert = "Tentative Site Visit Date cannot be before today";
				$alert_type = 0;
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Project
	$project_list = i_get_project_list('','1');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Enquiry List
	$enquiry_list = i_get_enquiry_list($enquiry_id,'','','','','','','','','','','','','','','','','','','');
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Site Visit Plan</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Site Visit Plan</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_site_visit_plan" class="form-horizontal" method="post" action="crm_add_site_visit_plan.php">
								<input type="hidden" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="enquiry_details">Enquiry Details</label>
											<div class="controls">
												Enquiry Number: <strong><?php echo $enquiry_list_data[0]["enquiry_number"]; ?></strong><br/>
												Customer Name: <strong><?php echo $enquiry_list_data[0]["name"]; ?></strong><br/>
												Customer Mobile: <strong><?php echo $enquiry_list_data[0]["cell"]; ?></strong><br/>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->		
												
										<div class="control-group">											
											<label class="control-label" for="ddl_project">Project*</label>
											<div class="controls">
												<select name="ddl_project">
												<option value="">- - Select a project - -</option>
												<?php
												for($count = 0; $count < count($project_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_list_data[$count]["project_id"]; ?>"><?php echo $project_list_data[$count]["project_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_site_visit">Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_site_visit" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
										
										<div class="control-group">											
											<label class="control-label" for="stxt_visit_time">Time*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_visit_time" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_pickup_location">Pickup Location*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_pickup_location" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_confirm_status">Confirmation Status*</label>
											<div class="controls">
												<input type="radio" name="rd_confirm_status" value="0" checked>&nbsp;&nbsp;&nbsp;Tentative
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_confirm_status" value="1">&nbsp;&nbsp;&nbsp;Confirm
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_drive_type">Drive Type*</label>
											<div class="controls">
												<input type="radio" name="rd_drive_type" value="0" checked>&nbsp;&nbsp;&nbsp;KNS
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_drive_type" value="1">&nbsp;&nbsp;&nbsp;Self-Drive
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                              				    																			
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_site_visit_plan_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
																														
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
