<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 19-April-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */

    if (isset($_GET['project_process_task_id'])) {
        $task_id = $_GET['project_process_task_id'];
    } else {
        $task_id = $_POST['hd_task_id'];
    }
    if (isset($_GET['location_id'])) {
        $road_id = $_GET['location_id'];
    } else {
        $road_id = $_POST["hd_road_id"];
    }
    if (isset($_GET['project_id'])) {
        $project_id = $_GET['project_id'];
    }

    //Get Measurement
    $get_details_data = i_get_details("Regular", $task_id, $road_id);

    $road_name =  $get_details_data["road"];
    $uom = $get_details_data['uom'];
    $planned_msmrt = $get_details_data["planned_msmrt"];

    $reg_mp_msmrt = $get_details_data["mp_msmrt"];
    $reg_mc_msmrt = $get_details_data["mc_msmrt"];
    $reg_cw_msmrt = $get_details_data["cw_msmrt"];
    $reg_overall_msmrt = $get_details_data["overall_msmrt"];

    $get_rework_data = i_get_details("Rework", $task_id, $road_id);
    $rework_mp_msmrt = $get_rework_data["mp_msmrt"];
    $rework_mc_msmrt = $get_rework_data["mc_msmrt"];
    $rework_cw_msmrt = $get_rework_data["cw_msmrt"];
    $rework_overall_msmrt = $get_rework_data["overall_msmrt"];

    $process = "";
    $contract_task = '';
    $planned_no  = 'SELECT CONTRACT TASK';
    $planned_l   = 'SELECT CONTRACT TASK';
    $planned_b   = 'SELECT CONTRACT TASK';
    $planned_d   = 'SELECT CONTRACT TASK';
    $planned_tot = 'SELECT CONTRACT TASK';
    $actual_no   = 'SELECT CONTRACT TASK';
    $actual_l    = 'SELECT CONTRACT TASK';
    $actual_b    = 'SELECT CONTRACT TASK';
    $actual_d    = 'SELECT CONTRACT TASK';
    $actual_tot  = 'SELECT CONTRACT TASK';
    $actual_tot_value  = 0;
    $planned_tot_value = 0;
    $contract_rate_f   = 0;
    $uom_f 			   = 'NOT SURE';
    $uom_id_f 		   = null;
    $vendor_id 		   = "";


    // Get Project Task Actual Contract modes already added
    // $project_task_actual_boq_search_data = array("task_id"=>$task_id,"active"=>'1',"work_type"=>'Regular');
    // $contract_latest_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);
    // if ($contract_latest_list['status'] != SUCCESS) {
    //     $contract_status = "NotStarted";
    //     $contract_percentage = "";
    // } else {
    //     $contract_status = "";
    //     $contract_percentage = $contract_latest_list["data"][0]["project_task_actual_boq_completion"];
    // }

    $work_type = "";
    // Capture the form data
    if (isset($_POST["add_project_task_boq_actual_submit"])) {
        $uom               = $_POST["unit"];
        $task_id           = $_POST["hd_task_id"];
        $road_id           = $_POST["hd_road_id"];
        $vendor_id         = $_POST["ddl_venodr"];
        $date         	   = $_POST["date"];
        $process           = $_POST["ddl_process"];
        $contract_task     = $_POST["ddl_task"];
        // $location		  		 = $_POST["hd_road_id"];
        $location		  		 = $_POST["ddl_location"];
        $number		      	 = $_POST["number"];
        $length    		  	 = $_POST["length"];
        $breadth   		  	 = $_POST["breadth"];
        $depth    		  	 = $_POST["depth"];
        $total_sqft		  	 = $_POST["total_sqft"];
        $amount            = $_POST["amount1"];
        $total_amount      = $_POST["amount"];
        $remarks 	       	 = $_POST["txt_remarks"];
        $work_type 	       = $_POST["work_type"];

        //Get Man power List
        $man_power_search_data = array("task_id"=>$task_id,"active"=>'1');
        $man_power_actual_data = i_get_man_power_list($man_power_search_data);
        if ($man_power_actual_data["status"] != SUCCESS) {
            $manpower_status = "NotStarted";
        } else {
            $manpower_status = "";
        }

        // Get Project Task Actual Machine modes already added
        $actual_machine_plan_search_data = array("task_id"=>$task_id,"active"=>'1',"work_type"=>"Regular");
        $machine_latest_list = i_get_machine_planning_list($actual_machine_plan_search_data);
        if ($machine_latest_list['status'] != SUCCESS) {
            $machine_status = "NotStarted";
        } else {
            $machine_status = "";
        }

        if (($manpower_status == "NotStarted") && ($machine_status == "NotStarted") && ($contract_status == "NotStarted")) {
            $project_process_task_update_data = array("actual_start_date"=>$date);
            $task_update_start_date_list = i_update_project_process_task($task_id, $project_process_task_update_data);
        } else {
            //Do nothing
        }
        // Check for mandatory fields
        if (($task_id != "")) {

            $project_task_boq_actual_iresult = i_add_project_task_boq_actual($task_id, $road_id, $vendor_id, $date, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, $total_amount, $remarks, $user, $work_type, '');

            if ($project_task_boq_actual_iresult["status"] == SUCCESS) {
                $alert_type = 1;
                if ($work_type == "Rework") {
                      send_mail($project_id,$task_id,'manpower','',$loggedin_name,$road_id,$remarks);
                    $project_task_contract_rework_iresult = i_add_project_contract_rework($task_id, $vendor_id, $date, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, '', '', $remarks, '', '', '', '', $user);
                }

              // after Successfull process navigate to list
              // header("location:project_task_planning_list.php?task_id=".$task_id);

            } else {
                $alert_type = 0;
            }

            $alert = $project_task_boq_actual_iresult["data"];
        } else {
            $alert = "Please fill all the mandatory fields";
            $alert_type = 0;
        }
    } elseif ((isset($_POST["ddl_task"])) && ($_POST["ddl_task"] != '')) {
        $process           = $_POST["ddl_process"];
        $task_id           = $_POST["hd_task_id"];
        $contract_task     = $_POST["ddl_task"];
        $vendor_id         = $_POST["ddl_venodr"];
        $work_type         = $_POST["work_type"];

        // Set UOM, rate etc.
        $rate_search_data = array("active"=>'1',"contract_rate_id"=>$contract_task);
        $rate_master_list = i_get_project_contract_rate_master($rate_search_data);
        if ($rate_master_list['status'] == SUCCESS) {
            $rate_master_list_data = $rate_master_list['data'];
            $contract_rate_f = $rate_master_list_data[0]['project_contract_rate_master_rate'];
            $uom_f 			 = $rate_master_list_data[0]['stock_unit_name'];
            $uom_id_f 		 = $rate_master_list_data[0]['project_contract_rate_master_uom'];
        }

        // Get Project Task BOQ plan already added
        $project_task_boq_search_data = array("active"=>'1',"task_id"=>$task_id,"contract_task"=>$contract_task);
        $project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
        if ($project_task_boq_list['status'] == SUCCESS) {
            $project_task_boq_list_data = $project_task_boq_list['data'];

            $planned_no  = 0;
            $planned_l   = 0;
            $planned_b   = 0;
            $planned_d   = 0;
            $planned_tot = 0;

            for ($count = 0; $count < count($project_task_boq_list_data); $count++) {
                $planned_no  = $planned_no + $project_task_boq_list_data[$count]['project_task_boq_number'];
                $planned_l   = $planned_l + $project_task_boq_list_data[$count]['project_task_boq_length'];
                $planned_b   = $planned_b + $project_task_boq_list_data[$count]['project_task_boq_breadth'];
                $planned_d   = $planned_d + $project_task_boq_list_data[$count]['project_task_boq_depth'];
                $planned_tot = $planned_tot + $project_task_boq_list_data[$count]['project_task_boq_total_measurement'];
            }

            $planned_tot_value = $planned_tot;
        } else {
            $planned_no  = 'NO BOQ PLAN';
            $planned_l   = 'NO BOQ PLAN';
            $planned_b   = 'NO BOQ PLAN';
            $planned_d   = 'NO BOQ PLAN';
            $planned_tot = 'NO BOQ PLAN';
        }

        // Get Project Task BOQ modes already added
        $project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$task_id,"contract_task"=>$contract_task);
        $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
        if ($project_task_boq_actual_list['status'] == SUCCESS) {
            $project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];

            $actual_no  = 0;
            $actual_l   = 0;
            $actual_b   = 0;
            $actual_d   = 0;
            $actual_tot = 0;

            for ($count = 0; $count < count($project_task_boq_actual_list_data); $count++) {
                $actual_no  = $actual_no + $project_task_boq_actual_list_data[$count]['project_task_boq_actual_number'];
                $actual_l   = $actual_l + $project_task_boq_actual_list_data[$count]['project_task_actual_boq_length'];
                $actual_b   = $actual_b + $project_task_boq_actual_list_data[$count]['project_task_actual_boq_breadth'];
                $actual_d   = $actual_d + $project_task_boq_actual_list_data[$count]['project_task_actual_boq_depth'];
                $actual_tot = $actual_tot + $project_task_boq_actual_list_data[$count]['project_task_actual_boq_total_measurement'];
            }

            $actual_tot_value = $actual_tot;
        } else {
            $actual_no  = 'NO BOQ ACTUALS';
            $actual_l   = 'NO BOQ ACTUALS';
            $actual_b   = 'NO BOQ ACTUALS';
            $actual_d   = 'NO BOQ ACTUALS';
            $actual_tot = 'NO BOQ ACTUALS';
        }
    }
    if (isset($_POST["ddl_process"])) {
        $vendor_id         = $_POST["ddl_venodr"];
        $process           = $_POST["ddl_process"];
        $task_id           = $_POST["hd_task_id"];
        $work_type         = $_POST["work_type"];
        // Get Project Contract Rate modes already added
        $project_contract_rate_master_search_data = array("active"=>'1',"process"=>$process,"vendor_id"=>$vendor_id);
        $project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
        if ($project_contract_rate_master_list['status'] == SUCCESS) {
            $project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
        } else {
            $alert = $alert."Alert: ".$project_contract_rate_master_list["data"];
        }
    }

    // Temp data
    $project_process_task_search_data = array("task_id"=>$task_id);
    $project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
    if ($project_plan_process_task_list["status"] == SUCCESS) {
        $project_plan_process_task_list_data = $project_plan_process_task_list["data"];
        $task_name = $project_plan_process_task_list_data[0]["project_task_master_name"];
        $project = $project_plan_process_task_list_data[0]["project_master_name"];
        $project_id = $project_plan_process_task_list_data[0]["project_management_master_id"];
        $process_name= $project_plan_process_task_list_data[0]["project_process_master_name"];
    } else {
        $alert = $project_plan_process_task_list["data"];
        $alert_type = 0;
        $task_name = "";
        $project = "";
        $process = "";
        $project_id = "";
    }

    // Get project Contract Process modes already added
    $project_contract_process_search_data = array("active"=>'1');
    $project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
    if ($project_contract_process_list['status'] == SUCCESS) {
        $project_contract_process_list_data = $project_contract_process_list['data'];
    } else {
        $alert = $project_contract_process_list["data"];
        $alert_type = 0;
    }


    // Get Stock master  modes already added
    $stock_unit_measure_search_data = array("active"=>'1');
    $stock_unit_measure_list = i_get_stock_unit_measure_list($stock_unit_measure_search_data);
    if ($stock_unit_measure_list['status'] == SUCCESS) {
        $stock_unit_measure_list_data = $stock_unit_measure_list['data'];
    }

    // Get Project Task BOQ modes already added
    $project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$task_id);
    $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
    if ($project_task_boq_actual_list['status'] == SUCCESS) {
        $project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
    } else {
        $alert = $alert."Alert: ".$project_task_boq_actual_list["data"];
    }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
        $alert = $project_manpower_agency_list["data"];
        $alert_type = 0;
    }

    // Get Project Location already added
    $project_site_location_mapping_master_search_data = array("active"=>'1',"project_id"=>$project_id);
    $project_site_location_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
    if ($project_site_location_list['status'] == SUCCESS) {
        $project_site_location_list_data = $project_site_location_list['data'];
    } else {
        $alert = $alert."Alert: ".$project_site_location_list["data"];
    }
} else {
    header("location:login.php");
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Add Project Task BOQ Actual</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header custom-header">
	      				<i class="icon-user"></i>
	      				<!-- <h3>
									Add Project Task BOQ Actual
									<h3>&nbsp;&nbsp; Project : <?php echo $project ;?> &nbsp;&nbsp;
									Process :	<?php echo  $process_name;?>&nbsp;&nbsp;
									Task:  &nbsp;  &nbsp;<?php echo $task_name; ?>&nbsp;&nbsp;&nbsp;<br/>
									&nbsp;&nbsp;  Road:  &nbsp;  &nbsp;<?php echo $road_name; ?>&nbsp;&nbsp;&nbsp;
									Planned Msrmnt: &nbsp;  &nbsp;<?= $planned_msmrt; ?>&nbsp;&nbsp;&nbsp;
									Completed Msmrt : (Regular : <?php echo $reg_mp_msmrt ;?> &nbsp;&nbsp;&nbsp;&nbsp; Rework : <?= $rework_mp_msmrt; ?>)
								</h3> -->
                <h3>
									<span class="header-label">Project :</span> <?= $project; ?>
									<span class="header-label">Process :</span> <?= $process_name; ?>
									<span class="header-label">Task:</span>  <?= $task_name; ?>
								</h3>
								<h3 style="margin-left:25px">
									<span class="header-label">Road:</span>  <?= $road_name; ?>
									<span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
									<span class="header-label">UOM:</span> <?= $uom; ?>
								</h3>
								<h3 >
									<span class="header-label">Regular:</span> <?= $reg_overall_msmrt; ?> (MP : <?= $reg_mp_msmrt; ?>  MC : <?= $reg_mc_msmrt; ?> CW : <?= $reg_cw_msmrt; ?>)
									<span class="header-label">Rework:</span> <?= $rework_overall_msmrt; ?> (MP : <?= $rework_mp_msmrt; ?>   MC : <?= $rework_mc_msmrt; ?> CW : <?= $rework_cw_msmrt; ?>)
								</h3>
						<span style="float:right; padding-right:10px;"><a href="project_task_boq_actuals_list.php">Project Task BOQ Actual List</a></span>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <!-- <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Task BOQ Actual</a>
						  </li>	 -->
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
                                if ($alert_type == 0) { // Failure?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>

								<?php
                                if ($alert_type == 1) { // Success
                                ?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
                                }
                                ?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_task_boq_form" class="form-horizontal" method="post" action="project_add_task_boq_actuals.php">
									<fieldset>
										<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
										<input type="hidden" name="hd_road_id" value="<?php echo $road_id; ?>" />
										<input type="hidden" name="work_type" value="<?php echo $work_type; ?>" />



										<div class="control-group">
											<label class="control-label" for="ddl_venodr">Vendor</label>
											<div class="controls">
												<select name="ddl_venodr" id="ddl_venodr" required>
												<option value="">- - Select Vendor- -</option>
												<?php
                                                for ($count = 0; $count < count($project_manpower_agency_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>" <?php if ($vendor_id == $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]) {
                                                        ?> selected <?php
                                                    } ?>><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_process">Contract Process*</label>
											<div class="controls">
												<select name="ddl_process" onchange="this.form.submit();" class="span6" required>
												<option value="">- - Select Process - -</option>
												<?php
                                                for ($count = 0; $count < count($project_contract_process_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_contract_process_list_data[$count]["project_contract_process_id"]; ?>"<?php if ($project_contract_process_list_data[$count]["project_contract_process_id"] == $process) {
                                                        ?> selected="selected" <?php
                                                    } ?>><?php echo $project_contract_process_list_data[$count]["project_contract_process_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_task">Contract Task</label>
											<div class="controls">
												<select name="ddl_task" id="ddl_task" class="span6" onchange="this.form.submit();" required>
												<option value="">- - Select Task - -</option>
												<?php
                                                for ($count = 0; $count < count($project_contract_rate_master_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_id"]; ?>" <?php if ($project_contract_rate_master_list_data[$count]["project_contract_rate_master_id"] == $contract_task) {
                                                        ?> selected <?php
                                                    } ?>><?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_work_task"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<input type="hidden" name="unit" id="unit" value="<?php echo $uom_id_f; ?>" />
										<div class="control-group">
											<label class="control-label" for="uom">UOM</label>
											<div class="controls">
												<input type="text" name="uom" disabled id="uom" value="<?php echo $uom_f; ?>">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="date">Date</label>
											<div class="controls">
												<input type="date" name="date" placeholder="Depth" required>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="ddl_location">Site Location</label>
											<div class="controls">
												<select name="ddl_location" id="ddl_location" required>
												<option value="">- - Select Site Location - -</option>
												<?php
                                                for ($count = 0; $count < count($project_site_location_list_data); $count++) {
                                                    ?>
												<option value="<?php echo $project_site_location_list_data[$count]["project_site_location_mapping_master_id"]; ?>"><?php echo $project_site_location_list_data[$count]["project_site_location_mapping_master_name"]; ?></option>
												<?php
                                                }
                                                ?>
												</select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label" for="number">Number</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="number" id="number" value="1" placeholder="Number" required>
												<p>Planned: <?php echo $planned_no; ?> <b>||</b> Actual: <?php echo $actual_no; ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="length">Length</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="length" id="length" value="1" onkeyup="return total_sqft_no();" placeholder="Length" required >
												<p>Planned: <?php echo $planned_l; ?><b>||</b> Actual: <?php echo $actual_l; ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="breadth">Breadth</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="breadth"  id="breadth" value="1" required onkeyup="return total_sqft_no();" placeholder="Breadth">
												<p>Planned: <?php echo $planned_b; ?><b>||</b> Actual: <?php echo $actual_b; ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="depth">Depth</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="depth"  id="depth"  value="1"  onkeyup="return total_sqft_no();" required placeholder="Depth">
												<p>Planned: <?php echo $planned_d; ?><b>||</b> Actual: <?php echo $actual_d; ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="total_sqft">Total Measurement</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" required name="total_sqft" id="total_sqft"  placeholder="Total Sqft" onchange="calculate_msrmnt(<?= $planned_msmrt; ?>, <?= $reg_overall_msmrt?>, <?= $rework_overall_msmrt;?>)" onkeyup="return total_cost();">
												<p>Planned: <?php echo $planned_tot; ?><b>||</b> Actual: <?php echo $actual_tot; ?>||</b> Balance: <?php echo($planned_tot_value - $actual_tot_value); ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<input type="hidden" name="amount1" id="amount1" value="<?php echo $contract_rate_f; ?>" />
										<div class="control-group">
											<label class="control-label" for="amount">Total Value
											(in Rs.)</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="amount" id="amount" value="<?php echo $contract_rate_f; ?>" placeholder="Contract Rate">
												<p>Rate: <?php echo $contract_rate_f; ?></p>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks*</label>
											<div class="controls">
											<textarea name="txt_remarks" placeholder="Remarks" required></textarea>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />

																																									<div class="control-group">
											<label class="control-label" for="work_type" >Work Type</label>
											<div class="controls">

									<table>
										<tr>
											<td>
												<input type="radio" onchange="calculate_msrmnt(<?= $planned_msmrt; ?>, <?= $reg_overall_msmrt?>)" required name="work_type" id="regular" value="Regular"
													<?php if ($reg_overall_msmrt == $planned_msmrt) {
                                                    ?> disabled <?php
                                                } ?>> Regular&nbsp;&nbsp;&nbsp;
											</td>
											<td>
												<input type="radio" onchange="calculate_msrmnt(<?= $planned_msmrt; ?>, <?= $reg_overall_msmrt?>, <?= $rework_overall_msmrt?>)" required name="work_type" id="rework"
													 	<?php if ($reg_overall_msmrt == $planned_msmrt) {
                                                    echo 'checked="checked"';
                                                } elseif ($reg_overall_msmrt == 0) {
                                                    ?>disabled <?php
                                                }
                                                        ?> value="Rework"> Rework &nbsp;&nbsp;&nbsp;
											 </td>
											</tr>
											</table>
										</div> <!-- /controls -->
									</div> <!-- /control-group -->


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" id="add_project_task_boq_actual_submit" name="add_project_task_boq_actual_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->

	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="src/project_add_task_boq_actuals.js"></script>

  </body>

</html>
