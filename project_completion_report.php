<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_plan_process_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'Projectmgmnt';

/* DEFINES - START */
define('PROJECT_COMPLETION_REPORT_FUNC_ID','251');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_COMPLETION_REPORT_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */


	// Query String Data
	// Nothing

	$search_project = "-1";
	$search_user   	= "-1";

	if(isset($_POST["file_search_submit"]))
	{
		$search_project = $_POST["search_project"];
		$search_user    = $_POST["search_user"];
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	if(($search_project != '-1') && ($search_project != ''))
	{
		/* Get total planned days - Start */

		$project_plan_process_search_data = array("active"=>'1',"project_id"=>$search_project,"order"=>'start_date_asc',"ignore_not_started"=>'1');

		$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
		if($project_plan_process_list["status"] == SUCCESS)

		{

			$project_plan_process_list_data = $project_plan_process_list["data"];

			$project_pl_start_date = $project_plan_process_list_data[0]['project_plan_process_start_date'];
		}
		else

		{

			$project_pl_start_date = date('Y-m-d');

		}



		$project_plan_process_search_data = array("active"=>'1',"project_id"=>$search_project,"order"=>'end_date_desc');

		$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);

		if($project_plan_process_list["status"] == SUCCESS)

		{

			$project_plan_process_list_data = $project_plan_process_list["data"];

			$project_pl_end_date = $project_plan_process_list_data[0]['project_plan_process_end_date'];

		}

		else

		{

			$project_pl_end_date = date('Y-m-d');

		}

		$planned_data = get_date_diff($project_pl_start_date,$project_pl_end_date);

		$total_plan_days = $planned_data['data'] + 1;

		/* Get total planned days - End */


		/* Get total actual days - Start */

		$project_process_task_search_data = array("active"=>'1',"project_id"=>$search_project,"order"=>'start_date_asc',"ignore_not_started"=>'1');

		$project_plan_process_task_start_date_list = i_get_project_process_task($project_process_task_search_data);

		if($project_plan_process_task_start_date_list["status"] == SUCCESS)

		{

			$project_plan_process_task_list_data = $project_plan_process_task_start_date_list["data"];

			$project_actual_start_date = $project_plan_process_task_list_data[0]['project_process_actual_start_date'];

		}

		else

		{

			$project_actual_start_date = date('Y-m-d');

		}



		$project_process_task_search_data = array("active"=>'1',"project_id"=>$search_project,"project_process_actual_end_date"=>'0000-00-00');

		$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);

		if($project_plan_process_task_list["status"] == SUCCESS)

		{

			$project_actual_end_date = date('Y-m-d');

		}

		else

		{

			$project_process_task_search_data = array("active"=>'1',"project_id"=>$search_project,"order"=>'end_date_desc');

			$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);

			if($project_plan_process_task_list["status"] == SUCCESS)

			{

				$project_plan_process_task_list_data = $project_plan_process_task_list["data"];

				$project_actual_end_date = $project_plan_process_task_list_data[0]['project_process_actual_end_date'];

			}

			else

			{

				$project_actual_end_date = date('Y-m-d');

			}

		}

		$actual_data = get_date_diff($project_actual_start_date,$project_actual_end_date);

		$actual_days = $actual_data['data'] + 1;

		/* Get total actual days - End */



		/* Get actual percentage */
		$project_process_task_search_data = array("active"=>'1',"project_id"=>$search_project);
		$project_plan_process_task_start_date_list = i_get_project_process_task($project_process_task_search_data);

		if($project_plan_process_task_start_date_list["status"] == SUCCESS)

		{
			$total_completion_percentage = 0;
			$project_plan_process_task_list_data = $project_plan_process_task_start_date_list["data"];

			$no_of_task = count($project_plan_process_task_list_data);

			for($task_count = 0 ; $task_count < count($project_plan_process_task_list_data) ; $task_count++)

			{

				$total_completion_percentage = $total_completion_percentage + $project_plan_process_task_list_data[$task_count]["project_process_task_completion"];

			}

			$actual_percentage = $total_completion_percentage/$no_of_task;
		}

		else

		{

			$actual_percentage = 0;

		}



		/* Get total pause days - Start */

		// Get all tasks of this project - which are started

		$no_of_paused_tasks = 0;

		$total_no_of_paused_days = 0;

		if($project_plan_process_task_list['status'] == SUCCESS)

		{

			$project_plan_process_task_list_data = $project_plan_process_task_list['data'];



			for($tcount = 0; $tcount < count($project_plan_process_task_list_data); $tcount++)

			{

				// Get pause for each task

				$delay_reason_search_data = array('task_id'=>$project_plan_process_task_list_data[$tcount]['project_process_task_id']);

				$task_pause_list = i_get_project_delay_reason($delay_reason_search_data);



				if($task_pause_list['status'] == SUCCESS)

				{

					for($pt_count = 0; $pt_count < count($task_pause_list['data']); $pt_count++)

					{

						$no_of_paused_tasks++;



						if($task_pause_list['data'][$pt_count]['project_task_delay_reason_end_date'] == '0000-00-00 00:00:00')

						{

							// Total for still open

							$tp_end_date = date('Y-m-d');

						}

						else

						{

							// Total for completed

							$tp_end_date = date('Y-m-d',strtotime($task_pause_list['data'][$pt_count]['project_task_delay_reason_end_date']));

						}



						$tp_start_date = date('Y-m-d',strtotime($task_pause_list['data'][$pt_count]['project_task_delay_reason_start_date']));

						$tp_diff = get_date_diff($tp_start_date,$tp_end_date);

						$total_no_of_paused_days = $total_no_of_paused_days + $tp_diff['data'] + 1;

					}

				}

			}

		}


		if($no_of_paused_tasks != 0)

		{

			$pause_days = round($total_no_of_paused_days/$no_of_paused_tasks,2);

		}

		else

		{

			$pause_days = '0';

		}

		/* Get total pause days - End */



		$ratio = round($actual_percentage,2)/$actual_days;

		$actual = 100/$total_plan_days;



		if($ratio < $actual) // Off track

		{

			$remarks = "OUT OF TRACK";

			$delay = $actual - $ratio;

		}

		else // On track

		{

			$remarks = "ON TRACK";

			$delay = 0;

		}

		if(($delay*$actual_days) >= 100)
		{
			$comments = '<span style="color:red;">OUT OF TRACK</span>';
		}
		else
		{
			$comments = '<span style="color:green;">ON TRACK</span>';
		}
	}
	else
	{
		$total_plan_days   = 'NA';
		$actual_days       = 'NA';
		$actual_percentage = 'NA';
		$pause_days		   = 'NA';
	}

	// Temp data
	$project_plan_process_search_data = array("active"=>'1',"project_id"=>$search_project,"assigned_to"=>$search_user);
	$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
	if($project_plan_process_list["status"] == SUCCESS)
	{
		$project_plan_process_list_data = $project_plan_process_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_plan_process_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Plan Process List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Plan Process List&nbsp;&nbsp;&nbsp;P Days: <?php echo $total_plan_days; ?>&nbsp;&nbsp;&nbsp;A Days: <?php echo $actual_days; ?>&nbsp;&nbsp;&nbsp;Avg Comp %: <?php echo round($actual_percentage,2); ?>&nbsp;&nbsp;&nbsp;Delay: <?php echo round((round($delay,2)*$actual_days),2).'%'; ?>&nbsp;&nbsp;&nbsp;Avg Pause Days: <?php echo $pause_days; ?>&nbsp;&nbsp;&nbsp;Project Status: <?php echo $comments; ?></h3>
            </div>

            <!-- /widget-header -->
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_completion_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>

			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>

            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="width:3%">SL No</th>
				    <th style="width:10%">Project</th>
					<th style="width:20%">Process Name</th>
					<th style="width:10%">P Start Date</th>
					<th style="width:10%">P End Date</th>
					<th style="width:4%">P Days</th>
					<th style="width:10%"> A Start Date</th>
					<th style="width:10%">A End Date</th>
					<th style="width:4%"> A Days</th>
					<th style="width:4%">D Days</th>
					<th style="width:7%">Percentage</th>
					<th style="width:10%">Pause Date</th>
					<th style="width:5%">Pause Days</th>
					<th style="width:10%">Pause Reason</th>
					<th style="width:4%">No of Times Paused</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_plan_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_plan_process_list_data); $count++)
					{
						$sl_no++;

						$planned_process_start_date = date("d-M-Y",strtotime($project_plan_process_list_data[$count]["project_plan_process_start_date"]));
						$planned_process_end_date = date("d-M-Y",strtotime($project_plan_process_list_data[$count]["project_plan_process_end_date"]));
						$plan_days = get_date_diff($project_plan_process_list_data[$count]["project_plan_process_start_date"],$project_plan_process_list_data[$count]["project_plan_process_end_date"]);

						//Get Task List
						$project_process_task_search_data = array("active"=>'1',"order"=>"start_date_asc","process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"]);
						$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
						if($project_plan_process_task_list["status"] == SUCCESS)
						{
							$project_plan_process_task_list_data = $project_plan_process_task_list["data"];
							$actual_start_date = get_formatted_date($project_plan_process_task_list_data[0]["project_process_actual_start_date"],"Y-m-d");
						}
						else
						{
							$actual_start_date = "";
						}

						//Get Task  End Date
						$project_process_task_search_data = array("active"=>'1',"process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"],"actual_end_date"=>"0000-00-00");
						$project_plan_process_task__end_date_list = i_get_project_process_task($project_process_task_search_data);
						if($project_plan_process_task__end_date_list["status"] == SUCCESS)
						{
							$actual_end_date = $actual_end_date = date("Y-m-d");
						}
						else
						{
							$project_process_task_search_data = array("active"=>'1',"process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"],"order"=>"end_date_desc");
							$project_plan_process_task__end_date_list1 = i_get_project_process_task($project_process_task_search_data);
							if($project_plan_process_task__end_date_list1["status"] == SUCCESS)
							{
								$project_plan_process_task__end_date_list_data1 = $project_plan_process_task__end_date_list1["data"];
								$actual_end_date = get_formatted_date($project_plan_process_task__end_date_list_data1[0]["project_process_actual_end_date"],"Y-m-d");
								$completion = $project_plan_process_task__end_date_list_data1[0]["project_process_task_completion"] ;
							}
							else
							{
								$actual_end_date = date("Y-m-d");
							}
						}

						$project_process_task_search_data = array("active"=>'1',"process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"],"order"=>"end_date_desc");
						$project_end_date = i_get_project_process_task($project_process_task_search_data);
						if($project_end_date["status"] == SUCCESS)
						{
							$completed_status = 1;
							$project_end_date_data = $project_end_date["data"];
							for($task_count = 0 ; $task_count < count($project_end_date_data) ; $task_count++)
							{
								if(($project_end_date_data[$task_count]["project_process_task_completion"] == 100) ||
								($project_end_date_data[$task_count]["project_process_machine_task_completion"] == 100) ||
								($project_end_date_data[$task_count]["project_process_contract_task_completion"] == 100))
								{

								}
								else
								{
									$completed_status = $completed_status&1;
								}
							}
							if($completed_status == "1")
							{
								$actual_end_date_display = get_formatted_date($project_end_date_data[0]["project_process_actual_end_date"],"d-M-Y");
							}
						}
						else
						{
							$actual_end_date_display = date("d-M-Y");
						}

						//Get Completion Percentage List
						$project_process_task_search_data = array("active"=>'1',"process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"]);
						$project_plan_process_completion_task_list = i_get_project_process_task($project_process_task_search_data);
						if($project_plan_process_completion_task_list["status"] == SUCCESS)
						{
							$project_plan_process_completion_task_list_data = $project_plan_process_completion_task_list["data"];
							for($task_count = 0 ; $task_count < count($project_plan_process_completion_task_list_data) ; $task_count++)
							{
								$completion = $project_plan_process_completion_task_list_data[$task_count]["project_process_task_completion"] ;
								$today = date("Y-m-d");
								if($completion == '100' )
								{
									$actual_days = get_date_diff($actual_start_date,$actual_end_date);

								}
								else
								{
									$actual_days = get_date_diff($actual_start_date,$today);

								}
							}
						}
						else
						{
							//
						}
						$total_actual_days = $total_actual_days + $actual_days["data"] ;

						//Get Delay Reason List
						$delay_reason_search_data = array("process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"],"delay_end_date"=>"0000-00-00 00:00:00");
						$delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
						$pause_days = "";
						if($delay_reason_list["status"] == SUCCESS)
						{
							$delay_reason_list_data = $delay_reason_list["data"];
							$pause_date = date("d-M-Y H:i:s", strtotime($delay_reason_list["data"][0]["project_task_delay_reason_start_date"]));
							$pause_reason = $delay_reason_list["data"][0]["project_reason_master_name"];

							// Get date difference
							$pause_duration = get_date_diff($delay_reason_list["data"][0]["project_task_delay_reason_start_date"],date("Y-m-d H:i:s"));
							$pause_days = $pause_duration['data'];
						}
						else
						{
							//$no_of_times_paused = "";
							$pause_date = "";
							$pause_reason = "";
						}
						//Get Delay Reason List
						$delay_reason_search_data = array("process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"]);
						$pause_list = i_get_project_delay_reason($delay_reason_search_data);

						if($pause_list["status"] == SUCCESS)
						{
							/*$total_pause_days = 0;
							for($pause_count = 0 ; $pause_count < count($pause_list["data"]) ; $pause_count++ )
							{
								$pause_list_data = $pause_list["data"];
								$no_of_times_paused = count($pause_list_data);
								$pause_start_date = get_formatted_date($pause_list_data[$pause_count]["project_task_delay_reason_start_date"],"Y-m-d"
								);
								$pause_end_date = get_formatted_date($pause_list_data[$pause_count]["project_task_delay_reason_end_date"],"Y-m-d");
								$today = date("Y-m-d");
								if($pause_list_data[$pause_count]["project_task_delay_reason_end_date"] != "0000-00-00 00:00:00" )
								{
									$pause_days = get_date_diff($pause_start_date,$pause_end_date);

								}
								else
								{
									$pause_days = get_date_diff($pause_start_date,$today);

								}
								$total_pause_days = $total_pause_days + $pause_days["data"] ;
							}*/
							$no_of_times_paused = count($pause_list["data"]);
						}
						else
						{
							$no_of_times_paused = "";
						}


						//Get Project Process Task Manpower
						$total_completion_percentage = 0;
						$total_machine_completion_percentage = 0;
						$total_contract_completion_percentage = 0;
						$project_process_task_search_data = array("process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"],"active"=>'1');
						$project_process_task_list = i_get_project_process_task($project_process_task_search_data);
						if($project_process_task_list["status"] == SUCCESS)
						{
							$project_process_task_list_data = $project_process_task_list["data"];
							$no_of_task = count($project_process_task_list_data);
							$total_value = 0;
							for($t_count = 0 ; $t_count < count($project_process_task_list_data) ; $t_count++)
							{
								$avg_count = 0;
								$task_id = $project_process_task_list_data[$t_count]["project_process_task_id"];
								//Get actual man power List
								$man_power_search_data = array("active"=>'1',"max_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
								$man_power_list = i_get_man_power_list($man_power_search_data);
								if($man_power_list["status"] == SUCCESS)
								{
									$man_power_list_data = $man_power_list["data"];
									$manpower_percentage = $man_power_list_data[0]["max_percentage"];
								}
								else
								{
									$manpower_percentage = 0;
								}

								//Get Actual Machine List
								$actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
								$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
								if($actual_machine_plan_list["status"] == SUCCESS)
								{
									$actual_machine_plan_list_data = $actual_machine_plan_list["data"];
									$machine_percentage = $actual_machine_plan_list_data[0]["project_task_actual_machine_completion"];
								}
								else
								{
									$machine_percentage = "0";
								}
								//Get contract List data
								$project_task_boq_actual_search_data = array("active"=>'1',"max_contract_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
								$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
								if($project_task_boq_actual_list['status'] == SUCCESS)
								{
									$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
									$contract_percentage = $project_task_boq_actual_list_data[0]["max_cpercentage"];
								}
								else
								{
									$contract_percentage = 0;
								}

								// Actual Manpower data
								$man_power_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
								$man_power_list = i_get_man_power_list($man_power_search_data);
								if($man_power_list["status"] == SUCCESS)
								{
									$avg_count = $avg_count + 1;
								}
								else
								{
									// Get Project Man Power Estimate modes already added
									$project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"]);
									$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
									if($project_man_power_estimate_list['status'] == SUCCESS)
									{
										$avg_count = $avg_count + 1;
									}
									else
									{
										//Do not Inncrement
									}
								}

								// Machine Planning data
								$actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
								$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
								if($actual_machine_plan_list["status"] == SUCCESS)
								{
									$avg_count = $avg_count + 1;
								}
								else
								{
									//Machine Planning
									$project_machine_planning_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"]);
									$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
									if($project_machine_planning_list["status"] == SUCCESS)
									{
										$avg_count = $avg_count + 1;
									}
									else
									{
										//Do not Increment
									}
								}

								// Actual Contract  Data
								$project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
								$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
								if($project_task_boq_actual_list['status'] == SUCCESS)
								{
									$avg_count = $avg_count + 1;
								}
								else
								{
									$project_task_boq_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"]);
									$project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
									if($project_task_boq_list['status'] == SUCCESS)
									{
										$avg_count = $avg_count + 1;
									}
									else
									{
										///Do not Increment
									}
								}
								$total_percentage = $manpower_percentage + $machine_percentage + $contract_percentage ;
								if($avg_count != 0)
								{
									$avg_percentage = $total_percentage/$avg_count;
								}
								else
								{
									$avg_percentage = 0;
								}
								$total_value = $total_value + $avg_percentage;
							}
							$total_overall_percenatge = $total_value/$no_of_task;
							$overall_pending_percentage = 100 - $total_overall_percenatge;

						}
						else
						{
							$total_overall_percenatge = 0;
							$overall_pending_percentage = 100;
						}


					?>

					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_plan_process_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_plan_process_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $planned_process_start_date ; ?></td>
					<td><?php echo $planned_process_end_date ; ?></td>
					<td><?php if($plan_days["status"] != "2") {?><a target="_blank" href="project_plan_process_list.php?plan_id=<?php echo $project_plan_process_list_data[$count]["project_plan_process_plan_id"];?>&source=<?php echo "view" ;?>&process_id=<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"];?>"><?php echo $plan_days["data"] + 1; ?><?php } else { echo $plan_days["data"]; } ?></a></td>
					<td><?php echo $actual_start_date ; ?></td>
					<td><?php echo $actual_end_date_display ;?></td>
					<td><?php if($actual_days["status"] != "2") {?><a target="_blank" href="project_process_level_details.php?process_id=<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"];?>"><?php echo $actual_days["data"] + 1  ;?><?php } else { echo $actual_days["data"] ; } ?></a></td>
					<td><?php if($plan_days["status"] == "2"){echo '';}
					else if($actual_days["status"] != "2"){echo $actual_days["data"] - $plan_days["data"];}
					else{echo '';}?></td>
					<td><div class="progress">
				  <div class="progress-bar progress-bar-success" style="width: <?php echo $total_overall_percenatge ;?>%;"><a href="project_process_level_details.php?process_id=<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"] ;?>" style="color:#000000" target="_blank"><?php echo round($total_overall_percenatge,2) ;?>%</a>					<span class="sr-only">(success)</span>
				  </div>
				  <div class="progress-bar progress-bar-danger" style="width: <?php echo $overall_pending_percentage ;?>%"><a href="project_process_level_details.php?process_id=<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"] ;?>" style="color:#FFFFFF" target="_blank"><?php echo round($overall_pending_percentage,2) ;?>%</a>
					<span class="sr-only">(danger)</span>
				  </div>
				</div>
					</td>
					<td><?php echo $pause_date ; ?></td>
					<td><?php echo $pause_days ; ?></td>
					<td><?php echo $pause_reason ; ?></td>
					<td><a href="project_task_delay_reason_list.php?process_id=<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"];?>&process_name=<?php echo $project_plan_process_list_data[$count]["project_process_master_name"]; ?>&project=<?php echo$project_plan_process_list_data[$count]["project_master_name"] ;?>" target="_blank"><?php echo $no_of_times_paused ; ?></a></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="13">No process added yet!</td>

				<?php
				}
				 ?>
				 <script>
				 //document.getElementById('total_man_power').innerHTML = <?php echo $total_man_power; ?>;
				 </script>
				 <script>
				 //document.getElementById('total_machine_planning').innerHTML = <?php echo $total_machine_planning; ?>;
				 </script>

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=0");
		}
	}
}
function project_approve_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}
}

function project_reject_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_reject_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}
}

function go_to_project_edit_project_plan_process(process_id,plan_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_project_plan_process.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_process_task(project_plan_process_id,plan_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_plan_process_task_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_plan_process_id");
	hiddenField1.setAttribute("value",project_plan_process_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
